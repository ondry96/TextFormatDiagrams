package pluginmain.imports.diagramcreators;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.diagram.IDiagramUIModel;
import com.vp.plugin.diagram.IInteractionDiagramUIModel;
import com.vp.plugin.diagram.connector.IMessageUIModel;
import com.vp.plugin.diagram.shape.*;
import com.vp.plugin.model.*;
import com.vp.plugin.model.factory.IModelElementFactory;
import pluginmain.elements.common.Element;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.GroupElement;
import pluginmain.elements.sequencediagram.MessageElement;
import pluginmain.elements.sequencediagram.ParticipantElement;

public class SequenceDiagramCreator extends DiagramCreator {

    int sequenceNumber = 1;

    public SequenceDiagramCreator(String filePath) {
        super();
        processFileNameOfPlantUML(filePath);
    }

    @Override
    public IDiagramUIModel createDiagram(String diagramType) {
        DiagramManager diagramManager = ApplicationManager.instance().getDiagramManager();
        IInteractionDiagramUIModel diagram = (IInteractionDiagramUIModel) diagramManager.createDiagram(DiagramManager.DIAGRAM_TYPE_INTERACTION_DIAGRAM);
        diagram.setAutoExtendActivations(true);
        diagram.setShowSequenceNumbers(true);
        diagram.setName(processedFileName);
        IFrame rootFrame = diagram.getRootFrame(true);
        IModel model = IModelElementFactory.instance().createModel();
        model.setName("diagram model");
        model.addSubDiagram(diagram);


        for (Element element : innerStructure) {
            if (element instanceof ParticipantElement) {
                ElementInfo elementInfo = drawParticipantElement(diagramManager, diagram, (ParticipantElement) element, model, rootFrame);
                if (((ParticipantElement) element).getAlias() == null)
                    elementsMap.put(element.getName(), elementInfo);
                else
                    elementsMap.put(((ParticipantElement) element).getAlias(), elementInfo);
            }
        }

        for (Element element : innerStructure) {
            if (element instanceof GroupElement) {
                drawGroupElement(diagramManager, diagram, (GroupElement) element, rootFrame, null, null);
            }

            if (element instanceof MessageElement) {
                drawMessageElement(diagramManager, diagram, (MessageElement) element, null);
            }
        }

        return diagram;
    }

    @Override
    public String getDiagramType() {
        return DiagramManager.DIAGRAM_TYPE_INTERACTION_DIAGRAM;
    }

    private ElementInfo drawParticipantElement(DiagramManager diagramManager, IInteractionDiagramUIModel diagram, ParticipantElement element, IModel model, IFrame rootFrame) {
        if (element.getStereotypes().contains("actor")) {
            return drawActorElement(diagramManager, diagram, element, model, rootFrame);
        }

        IInteractionLifeLine iLifeline = IModelElementFactory.instance().createInteractionLifeLine();
        rootFrame.addChild(iLifeline);
        iLifeline.setName(element.getName());

        element.getStereotypes().forEach(iLifeline::addStereotype);

        IInteractionLifeLineUIModel lifelineModel = (IInteractionLifeLineUIModel) diagramManager.createDiagramElement(diagram, iLifeline);
        if (element.getAlias() != null) {
            iLifeline.addStereotype("$alias: " + element.getAlias());
            IStereotype[] stereotypeIterator = iLifeline.toStereotypeModelArray();
            hideAliasStereotype(lifelineModel, stereotypeIterator);
        }

        if (!element.isCreatedByMessage())
            lifelineModel.setY(35);

        setColorToShape(element.getBackgroundColor(), lifelineModel);

        lifelineModel.fitSize();
        lifelineModel.resetCaption();

        return new ElementInfo(iLifeline, lifelineModel, "lifeline");
    }

    private ElementInfo drawActorElement(DiagramManager diagramManager, IInteractionDiagramUIModel diagram, ParticipantElement element, IModel model, IFrame rootFrame) {
        IInteractionActor iInteractionActor = IModelElementFactory.instance().createInteractionActor();
        rootFrame.addChild(iInteractionActor);
        iInteractionActor.setName(element.getName());

        element.getStereotypes().forEach(iInteractionActor::addStereotype);
        iInteractionActor.removeStereotype("actor");

        IInteractionActorUIModel actorModel = (IInteractionActorUIModel) diagramManager.createDiagramElement(diagram, iInteractionActor);
        if (element.getAlias() != null) {
            iInteractionActor.addStereotype("$alias: " + element.getAlias());
            IStereotype[] stereotypeIterator = iInteractionActor.toStereotypeModelArray();
            hideAliasStereotype(actorModel, stereotypeIterator);
        }

        if (!element.isCreatedByMessage())
            actorModel.setY(35);

        setColorToShape(element.getBackgroundColor(), actorModel);
        actorModel.fitSize();
        actorModel.resetCaption();

        return new ElementInfo(iInteractionActor, actorModel, "lifeline");
    }

    private void drawMessageElement(DiagramManager diagramManager, IInteractionDiagramUIModel diagram, MessageElement element, IInteractionOperand operand) {
        IMessage iMessage = IModelElementFactory.instance().createMessage();
        if (element.getName() != null)
            iMessage.setName(element.getName());

        iMessage.setSequenceNumber(String.valueOf(sequenceNumber++));

        INoChildrenNoRelationshipBaseModelElement actionType = null;
        if (element.isDotted()) {
            actionType = IModelElementFactory.instance().createActionTypeReturn();
            actionType.setName("Return");
            iMessage.setActionType(actionType);
        }

        if (element.isCreateMessage()) {
            actionType = IModelElementFactory.instance().createActionTypeCreate();
            actionType.setName("Create");
            iMessage.setType(2);
            iMessage.setActionType(actionType);
        }


        ElementInfo fromElementInfo = elementsMap.get(element.getFromName());
        ElementInfo toElementInfo = elementsMap.get(element.getToName());

        if (fromElementInfo != null && toElementInfo != null && fromElementInfo.getShapeUIModel().getModelElement().getName().equals(toElementInfo.getShapeUIModel().getModelElement().getName())) {
            iMessage.setType(IMessage.TYPE_SELF_MESSAGE);
        }

        if (element.getRightArrowPart().contains(">>") || element.getLeftArrowPart().contains("<<"))
            iMessage.setAsynchronous(true);

        if (fromElementInfo != null && fromElementInfo.getShapeUIModel().getModelElement() instanceof IInteractionLifeLine) {
            IActivation fromActivation = IModelElementFactory.instance().createActivation();
            ((IInteractionLifeLine) fromElementInfo.getShapeUIModel().getModelElement()).addActivation(fromActivation);
            iMessage.setFrom(fromElementInfo.getShapeUIModel().getModelElement());

            IActivationUIModel shapeFromActivation = (IActivationUIModel) diagramManager.createDiagramElement(diagram, fromActivation);
            iMessage.setFromActivation(fromActivation);
        }

        if (toElementInfo != null && toElementInfo.getShapeUIModel().getModelElement() instanceof IInteractionLifeLine) {
            iMessage.setTo(toElementInfo.getShapeUIModel().getModelElement());
            if (!element.isCreateMessage()) {
                IActivation toActivation = IModelElementFactory.instance().createActivation();
                ((IInteractionLifeLine) toElementInfo.getShapeUIModel().getModelElement()).addActivation(toActivation);

                IActivationUIModel shapeToActivation = (IActivationUIModel) diagramManager.createDiagramElement(diagram, toActivation);
                iMessage.setToActivation(toActivation);
            }
        }

        if (operand != null) {
            operand.addMessage(iMessage);
        }

        IDiagramElement fromShapeModel = null;
        IDiagramElement toShapeModel = null;

        if (fromElementInfo == null) {
            fromShapeModel = diagram.createDiagramElement(IInteractionDiagramUIModel.SHAPETYPE_LOST_FOUND_MESSAGE_END);
        } else
            fromShapeModel = fromElementInfo.getShapeUIModel();

        if (toElementInfo == null) {
            toShapeModel = diagram.createDiagramElement(IInteractionDiagramUIModel.SHAPETYPE_LOST_FOUND_MESSAGE_END);
        } else
            toShapeModel = toElementInfo.getShapeUIModel();

        IMessageUIModel messageUIModel = (IMessageUIModel) diagramManager.createConnector(diagram, iMessage, fromShapeModel, toShapeModel, null);
    }

    private void drawGroupElement(DiagramManager diagramManager, IInteractionDiagramUIModel diagram, GroupElement element, IHasChildrenBaseModelElement parentOperandOrRootFrame, IInteractionOperandUIModel parentOperandUIModel, ICombinedFragment parentFragment) {
        String typeOfGroup = element.getGroupCases().get(0).getType();
        if (typeOfGroup.equals("group")) {
            typeOfGroup = "alt";
            element.getGroupCases().get(0).setType("alt");
        }

        //create fragment
        ICombinedFragment combinedFragment = IModelElementFactory.instance().createCombinedFragment();

        ICombinedFragmentUIModel combinedFragmentUIModel = (ICombinedFragmentUIModel) diagramManager.createDiagramElement(diagram, combinedFragment);
        if (parentOperandOrRootFrame instanceof IFrame) {
            parentOperandOrRootFrame.addChild(combinedFragment);
        } else {
            parentFragment.addChild(combinedFragment);
            parentOperandOrRootFrame.addChild(combinedFragment);
            parentOperandUIModel.addChild(combinedFragmentUIModel);
        }
        combinedFragment.setInteractionOperator(typeOfGroup);


        for (GroupCaseElement groupCase : element.getGroupCases()) {
            IInteractionOperand operand = IModelElementFactory.instance().createInteractionOperand();

            IInteractionConstraint constraint = IModelElementFactory.instance().createInteractionConstraint();

            if (groupCase.getName() != null) {
                operand.setName(groupCase.getName());
                constraint.setName(groupCase.getName());
                constraint.setConstraint(groupCase.getName());

            }
            operand.setGuard(constraint);

            IInteractionOperandUIModel operandUIModel = (IInteractionOperandUIModel) diagramManager.createDiagramElement(diagram, operand);
            combinedFragment.addOperand(operand);
            combinedFragmentUIModel.addChild(operandUIModel);

            for (MessageElement message : groupCase.getMessageElements()) {
                String fromName = message.getFromName();
                String toName = message.getToName();

                if (fromName != null) {
                    ElementInfo elementInfo = elementsMap.get(fromName);
                    if (elementInfo != null) {
                        combinedFragment.addCoveredLifeLine(elementInfo.getShapeUIModel().getModelElement());
                        if (!(parentOperandOrRootFrame instanceof IFrame))
                            ((ICombinedFragment) (parentOperandOrRootFrame).getParent()).addCoveredLifeLine(elementInfo.getShapeUIModel().getModelElement());
                    }
                }
                if (toName != null) {
                    ElementInfo elementInfo = elementsMap.get(toName);
                    if (elementInfo != null) {
                        combinedFragment.addCoveredLifeLine(elementInfo.getShapeUIModel().getModelElement());
                        if (!(parentOperandOrRootFrame instanceof IFrame))
                            ((ICombinedFragment) (parentOperandOrRootFrame).getParent()).addCoveredLifeLine(elementInfo.getShapeUIModel().getModelElement());
                    }
                }
                drawMessageElement(diagramManager, diagram, message, operand);

            }


            groupCase.getGroupElements().forEach(groupElement -> {
                drawGroupElement(diagramManager, diagram, groupElement, operand, operandUIModel, combinedFragment);
            });
        }

    }
}
