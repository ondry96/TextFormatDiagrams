package pluginmain.imports.diagramcreators;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.diagram.IDiagramUIModel;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.diagram.IUseCaseDiagramUIModel;
import com.vp.plugin.diagram.shape.IActorUIModel;
import com.vp.plugin.diagram.shape.ISystemUIModel;
import com.vp.plugin.diagram.shape.IUseCaseUIModel;
import com.vp.plugin.model.*;
import com.vp.plugin.model.factory.IModelElementFactory;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationElement;
import pluginmain.elements.relationships.RelationshipElement;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;

public class UseCaseDiagramCreator extends DiagramCreator {

    public UseCaseDiagramCreator(String filePath) {
        super();
        processFileNameOfPlantUML(filePath);
    }

    @Override
    public IDiagramUIModel createDiagram(String diagramType) {
        DiagramManager diagramManager = ApplicationManager.instance().getDiagramManager();
        IUseCaseDiagramUIModel diagram = (IUseCaseDiagramUIModel) diagramManager.createDiagram(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM);
        diagram.setName(processedFileName);
        for (Element element : innerStructure) {
            if (element instanceof UseCasePackageElement) {
                drawUseCasePackageElement(diagramManager, diagram, (UseCasePackageElement) element, null);
            } else if (element instanceof ActorElement) {
                ElementInfo elementInfo = drawActorElement(diagramManager, diagram, (ActorElement) element);
                if (((ActorElement) element).getAlias() == null)
                    elementsMap.put(element.getName(), elementInfo);
                else
                    elementsMap.put(((ActorElement) element).getAlias(), elementInfo);
            } else if (element instanceof UseCaseElement) {
                ElementInfo elementInfo = drawUseCaseElement(diagramManager, diagram, (UseCaseElement) element);
                if (((UseCaseElement) element).getAlias() == null)
                    elementsMap.put(element.getName(), elementInfo);
                else
                    elementsMap.put(((UseCaseElement) element).getAlias(), elementInfo);
            } else if (element instanceof NoteElement) {
                ElementInfo elementInfo = drawNoteElement(diagramManager, diagram, (NoteElement) element);
                elementsMap.put(((NoteElement) element).getAlias(), elementInfo);
            }
        }

        for (Element element : innerStructure) {
            if (element instanceof RelationshipElement) {
                drawRelationshipElement(diagramManager, diagram, (ImportAssociationElement) element, diagramType);
            }
        }

        return diagram;
    }

    @Override
    public String getDiagramType() {
        return DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM;
    }

    private ElementInfo drawUseCaseElement(DiagramManager diagramManager, IUseCaseDiagramUIModel diagram, UseCaseElement useCaseElement) {
        IUseCase iUseCase = IModelElementFactory.instance().createUseCase();
        iUseCase.setName(useCaseElement.getName());
        if (useCaseElement.getAlias() != null)
            iUseCase.addStereotype("$alias: " + useCaseElement.getAlias());

        useCaseElement.getStereotypes().forEach(iUseCase::addStereotype);

        IUseCaseUIModel useCaseShape = null;

        if (!iUseCase.getName().equals(useCaseElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof IUseCase && modelElement.getName().equals(useCaseElement.getName())) {
                    useCaseShape = (IUseCaseUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            useCaseShape = (IUseCaseUIModel) diagramManager.createDiagramElement(diagram, iUseCase);
        }
        if (useCaseShape == null)
            useCaseShape = (IUseCaseUIModel) diagramManager.createDiagramElement(diagram, iUseCase);

        useCaseShape.setRequestResetCaption(true);
        useCaseShape.setRequestFitSize(true);

        IStereotype[] stereotypeIterator = iUseCase.toStereotypeModelArray();
        hideAliasStereotype(useCaseShape, stereotypeIterator);

        setColorToShape(useCaseElement.getBackgroundColor(), useCaseShape);

        return new ElementInfo(iUseCase, useCaseShape, "usecase");

    }

    private ElementInfo drawActorElement(DiagramManager diagramManager, IUseCaseDiagramUIModel diagram, ActorElement actorElement) {
        IActor iActor = IModelElementFactory.instance().createActor();
        iActor.setName(actorElement.getName());
        if (actorElement.getAlias() != null)
            iActor.addStereotype("$alias: " + actorElement.getAlias());

        actorElement.getStereotypes().forEach(iActor::addStereotype);

        IActorUIModel actorShape = null;

        if (!iActor.getName().equals(actorElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof IActor && modelElement.getName().equals(actorElement.getName())) {
                    actorShape = (IActorUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            actorShape = (IActorUIModel) diagramManager.createDiagramElement(diagram, iActor);
        }
        if (actorShape == null)
            actorShape = (IActorUIModel) diagramManager.createDiagramElement(diagram, iActor);

        actorShape.setRequestResetCaption(true);
        actorShape.setRequestFitSize(true);

        IStereotype[] stereotypeIterator = iActor.toStereotypeModelArray();
        hideAliasStereotype(actorShape, stereotypeIterator);

        setColorToShape(actorElement.getBackgroundColor(), actorShape);

        return new ElementInfo(iActor, actorShape, "actor");
    }

    private void drawUseCasePackageElement(DiagramManager diagramManager, IUseCaseDiagramUIModel diagram, UseCasePackageElement currentPackageElement, ElementInfo parentPackageInfo) {
        ISystem iSystem = IModelElementFactory.instance().createSystem();
        iSystem.setName(currentPackageElement.getName());
        if (currentPackageElement.getAlias() != null)
            iSystem.addStereotype("$alias: " + currentPackageElement.getAlias());

        currentPackageElement.getStereotypes().forEach(iSystem::addStereotype);

        ISystemUIModel basePackageShape = null;

        if (!iSystem.getName().equals(currentPackageElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof ISystem && modelElement.getName().equals(currentPackageElement.getName())) {
                    basePackageShape = (ISystemUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            basePackageShape = (ISystemUIModel) diagramManager.createDiagramElement(diagram, iSystem);
        }
        if (basePackageShape == null)
            basePackageShape = (ISystemUIModel) diagramManager.createDiagramElement(diagram, iSystem);

        basePackageShape.setRequestResetCaption(true);
        basePackageShape.setRequestFitSize(true);

        IStereotype[] stereotypeIterator = iSystem.toStereotypeModelArray();
        hideAliasStereotype(basePackageShape, stereotypeIterator);

        setColorToShape(currentPackageElement.getBackgroundColor(), basePackageShape);

        if (currentPackageElement.getAlias() == null)
            elementsMap.put(currentPackageElement.getName(), new ElementInfo(iSystem, basePackageShape, "package"));
        else
            elementsMap.put(currentPackageElement.getAlias(), new ElementInfo(iSystem, basePackageShape, "package"));

        if (parentPackageInfo != null) {
            parentPackageInfo.getShapeUIModel().getModelElement().addChild(iSystem);
            parentPackageInfo.getShapeUIModel().addChild(basePackageShape);
        }

        for (ActorElement actorElement : currentPackageElement.getActors()) {
            ElementInfo elementInfo = drawActorElement(diagramManager, diagram, actorElement);
            iSystem.addChild(elementInfo.getShapeUIModel().getModelElement());
            basePackageShape.addChild((IShapeUIModel) elementInfo.getShapeUIModel());
            if (actorElement.getAlias() == null)
                elementsMap.put(actorElement.getName(), elementInfo);
            else
                elementsMap.put(actorElement.getAlias(), elementInfo);
        }

        for (UseCaseElement useCaseElement : currentPackageElement.getUseCases()) {
            ElementInfo elementInfo = drawUseCaseElement(diagramManager, diagram, useCaseElement);
            iSystem.addChild(elementInfo.getShapeUIModel().getModelElement());
            basePackageShape.addChild((IShapeUIModel) elementInfo.getShapeUIModel());
            if (useCaseElement.getAlias() == null)
                elementsMap.put(useCaseElement.getName(), elementInfo);
            else
                elementsMap.put(useCaseElement.getAlias(), elementInfo);
        }

        for (NoteElement noteElement : currentPackageElement.getNotes()) {
            ElementInfo elementInfo = drawNoteElement(diagramManager, diagram, noteElement);
            iSystem.addChild(elementInfo.getShapeUIModel().getModelElement());
            basePackageShape.addChild((IShapeUIModel) elementInfo.getShapeUIModel());
            elementsMap.put(noteElement.getAlias(), elementInfo);
        }

        for (ChildrenBasedElement subpackageElement : currentPackageElement.getPackages()) {
            drawUseCasePackageElement(diagramManager, diagram, (UseCasePackageElement) subpackageElement, new ElementInfo(iSystem, basePackageShape, "package"));
        }
    }
}
