package pluginmain.imports.diagramcreators;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.diagram.IClassDiagramUIModel;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.diagram.connector.IAssociationClassUIModel;
import com.vp.plugin.diagram.shape.IClassUIModel;
import com.vp.plugin.diagram.shape.IModelUIModel;
import com.vp.plugin.diagram.shape.IPackageUIModel;
import com.vp.plugin.model.*;
import com.vp.plugin.model.factory.IModelElementFactory;
import pluginmain.elements.classdiagram.AttributeElement;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.classdiagram.OperationElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationClass;
import pluginmain.elements.relationships.ImportAssociationElement;
import pluginmain.elements.relationships.RelationshipElement;
import pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassProcessor;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ClassDiagramCreator extends DiagramCreator {

    private final ArrayList<IAssociationClass> createdAssociationsClasses = new ArrayList<>();

    public ClassDiagramCreator(String diagramName) {
        super();
        processFileNameOfPlantUML(diagramName);
    }

    @Override
    public IClassDiagramUIModel createDiagram(String diagramType) {
        DiagramManager diagramManager = ApplicationManager.instance().getDiagramManager();
        IClassDiagramUIModel diagram = (IClassDiagramUIModel) diagramManager.createDiagram(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM);
        diagram.setName(processedFileName);
        for (Element element : innerStructure) {
            if (element instanceof ClassPackageElement) {
                drawClassPackageElement(diagramManager, diagram, (ClassPackageElement) element, null);
            } else if (element instanceof ClassElement) {
                ElementInfo elementInfo = drawClassElement(diagramManager, diagram, (ClassElement) element);
                if (((ClassElement) element).getAlias() == null)
                    elementsMap.put(element.getName(), elementInfo);
                else
                    elementsMap.put(((ClassElement) element).getAlias(), elementInfo);
            } else if (element instanceof NoteElement) {
                ElementInfo elementInfo = drawNoteElement(diagramManager, diagram, (NoteElement) element);
                elementsMap.put(((NoteElement) element).getAlias(), elementInfo);
            }
        }

        for (Element element : innerStructure) {
            if (element instanceof ImportAssociationClass) {
                drawAssociationClass(diagramManager, diagram, (ImportAssociationClass) element);
            } else if (element instanceof RelationshipElement) {
                drawRelationshipElement(diagramManager, diagram, (ImportAssociationElement) element, diagramType);
            }
        }

        return diagram;
    }

    @Override
    public String getDiagramType() {
        return DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM;
    }

    private void drawAssociationClass(DiagramManager diagramManager, IClassDiagramUIModel diagram, ImportAssociationClass importElement) {
        ElementInfo fromAssociationClassInfo = elementsMap.get(importElement.getAssociation().getFromName());
        String fromClassName = fromAssociationClassInfo.getShapeUIModel().getModelElement().getName();
        ElementInfo toAssociationClassInfo = elementsMap.get(importElement.getAssociation().getToName());
        String toClassName = toAssociationClassInfo.getShapeUIModel().getModelElement().getName();

        ElementInfo rightSideInfo = elementsMap.get(importElement.getToClassName());
        String rightSideClassName = rightSideInfo.getShapeUIModel().getModelElement().getName();

        if (fromClassName.equals(rightSideClassName) || toClassName.equals(rightSideClassName)) {
            errors.add("Association class cannot be created, it cannot be one of base classes");
            return;
        }

        List<ElementInfo> associationsWithClassesOfLeftSide = createdAssociations.stream().filter(associationElemInfo -> {
            IAssociation association = (IAssociation) associationElemInfo.getShapeUIModel().getModelElement();
                    return ((association.getFrom().getName().equals(fromClassName) && association.getTo().getName().equals(toClassName)) ||
                            (association.getFrom().getName().equals(toClassName) && association.getTo().getName().equals(fromClassName)));
                }
        ).collect(Collectors.toList());

        ElementInfo lastAssociationBetweenClassesInfo;
        while (true) {
            if (associationsWithClassesOfLeftSide.size() == 0) {
                errors.add("Association class cannot be created, it is missing base association");
                return;
            }

            lastAssociationBetweenClassesInfo = associationsWithClassesOfLeftSide.get(associationsWithClassesOfLeftSide.size() - 1);

            ElementInfo finalLastAssociationBetweenClassesInfoCopy = lastAssociationBetweenClassesInfo;
            Optional<IAssociationClass> foundedOptional = createdAssociationsClasses.stream().filter(assocClass ->
                    assocClass.getFrom() == finalLastAssociationBetweenClassesInfoCopy.getShapeUIModel().getModelElement() && assocClass.getTo() == rightSideInfo.getShapeUIModel().getModelElement()
            ).findAny();
            if (foundedOptional.isPresent()) {
                associationsWithClassesOfLeftSide.remove(associationsWithClassesOfLeftSide.size() - 1);
            } else {
                createdAssociations.remove(associationsWithClassesOfLeftSide.size() - 1);
                break;
            }
        }

        IAssociationClass associationClassModel = IModelElementFactory.instance().createAssociationClass();
        associationClassModel.setFrom(lastAssociationBetweenClassesInfo.getShapeUIModel().getModelElement());
        associationClassModel.setTo(rightSideInfo.getShapeUIModel().getModelElement());

        IAssociationClassUIModel associationClassConnector = (IAssociationClassUIModel) diagramManager.createConnector(
                diagram, associationClassModel, lastAssociationBetweenClassesInfo.getShapeUIModel(), rightSideInfo.getShapeUIModel(), null
        );
        createdAssociationsClasses.add(associationClassModel);
    }

    private ElementInfo drawClassElement(DiagramManager diagramManager, IClassDiagramUIModel diagram, ClassElement classElement) {
        IClass iClass = IModelElementFactory.instance().createClass();
        iClass.setName(classElement.getName());
        if (classElement.getAlias() != null)
            iClass.addStereotype("$alias: " + classElement.getAlias());

        classElement.getStereotypes().forEach(iClass::addStereotype);

        IClassUIModel classShape = null;


        if (!iClass.getName().equals(classElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof IClass && modelElement.getName().equals(classElement.getName())) {
                    classShape = (IClassUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            classShape = (IClassUIModel) diagramManager.createDiagramElement(diagram, iClass);
        }

        if (classShape == null)
            classShape = (IClassUIModel) diagramManager.createDiagramElement(diagram, iClass);

        classShape.setRequestResetCaption(true);
        classShape.setRequestFitSize(true);

        IStereotype[] stereotypeIterator = iClass.toStereotypeModelArray();
        hideAliasStereotype(classShape, stereotypeIterator);

        setColorToShape(classElement.getBackgroundColor(), classShape);

        for (String s : classElement.getTemplateParameters()) {
            ITemplateParameter templateParameter = IModelElementFactory.instance().createTemplateParameter();
            templateParameter.setName(s);
            iClass.addTemplateParameter(templateParameter);
        }

        for (AttributeElement attributeElement : classElement.getAttributes()) {
            IAttribute iAttribute = IModelElementFactory.instance().createAttribute();
            iAttribute.setName(attributeElement.getName());
            iAttribute.setAbstract(attributeElement.isAbstract());
            if (attributeElement.isStatic())
                iAttribute.setScope(IAttribute.SCOPE_CLASSIFIER);
            if (attributeElement.getVisibility() == null)
                iAttribute.setVisibility(IAttribute.VISIBILITY_UNSPECIFIED);
            else if (attributeElement.getVisibility().equals("package private"))
                iAttribute.setVisibility(IAttribute.VISIBILITY_PACKAGE);
            else
                iAttribute.setVisibility(attributeElement.getVisibility());
            iClass.addAttribute(iAttribute);
        }

        for (OperationElement operationElement : classElement.getOperations()) {
            IOperation iOperation = IModelElementFactory.instance().createOperation();
            iOperation.setAbstract(operationElement.isAbstract());
            if (operationElement.isStatic())
                iOperation.setScope(IAttribute.SCOPE_CLASSIFIER);

            if (operationElement.getName().contains("(") && operationElement.getName().contains(")") && !operationElement.getName().contains("()")) {
                String[] parameters = ClassProcessor.getParameters(operationElement.getName());
                for (String parameter : parameters) {
                    IParameter iParameter = IModelElementFactory.instance().createParameter();
                    if (parameter.contains(")")) {
                        int indexOfEnd = parameter.indexOf(")");
                        iParameter.setName(parameter.substring(0, indexOfEnd));
                    } else
                        iParameter.setName(parameter);
                    iOperation.addParameter(iParameter);
                }
            }

            processOperationName(operationElement, iOperation);

            if (operationElement.getVisibility() == null)
                iOperation.setVisibility(IOperation.VISIBILITY_UNSPECIFIED);
            else if (operationElement.getVisibility().equals("package private"))
                iOperation.setVisibility(IOperation.VISIBILITY_PACKAGE);
            else
                iOperation.setVisibility(operationElement.getVisibility());
            iClass.addOperation(iOperation);
        }

        return new ElementInfo(iClass, classShape, "class");
    }

    private void processOperationName(OperationElement operationElement, IOperation iOperation) {
        String name = ClassProcessor.removeBracketsWithParameters(operationElement.getName());
        if (name.contains(":")) {
            String trueName;
            int indexOfColon = name.indexOf(":");
            trueName = name.substring(0, indexOfColon).trim();
            iOperation.setName(trueName);
            iOperation.setReturnType(name.substring(indexOfColon + 1).trim());
        } else
            iOperation.setName(name);
    }

    private void drawClassPackageElement(DiagramManager diagramManager, IClassDiagramUIModel diagram, ClassPackageElement currentPackageElement, ElementInfo parentPackageInfo) {
        if (currentPackageElement.getStereotypes().contains("Model")) {
            drawModelElement(diagramManager, diagram, currentPackageElement, parentPackageInfo);
            return;
        }

        IPackage iPackage = IModelElementFactory.instance().createPackage();
        iPackage.setName(currentPackageElement.getName());
        if (currentPackageElement.getAlias() != null)
            iPackage.addStereotype("$alias: " + currentPackageElement.getAlias());

        currentPackageElement.getStereotypes().forEach(iPackage::addStereotype);


        IPackageUIModel basePackageShape = null;

        if (!iPackage.getName().equals(currentPackageElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof IPackage && modelElement.getName().equals(currentPackageElement.getName())) {
                    basePackageShape = (IPackageUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            basePackageShape = (IPackageUIModel) diagramManager.createDiagramElement(diagram, iPackage);
        }

        if (basePackageShape == null)
            basePackageShape = (IPackageUIModel) diagramManager.createDiagramElement(diagram, iPackage);

        basePackageShape.setRequestResetCaption(true);
        basePackageShape.setRequestFitSize(true);

        IStereotype[] stereotypeIterator = iPackage.toStereotypeModelArray();
        hideAliasStereotype(basePackageShape, stereotypeIterator);


        setColorToShape(currentPackageElement.getBackgroundColor(), basePackageShape);

        if (currentPackageElement.getAlias() == null)
            elementsMap.put(currentPackageElement.getName(), new ElementInfo(iPackage, basePackageShape, "package"));
        else
            elementsMap.put(currentPackageElement.getAlias(), new ElementInfo(iPackage, basePackageShape, "package"));

        if (parentPackageInfo != null) {
            parentPackageInfo.getShapeUIModel().getModelElement().addChild(iPackage);
            parentPackageInfo.getShapeUIModel().addChild(basePackageShape);
        }

        for (ClassElement classElement : currentPackageElement.getClasses()) {
            ElementInfo elementInfo = drawClassElement(diagramManager, diagram, classElement);
            iPackage.addChild(elementInfo.getShapeUIModel().getModelElement());
            basePackageShape.addChild((IShapeUIModel) elementInfo.getShapeUIModel());
            if (classElement.getAlias() == null)
                elementsMap.put(classElement.getName(), elementInfo);
            else
                elementsMap.put(classElement.getAlias(), elementInfo);
        }

        for (NoteElement noteElement : currentPackageElement.getNotes()) {
            ElementInfo elementInfo = drawNoteElement(diagramManager, diagram, noteElement);
            iPackage.addChild(elementInfo.getShapeUIModel().getModelElement());
            basePackageShape.addChild((IShapeUIModel) elementInfo.getShapeUIModel());
            elementsMap.put(noteElement.getAlias(), elementInfo);
        }

        for (ChildrenBasedElement subpackageElement : currentPackageElement.getPackages()) {
            drawClassPackageElement(diagramManager, diagram, (ClassPackageElement) subpackageElement, new ElementInfo(iPackage, basePackageShape, "package"));
        }
    }

    private void drawModelElement(DiagramManager diagramManager, IClassDiagramUIModel diagram, ClassPackageElement currentModelElement, ElementInfo parentPackageInfo) {
        IModel iModel = IModelElementFactory.instance().createModel();
        iModel.setName(currentModelElement.getName());
        if (currentModelElement.getAlias() != null)
            iModel.addStereotype("$alias: " + currentModelElement.getAlias());

        currentModelElement.getStereotypes().forEach(iModel::addStereotype);
        iModel.removeStereotype("Model");

        IModelUIModel baseModelShape = null;

        if (!iModel.getName().equals(currentModelElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof IModel && modelElement.getName().equals(currentModelElement.getName())) {
                    baseModelShape = (IModelUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            baseModelShape = (IModelUIModel) diagramManager.createDiagramElement(diagram, iModel);
        }

        if (baseModelShape == null)
            baseModelShape = (IModelUIModel) diagramManager.createDiagramElement(diagram, iModel);

        baseModelShape.setRequestResetCaption(true);
        baseModelShape.setRequestFitSize(true);
        setColorToShape(currentModelElement.getBackgroundColor(), baseModelShape);

        if (currentModelElement.getAlias() == null)
            elementsMap.put(currentModelElement.getName(), new ElementInfo(iModel, baseModelShape, "package"));
        else
            elementsMap.put(currentModelElement.getAlias(), new ElementInfo(iModel, baseModelShape, "package"));

        if (parentPackageInfo != null) {
            parentPackageInfo.getShapeUIModel().getModelElement().addChild(iModel);
            parentPackageInfo.getShapeUIModel().addChild(baseModelShape);
        }

        for (ClassElement classElement : currentModelElement.getClasses()) {
            ElementInfo elementInfo = drawClassElement(diagramManager, diagram, classElement);
            iModel.addChild(elementInfo.getShapeUIModel().getModelElement());
            baseModelShape.addChild((IShapeUIModel) elementInfo.getShapeUIModel());
            if (classElement.getAlias() == null)
                elementsMap.put(classElement.getName(), elementInfo);
            else
                elementsMap.put(classElement.getAlias(), elementInfo);

            elementsMap.put(classElement.getName(), elementInfo);
        }

        for (ChildrenBasedElement subpackageElement : currentModelElement.getPackages()) {
            drawClassPackageElement(diagramManager, diagram, (ClassPackageElement) subpackageElement, new ElementInfo(iModel, baseModelShape, "package"));
        }
    }
}
