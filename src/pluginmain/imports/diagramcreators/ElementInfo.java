package pluginmain.imports.diagramcreators;

import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.model.IModelElement;

public class ElementInfo {

    private IModelElement modelElement;
    private IDiagramElement shapeUIModel;
    private String elementType;

    ElementInfo() {
    }

    public ElementInfo(IModelElement modelElement, IDiagramElement shapeUIModel, String elementType) {
        this.modelElement = modelElement;
        this.shapeUIModel = shapeUIModel;
        this.elementType = elementType;
    }

    public IModelElement getModelElement() {
        return modelElement;
    }

    public void setModelElement(IModelElement modelElement) {
        this.modelElement = modelElement;
    }

    public IDiagramElement getShapeUIModel() {
        return shapeUIModel;
    }

    public void setShapeUIModel(IDiagramElement shapeUIModel) {
        this.shapeUIModel = shapeUIModel;
    }

    public String getElementType() {
        return elementType;
    }

    public void setElementType(String elementType) {
        this.elementType = elementType;
    }
}
