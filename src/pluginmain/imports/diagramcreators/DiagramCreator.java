package pluginmain.imports.diagramcreators;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.diagram.IClassDiagramUIModel;
import com.vp.plugin.diagram.IDiagramUIModel;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.diagram.connector.*;
import com.vp.plugin.diagram.format.IFillColor;
import com.vp.plugin.diagram.shape.INoteUIModel;
import com.vp.plugin.model.*;
import com.vp.plugin.model.factory.IModelElementFactory;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationElement;

import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public abstract class DiagramCreator {
    protected final HashMap<String, ElementInfo> elementsMap = new HashMap<>(); //element name -> {IModelElement, IShapeUIModel}
    protected final ArrayList<ElementInfo> createdAssociations = new ArrayList<>();
    public List<String> usedRenames = new ArrayList<>();
    protected List<Element> innerStructure;
    protected List<String> errors;
    protected String processedFileName;

    public abstract IDiagramUIModel createDiagram(String diagramType);

    public abstract String getDiagramType();

    public static void hideAliasStereotype(IShapeUIModel shapeOfElement, IStereotype[] stereotypeIterator) {
        if (stereotypeIterator == null)
            return;

        List<String> ids = new ArrayList<>();
        for (int i = 0; i < stereotypeIterator.length; i++) {
            IStereotype stereotype = stereotypeIterator[i];
            if (stereotype.getName().startsWith("$alias")) {
                ids.add(stereotype.getId());
            }
        }
        String[] array = new String[1];
        shapeOfElement.setHiddenStereotypeIds(ids.toArray(array));
    }

    protected void processFileNameOfPlantUML(String filePath) {
        File f = new File(filePath);
        this.processedFileName = "imp-" + f.getName();
        if (processedFileName.endsWith(".puml") || processedFileName.endsWith(".txt")) {
            processedFileName = processedFileName.substring(0, processedFileName.lastIndexOf("."));
        }
    }

    public String getProcessedFileName() {
        return processedFileName;
    }

    public void setProcessedFileName(String processedFileName) {
        this.processedFileName = processedFileName;
    }

    public List<Element> getInnerStructure() {
        return innerStructure;
    }

    public void setInnerStructure(List<Element> innerStructure) {
        this.innerStructure = innerStructure;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }

    protected void setColorToShape(String colorAsHex, IShapeUIModel shapeUIModel) {
        if (colorAsHex != null) {
            IFillColor fillColor = shapeUIModel.getFillColor();
            fillColor.setType(IFillColor.TYPE_SOLID);
            fillColor.setColor1(Color.decode(colorAsHex));
            fillColor.setTransparency(0);
        }
    }

    protected ElementInfo drawNoteElement(DiagramManager diagramManager, IDiagramUIModel diagram, NoteElement noteElement) {
        INOTE iNote = IModelElementFactory.instance().createNOTE();
        iNote.setDescription(noteElement.getDescription());
        iNote.setName(noteElement.getAlias());

        INoteUIModel noteShape = null;

        if (!iNote.getName().equals(noteElement.getName())) {
            IModelElement[] iModelElementsUIModels = ApplicationManager.instance().getProjectManager().getProject().toModelElementArray();
            for (IModelElement modelElement : iModelElementsUIModels) {
                if (modelElement instanceof INOTE && modelElement.getName().equals(noteElement.getName())) {
                    noteShape = (INoteUIModel) diagramManager.createDiagramElement(diagram, modelElement);
                    break;
                }
            }
        } else {
            noteShape = (INoteUIModel) diagramManager.createDiagramElement(diagram, iNote);
        }
        if (noteShape == null)
            noteShape = (INoteUIModel) diagramManager.createDiagramElement(diagram, iNote);


        noteShape.setRequestResetCaption(true);
        noteShape.setRequestFitSize(true);
        setColorToShape(noteElement.getBackgroundColor(), noteShape);

        return new ElementInfo(iNote, noteShape, "note");
    }

    protected void drawRelationshipElement(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, String diagramType) {
        String fromAggregationKind = element.getFromAggregationKind();
        String toAggregationKind = element.getToAggregationKind();
        if (element.isMiddleDotted()) {
            drawDottedRelation(diagramManager, diagram, element, fromAggregationKind, toAggregationKind, diagramType);
        } else {
            drawDashedRelation(diagramManager, diagram, element, fromAggregationKind, toAggregationKind);
        }

    }

    protected void drawDashedRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, String fromAggregationKind, String toAggregationKind) {
        if (fromAggregationKind == null && toAggregationKind == null) {
            createAssociationRelation(diagramManager, diagram, element, false, false); //association
        } else if (fromAggregationKind != null && toAggregationKind == null) {
            if (fromAggregationKind.equals("<|") || fromAggregationKind.equals("^"))
                createGeneralizationRelation(diagramManager, diagram, element, true); //generalization <|--
            else if (fromAggregationKind.equals("+"))
                createContainmentRelation(diagramManager, diagram, element, true); //containment
            else
                createAssociationRelation(diagramManager, diagram, element, true, false); //association
        } else if (fromAggregationKind == null && toAggregationKind != null) {
            if (toAggregationKind.equals("|>") || toAggregationKind.equals("^"))
                createGeneralizationRelation(diagramManager, diagram, element, false); //generalization --|>
            else if (toAggregationKind.equals("+"))
                createContainmentRelation(diagramManager, diagram, element, false); //containment
            else
                createAssociationRelation(diagramManager, diagram, element, false, true); //association
        } else {
            createAssociationRelation(diagramManager, diagram, element, true, true); //association
        }
    }

    protected void processArrowOfRelation(ImportAssociationElement element, IAssociationEnd associationEnd, String aggregationKind) {
        switch (aggregationKind) {
            case "*":
                associationEnd.setAggregationKind(IAssociationEnd.AGGREGATION_KIND_COMPOSITED);
                break;

            case "o":
                associationEnd.setAggregationKind(IAssociationEnd.AGGREGATION_KIND_SHARED);
                break;

            case ">":
            case "<":
                associationEnd.setNavigable(0);
                break;

            case "x":
                associationEnd.setNavigable(2);
                break;
            case "#":
            case "}":
            case "{":
                break;
            default:
                errors.add("This kind of relationship between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw in Visual Paradigm");
        }
    }

    protected void createContainmentRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, boolean isLeftFrom) {
        ElementInfo elementInfoFromElement = elementsMap.get(element.getFromName());
        ElementInfo elementInfoToElement;
        if (isLeftFrom) {
            //+.. (switch element infos)
            elementInfoToElement = elementsMap.get(element.getToName());
        } else {
            //  ..+
            elementInfoToElement = elementInfoFromElement;
            elementInfoFromElement = elementsMap.get(element.getToName());
        }

        IContainmentUIModel uiModel = (IContainmentUIModel) diagramManager.createConnector(diagram, IClassDiagramUIModel.SHAPETYPE_CONTAINMENT,
                elementInfoFromElement.getShapeUIModel(), elementInfoToElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);
    }

    protected void createAssociationRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, boolean hasLeftArrowHead, boolean hasRightArrowHead) {
        ElementInfo elementInfoFromClass = elementsMap.get(element.getFromName());
        ElementInfo elementInfoToClass = elementsMap.get(element.getToName());
        if (elementInfoFromClass == null || elementInfoToClass == null)
            return;

        String fromMultiplicity = element.getFromMultiplicity();
        String toMultiplicity = element.getToMultiplicity();

        IAssociation associationModel = IModelElementFactory.instance().createAssociation();
        if (element.getName() != null) {
            setStereotypesAndName(element, associationModel);
        }

        boolean isAssociationBetweenCorrectElements = (
                (elementInfoFromClass.getModelElement() instanceof IClass && elementInfoToClass.getModelElement() instanceof IClass) ||
                        (elementInfoFromClass.getModelElement() instanceof IActor && elementInfoToClass.getModelElement() instanceof IActor) ||
                        (elementInfoFromClass.getModelElement() instanceof IUseCase && elementInfoToClass.getModelElement() instanceof IUseCase) ||
                        (elementInfoFromClass.getModelElement() instanceof IActor && elementInfoToClass.getModelElement() instanceof IUseCase) ||
                        (elementInfoFromClass.getModelElement() instanceof IUseCase && elementInfoToClass.getModelElement() instanceof IActor)
        );

        if (!isAssociationBetweenCorrectElements) {
            errors.add("Association between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw - one or both elements are not correct elements");
            return;
        }

        associationModel.setFrom(elementInfoFromClass.getShapeUIModel().getModelElement());
        associationModel.setTo(elementInfoToClass.getShapeUIModel().getModelElement());

        IAssociationEnd associationFromEnd = (IAssociationEnd) associationModel.getFromEnd();
        associationFromEnd.setAggregationKind(IAssociationEnd.AGGREGATION_KIND_NONE);
        associationFromEnd.setNavigable(1);
        if (fromMultiplicity != null)
            associationFromEnd.setMultiplicity(fromMultiplicity);
        if (hasLeftArrowHead)
            processArrowOfRelation(element, associationFromEnd, element.getFromAggregationKind());


        IAssociationEnd associationToEnd = (IAssociationEnd) associationModel.getToEnd();
        associationToEnd.setAggregationKind(IAssociationEnd.AGGREGATION_KIND_NONE);
        associationToEnd.setNavigable(1);
        if (toMultiplicity != null)
            associationToEnd.setMultiplicity(toMultiplicity);
        if (hasRightArrowHead)
            processArrowOfRelation(element, associationToEnd, element.getToAggregationKind());


        IAssociationUIModel uiModel = (IAssociationUIModel) diagramManager.createConnector(diagram, associationModel,
                elementInfoFromClass.getShapeUIModel(), elementInfoToClass.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);

        createdAssociations.add(new ElementInfo(associationModel, uiModel, "association"));
    }

    protected void drawDottedRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, String fromAggregationKind, String toAggregationKind, String diagramType) {
        if (element.getFromMultiplicity() != null || element.getToMultiplicity() != null) {
            errors.add("This kind of relationship between elements '" + element.getFromName() + "' and '" + element.getToName() + "' cannot have multiplicity set, it is impossible to draw in Visual Paradigm");
            return;
        }

        if (fromAggregationKind != null && toAggregationKind != null) {
            errors.add("Relationship between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw in Visual Paradigm");
            return;
        }

        if (fromAggregationKind != null && toAggregationKind == null) {
            if (fromAggregationKind.equals("<|") || fromAggregationKind.equals("^"))
                createRealizationRelation(diagramManager, diagram, element, true); //realization <|.. or ^..
            else if (fromAggregationKind.equals("<"))
                createDependencyRelation(diagramManager, diagram, element, true, diagramType); //dependency <.. or ..^
            else
                errors.add("Relationship between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw in Visual Paradigm");
        } else if (fromAggregationKind == null && toAggregationKind != null) {
            if (toAggregationKind.equals("|>") || toAggregationKind.equals("^"))
                createRealizationRelation(diagramManager, diagram, element, false); //realization ..|>
            else if (toAggregationKind.equals(">"))
                createDependencyRelation(diagramManager, diagram, element, false, diagramType); //dependency ..>
            else
                errors.add("Relationship between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw in Visual Paradigm");
        } else {
            createConstraintRelation(diagramManager, diagram, element); //constraint ..
        }
    }

    protected void createGeneralizationRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, boolean isLeftFrom) {
        ElementInfo elementInfoFromElement = elementsMap.get(element.getFromName());
        ElementInfo elementInfoToElement;
        if (isLeftFrom) {
            //<|-- (switch element infos)
            elementInfoToElement = elementsMap.get(element.getToName());
        } else {
            //  --|>
            elementInfoToElement = elementInfoFromElement;
            elementInfoFromElement = elementsMap.get(element.getToName());
        }

        boolean isGeneralizationBetweenWrongElements = elementInfoFromElement.getShapeUIModel().getModelElement() instanceof INOTE ||
                elementInfoToElement.getShapeUIModel().getModelElement() instanceof INOTE;

        if (isGeneralizationBetweenWrongElements) {
            errors.add("Generalization between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw - one or both elements are notes");
            return;
        }

        IGeneralization generalizationModel = IModelElementFactory.instance().createGeneralization();
        if (element.getName() != null) {
            setStereotypesAndName(element, generalizationModel);
        }

        generalizationModel.setFrom(elementInfoFromElement.getShapeUIModel().getModelElement());
        generalizationModel.setTo(elementInfoToElement.getShapeUIModel().getModelElement());

        IGeneralizationUIModel uiModel = (IGeneralizationUIModel) diagramManager.createConnector(diagram, generalizationModel,
                elementInfoFromElement.getShapeUIModel(), elementInfoToElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);
    }

    protected void createConstraintRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element) {
        ElementInfo elementInfoFromElement = elementsMap.get(element.getFromName());
        ElementInfo elementInfoToElement = elementsMap.get(element.getToName());

        IConstraint constraintModel = IModelElementFactory.instance().createConstraint();
        if (element.getName() != null) {
            setStereotypesAndName(element, constraintModel);
        }

        constraintModel.setFrom(elementInfoFromElement.getShapeUIModel().getModelElement());
        constraintModel.setTo(elementInfoToElement.getShapeUIModel().getModelElement());

        IConstraintUIModel uiModel = (IConstraintUIModel) diagramManager.createConnector(diagram, constraintModel, elementInfoFromElement.getShapeUIModel(), elementInfoToElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);
    }

    protected void createDependencyRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, boolean isLeftFrom, String diagramType) {
        ElementInfo elementInfoFromElement = elementsMap.get(element.getFromName());
        ElementInfo elementInfoToElement;
        if (isLeftFrom) {
            //..> (switch element infos)
            elementInfoToElement = elementInfoFromElement;
            elementInfoFromElement = elementsMap.get(element.getToName());
        } else {
            // <..
            elementInfoToElement = elementsMap.get(element.getToName());
        }

        boolean isDependencyBetweenWrongElements = elementInfoFromElement.getShapeUIModel().getModelElement() instanceof INOTE ||
                elementInfoToElement.getShapeUIModel().getModelElement() instanceof INOTE;

        if (isDependencyBetweenWrongElements) {
            errors.add("Dependency between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw - one or both elements are notes");
            return;
        }

        if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
            if (element.getName().startsWith("extends, ")) {
                createExtendsRelation(diagramManager, elementInfoFromElement, elementInfoToElement, diagram);
                return;
            } else if (element.getName().startsWith("include, ")) {
                createIncludeRelation(diagramManager, elementInfoFromElement, elementInfoToElement, diagram);
                return;
            } //else create dependency
        }
        //else create dependency
        IDependency dependencyModel = IModelElementFactory.instance().createDependency();
        if (element.getName() != null) {
            setStereotypesAndName(element, dependencyModel);
        }

        dependencyModel.setFrom(elementInfoFromElement.getShapeUIModel().getModelElement());
        dependencyModel.setTo(elementInfoToElement.getShapeUIModel().getModelElement());

        IDependencyUIModel uiModel = (IDependencyUIModel) diagramManager.createConnector(diagram, dependencyModel, elementInfoFromElement.getShapeUIModel(), elementInfoToElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);

    }

    protected void createRealizationRelation(DiagramManager diagramManager, IDiagramUIModel diagram, ImportAssociationElement element, boolean isLeftFrom) {
        ElementInfo elementInfoFromElement = elementsMap.get(element.getFromName());
        ElementInfo elementInfoToElement;
        if (isLeftFrom) { // <|..
            elementInfoToElement = elementsMap.get(element.getToName());
        } else {
            //..|> (switch element infos)
            elementInfoToElement = elementInfoFromElement;
            elementInfoFromElement = elementsMap.get(element.getToName());
        }

        boolean isRealizationBetweenWrongElements = elementInfoFromElement.getShapeUIModel().getModelElement() instanceof INOTE ||
                elementInfoToElement.getShapeUIModel().getModelElement() instanceof INOTE;

        if (isRealizationBetweenWrongElements) {
            errors.add("Realization between elements '" + element.getFromName() + "' and '" + element.getToName() + "' is impossible to draw - one or both elements are notes");
            return;
        }

        IRealization realizationModel = IModelElementFactory.instance().createRealization();
        if (element.getName() != null) {
            setStereotypesAndName(element, realizationModel);
        }
        realizationModel.setFrom(elementInfoFromElement.getShapeUIModel().getModelElement());
        realizationModel.setTo(elementInfoToElement.getShapeUIModel().getModelElement());

        IRealizationUIModel uiModel = (IRealizationUIModel) diagramManager.createConnector(diagram, realizationModel, elementInfoFromElement.getShapeUIModel(), elementInfoToElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);
    }

    public void setStereotypesAndName(ImportAssociationElement element, IRelationship simpleRelationship) {
        String nameWithStereotypes = element.getName();
        while (nameWithStereotypes.startsWith("<<") && nameWithStereotypes.contains(">>")) {
            int indexOfStereotypeEnd = nameWithStereotypes.indexOf(" >>");
            simpleRelationship.addStereotype(nameWithStereotypes.substring(2, indexOfStereotypeEnd).trim());
            nameWithStereotypes = nameWithStereotypes.substring(indexOfStereotypeEnd + 3).trim();
        }
        if (!nameWithStereotypes.equals(""))
            simpleRelationship.setName(nameWithStereotypes);

    }


    //without name
    private void createExtendsRelation(DiagramManager diagramManager, ElementInfo elementInfoFromElement, ElementInfo elementInfoToElement, IDiagramUIModel diagram) {
        IExtend extendsModel = IModelElementFactory.instance().createExtend();

        //set to as from, from as to! in ui model too
        extendsModel.setFrom(elementInfoToElement.getShapeUIModel().getModelElement());
        extendsModel.setTo(elementInfoFromElement.getShapeUIModel().getModelElement());

        IExtendUIModel uiModel = (IExtendUIModel) diagramManager.createConnector(diagram, extendsModel, elementInfoToElement.getShapeUIModel(), elementInfoFromElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);

        IExtensionPoint extensionPoint = IModelElementFactory.instance().createExtensionPoint();
        extensionPoint.setName(elementInfoFromElement.getShapeUIModel().getModelElement().getName());
        extendsModel.setExtensionPoint(extensionPoint);
    }

    //without name
    private void createIncludeRelation(DiagramManager diagramManager, ElementInfo elementInfoFromElement, ElementInfo elementInfoToElement, IDiagramUIModel diagram) {
        IInclude extendsModel = IModelElementFactory.instance().createInclude();

        extendsModel.setFrom(elementInfoFromElement.getShapeUIModel().getModelElement());
        extendsModel.setTo(elementInfoToElement.getShapeUIModel().getModelElement());

        IIncludeUIModel uiModel = (IIncludeUIModel) diagramManager.createConnector(diagram, extendsModel, elementInfoFromElement.getShapeUIModel(), elementInfoToElement.getShapeUIModel(), null);
        uiModel.setRequestResetCaption(true);
    }


}
