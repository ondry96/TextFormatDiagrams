package pluginmain.imports.diagramimporters;

import antlr.visitors.IDiagramType;
import com.vp.plugin.ViewManager;
import pluginmain.enums.TextFormat;

public abstract class DiagramImporter {

    public abstract IDiagramType importToInnerStructure(ViewManager viewManager, TextFormat chosenFormat, String chosenFilePath, String diagramName);
}
