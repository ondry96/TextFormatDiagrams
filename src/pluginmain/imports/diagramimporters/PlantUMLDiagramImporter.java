package pluginmain.imports.diagramimporters;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.syntaxerrorhandling.SyntaxErrorListener;
import antlr.visitors.PlantUMLBaseDiagramVisitor;
import antlr.visitors.PlantUMLDiagramTypeVisitor;
import com.vp.plugin.ViewManager;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import pluginmain.enums.TextFormat;

import java.io.IOException;

import static pluginmain.utils.UserInputHandler.showMessageToUser;

public class PlantUMLDiagramImporter extends DiagramImporter {

    @Override
    public PlantUMLDiagramTypeVisitor importToInnerStructure(ViewManager viewManager, TextFormat chosenFormat, String chosenFilePath, String diagramName) {
        CharStream charStream;
        try {
            charStream = CharStreams.fromFileName(chosenFilePath);
        } catch (IOException e) {
            showMessageToUser(viewManager, "Selected file not found or it cannot be read.", "File not found or cannot be read");
            return null;
        }

        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        SyntaxErrorListener listener = new SyntaxErrorListener();
        parser.addErrorListener(listener);

        ParseTree parseTree = parser.uml();
        if (listener.getSyntaxErrors().size() != 0) {
            showMessageToUser(viewManager, "Following syntax errors occurred in grammar:\n" + listener.toString(), "Syntax errors");
            return null;
        }

        PlantUMLDiagramTypeVisitor findDiagramTypeVisitor;
        if (diagramName != null)
            findDiagramTypeVisitor = new PlantUMLDiagramTypeVisitor(diagramName);
        else
            findDiagramTypeVisitor = new PlantUMLDiagramTypeVisitor(chosenFilePath);

        findDiagramTypeVisitor.visit(parseTree);

        PlantUMLBaseDiagramVisitor pumlVisitor = findDiagramTypeVisitor.getVisitor();

        if (pumlVisitor == null) {
            showMessageToUser(viewManager, "This diagram is not supported yet", "Not supported diagram");
            return null;
        }
        pumlVisitor.visit(parseTree);

        return findDiagramTypeVisitor;
    }
}
