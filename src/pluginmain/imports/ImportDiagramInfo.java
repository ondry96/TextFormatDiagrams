package pluginmain.imports;

import com.vp.plugin.diagram.LayoutOption$Orientation;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.relationships.ImportAssociationElement;

import java.util.ArrayList;
import java.util.List;

public class ImportDiagramInfo {

    private static ImportDiagramInfo instance = new ImportDiagramInfo();
    private Element lastSavedElementExceptPackageAndRelation = null;
    private boolean lastSavedElementHasAlias = false;
    private int notesWithoutAliasCount = 0;
    private int groupCasesAliasCount = 0;
    private List<String> participantsCreatedByMessage = new ArrayList<>();

    private List<String> skinparamOptions = new ArrayList<>();
    private LayoutOption$Orientation orientation;

    private ImportDiagramInfo() {
    }

    public static ImportDiagramInfo getInstance() {
        return instance;
    }

    public Element getLastSavedElementExceptPackageAndRelation() {
        return lastSavedElementExceptPackageAndRelation;
    }

    public void setLastSavedElement(Element lastSavedElement, boolean lastSavedElementHasAlias) {
        if (lastSavedElement instanceof ImportAssociationElement || lastSavedElement instanceof ChildrenBasedElement) {
            return;
        }


        this.lastSavedElementExceptPackageAndRelation = lastSavedElement;
        this.lastSavedElementHasAlias = lastSavedElementHasAlias;
    }

    public List<String> getParticipantsCreatedByMessage() {
        return participantsCreatedByMessage;
    }

    public boolean isLastSavedElementHasAlias() {
        return lastSavedElementHasAlias;
    }

    public int getNextNoteWithoutAliasNumber() {
        return notesWithoutAliasCount++;
    }

    public int getNextGroupCaseAliasNumber() {
        return groupCasesAliasCount++;
    }

    public List<String> getSkinparamOptions() {
        return skinparamOptions;
    }

    public void setSkinparamOptions(List<String> skinparamOptions) {
        this.skinparamOptions = skinparamOptions;
    }

    public LayoutOption$Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(LayoutOption$Orientation orientation) {
        this.orientation = orientation;
    }

    public void reset() {
        lastSavedElementExceptPackageAndRelation = null;
        lastSavedElementHasAlias = false;
        notesWithoutAliasCount = 0;
        groupCasesAliasCount = 0;
        skinparamOptions = new ArrayList<>();
        orientation = null;
        participantsCreatedByMessage = new ArrayList<>();
    }
}
