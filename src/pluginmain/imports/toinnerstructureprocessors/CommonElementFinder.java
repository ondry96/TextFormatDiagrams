package pluginmain.imports.toinnerstructureprocessors;

import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.exceptions.compiler.CorrectNoteFoundException;
import pluginmain.exceptions.compiler.CorrectPackageFoundException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.PackageNotFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CommonElementFinder {


    public static NoteElement findNoteByAlias(List<Element> elements, String noteAlias) {
        for (Element element : elements) {
            if (element instanceof NoteElement) {
                NoteElement noteElement = (NoteElement) element;
                if (noteElement.getAlias() != null && noteElement.getAlias().equals(noteAlias))
                    return (NoteElement) element;
            }
        }

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof ChildrenBasedElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ChildrenBasedElement) elem));
        //find in subpackages
        try {
            findNoteElementInPackageList(noteAlias, outputPackageElements);
        } catch (CorrectNoteFoundException e) {
            return e.getParentPackageElement().getNotes().get(e.getIndexOfNote());
        }
        return null;
    }

    private static NoteElement findNoteElementInPackageList(String noteAlias, ArrayList<ChildrenBasedElement> outputPackageElements) throws CorrectNoteFoundException {
        for (int i = 0; i < outputPackageElements.size(); i++) {
            ChildrenBasedElement packageElement = outputPackageElements.get(i);
            for (int j = 0; j < packageElement.getNotes().size(); j++) {
                NoteElement noteElement = packageElement.getNotes().get(j);
                if (noteElement.getAlias() != null && noteElement.getAlias().equals(noteAlias))
                    throw new CorrectNoteFoundException(j, packageElement);
            }
            findNoteElementInPackageList(noteAlias, packageElement.getPackages());
        }
        return null;
    }


    public static void removeNoteByAlias(List<Element> elements, String noteAlias) {
        int elementIndexToRemove = -1;

        for (int i = 0; i < elements.size(); i++) {
            Element element = elements.get(i);
            if (element instanceof NoteElement) {
                NoteElement noteElement = (NoteElement) element;
                if (noteElement.getAlias() != null && noteElement.getAlias().equals(noteAlias))
                    elementIndexToRemove = i;
            }
        }
        if (elementIndexToRemove != -1)
            elements.remove(elementIndexToRemove);

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof ChildrenBasedElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ChildrenBasedElement) elem));
        //find in subpackages
        try {
            findNoteElementInPackageList(noteAlias, outputPackageElements);
        } catch (CorrectNoteFoundException e) {
            e.getParentPackageElement().getNotes().remove(e.getIndexOfNote());
        }
    }

    /**
     * Method returns Class Package Element from any elements list structure (even in subpackages).
     *
     * @throws PackageNotFoundException is thrown if this package is not in list
     */
    public static ChildrenBasedElement findPackageByNameOrAlias(List<Element> elements, String packageToFind, String alias) throws PackageNotFoundException, MoreElementsFoundException {
        List<Element> onlyPackageElements = elements.stream()
                .filter(elem -> elem instanceof ChildrenBasedElement).collect(Collectors.toList());
        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ChildrenBasedElement) elem));
        List<ChildrenBasedElement> foundPackagesWithThisNameOrAlias = outputPackageElements.stream().filter(
                packageElement ->
                        (packageElement.getName().equals(packageToFind) && packageElement.getAlias() == null) ||
                                (packageElement.getAlias() != null && packageElement.getAlias().equals(alias))
        ).collect(Collectors.toList());
        if (foundPackagesWithThisNameOrAlias.isEmpty()) {
            ChildrenBasedElement foundPackage = null;
            try {
                foundPackage = findPackageByNameInSubpackages(outputPackageElements, packageToFind, alias);
            } catch (CorrectPackageFoundException e) {
                return e.getPackageElement();
            }
            if (foundPackage != null)
                return foundPackage;
        } else {
            if (foundPackagesWithThisNameOrAlias.size() > 1)
                throw new MoreElementsFoundException("More packages with same name or alias were found");
            else
                return foundPackagesWithThisNameOrAlias.get(0);
        }
        throw new PackageNotFoundException("Package was not found");
    }

    public static ChildrenBasedElement findPackageByNameInSubpackages(ArrayList<ChildrenBasedElement> elements, String packageToFind, String alias) throws MoreElementsFoundException, CorrectPackageFoundException {
        for (ChildrenBasedElement packageElement : elements) {
            List<ChildrenBasedElement> onlyPackagesWithSeekName = packageElement.getPackages()
                    .stream()
                    .filter(pack ->
                            (pack.getName().equals(packageToFind) && pack.getAlias() == null) ||
                                    (pack.getAlias() != null && pack.getAlias().equals(alias)
                                    ))
                    .collect(Collectors.toList());
            if (onlyPackagesWithSeekName.isEmpty())
                findPackageByNameInSubpackages(packageElement.getPackages(), packageToFind, alias);
            else if (onlyPackagesWithSeekName.size() > 1)
                throw new MoreElementsFoundException();
            else
                throw new CorrectPackageFoundException(onlyPackagesWithSeekName.get(0));
        }
        return null;
    }
}
