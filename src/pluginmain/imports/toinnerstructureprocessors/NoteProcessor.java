package pluginmain.imports.toinnerstructureprocessors;

import com.vp.plugin.DiagramManager;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.IHasAlias;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationElement;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.exceptions.UnsupportedDiagramException;
import pluginmain.exceptions.compiler.AlreadyInStructureException;
import pluginmain.exceptions.compiler.CannotAttachNoteException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.PackageNotFoundException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.utils.StringModifier;

import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findNoteByAlias;
import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findPackageByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processIdentifierWithSpace;
import static pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassDiagramElementFinder.findClassByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.findActorByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.findUseCaseByNameOrAlias;

public class NoteProcessor {


    public void processNote(ParserRuleContext ctx, List<Element> elements, String currentPackageName, String diagramType) throws PackageNotFoundException, MoreElementsFoundException, CannotAttachNoteException, AlreadyInStructureException, UnsupportedDiagramException {
        NoteElement newNote = new NoteElement();
        int i = 1;
        String firstChild = ctx.getChild(i++).getText();
        if (firstChild.startsWith("\"") && firstChild.endsWith("\"")) { //note "description" as Alias
            processBasicNote(ctx, elements, currentPackageName, newNote, i, firstChild);
        } else {
            if (ctx.getChild(2).getText().equals("of")) {
                // note direction of ClassNameOrAlias color? : (: description) | multiple line
                processNoteWithDirectionAndObject(ctx, elements, currentPackageName, newNote, diagramType);
            } else if (firstChild.startsWith("as")) {
                // note as alias ... end note
                processNoteWithEnd(ctx, elements, currentPackageName, newNote, i, firstChild);
            } else {
                // note direction color? (: description) | multiple line
                processNoteWithDirectionOnly(ctx, elements, currentPackageName, newNote, diagramType);
            }
        }
    }

    // note as alias ... end note
    private void processNoteWithEnd(ParserRuleContext ctx, List<Element> elements, String currentPackageName, NoteElement newNote, int i, String firstChild) throws PackageNotFoundException, MoreElementsFoundException, AlreadyInStructureException {
        newNote.setAlias(firstChild.substring(2));
        if (ctx.getChild(i) != null && ctx.getChild(i).getText().startsWith("#"))
            newNote.setBackgroundColor(ctx.getChild(i++).getText());
        NoteElement alreadyExistingNote = findNoteByAlias(elements, newNote.getAlias());
        if (alreadyExistingNote != null)
            throw new AlreadyInStructureException("Note with alias " + alreadyExistingNote.getAlias() + " already exists");

        processMultipleLineNoteEnd(ctx, newNote, i);

        addNoteToStructure(elements, currentPackageName, newNote);
    }

    // note direction color? (: description) | multiple line
    private void processNoteWithDirectionOnly(ParserRuleContext ctx, List<Element> elements, String currentPackageName, NoteElement newNote, String diagramType) throws CannotAttachNoteException, PackageNotFoundException, MoreElementsFoundException, UnsupportedDiagramException {
        int hasColor = 0;
        if (ctx.getChild(2).getText().startsWith("#")) {
            newNote.setBackgroundColor(ctx.getChild(2).getText());
            hasColor = 1;
        }
        if (ctx.getChild(2 + hasColor).getText().equals(":")) {
            processOneLineNoteEnd(ctx, newNote, 3 + hasColor);
        } else {
            processMultipleLineNoteEnd(ctx, newNote, 2 + hasColor);
        }

        setGeneratedAliasToNote(elements, newNote, diagramType);

        ImportAssociationElement relationElement = new ImportAssociationElement();
        relationElement.setMiddleDotted(true);
        relationElement.setFromName(newNote.getAlias());
        Element lastSavedElement = ImportDiagramInfo.getInstance().getLastSavedElementExceptPackageAndRelation();
        if (lastSavedElement == null)
            throw new CannotAttachNoteException("No elements to attach note to, when processing note " + newNote.getAlias());

        if (ImportDiagramInfo.getInstance().isLastSavedElementHasAlias())
            relationElement.setToName(((IHasAlias) lastSavedElement).getAlias());
        else
            relationElement.setToName(lastSavedElement.getName());

        elements.add(relationElement);


        addNoteToStructure(elements, currentPackageName, newNote);
    }

    private void processMultipleLineNoteEnd(ParserRuleContext tree, NoteElement newNote, int i) {
        StringBuilder descriptionBuilder = new StringBuilder();
        if (tree.getChild(i).getText().equals("{")) {
            while (!(tree.getChild(i).getText().equals("}"))) {
                if (tree.getChild(i).getText().equals(System.lineSeparator())) {
                    i++;
                    continue;
                }
                if (tree.getChild(i + 1).getText().equals("}")) {
                    descriptionBuilder.append(processIdentifierWithSpaceOffsetOneFromEnd(tree.getChild(i++)));
                    break;
                } else {
                    descriptionBuilder.append(processIdentifierWithSpace(tree.getChild(i++)));
                }
            }
        } else {
            while (!(tree.getChild(i).getText().equals("end") && tree.getChild(i + 1).getText().equals("note"))) {
                if (tree.getChild(i).getText().equals(System.lineSeparator())) {
                    i++;
                    continue;
                }
                if (tree.getChild(i + 1).getText().equals("end") && tree.getChild(i + 2).getText().equals("note")) {
                    descriptionBuilder.append(processIdentifierWithSpaceOffsetOneFromEnd(tree.getChild(i++)));
                    break;
                } else {
                    descriptionBuilder.append(processIdentifierWithSpace(tree.getChild(i++)));
                }
            }
        }
        newNote.setDescription(descriptionBuilder.toString().replace("\r\n", System.lineSeparator()));
    }

    private void processOneLineNoteEnd(ParserRuleContext tree, NoteElement newNote, int i) {
        String description = tree.getChild(i).getText();
        if (!(description.startsWith("\"") && description.endsWith("\"")))
            description = processIdentifierWithSpace(tree.getChild(i));
        newNote.setDescription(description.replace("\r\n", System.lineSeparator()));
    }

    // note direction of ElementNameOrAlias color? : (: description) | multiple line
    private void processNoteWithDirectionAndObject(ParserRuleContext ctx, List<Element> elements, String currentPackageName, NoteElement newNote, String diagramType) throws PackageNotFoundException, MoreElementsFoundException, UnsupportedDiagramException {
        String nameOrAliasToFound = ctx.getChild(3).getText();
        int hasColor = 0;
        if (ctx.getChild(4).getText().startsWith("#")) {
            newNote.setBackgroundColor(ctx.getChild(4).getText());
            hasColor = 1;
        }

        if (ctx.getChild(4 + hasColor).getText().equals(":")) {
            processOneLineNoteEnd(ctx, newNote, 5 + hasColor);
        } else {
            processMultipleLineNoteEnd(ctx, newNote, 4 + hasColor);
        }

        setGeneratedAliasToNote(elements, newNote, diagramType);

        ChildrenBasedElement currentPackage = null;
        try {
            currentPackage = findPackageByNameOrAlias(elements, currentPackageName, currentPackageName);
        } catch (PackageNotFoundException ignored) {
        }

        if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
            //create class if needed
            nameOrAliasToFound = StringModifier.removeQuotesOfStartAndEnd(nameOrAliasToFound);
            ClassElement foundedClass = findClassByNameOrAlias(elements, nameOrAliasToFound);
            if (foundedClass == null) {
                try {
                    ChildrenBasedElement foundedPackage = findPackageByNameOrAlias(elements, nameOrAliasToFound, nameOrAliasToFound);
                } catch (PackageNotFoundException e) {
                    NoteElement foundedNote = findNoteByAlias(elements, nameOrAliasToFound);
                    if (foundedNote == null) {
                        ClassElement newClass = new ClassElement();
                        newClass.setName(nameOrAliasToFound);
                        newClass.setFullName(nameOrAliasToFound);
                        ImportDiagramInfo.getInstance().setLastSavedElement(newClass, newClass.getAlias() != null);
                        if (currentPackage == null) {
                            elements.add(newClass);
                        } else {
                            ((ClassPackageElement) currentPackage).getClasses().add(newClass);
                        }
                    }
                }
            }
        } else if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
            //create actor if needed

            if (nameOrAliasToFound.startsWith("(") && nameOrAliasToFound.endsWith(")")) {
                nameOrAliasToFound = StringModifier.removeBracketsOfStartAndEnd(nameOrAliasToFound);
            } else if (nameOrAliasToFound.startsWith(":") && nameOrAliasToFound.endsWith(":")) {
                nameOrAliasToFound = StringModifier.removeColonsOfStartAndEnd(nameOrAliasToFound);
            }


            ActorElement foundedActor = findActorByNameOrAlias(elements, nameOrAliasToFound);
            if (foundedActor == null) {
                UseCaseElement foundedUseCase = findUseCaseByNameOrAlias(elements, nameOrAliasToFound);
                if (foundedUseCase == null) {
                    try {
                        ChildrenBasedElement foundedPackage = findPackageByNameOrAlias(elements, nameOrAliasToFound, nameOrAliasToFound);
                    } catch (PackageNotFoundException e) {
                        NoteElement foundedNote = findNoteByAlias(elements, nameOrAliasToFound);
                        if (foundedNote == null) {
                            ActorElement newActor = new ActorElement();
                            newActor.setName(nameOrAliasToFound);
                            ImportDiagramInfo.getInstance().setLastSavedElement(newActor, newActor.getAlias() != null);
                            if (currentPackage == null) {
                                elements.add(newActor);
                            } else {
                                ((UseCasePackageElement) currentPackage).getActors().add(newActor);
                            }

                        }
                    }
                }
            }
        } else {
            throw new UnsupportedDiagramException();
        }

        addNoteToStructure(elements, currentPackageName, newNote);

        ImportAssociationElement relationElement = new ImportAssociationElement();
        relationElement.setMiddleDotted(true);
        relationElement.setFromName(newNote.getAlias());
        relationElement.setToName(nameOrAliasToFound);
        elements.add(relationElement);
    }

    private void setGeneratedAliasToNote(List<Element> elements, NoteElement newNote, String diagramType) throws MoreElementsFoundException, UnsupportedDiagramException {
        newNote.setAlias("N" + ImportDiagramInfo.getInstance().getNextNoteWithoutAliasNumber());
        NoteElement foundNote = findNoteByAlias(elements, newNote.getAlias());
        ChildrenBasedElement foundPackage;
        try {
            foundPackage = findPackageByNameOrAlias(elements, newNote.getAlias(), newNote.getAlias());
        } catch (PackageNotFoundException e) {
            foundPackage = null;
        }

        if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
            ClassElement foundClass = findClassByNameOrAlias(elements, newNote.getAlias());

            while (foundNote != null || foundClass != null || foundPackage != null) {
                newNote.setAlias("N" + ImportDiagramInfo.getInstance().getNextNoteWithoutAliasNumber());
                foundNote = findNoteByAlias(elements, newNote.getAlias());
                foundClass = findClassByNameOrAlias(elements, newNote.getAlias());
                try {
                    foundPackage = findPackageByNameOrAlias(elements, newNote.getAlias(), newNote.getAlias());
                } catch (PackageNotFoundException e) {
                    foundPackage = null;
                }
            }
        } else if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
            ActorElement actorElement = findActorByNameOrAlias(elements, newNote.getAlias());
            UseCaseElement foundedUseCase = findUseCaseByNameOrAlias(elements, newNote.getAlias());

            while (foundNote != null || foundedUseCase != null || actorElement != null || foundPackage != null) {
                newNote.setAlias("N" + ImportDiagramInfo.getInstance().getNextNoteWithoutAliasNumber());
                foundNote = findNoteByAlias(elements, newNote.getAlias());
                actorElement = findActorByNameOrAlias(elements, newNote.getAlias());
                foundedUseCase = findUseCaseByNameOrAlias(elements, newNote.getAlias());
                try {
                    foundPackage = findPackageByNameOrAlias(elements, newNote.getAlias(), newNote.getAlias());
                } catch (PackageNotFoundException e) {
                    foundPackage = null;
                }
            }
        } else {
            throw new UnsupportedDiagramException();
        }
    }

    //note "description description" as Alias
    private void processBasicNote(ParserRuleContext ctx, List<Element> elements, String currentPackageName, NoteElement newNote, int i, String firstChild) throws PackageNotFoundException, MoreElementsFoundException, AlreadyInStructureException {
        newNote.setDescription(StringModifier.removeQuotesOfStartAndEnd(firstChild).replace("\r\n", System.lineSeparator()));
        newNote.setAlias(ctx.getChild(i++).getText().substring(2));
        NoteElement alreadyExistingNote = findNoteByAlias(elements, newNote.getAlias());
        if (alreadyExistingNote != null)
            throw new AlreadyInStructureException("Note with alias " + alreadyExistingNote.getAlias() + " already exists");


        addNoteToStructure(elements, currentPackageName, newNote);
        if (ctx.getChild(i) != null)
            newNote.setBackgroundColor(ctx.getChild(i).getText());
    }

    private ChildrenBasedElement addNoteToStructure(List<Element> elements, String currentPackage, NoteElement noteElement) throws PackageNotFoundException, MoreElementsFoundException {
        if (currentPackage == null) {
            ImportDiagramInfo.getInstance().setLastSavedElement(noteElement, noteElement.getAlias() != null);
            elements.add(noteElement);
            return null;
        } else {
            ChildrenBasedElement currentPackageElement = findPackageByNameOrAlias(elements, currentPackage, currentPackage);
            ImportDiagramInfo.getInstance().setLastSavedElement(noteElement, noteElement.getAlias() != null);
            currentPackageElement.getNotes().add(noteElement);
            return currentPackageElement;
        }
    }

    private String processIdentifierWithSpaceOffsetOneFromEnd(ParseTree child) {
        String textWithSpaces = "";
        for (int i = 0; i < child.getChildCount() - 1; i++) {
            textWithSpaces = textWithSpaces.concat(child.getChild(i).getText());
            if (i != child.getChildCount() - 2)
                textWithSpaces = textWithSpaces.concat(" ");
        }
        return textWithSpaces;
    }
}
