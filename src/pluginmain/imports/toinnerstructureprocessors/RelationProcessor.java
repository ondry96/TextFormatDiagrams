package pluginmain.imports.toinnerstructureprocessors;

import com.vp.plugin.DiagramManager;
import org.antlr.v4.runtime.ParserRuleContext;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationClass;
import pluginmain.elements.relationships.ImportAssociationElement;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.exceptions.UnsupportedDiagramException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.PackageNotFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.utils.StringModifier;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findNoteByAlias;
import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findPackageByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassDiagramElementFinder.findClassByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassProcessor.addClassToStructure;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.ActorProcessor.addActorToStructure;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.findActorByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.findUseCaseByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseProcessor.addUseCaseToStructure;

public class RelationProcessor {

    private ArrayList<ImportAssociationElement> createdAssociations = new ArrayList<>();

    public void processRelation(ParserRuleContext ctx, List<Element> elements, String currentPackageName, String diagramType) throws PackageNotFoundException, MoreElementsFoundException, SyntaxException, UnsupportedDiagramException {
        int i = 0;
        ImportAssociationElement relationElement = new ImportAssociationElement();
        String firstChild = ctx.getChild(i++).getText();
        if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
            if (firstChild.startsWith("(") && firstChild.endsWith(")") && firstChild.contains(",")) {
                processAssociationClass(ctx, elements, currentPackageName);
                return;
            }
        }

        if (firstChild.startsWith("\"") && firstChild.endsWith("\"")) {
            firstChild = StringModifier.removeQuotesOfStartAndEnd(firstChild);
        }

        relationElement.setFromName(processElementInRelation(firstChild, elements, currentPackageName, diagramType));
        String nextChild = ctx.getChild(i).getText();
        if (nextChild.startsWith("\"")) {
            relationElement.setFromMultiplicity(StringModifier.removeQuotesOfStartAndEnd(nextChild));
            i++;
            nextChild = ctx.getChild(i).getText();
        }

        int arrowLength = nextChild.length();
        relationElement.setMiddleDotted(nextChild.contains("."));

        processRelationAggregationCount(relationElement, nextChild, arrowLength);
        i++;

        nextChild = ctx.getChild(i).getText();
        if (ctx.getChildCount() - 1 != i && !ctx.getChild(i + 1).getText().startsWith(":")) {
            relationElement.setToMultiplicity(StringModifier.removeQuotesOfStartAndEnd(nextChild));
            nextChild = ctx.getChild(++i).getText();
        }

        if (nextChild.startsWith("\"") && nextChild.endsWith("\"")) {
            nextChild = StringModifier.removeQuotesOfStartAndEnd(nextChild);
        }

        relationElement.setToName(processElementInRelation(nextChild, elements, currentPackageName, diagramType));
        try {
            processRelationName(ctx, i, relationElement);
        } catch (NullPointerException e) {
        }

        elements.add(relationElement);

        if (!relationElement.isMiddleDotted())
            createdAssociations.add(relationElement);
    }

    private void processAssociationClass(ParserRuleContext ctx, List<Element> elements, String currentPackageName) throws SyntaxException, MoreElementsFoundException {
        String wholeLeftSide = ctx.getChild(0).getText();
        String[] splitted = wholeLeftSide.split(",");
        if (splitted.length != 2)
            throw new SyntaxException("Association class has incorrect syntax");
        String leftClassNameOfLeftSide = StringModifier.removeQuotesOfStartAndEnd(splitted[0].substring(1).trim());
        String rightClassNameOfLeftSide = StringModifier.removeQuotesOfStartAndEnd(splitted[1].substring(0, splitted[1].length() - 1).trim());
        ClassElement leftClassOfLeftSide = findClassByNameOrAlias(elements, leftClassNameOfLeftSide);
        ClassElement rightClassOfLeftSide = findClassByNameOrAlias(elements, rightClassNameOfLeftSide);

        int i = 1;

        if (leftClassOfLeftSide == null || rightClassOfLeftSide == null)
            throw new SyntaxException("Association class has incorrect syntax, classes must be defined before creation of association class");

        boolean hasLeftMultiplicity = ctx.getChild(i).getText().startsWith("\"");
        if (hasLeftMultiplicity)
            i++;

        String arrow = ctx.getChild(i++).getText();
        if (!arrow.startsWith(".") || !arrow.endsWith("."))
            throw new SyntaxException("Association class has incorrect syntax, arrow has to contain only dots");

        boolean hasRightMultiplicity = ctx.getChild(i).getText().startsWith("\"") &&
                ctx.getChildCount() - 1 != i && !ctx.getChild(i + 1).getText().startsWith(":");
        if (hasRightMultiplicity)
            i++;

        String classRightSide = StringModifier.removeQuotesOfStartAndEnd(ctx.getChild(i++).getText());
        ClassElement rightClass = findClassByNameOrAlias(elements, classRightSide);
        if (rightClass == null) {
            ClassElement classElement = new ClassElement();
            classElement.setName(classRightSide);
            elements.add(classElement);
        }

        List<ImportAssociationElement> associationsWithClassesOfLeftSide = createdAssociations.stream().filter(association ->
                (association.getFromName().equals(leftClassNameOfLeftSide) && association.getToName().equals(rightClassNameOfLeftSide)) ||
                        (association.getFromName().equals(rightClassNameOfLeftSide) && association.getToName().equals(leftClassNameOfLeftSide))
        ).collect(Collectors.toList());

        if (associationsWithClassesOfLeftSide.size() == 0) {
            ImportAssociationElement newAssociation = new ImportAssociationElement();
            newAssociation.setMiddleDotted(false);
            newAssociation.setFromName(leftClassNameOfLeftSide);
            newAssociation.setToName(rightClassNameOfLeftSide);

            elements.add(newAssociation);
            createdAssociations.add(newAssociation);

            ImportAssociationClass associationClass = new ImportAssociationClass(newAssociation, classRightSide, null);
            elements.add(associationClass);

        } else {
            ImportAssociationElement lastAssociationInList = associationsWithClassesOfLeftSide.get(associationsWithClassesOfLeftSide.size() - 1);
            ImportAssociationClass associationClass = new ImportAssociationClass(lastAssociationInList, classRightSide, null);
            elements.add(associationClass);
        }
    }

    private void processRelationAggregationCount(ImportAssociationElement relationElement, String nextChild, int arrowLength) {
        if (nextChild.charAt(0) != '.' && nextChild.charAt(0) != '-') {
            if (nextChild.charAt(1) == '|')
                relationElement.setFromAggregationKind(nextChild.substring(0, 2));   //  <|
            else
                relationElement.setFromAggregationKind(nextChild.substring(0, 1));   // others
        }

        if (nextChild.charAt(nextChild.length() - 1) != '.' && nextChild.charAt(nextChild.length() - 1) != '-') {
            if (nextChild.charAt(arrowLength - 2) == '|')
                relationElement.setToAggregationKind(nextChild.substring(arrowLength - 2, arrowLength));    //  |>
            else
                relationElement.setToAggregationKind(nextChild.substring(arrowLength - 1, arrowLength));    //  others;
        }
    }

    private void processRelationName(ParserRuleContext ctx, int i, ImportAssociationElement relationElement) {
        String nextChild;
        if (ctx.getChild(i + 2).getChildCount() == 0) {
            nextChild = ctx.getChild(i + 2).getText();
            relationElement.setName(StringModifier.removeQuotesOfStartAndEnd(nextChild));
        } else {
            StringBuilder name = new StringBuilder();
            for (int j = 0; j < ctx.getChild(i + 2).getChildCount(); j++) {
                name.append(ctx.getChild(i + 2).getChild(j).getText());
                if (j != ctx.getChild(i + 2).getChildCount() - 1)
                    name.append(" ");
            }
            relationElement.setName(StringModifier.removeQuotesOfStartAndEnd(name.toString()));

        }
    }

    private String processElementInRelation(String elementNameOrAlias, List<Element> elements, String currentPackageName, String diagramType) throws PackageNotFoundException, MoreElementsFoundException, UnsupportedDiagramException {
        if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
            ClassElement foundedClass = findClassByNameOrAlias(elements, elementNameOrAlias);
            if (foundedClass == null) {
                NoteElement foundedNote = findNoteByAlias(elements, elementNameOrAlias);
                if (foundedNote != null)
                    return elementNameOrAlias;
                try {
                    findPackageByNameOrAlias(elements, elementNameOrAlias, elementNameOrAlias);
                } catch (PackageNotFoundException e) {
                    //class, note or package with this name or alias does not exists, create class
                    foundedClass = new ClassElement(elementNameOrAlias);
                    addClassToStructure(elements, currentPackageName, foundedClass);
                }
            }
        } else if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
            boolean shouldCreateUseCase = false;

            if (elementNameOrAlias.startsWith("(") && elementNameOrAlias.endsWith(")")) {
                elementNameOrAlias = StringModifier.removeBracketsOfStartAndEnd(elementNameOrAlias);
                shouldCreateUseCase = true;
            } else if (elementNameOrAlias.startsWith(":") && elementNameOrAlias.endsWith(":")) {
                elementNameOrAlias = StringModifier.removeColonsOfStartAndEnd(elementNameOrAlias);
            }

            Element foundedElement = findActorByNameOrAlias(elements, elementNameOrAlias);
            if (foundedElement == null) {
                UseCaseElement foundedUseCase = findUseCaseByNameOrAlias(elements, elementNameOrAlias);
                if (foundedUseCase == null) {
                    NoteElement foundedNote = findNoteByAlias(elements, elementNameOrAlias);
                    if (foundedNote != null)
                        return elementNameOrAlias;
                    try {
                        findPackageByNameOrAlias(elements, elementNameOrAlias, elementNameOrAlias);
                    } catch (PackageNotFoundException e) {
                        //actor, usecase, note or package with this name or alias does not exists, create actor or usecase
                        if (shouldCreateUseCase) {
                            foundedElement = new UseCaseElement(elementNameOrAlias);
                            addUseCaseToStructure(elements, currentPackageName, (UseCaseElement) foundedElement);
                        } else {
                            foundedElement = new ActorElement(elementNameOrAlias);
                            addActorToStructure(elements, currentPackageName, (ActorElement) foundedElement);
                        }
                    }
                }
            }
        } else {
            throw new UnsupportedDiagramException();
        }
        return elementNameOrAlias;
    }


}
