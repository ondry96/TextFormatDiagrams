package pluginmain.imports.toinnerstructureprocessors.usecasediagram;

import antlr.plantumlgrammar.PlantUMLParser;
import pluginmain.elements.common.Element;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.PackageNotFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.utils.StringModifier;

import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findPackageByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processColor;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processStereotypes;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.findUseCaseByNameOrAlias;

public class UseCaseProcessor {

    public void processUseCase(PlantUMLParser.UseCaseRuleContext ctx, List<Element> elements, String currentPackage) throws MoreElementsFoundException, SyntaxException, PackageNotFoundException {
        UseCaseElement useCaseElement = new UseCaseElement();
        boolean isUseCaseAlreadyInStructure = false;

        int i = 0;
        if (ctx.getChild(i).getText().equals("usecase"))
            i++;

        String firstNameAsText = ctx.getChild(i++).getText();
        String secondNameAsText = null;
        if (ctx.getChildCount() > i && ctx.getChild(i).getText().startsWith("as")) {
            secondNameAsText = ctx.getChild(i).getText().substring(2);
            i++;
        }

        setCorrectNameAndAlias(useCaseElement, firstNameAsText, secondNameAsText);
        String correctName = useCaseElement.getName();
        String correctAlias = useCaseElement.getAlias();

        UseCaseElement foundedUseCase = null;
        if (correctAlias == null)
            foundedUseCase = findUseCaseByNameOrAlias(elements, correctName);
        else
            foundedUseCase = findUseCaseByNameOrAlias(elements, correctAlias);

        if (foundedUseCase != null) {
            isUseCaseAlreadyInStructure = true;
            foundedUseCase.setName(useCaseElement.getName());
            useCaseElement = foundedUseCase;
        }

        try {
            i = processStereotypes(ctx, useCaseElement, i, isUseCaseAlreadyInStructure);
            useCaseElement.setBackgroundColor(null);
            processColor(ctx, useCaseElement, i);
        } catch (NullPointerException ignored) {
        }
        if (!isUseCaseAlreadyInStructure)
            addUseCaseToStructure(elements, currentPackage, useCaseElement);
    }

    public static void addUseCaseToStructure(List<Element> elements, String currentPackage, UseCaseElement useCaseElement) throws MoreElementsFoundException, PackageNotFoundException {
        if (currentPackage == null) {
            elements.add(useCaseElement);
        } else {
            UseCasePackageElement foundPackage = (UseCasePackageElement) findPackageByNameOrAlias(elements, currentPackage, currentPackage);
            foundPackage.getUseCases().add(useCaseElement);
        }

        ImportDiagramInfo.getInstance().setLastSavedElement(useCaseElement, useCaseElement.getAlias() != null);
    }

    private void setCorrectNameAndAlias(UseCaseElement useCaseElement, String firstName, String secondName) throws SyntaxException {
        boolean firstNameHasBrackets = false;
        boolean firstNameHasQuotes = false;
        boolean secondNameHasBrackets = false;
        boolean secondNameHasQuotes = false;

        if (firstName.startsWith("(") && firstName.endsWith(")")) {
            firstName = StringModifier.removeBracketsOfStartAndEnd(firstName);
            firstNameHasBrackets = true;
        } else if (firstName.startsWith("\"") && firstName.endsWith("\"")) {
            firstName = StringModifier.removeQuotesOfStartAndEnd(firstName);
            firstNameHasQuotes = true;
        }

        if (secondName == null) {
            useCaseElement.setName(firstName);
            return;
        }

        if (secondName.startsWith("\"") && secondName.endsWith("\"")) {
            secondName = StringModifier.removeQuotesOfStartAndEnd(secondName);
            secondNameHasQuotes = true;
        } else if (secondName.startsWith("(") && secondName.endsWith(")")) {
            secondName = StringModifier.removeBracketsOfStartAndEnd(secondName);
            secondNameHasBrackets = true;
        }


        if (firstNameHasQuotes && secondNameHasQuotes) {
            useCaseElement.setName(firstName + "\" as \"" + secondName);
            return;
        }

        if (secondNameHasQuotes) {
            useCaseElement.setName(secondName);
            useCaseElement.setAlias(firstName);
            return;
        }

        if (firstNameHasBrackets) {
            useCaseElement.setName(firstName);
            useCaseElement.setAlias(secondName);
            return;
        }

        if (firstNameHasQuotes && secondNameHasBrackets) {
            useCaseElement.setName(firstName);
            useCaseElement.setAlias(secondName);
            return;
        }

        if (secondNameHasBrackets) {
            useCaseElement.setName(secondName);
            useCaseElement.setAlias(firstName);
            return;
        }

        useCaseElement.setName(firstName);
        useCaseElement.setAlias(secondName);
    }

}
