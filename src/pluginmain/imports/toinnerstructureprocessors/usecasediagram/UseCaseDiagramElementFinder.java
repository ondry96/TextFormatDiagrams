package pluginmain.imports.toinnerstructureprocessors.usecasediagram;

import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.exceptions.compiler.CorrectElementFoundException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UseCaseDiagramElementFinder {

    public static UseCaseElement findUseCaseByNameOrAlias(List<Element> elements, String useCaseToFind) {
        for (Element element : elements) {
            if (element instanceof UseCaseElement) {
                UseCaseElement useCaseElement = (UseCaseElement) element;
                if ((useCaseElement.getName().equals(useCaseToFind) && useCaseElement.getAlias() == null) ||
                        (useCaseElement.getAlias() != null && useCaseElement.getAlias().equals(useCaseToFind)))
                    return (UseCaseElement) element;
            }
        }

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof UseCasePackageElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((UseCasePackageElement) elem));
        //find in subpackages
        try {
            findUseCaseElementInPackageList(useCaseToFind, outputPackageElements);
        } catch (CorrectElementFoundException e) {
            return (UseCaseElement) e.getElement();
        }
        return null;
    }

    private static void findUseCaseElementInPackageList(String useCaseToFind, ArrayList<ChildrenBasedElement> onlyPackageElements) throws CorrectElementFoundException {
        for (int i = 0; i < onlyPackageElements.size(); i++) {
            UseCasePackageElement packageUseCasePackageElement = (UseCasePackageElement) onlyPackageElements.get(i);
            for (UseCaseElement useCaseElement : packageUseCasePackageElement.getUseCases()) {
                if ((useCaseElement.getName().equals(useCaseToFind) && useCaseElement.getAlias() == null) ||
                        (useCaseElement.getAlias() != null && useCaseElement.getAlias().equals(useCaseToFind)))
                    throw new CorrectElementFoundException(useCaseElement, packageUseCasePackageElement);
            }
            findUseCaseElementInPackageList(useCaseToFind, packageUseCasePackageElement.getPackages());
        }
    }

    public static ActorElement findActorByNameOrAlias(List<Element> elements, String actorToFind) throws MoreElementsFoundException {
        for (Element element : elements) {
            if (element instanceof ActorElement) {
                ActorElement actorElement = (ActorElement) element;
                if ((actorElement.getName().equals(actorToFind) && actorElement.getAlias() == null) ||
                        (actorElement.getAlias() != null && actorElement.getAlias().equals(actorToFind)))
                    return (ActorElement) element;
            }
        }

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof UseCasePackageElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((UseCasePackageElement) elem));
        //find in subpackages
        try {
            findActorElementInPackageList(actorToFind, outputPackageElements);
        } catch (CorrectElementFoundException e) {
            return (ActorElement) e.getElement();
        }
        return null;
    }

    private static void findActorElementInPackageList(String actorToFind, ArrayList<ChildrenBasedElement> onlyPackageElements) throws CorrectElementFoundException {
        for (int i = 0; i < onlyPackageElements.size(); i++) {
            UseCasePackageElement packageUseCasePackageElement = (UseCasePackageElement) onlyPackageElements.get(i);
            for (ActorElement actorElement : packageUseCasePackageElement.getActors()) {
                if ((actorElement.getName().equals(actorToFind) && actorElement.getAlias() == null) ||
                        (actorElement.getAlias() != null && actorElement.getAlias().equals(actorToFind)))
                    throw new CorrectElementFoundException(actorElement, packageUseCasePackageElement);
            }
            findActorElementInPackageList(actorToFind, packageUseCasePackageElement.getPackages());
        }
    }

    public static void removeActorByNameOrAlias(List<Element> elements, String actorToFind) {
        Element elementToRemove = null;

        for (Element element : elements) {
            if (element instanceof ActorElement) {
                ActorElement actorElement = (ActorElement) element;
                if ((actorElement.getName().equals(actorToFind) && actorElement.getAlias() == null) ||
                        (actorElement.getAlias() != null && actorElement.getAlias().equals(actorToFind)))
                    elementToRemove = element;
            }
        }
        if (elementToRemove != null)
            elements.remove(elementToRemove);

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof ClassPackageElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ClassPackageElement) elem));
        //find in subpackages
        try {
            findActorElementInPackageList(actorToFind, outputPackageElements);
        } catch (CorrectElementFoundException e) {
            ((UseCasePackageElement) e.getParentPackageElement()).getActors().remove(e.getElement());
        }
    }

    public static void removeUseCaseByNameOrAlias(List<Element> elements, String useCaseToFind) {
        Element elementToRemove = null;

        for (Element element : elements) {
            if (element instanceof UseCaseElement) {
                UseCaseElement useCaseElement = (UseCaseElement) element;
                if ((useCaseElement.getName().equals(useCaseToFind) && useCaseElement.getAlias() == null) ||
                        (useCaseElement.getAlias() != null && useCaseElement.getAlias().equals(useCaseToFind)))
                    elementToRemove = element;
            }
        }
        if (elementToRemove != null)
            elements.remove(elementToRemove);

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof ClassPackageElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ClassPackageElement) elem));
        //find in subpackages
        try {
            findUseCaseElementInPackageList(useCaseToFind, outputPackageElements);
        } catch (CorrectElementFoundException e) {
            ((UseCasePackageElement) e.getParentPackageElement()).getUseCases().remove(e.getElement());
        }
    }
}
