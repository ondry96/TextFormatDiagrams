package pluginmain.imports.toinnerstructureprocessors.usecasediagram;

import antlr.plantumlgrammar.PlantUMLParser;
import pluginmain.elements.common.Element;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.PackageNotFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.utils.StringModifier;

import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findPackageByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processColor;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processStereotypes;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.findActorByNameOrAlias;

public class ActorProcessor {

    public void processActor(PlantUMLParser.ActorRuleContext ctx, List<Element> elements, String currentPackage) throws SyntaxException, MoreElementsFoundException, PackageNotFoundException {
        ActorElement actorElement = new ActorElement();
        boolean isActorAlreadyInStructure = false;

        int i = 0;
        if (ctx.getChild(i).getText().equals("actor"))
            i++;

        String firstNameAsText = ctx.getChild(i++).getText();
        String secondNameAsText = null;
        if (ctx.getChildCount() > i && ctx.getChild(i).getText().startsWith("as")) {
            secondNameAsText = ctx.getChild(i).getText().substring(2);
            i++;
        }

        setCorrectNameAndAlias(actorElement, firstNameAsText, secondNameAsText);
        String correctName = actorElement.getName();
        String correctAlias = actorElement.getAlias();

        ActorElement foundedActor = null;
        if (correctAlias == null)
            foundedActor = findActorByNameOrAlias(elements, correctName);
        else
            foundedActor = findActorByNameOrAlias(elements, correctAlias);

        if (foundedActor != null) {
            isActorAlreadyInStructure = true;
            foundedActor.setName(actorElement.getName());
            actorElement = foundedActor;
        }

        try {
            i = processStereotypes(ctx, actorElement, i, isActorAlreadyInStructure);
            actorElement.setBackgroundColor(null);
            processColor(ctx, actorElement, i);
        } catch (NullPointerException ignored) {
        }
        if (!isActorAlreadyInStructure)
            addActorToStructure(elements, currentPackage, actorElement);

    }

    public static void addActorToStructure(List<Element> elements, String currentPackage, ActorElement actorElement) throws PackageNotFoundException, MoreElementsFoundException {
        if (currentPackage == null) {
            elements.add(actorElement);
        } else {
            UseCasePackageElement foundPackage = (UseCasePackageElement) findPackageByNameOrAlias(elements, currentPackage, currentPackage);
            foundPackage.getActors().add(actorElement);
        }

        ImportDiagramInfo.getInstance().setLastSavedElement(actorElement, actorElement.getAlias() != null);
    }

    private void setCorrectNameAndAlias(ActorElement actorElement, String firstName, String secondName) throws SyntaxException {
        boolean firstNameHasColons = false;
        boolean firstNameHasQuotes = false;
        boolean secondNameHasColons = false;
        boolean secondNameHasQuotes = false;

        if (firstName.startsWith(":") && firstName.endsWith(":")) {
            firstName = StringModifier.removeColonsOfStartAndEnd(firstName);
            firstNameHasColons = true;
        } else if (firstName.startsWith("\"") && firstName.endsWith("\"")) {
            firstName = StringModifier.removeQuotesOfStartAndEnd(firstName);
            firstNameHasQuotes = true;
        }

        if (secondName == null) {
            actorElement.setName(firstName);
            return;
        }

        if (secondName.startsWith("\"") && secondName.endsWith("\"")) {
            secondName = StringModifier.removeQuotesOfStartAndEnd(secondName);
            secondNameHasQuotes = true;
        } else if (secondName.startsWith(":") && secondName.endsWith(":")) {
            secondName = StringModifier.removeColonsOfStartAndEnd(secondName);
            secondNameHasColons = true;
        }


        if (firstNameHasQuotes && secondNameHasQuotes) {
            actorElement.setName(firstName + "\" as \"" + secondName);
            return;
        }

        if (secondNameHasQuotes) {
            actorElement.setName(secondName);
            actorElement.setAlias(firstName);
            return;
        }

        if (firstNameHasColons) {
            actorElement.setName(firstName);
            actorElement.setAlias(secondName);
            return;
        }

        if (firstNameHasQuotes && secondNameHasColons) {
            actorElement.setName(firstName);
            actorElement.setAlias(secondName);
            return;
        }

        if (secondNameHasColons) {
            actorElement.setName(secondName);
            actorElement.setAlias(firstName);
            return;
        }

        actorElement.setName(firstName);
        actorElement.setAlias(secondName);
    }

}
