package pluginmain.imports.toinnerstructureprocessors;

import pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassProcessor;
import pluginmain.imports.toinnerstructureprocessors.sequencediagram.GroupProcessor;
import pluginmain.imports.toinnerstructureprocessors.sequencediagram.MessageProcessor;
import pluginmain.imports.toinnerstructureprocessors.sequencediagram.ParticipantProcessor;
import pluginmain.imports.toinnerstructureprocessors.sequencediagram.RefProcessor;
import pluginmain.imports.toinnerstructureprocessors.usecasediagram.ActorProcessor;
import pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseProcessor;

public class FacadeProcessor {
    public ClassProcessor classProcessor = new ClassProcessor();
    public PackageProcessor packageProcessor = new PackageProcessor();

    public RelationProcessor relationProcessor = new RelationProcessor();
    public NoteProcessor noteProcessor = new NoteProcessor();

    public UseCaseProcessor useCaseProcessor = new UseCaseProcessor();
    public ActorProcessor actorProcessor = new ActorProcessor();

    public ParticipantProcessor participantProcessor = new ParticipantProcessor();
    public RefProcessor refProcessor = new RefProcessor();
    public GroupProcessor groupProcessor = new GroupProcessor();
    public MessageProcessor messageProcessor = new MessageProcessor();

}
