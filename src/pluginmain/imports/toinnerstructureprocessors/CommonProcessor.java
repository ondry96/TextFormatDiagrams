package pluginmain.imports.toinnerstructureprocessors;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.HasStereotypeElement;

public class CommonProcessor {

    public static String processIdentifierWithSpace(ParseTree child) {
        String textWithSpaces = "";
        for (int i = 0; i < child.getChildCount(); i++) {
            textWithSpaces = textWithSpaces.concat(child.getChild(i).getText());
            if (i != child.getChildCount() - 1)
                textWithSpaces = textWithSpaces.concat(" ");
        }
        return textWithSpaces;
    }

    public static int processColor(ParserRuleContext ctx, Element element, int i) {
        if (ctx.getChild(i).getText().startsWith("#")) {
            element.setBackgroundColor(ctx.getChild(i++).getText());
        }
        return i;
    }

    public static int processStereotypes(ParserRuleContext ctx, HasStereotypeElement element, int i, boolean isElementAlreadyInInnerStructure) {
        boolean isFirstStereotype = true;
        if (ctx.getChild(i).getText().startsWith("<<"))
            element.getStereotypes().clear();

        while (ctx.getChild(i).getText().startsWith("<<")) {
            if (isFirstStereotype) {
                isFirstStereotype = false;
                if (isElementAlreadyInInnerStructure) {
                    element.getStereotypes().clear();
                }
            }
            String textWithSpaces = processIdentifierWithSpace(ctx.getChild(i++).getChild(1));
            if (!element.getStereotypes().contains(textWithSpaces))
                element.getStereotypes().add(textWithSpaces);
        }
        return i;
    }
}
