package pluginmain.imports.toinnerstructureprocessors.classdiagram;

import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.exceptions.compiler.CorrectElementFoundException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClassDiagramElementFinder {

    /**
     * Method returns Class Element from any elements list structure (even in subpackages).
     *
     * @throws MoreElementsFoundException is thrown if more classes with this name found
     */
    public static ClassElement findClassByNameOrAlias(List<Element> elements, String classToFind) throws MoreElementsFoundException {
        for (Element element : elements) {
            if (element instanceof ClassElement) {
                ClassElement classElement = (ClassElement) element;
                if ((classElement.getName().equals(classToFind) && classElement.getAlias() == null) ||
                        (classElement.getAlias() != null && classElement.getAlias().equals(classToFind)))
                    return (ClassElement) element;
            }
        }

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof ClassPackageElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ClassPackageElement) elem));
        //find in subpackages
        try {
            findClassElementInPackageList(classToFind, outputPackageElements);
        } catch (CorrectElementFoundException e) {
            return (ClassElement) e.getElement();
        }
        return null;
    }

    public static void removeClassByNameOrAlias(List<Element> elements, String classToFind) {
        Element elementToRemove = null;

        for (Element element : elements) {
            if (element instanceof ClassElement) {
                ClassElement classElement = (ClassElement) element;
                if ((classElement.getName().equals(classToFind) && classElement.getAlias() == null) ||
                        (classElement.getAlias() != null && classElement.getAlias().equals(classToFind)))
                    elementToRemove = element;
            }
        }
        if (elementToRemove != null)
            elements.remove(elementToRemove);

        List<Element> onlyPackageElements = elements
                .stream()
                .filter(elem -> elem instanceof ClassPackageElement)
                .collect(Collectors.toList());

        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ClassPackageElement) elem));
        //find in subpackages
        try {
            findClassElementInPackageList(classToFind, outputPackageElements);
        } catch (CorrectElementFoundException e) {
            ((ClassPackageElement) e.getParentPackageElement()).getClasses().remove(e.getElement());
        }
    }

    private static void findClassElementInPackageList(String classToFind, ArrayList<ChildrenBasedElement> onlyPackageElements) throws CorrectElementFoundException {
        for (int i = 0; i < onlyPackageElements.size(); i++) {
            ClassPackageElement packageClassPackageElement = (ClassPackageElement) onlyPackageElements.get(i);
            for (ClassElement classElement : packageClassPackageElement.getClasses()) {
                if ((classElement.getName().equals(classToFind) && classElement.getAlias() == null) ||
                        (classElement.getAlias() != null && classElement.getAlias().equals(classToFind)))
                    throw new CorrectElementFoundException(classElement, packageClassPackageElement);
            }
            findClassElementInPackageList(classToFind, packageClassPackageElement.getPackages());
        }
    }

}
