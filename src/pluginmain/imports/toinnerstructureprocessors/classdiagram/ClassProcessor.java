package pluginmain.imports.toinnerstructureprocessors.classdiagram;

import antlr.plantumlgrammar.PlantUMLParser;
import org.antlr.v4.runtime.tree.ParseTree;
import pluginmain.elements.classdiagram.*;
import pluginmain.elements.common.Element;
import pluginmain.elements.relationships.ImportAssociationElement;
import pluginmain.exceptions.compiler.*;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.utils.StringModifier;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findPackageByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.*;
import static pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassDiagramElementFinder.findClassByNameOrAlias;
import static pluginmain.utils.StringModifier.getPositionsOfString;

public class ClassProcessor {

    private final String signsRegex = "(~|\\+|-|#)";
    private final Map<String, String> signsWithKeywords = new HashMap<>();

    public ClassProcessor() {
        signsWithKeywords.put("-", "private");
        signsWithKeywords.put("+", "public");
        signsWithKeywords.put("#", "protected");
        signsWithKeywords.put("~", "package private");
    }

    public static String[] getParameters(String operationName) {
        int indexOfParametersStart = operationName.lastIndexOf("(") + 1;
        int indexOfParametersEnd = operationName.lastIndexOf(")");

        if (indexOfParametersStart >= indexOfParametersEnd || indexOfParametersStart == 0)
            return new String[0];
        String parametersAsString = operationName.substring(indexOfParametersStart, indexOfParametersEnd);
        String[] parameters = parametersAsString.split(",");
        for (int i = 0; i < parameters.length; i++) {
            parameters[i] = parameters[i].trim();
        }
        return parameters;
    }

    public static String removeBracketsWithParameters(String operationName) {
        int indexOfParametersStart = operationName.lastIndexOf("(");
        int indexOfParametersEnd = operationName.lastIndexOf(")");

        if (indexOfParametersStart == -1 || indexOfParametersEnd == -1)
            return operationName;
        else
            return operationName.substring(0, indexOfParametersStart) + operationName.substring(indexOfParametersEnd + 1);
    }

    public static void addClassToStructure(List<Element> elements, String currentPackage, ClassElement classElement) throws PackageNotFoundException, MoreElementsFoundException {
        if (classElement.getFullName() == null)
            classElement.setFullName(classElement.getName());
        if (currentPackage == null) {
            if (classElement.getName().equals(classElement.getFullName())) {
                ImportDiagramInfo.getInstance().setLastSavedElement(classElement, classElement.getAlias() != null);
                elements.add(classElement);
            } else {
                String packageName = classElement.getFullName().substring(0, classElement.getFullName().lastIndexOf("."));
                ClassPackageElement foundedPackage = (ClassPackageElement) findPackageByNameOrAlias(elements, packageName, packageName);
                ImportDiagramInfo.getInstance().setLastSavedElement(classElement, classElement.getAlias() != null);
                foundedPackage.getClasses().add(classElement);
            }
        } else {
            ClassPackageElement packageClassPackageElement = (ClassPackageElement) findPackageByNameOrAlias(elements, currentPackage, currentPackage);
            classElement.setFullName(packageClassPackageElement.getName() + "." + classElement.getName());
            ImportDiagramInfo.getInstance().setLastSavedElement(classElement, classElement.getAlias() != null);
            packageClassPackageElement.getClasses().add(classElement);
        }
    }

    public void processClass(PlantUMLParser.ClassRuleContext ctx, List<Element> elements, String currentPackage) throws SyntaxException, PackageNotFoundException, MoreElementsFoundException, NameAndAliasStartsWithSpecialSymbolsException {
        ClassElement classElement = new ClassElement();
        boolean isClassAlreadyInStructure = false;
        String className;
        boolean classNameHasQuotes = false;

        int i = 1;
        String firstChild = ctx.getChild(i++).getText();
        if (firstChild.startsWith("\"") && firstChild.endsWith("\"")) {
            className = StringModifier.removeQuotesOfStartAndEnd(firstChild);
            classNameHasQuotes = true;
        } else {
            className = firstChild;
        }

        ClassElement foundedClass = findClassByNameOrAlias(elements, className);
        try {
            if (ctx.getChild(2).getText().startsWith("as")) {
                String aliasName = ctx.getChild(2).getText().substring(2);
                if (!(aliasName.startsWith("\"") && aliasName.endsWith("\"")))
                    foundedClass = findClassByNameOrAlias(elements, aliasName);
            }
        } catch (NullPointerException ignored) {
        }

        if (foundedClass != null) {
            isClassAlreadyInStructure = true;
            classElement = foundedClass;
        }

        boolean nameWasSetInProcessAlias = false;

        try {
            String anotherChild = ctx.getChild(i).getText();
            try {
                i = processAlias(classElement, i, anotherChild, className, classNameHasQuotes, elements);
            } catch (AlreadyInStructureException e) {
                i++;
                isClassAlreadyInStructure = true;
                if (e.getFoundedClassByAlias() != null)
                    classElement = (ClassElement) e.getFoundedClassByAlias();
            }
            nameWasSetInProcessAlias = true;
            i = processClassHeader(ctx, classElement, i, elements, currentPackage, isClassAlreadyInStructure);
            processClassBody(ctx, classElement, i);
        } catch (NullPointerException e) {
            if (!nameWasSetInProcessAlias)
                setClassName(classElement, className, elements);
        }

        setClassType(ctx, classElement);

        if (!isClassAlreadyInStructure)
            addClassToStructure(elements, currentPackage, classElement);
    }

    private void setClassType(PlantUMLParser.ClassRuleContext ctx, ClassElement classElement) {
        String firstChild = ctx.getChild(0).getText();
        boolean isClassAbstract = firstChild.equals("abstract") || firstChild.equals("abstractclass");
        if (isClassAbstract) {
            classElement.getStereotypes().add("abstract");
            classElement.setEnum(false);
        } else {
            String classType = ctx.getChild(0).getText();
            if (!classType.equals("class")) {
                if (!classElement.getStereotypes().contains(classType))
                    classElement.getStereotypes().add(classType);
                if (classType.equals("enum"))
                    classElement.setEnum(true);
                else
                    classElement.setEnum(false);
            }
        }
    }

    private int processAlias(ClassElement classElement, int i, String child, String className, boolean classNameHasQuotes, List<Element> elements) throws NameAndAliasStartsWithSpecialSymbolsException, PackageNotFoundException, MoreElementsFoundException, AlreadyInStructureException {
        if (child.startsWith("as")) {
            String aliasName = child.substring(2);
            ClassElement foundedClassByAlias = findClassByNameOrAlias(elements, aliasName);
            if (classElement.getAlias() != null || foundedClassByAlias != null) {
                throw new AlreadyInStructureException(foundedClassByAlias);
            }

            if (classNameHasQuotes) {
                if (aliasName.startsWith("\"") && aliasName.endsWith("\""))
                    throw new NameAndAliasStartsWithSpecialSymbolsException("Class: " + className + " has name and alias with quotes");
                else {
                    classElement.setAlias(StringModifier.removeQuotesOfStartAndEnd(aliasName));
                    setClassName(classElement, className, elements);
                }
            } else {
                if (aliasName.startsWith("\"") && aliasName.endsWith("\"")) {
                    classElement.setAlias(className);
                    classElement.setName(StringModifier.removeQuotesOfStartAndEnd(aliasName));
                } else
                    throw new NameAndAliasStartsWithSpecialSymbolsException("Class: " + aliasName + " has name and alias without quotes");
            }

            i++;
        } else {
            setClassName(classElement, className, elements);
        }
        return i;
    }

    private void processClassBody(PlantUMLParser.ClassRuleContext ctx, ClassElement classElement, int i) throws SyntaxException {
        if (ctx.getChild(i).getText().startsWith("{")) {
            classElement.setHasBody(true);
            ParseTree wholeBody = ctx.getChild(i);
            for (int j = 0; j < wholeBody.getChildCount(); j++) {
                ParseTree bodyLine = wholeBody.getChild(j);
                processBodyLine(classElement, bodyLine);
            }
        }
    }


    private void setClassName(ClassElement classElement, String className, List<Element> elements) throws MoreElementsFoundException {
        className = StringModifier.removeQuotesOfStartAndEnd(className);

        if (className.contains(".")) {
            int indexOflastDot = className.lastIndexOf(".");
            if (indexOflastDot == 0) {
                classElement.setFullName(className.substring(1));
                classElement.setName(className.substring(1));
                return;
            }

            String packageNameOrAlias = className.substring(0, indexOflastDot);
            classElement.setFullName(className);
            classElement.setName(className.substring(indexOflastDot + 1));
            try {
                findPackageByNameOrAlias(elements, packageNameOrAlias, packageNameOrAlias);
            } catch (PackageNotFoundException e) {
                if (findClassByNameOrAlias(elements, packageNameOrAlias) != null) {
                    //this package could not be made, it is class name
                    classElement.setFullName(className);
                    classElement.setName(className);
                    return;
                }
                ClassPackageElement newPackage = new ClassPackageElement(StringModifier.removeQuotesOfStartAndEnd(packageNameOrAlias));
                elements.add(newPackage);
            }
        } else {
            classElement.setFullName(className);
            classElement.setName(className);
        }
    }

    public void processBodyLine(ClassElement classElement, ParseTree bodyLine) throws SyntaxException {
        if (bodyLine.getChildCount() == 0)
            return;

        StringBuilder textWithSpace = new StringBuilder();
        for (int k = 0; k < bodyLine.getChildCount(); k++) {
            ParseTree child = bodyLine.getChild(k);
            if (child.getChildCount() > 0) {
                textWithSpace.append(processIdentifierWithSpace(child));
            } else {
                textWithSpace.append(child.getText());
            }
        }
        String fieldOrMethod = textWithSpace.toString();
        processResource(fieldOrMethod, classElement);
    }

    private void processResource(String fieldOrMethod, ClassElement classElement) throws SyntaxException {

        ResourceElement resourceElement = processResourceBody(fieldOrMethod, classElement.getName());
        if (resourceElement instanceof AttributeElement)
            classElement.getAttributes().add((AttributeElement) resourceElement);
        else
            classElement.getOperations().add((OperationElement) resourceElement);

    }

    private ResourceElement processResourceBody(String fieldOrMethodText, String className) throws SyntaxException {
        Map<String, Boolean> signAndKeywords = initializeMap();
        fieldOrMethodText = fieldOrMethodText.replaceAll("\\{classifier}", "\\{static}");
        List<Integer> positionsOfOpenedBrackets = getPositionsOfString(fieldOrMethodText, "{");
        List<Integer> positionsOfClosedBrackets = getPositionsOfString(fieldOrMethodText, "}");
        if (positionsOfOpenedBrackets.size() != positionsOfClosedBrackets.size())
            throw new SyntaxException("Resource of class body has incorrect syntax, incorrect brackets in attribute or method of class " + className);

        for (int i = 0; i < positionsOfOpenedBrackets.size(); i++) {
            String keyword = fieldOrMethodText.substring(positionsOfOpenedBrackets.get(i) + 1, positionsOfClosedBrackets.get(i));
            signAndKeywords.put(keyword, true);
        }
        String attributeName = fieldOrMethodText.replaceAll("\\{static}|\\{abstract}|\\{field}|\\{method}", " ").trim();
        String visibility = null;

        if (attributeName.matches(signsRegex + ".*")) {
            String visibilityChar = attributeName.substring(0, 1);
            visibility = signsWithKeywords.get(visibilityChar);
            attributeName = attributeName.substring(1).trim();
            while (attributeName.contains("  "))
                attributeName = attributeName.replaceAll("  ", " ");
        }
        return finishCreationOfResource(attributeName, visibility, signAndKeywords);
    }

    private ResourceElement finishCreationOfResource(String resourceName, String visibility, Map<String, Boolean> signAndKeywords) {
        Boolean hasFieldKeyword = signAndKeywords.get("field");
        Boolean hasMethodKeyword = signAndKeywords.get("method");

        if (hasMethodKeyword)
            return new OperationElement(resourceName, visibility, signAndKeywords.get("abstract"), signAndKeywords.get("static"), null);
        if (hasFieldKeyword)
            return new AttributeElement(resourceName, visibility, signAndKeywords.get("abstract"), signAndKeywords.get("static"), null, null);

        return createFieldOrMethodBasedOnParentheses(resourceName, visibility, signAndKeywords);
    }

    private ResourceElement createFieldOrMethodBasedOnParentheses(String resourceName, String visibility, Map<String, Boolean> signAndKeywords) {
        if (resourceName.contains("(") && resourceName.contains(")")) {
            return new OperationElement(resourceName, visibility, signAndKeywords.get("abstract"), signAndKeywords.get("static"), null);
        } else
            return new AttributeElement(resourceName, visibility, signAndKeywords.get("abstract"), signAndKeywords.get("static"), null, null);
    }

    private Map<String, Boolean> initializeMap() {
        Map<String, Boolean> signAndKeywords = new HashMap<>();
        signAndKeywords.put("abstract", false);
        signAndKeywords.put("static", false);
        signAndKeywords.put("field", false);
        signAndKeywords.put("method", false);
        return signAndKeywords;
    }

    private int processTemplateParameters(PlantUMLParser.ClassRuleContext ctx, ClassElement classElement, int i, String anotherChild) {
        if (anotherChild.startsWith("<") && !anotherChild.startsWith("<<")) {
            classElement.getTemplateParameters().clear();
            String textWithSpaces = processIdentifierWithSpace(ctx.getChild(i).getChild(1));
            if (!classElement.getTemplateParameters().contains(textWithSpaces))
                classElement.getTemplateParameters().add(textWithSpaces);
            i++;
        }
        return i;
    }

    private int processClassHeader(PlantUMLParser.ClassRuleContext ctx, ClassElement classElement, int i, List<Element> elements, String currentPackage, boolean isClassAlreadyInStructure) throws PackageNotFoundException, MoreElementsFoundException {
        String anotherChild = ctx.getChild(i).getText();
        classElement.setBackgroundColor(null);
        i = processTemplateParameters(ctx, classElement, i, anotherChild);
        i = processStereotypes(ctx, classElement, i, isClassAlreadyInStructure);

        i = processColor(ctx, classElement, i);

        //extends
        if (ctx.getChild(i).getText().equals("extends")) {
            i++;
            String parentClassName = ctx.getChild(i++).getText();
            ClassElement foundedParentClass = findClassByNameOrAlias(elements, parentClassName);
            if (foundedParentClass == null && !parentClassName.equals(classElement.getName())) {
                foundedParentClass = new ClassElement(parentClassName);
                addClassToStructure(elements, currentPackage, foundedParentClass);
            }

            ImportAssociationElement relationElement = new ImportAssociationElement();
            relationElement.setFromName(parentClassName);
            relationElement.setToName(classElement.getName());
            relationElement.setFromAggregationKind("<|");
            relationElement.setMiddleDotted(false);
            elements.add(relationElement);
        }

        //implements
        if (ctx.getChild(i).getText().equals("implements")) {
            i++;
            String interfaceName = ctx.getChild(i++).getText();
            ClassElement foundedInterface = findClassByNameOrAlias(elements, interfaceName);
            if (foundedInterface == null && !interfaceName.equals(classElement.getName())) {
                foundedInterface = new ClassElement(interfaceName);
                foundedInterface.getStereotypes().add("interface");
                addClassToStructure(elements, currentPackage, foundedInterface);
            }

            ImportAssociationElement relationElement = new ImportAssociationElement();
            relationElement.setFromName(classElement.getName());
            relationElement.setToName(interfaceName);
            relationElement.setToAggregationKind("|>");
            relationElement.setMiddleDotted(true);
            elements.add(relationElement);
        }
        return i;
    }

}
