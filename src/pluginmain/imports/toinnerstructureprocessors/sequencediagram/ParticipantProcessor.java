package pluginmain.imports.toinnerstructureprocessors.sequencediagram;

import org.antlr.v4.runtime.ParserRuleContext;
import pluginmain.elements.common.Element;
import pluginmain.elements.sequencediagram.ParticipantElement;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.utils.StringModifier;

import java.util.IllegalFormatConversionException;
import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processColor;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processStereotypes;
import static pluginmain.imports.toinnerstructureprocessors.sequencediagram.SequenceDiagramElementFinder.findParticipantByNameOrAlias;

public class ParticipantProcessor {


    public static int findCorrectPositionBasedOnOrder(ParticipantElement participantElement, List<Element> elements) {
        int i = 0;
        for (Element element : elements) {
            if (element instanceof ParticipantElement) {
                if (((ParticipantElement) element).getOrder() <= participantElement.getOrder()) {
                    i++;
                }

            } else {
                i++;
            }
        }
        return i;
    }

    public void processParticipant(ParserRuleContext ctx, List<Element> elements) throws SyntaxException, MoreElementsFoundException {
        ParticipantElement participantElement = new ParticipantElement();

        int i = 0;
        if (ctx.getChild(i).getText().equals("create")) {
            participantElement.setCreatedByMessage(true);
            i++;
        }

        String participantType = ctx.getChild(i++).getText();

        String firstNameAsText = ctx.getChild(i++).getText();
        String secondNameAsText = null;
        if (ctx.getChildCount() > i && ctx.getChild(i).getText().startsWith("as")) {
            secondNameAsText = ctx.getChild(i).getText().substring(2);
            i++;
        }

        if (firstNameAsText.contains(",") || (secondNameAsText != null && secondNameAsText.contains(",")))
            throw new SyntaxException("Participant cannot have comma in name or alias");

        setCorrectNameAndAlias(participantElement, firstNameAsText, secondNameAsText);
        String correctName = participantElement.getName();
        String correctAlias = participantElement.getAlias();

        ParticipantElement foundedParticipant = null;
        if (correctAlias == null)
            foundedParticipant = findParticipantByNameOrAlias(elements, correctName);
        else
            foundedParticipant = findParticipantByNameOrAlias(elements, correctAlias);

        if (foundedParticipant != null) {
            //for correct order
            elements.remove(participantElement);
            int order = findCorrectPositionBasedOnOrder(foundedParticipant, elements);
            elements.add(order, foundedParticipant);
            return;
        }

        try {
            i = processStereotypes(ctx, participantElement, i, false);
            participantElement.setBackgroundColor(null);
            i = processOrder(ctx, participantElement, i);
            processColor(ctx, participantElement, i);
        } catch (NullPointerException ignored) {
        }

        if (!participantType.equals("participant"))
            participantElement.getStereotypes().add(participantType);

        int position = findCorrectPositionBasedOnOrder(participantElement, elements);

        if (participantElement.isCreatedByMessage()) {
            if (participantElement.getAlias() == null)
                ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage().add(participantElement.getName());
            else
                ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage().add(participantElement.getAlias());
        }

        elements.add(position, participantElement);
    }

    private int processOrder(ParserRuleContext ctx, ParticipantElement participantElement, int i) throws SyntaxException {
        if (ctx.getChild(i).getText().startsWith("order")) {
            try {
                int value = Integer.parseInt(ctx.getChild(i++).getChild(1).getText());
                participantElement.setOrder(value);
            } catch (IllegalFormatConversionException e) {
                throw new SyntaxException("Participant has incorrect syntax, order has to be whole number");
            }
        }
        return i;
    }

    private void setCorrectNameAndAlias(ParticipantElement participantElement, String firstName, String secondName) throws SyntaxException {
        boolean firstNameHasQuotes = false;

        if (firstName.startsWith("\"") && firstName.endsWith("\"")) {
            firstName = StringModifier.removeQuotesOfStartAndEnd(firstName);
            firstNameHasQuotes = true;
        }

        if (secondName == null) {
            participantElement.setName(firstName);
            return;
        }

        if (secondName.startsWith("\"") && secondName.endsWith("\"")) {
            if (firstNameHasQuotes)
                throw new SyntaxException("Participant has incorrect syntax, name and alias could not have both quotes");
            secondName = StringModifier.removeQuotesOfStartAndEnd(secondName);
            participantElement.setName(secondName);
            participantElement.setAlias(firstName);
        } else {
            participantElement.setName(firstName);
            participantElement.setAlias(secondName);
        }
    }
}
