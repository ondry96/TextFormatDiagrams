package pluginmain.imports.toinnerstructureprocessors.sequencediagram;

import pluginmain.elements.common.Element;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.GroupElement;
import pluginmain.elements.sequencediagram.ParticipantElement;
import pluginmain.exceptions.compiler.CorrectGroupCaseFoundException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class SequenceDiagramElementFinder {

    public static ParticipantElement findParticipantByNameOrAlias(List<Element> elements, String participantToFind) {
        for (Element element : elements) {
            if (element instanceof ParticipantElement) {
                ParticipantElement participantElement = (ParticipantElement) element;
                if ((participantElement.getName().equals(participantToFind) && participantElement.getAlias() == null) ||
                        (participantElement.getAlias() != null && participantElement.getAlias().equals(participantToFind)))
                    return (ParticipantElement) element;
            }
        }
        return null;
    }

    public static GroupCaseElement findGroupCaseByAlias(List<Element> elements, String groupAliasToFind) throws MoreElementsFoundException {
        List<Element> onlyGroupElements = elements.stream()
                .filter(elem -> elem instanceof GroupElement).collect(Collectors.toList());
        ArrayList<GroupElement> outputGroupElements = new ArrayList<>();
        onlyGroupElements.forEach(elem -> outputGroupElements.add((GroupElement) elem));

        GroupCaseElement foundGroupCase = null;
        for (GroupElement group : outputGroupElements) {
            for (GroupCaseElement groupCaseElement : group.getGroupCases()) {
                if (groupCaseElement.getAlias().equals(groupAliasToFind))
                    return groupCaseElement;
                else {
                    try {
                        foundGroupCase = findGroupCaseByAliasInSubpackages(groupCaseElement.getGroupElements(), groupAliasToFind);
                    } catch (CorrectGroupCaseFoundException e) {
                        return e.getFoundGroupCase();
                    }
                }
            }
        }
        if (foundGroupCase != null)
            return foundGroupCase;
        else
            return null;
    }

    private static GroupCaseElement findGroupCaseByAliasInSubpackages(ArrayList<GroupElement> elements, String groupAliasToFind) throws CorrectGroupCaseFoundException, MoreElementsFoundException {
        for (GroupElement groupElement : elements) {
            for (GroupCaseElement groupCaseElement : groupElement.getGroupCases()) {
                if (groupCaseElement.getAlias().equals(groupAliasToFind))
                    throw new CorrectGroupCaseFoundException(groupCaseElement);
                else
                    findGroupCaseByAliasInSubpackages(groupCaseElement.getGroupElements(), groupAliasToFind);
            }
        }
        return null;
    }
}
