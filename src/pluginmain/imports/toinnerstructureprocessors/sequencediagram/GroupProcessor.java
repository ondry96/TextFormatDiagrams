package pluginmain.imports.toinnerstructureprocessors.sequencediagram;

import antlr.plantumlgrammar.PlantUMLParser;
import pluginmain.elements.common.Element;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.GroupElement;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.imports.toinnerstructureprocessors.CommonProcessor;

import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.sequencediagram.SequenceDiagramElementFinder.findGroupCaseByAlias;

public class GroupProcessor {

    public String processGroup(PlantUMLParser.GroupRuleContext ctx, List<Element> elements, String currentGroupCaseAlias) throws SyntaxException, MoreElementsFoundException {
        GroupCaseElement newGroupCase = new GroupCaseElement();

        String type = ctx.getChild(0).getText();
        newGroupCase.setType(type);

        if (!ctx.getChild(1).getChild(0).getText().equals(System.getProperty("line.separator")))
            newGroupCase.setName(CommonProcessor.processIdentifierWithSpace(ctx.getChild(1)));

        String aliasOfGroupCase = "GC" + ImportDiagramInfo.getInstance().getNextGroupCaseAliasNumber();
        newGroupCase.setAlias(aliasOfGroupCase);

        //it is new group
        GroupElement newGroup = new GroupElement();
        newGroup.getGroupCases().add(newGroupCase);
        newGroupCase.setParentGroup(newGroup);

        if (currentGroupCaseAlias == null)
            elements.add(newGroup);
        else {
            GroupCaseElement parentCase = findGroupCaseByAlias(elements, currentGroupCaseAlias);
            parentCase.getGroupElements().add(newGroup);
        }

        return aliasOfGroupCase;
    }

    public String processElseGroupBranch(PlantUMLParser.ElseGroupBranchContext ctx, List<Element> elements, String currentGroupCaseAlias) throws MoreElementsFoundException, SyntaxException {

        GroupCaseElement newGroupCase = new GroupCaseElement();

        if (!ctx.getChild(1).getChild(0).getText().equals(System.getProperty("line.separator")))
            newGroupCase.setName(CommonProcessor.processIdentifierWithSpace(ctx.getChild(1)));

        String aliasOfGroupCase = "GC" + ImportDiagramInfo.getInstance().getNextGroupCaseAliasNumber();
        newGroupCase.setAlias(aliasOfGroupCase);

        if (currentGroupCaseAlias == null)
            throw new SyntaxException("Group cannot start with else");

        GroupCaseElement parentCase = findGroupCaseByAlias(elements, currentGroupCaseAlias);
        GroupElement groupElement = parentCase.getParentGroup();
        groupElement.getGroupCases().add(newGroupCase);
        newGroupCase.setParentGroup(groupElement);

        return aliasOfGroupCase;
    }
}
