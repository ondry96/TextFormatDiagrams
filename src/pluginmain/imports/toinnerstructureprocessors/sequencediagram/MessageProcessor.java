package pluginmain.imports.toinnerstructureprocessors.sequencediagram;

import org.antlr.v4.runtime.ParserRuleContext;
import pluginmain.elements.common.Element;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.MessageElement;
import pluginmain.elements.sequencediagram.ParticipantElement;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.utils.StringModifier;

import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processIdentifierWithSpace;
import static pluginmain.imports.toinnerstructureprocessors.sequencediagram.ParticipantProcessor.findCorrectPositionBasedOnOrder;
import static pluginmain.imports.toinnerstructureprocessors.sequencediagram.SequenceDiagramElementFinder.findGroupCaseByAlias;
import static pluginmain.imports.toinnerstructureprocessors.sequencediagram.SequenceDiagramElementFinder.findParticipantByNameOrAlias;

public class MessageProcessor {


    public void processCreateMessage(ParserRuleContext ctx, List<Element> elements) throws SyntaxException, MoreElementsFoundException {
        new ParticipantProcessor().processParticipant(ctx, elements);
    }

    public void processMessage(ParserRuleContext ctx, List<Element> elements, String currentGroupCaseAlias) throws SyntaxException, MoreElementsFoundException {
        MessageElement newMessage = new MessageElement();
        int i = 0;

        String firstNameAsText = ctx.getChild(i++).getText();
        String secondNameAsText = null;
        if (ctx.getChild(i).getText().startsWith("as")) {
            secondNameAsText = ctx.getChild(i).getText().substring(2);
            i++;
        }

        String arrow = ctx.getChild(i++).getText();
        boolean hasParticipant;
        if (firstNameAsText.contains("-")) {
            arrow = firstNameAsText;
            processArrow(newMessage, arrow);
            i--;
        } else {
            processArrow(newMessage, arrow);
            ParticipantElement leftParticipant = new ParticipantElement();
            boolean isLeft = true;
            if (arrow.contains("<"))
                isLeft = false;

            hasParticipant = setCorrectNameAndAliasOfParticipant(leftParticipant, firstNameAsText, secondNameAsText, isLeft, elements, newMessage);
            if (hasParticipant && (newMessage.getLeftArrowPart().startsWith("[") || newMessage.getLeftArrowPart().startsWith("?")))
                throw new SyntaxException("Founded message cannot have participant on left side");
        }
        hasParticipant = false;
        try {
            firstNameAsText = ctx.getChild(i++).getText();
            if (firstNameAsText.equals(":"))
                i--;
            secondNameAsText = null;

            if (ctx.getChildCount() >= i + 1 && ctx.getChild(i).getText().startsWith("as")) {
                secondNameAsText = ctx.getChild(i).getText().substring(2);
                i++;
            }
            ParticipantElement rightParticipant = new ParticipantElement();
            boolean isLeft = false;
            if (arrow.contains("<"))
                isLeft = true;

            hasParticipant = setCorrectNameAndAliasOfParticipant(rightParticipant, firstNameAsText, secondNameAsText, isLeft, elements, newMessage);
            if (hasParticipant && (newMessage.getRightArrowPart().endsWith("]") || newMessage.getRightArrowPart().endsWith("?")))
                throw new SyntaxException("Lost message cannot have participant on right side");
        } catch (NullPointerException ignored) {
            if (hasParticipant && (newMessage.getRightArrowPart().endsWith("]") || newMessage.getRightArrowPart().endsWith("?")))
                throw new SyntaxException("Lost message cannot have participant on right side");
        }

        try {
            String name = null;
            if (ctx.getChild(i).getText().startsWith(":") && ctx.getChild(i).getText().length() != 1) {
                if (ctx.getChild(i).getText().startsWith("\""))
                    name = StringModifier.removeQuotesOfStartAndEnd(ctx.getChild(i).getText());
                else
                    name = ctx.getChild(i).getText();
                name = name.substring(1).trim();
                if (ctx.getChild(++i) != null)
                    name += " " + processIdentifierWithSpace(ctx.getChild(i));
            } else if (ctx.getChild(i++).getText().startsWith(":")) {
                if (ctx.getChild(i).getText().startsWith("\""))
                    name = StringModifier.removeQuotesOfStartAndEnd(ctx.getChild(i).getText());
                else
                    name = processIdentifierWithSpace(ctx.getChild(i));
            }

            newMessage.setName(name);

        } catch (NullPointerException ignored) {
        }


        if (ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage().size() != 0) {
            newMessage.setCreateMessage(true);
            List<String> participantsCreatedByMessage = ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage();

            if (newMessage.getToName() != null && participantsCreatedByMessage.size() != 0 &&
                    !newMessage.getToName().equals(participantsCreatedByMessage.get(participantsCreatedByMessage.size() - 1))) {
                participantsCreatedByMessage.remove(participantsCreatedByMessage.size() - 1);
                throw new SyntaxException("Next message after create participant has to be message to this participant");
            }
            participantsCreatedByMessage.remove(participantsCreatedByMessage.size() - 1);

        }

        if (currentGroupCaseAlias == null)
            elements.add(newMessage);
        else {
            GroupCaseElement foundedGroup = findGroupCaseByAlias(elements, currentGroupCaseAlias);
            foundedGroup.getMessageElements().add(newMessage);
        }

    }

    private void processArrow(MessageElement newMessage, String arrow) {
        if (arrow.contains("--"))
            newMessage.setDotted(true);

        while (arrow.contains("--"))
            arrow = arrow.replace("--", "-");

        String leftArrowPart = "";
        for (char charOfArrow : arrow.toCharArray()) {
            if (charOfArrow == '-')
                break;
            leftArrowPart = leftArrowPart + charOfArrow;
        }

        String rightArrowPart = "";
        boolean hadDash = false;
        for (char charOfArrow : arrow.toCharArray()) {
            if (charOfArrow == '-') {
                hadDash = true;
                continue;
            }
            if (!hadDash)
                continue;
            rightArrowPart = rightArrowPart + charOfArrow;
        }

        newMessage.setLeftArrowPart(leftArrowPart);
        newMessage.setRightArrowPart(rightArrowPart);
    }

    private boolean setCorrectNameAndAliasOfParticipant(ParticipantElement participant, String firstName, String secondName, boolean isLeftParticipant, List<Element> elements, MessageElement newMessage) throws SyntaxException {
        boolean firstNameHasQuotes = false;
        if (firstName.equals(":"))
            return false;

        if ((firstName.startsWith("[") && isLeftParticipant) || (firstName.startsWith("?") && isLeftParticipant) ||
                (firstName.endsWith("]") && !isLeftParticipant) || (firstName.endsWith("?") && !isLeftParticipant)) {
            //dont have participant

            if (isLeftParticipant)
                newMessage.setFromName(firstName);
            else
                newMessage.setToName(firstName);
            return false;
        }

        if (firstName.startsWith("\"") && firstName.endsWith("\"")) {
            firstName = StringModifier.removeQuotesOfStartAndEnd(firstName);
            firstNameHasQuotes = true;
        }
        if (secondName == null) {
            if (isLeftParticipant)
                newMessage.setFromName(firstName);
            else
                newMessage.setToName(firstName);

            participant.setName(firstName);
            createParticipantIfNeeded(participant, isLeftParticipant, elements, newMessage);
            return true;
        }


        if (secondName.startsWith("\"") && secondName.endsWith("\"")) {
            if (firstNameHasQuotes)
                throw new SyntaxException("Message has incorrect syntax, name and alias could not have both quotes");
            secondName = StringModifier.removeQuotesOfStartAndEnd(secondName);
            participant.setName(secondName);
            participant.setAlias(firstName);
        } else {
            participant.setName(firstName);
            participant.setAlias(secondName);
        }

        createParticipantIfNeeded(participant, isLeftParticipant, elements, newMessage);
        return true;
    }

    private void createParticipantIfNeeded(ParticipantElement participant, boolean isLeftParticipant, List<Element> elements, MessageElement newMessage) {
        ParticipantElement foundedParticipant;
        if (participant.getAlias() == null) {
            foundedParticipant = findParticipantByNameOrAlias(elements, participant.getName());

            if (isLeftParticipant)
                newMessage.setFromName(participant.getName());
            else
                newMessage.setToName(participant.getName());
        } else {
            foundedParticipant = findParticipantByNameOrAlias(elements, participant.getAlias());
            if (isLeftParticipant)
                newMessage.setFromName(participant.getAlias());
            else
                newMessage.setToName(participant.getAlias());
        }
        if (foundedParticipant == null) {
            int position = findCorrectPositionBasedOnOrder(participant, elements);
            elements.add(position, participant);
        }
    }

}
