package pluginmain.imports.toinnerstructureprocessors;

import com.vp.plugin.DiagramManager;
import org.antlr.v4.runtime.ParserRuleContext;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.exceptions.UnsupportedDiagramException;
import pluginmain.exceptions.compiler.CorrectPackageFoundException;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.PackageNotFoundException;
import pluginmain.utils.StringModifier;

import java.util.ArrayList;
import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.*;
import static pluginmain.imports.toinnerstructureprocessors.CommonProcessor.processIdentifierWithSpace;
import static pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassDiagramElementFinder.removeClassByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.removeActorByNameOrAlias;
import static pluginmain.imports.toinnerstructureprocessors.usecasediagram.UseCaseDiagramElementFinder.removeUseCaseByNameOrAlias;

public class PackageProcessor {


    public String processPackage(ParserRuleContext ctx, List<Element> elements, String parentPackageName, String diagramName) throws MoreElementsFoundException, PackageNotFoundException, UnsupportedDiagramException {
        String newPackageName = ctx.getChild(1).getText();
        String alias = null;
        String colorAsHex = null;
        ArrayList<String> stereotypes = new ArrayList<>();
        int i = 2;
        try {
            String thirdChild = ctx.getChild(i).getText();
            if (thirdChild.startsWith("as")) {
                alias = ctx.getChild(i++).getChild(1).getText();
            }
            i = processStereotypes(ctx, stereotypes, i);

            String lastChild = ctx.getChild(i).getText();
            if (lastChild.startsWith("#")) {
                colorAsHex = lastChild;
            }

            String nextChild = ctx.getChild(i).getText();
            if (nextChild.startsWith("#"))
                colorAsHex = nextChild;
        } catch (NullPointerException ignored) {
        }

        newPackageName = StringModifier.removeQuotesOfStartAndEnd(newPackageName);

        if (parentPackageName != null) {
            processSubpackage(elements, parentPackageName, newPackageName, alias, colorAsHex, stereotypes, diagramName);
        } else {
            processPackageInElements(elements, newPackageName, alias, colorAsHex, stereotypes, diagramName);
        }

        if (alias == null)
            return newPackageName;
        else
            return alias;
    }

    private void processPackageInElements(List<Element> elements, String newPackageName, String alias, String colorAsHex, ArrayList<String> stereotypes, String diagramName) throws MoreElementsFoundException, UnsupportedDiagramException {
        try {
            ChildrenBasedElement alreadyExistingNewPackage = findPackageByNameOrAlias(elements, newPackageName, alias);
            if (alreadyExistingNewPackage.getFullName() == null)
                alreadyExistingNewPackage.setFullName(alreadyExistingNewPackage.getName());

            if (alias != null)
                alreadyExistingNewPackage.setAlias(alias);

            alreadyExistingNewPackage.setBackgroundColor(colorAsHex);

            if (!stereotypes.isEmpty()) {
                alreadyExistingNewPackage.getStereotypes().clear();
                alreadyExistingNewPackage.setStereotypes(stereotypes);
            }

        } catch (PackageNotFoundException e) {
            ChildrenBasedElement newPackage;
            if (diagramName.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
                newPackage = new ClassPackageElement(newPackageName, colorAsHex, alias);
                newPackage.setFullName(newPackageName);
                if (alias == null) {
                    removeClassByNameOrAlias(elements, newPackageName);
                    removeNoteByAlias(elements, newPackageName);
                } else {
                    removeClassByNameOrAlias(elements, alias);
                    removeNoteByAlias(elements, alias);
                }
            } else if (diagramName.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
                newPackage = new UseCasePackageElement(newPackageName, colorAsHex, alias);
                newPackage.setFullName(newPackageName);
                if (alias == null) {
                    removeUseCaseByNameOrAlias(elements, newPackageName);
                    removeActorByNameOrAlias(elements, newPackageName);
                    removeNoteByAlias(elements, newPackageName);
                } else {
                    removeUseCaseByNameOrAlias(elements, alias);
                    removeActorByNameOrAlias(elements, alias);
                    removeNoteByAlias(elements, alias);
                }
            } else {
                throw new UnsupportedDiagramException();
            }
            newPackage.setStereotypes(stereotypes);

            elements.add(newPackage);
        }
    }

    private void processSubpackage(List<Element> elements, String parentPackageName, String newPackageName, String alias, String colorAsHex, ArrayList<String> stereotypes, String diagramName) throws PackageNotFoundException, MoreElementsFoundException, UnsupportedDiagramException {
        ChildrenBasedElement parentPackage = findPackageByNameOrAlias(elements, parentPackageName, parentPackageName);
        ChildrenBasedElement newPackage;

        if (diagramName.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
            newPackage = new ClassPackageElement(newPackageName, colorAsHex, alias);
        } else if (diagramName.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
            newPackage = new UseCasePackageElement(newPackageName, colorAsHex, alias);
        } else {
            throw new UnsupportedDiagramException();
        }
        newPackage.setFullName(newPackageName);
        ChildrenBasedElement alreadyExistingNewPackage = null;
        try {
            findPackageByNameInSubpackages(parentPackage.getPackages(), newPackageName, alias);
        } catch (CorrectPackageFoundException e) {
            alreadyExistingNewPackage = e.getPackageElement();
        }
        if (alreadyExistingNewPackage == null) {
            if (diagramName.equals(DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM)) {
                if (alias == null) {
                    removeClassByNameOrAlias(elements, newPackageName);
                } else {
                    removeClassByNameOrAlias(elements, alias);
                }
            } else if (diagramName.equals(DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM)) {
                if (alias == null) {
                    removeUseCaseByNameOrAlias(elements, newPackageName);
                    removeActorByNameOrAlias(elements, newPackageName);
                } else {
                    removeUseCaseByNameOrAlias(elements, alias);
                    removeActorByNameOrAlias(elements, alias);
                }
            } else {
                throw new UnsupportedDiagramException();
            }
            newPackage.setStereotypes(stereotypes);
            parentPackage.getPackages().add(newPackage);
        } else {
            if (alias != null)
                alreadyExistingNewPackage.setAlias(alias);

            if (colorAsHex != null)
                alreadyExistingNewPackage.setBackgroundColor(colorAsHex);

            if (!stereotypes.isEmpty()) {
                alreadyExistingNewPackage.getStereotypes().clear();
                alreadyExistingNewPackage.setStereotypes(stereotypes);
            }
        }
    }

    private int processStereotypes(ParserRuleContext ctx, List<String> stereotypes, int i) {
        while (ctx.getChild(i).getText().startsWith("<<")) {
            String textWithSpaces = processIdentifierWithSpace(ctx.getChild(i++).getChild(1));
            if (!stereotypes.contains(textWithSpaces))
                stereotypes.add(textWithSpaces);
        }
        return i;
    }


}
