package pluginmain.utils;

import java.util.ArrayList;

public class StringModifier {

    public static String addQuotesToStartAndEndIfMoreWordsAndDontHaveThem(String string) {
        if (string != null && string.contains(" ") && !string.startsWith("\"") && !string.endsWith("\"")) {
            return "\"" + string + "\"";
        }
        return string;
    }

    public static String removeQuotesOfStartAndEnd(String string) {
        if (string != null && string.startsWith("\"") && string.endsWith("\"")) {
            return string.substring(1, string.length() - 1);
        }
        return string;
    }

    public static String removeColonsOfStartAndEnd(String string) {
        if (string != null && string.startsWith(":") && string.endsWith(":")) {
            return string.substring(1, string.length() - 1);
        }
        return string;
    }

    public static String removeBracketsOfStartAndEnd(String string) {
        if (string != null && string.startsWith("(") && string.endsWith(")")) {
            return string.substring(1, string.length() - 1);
        }
        return string;
    }

    public static String removeAngleBracketsOfStartAndEnd(String string) {
        if (string != null) {
            if (string.startsWith("<<") && string.endsWith(">>")) {
                return string.substring(2, string.length() - 2);
            } else if (string.startsWith("<") && string.endsWith(">")) {
                return string.substring(1, string.length() - 1);
            }
        }
        return string;
    }


    public static ArrayList<Integer> getPositionsOfString(String inputText, String textToFind) {
        ArrayList<Integer> positions = new ArrayList<>();
        int lastIndex = 0;
        while (lastIndex != -1) {
            lastIndex = inputText.indexOf(textToFind, lastIndex);

            if (lastIndex != -1) {
                positions.add(lastIndex);
                lastIndex += 1;
            }
        }
        return positions;
    }
}
