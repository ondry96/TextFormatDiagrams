package pluginmain.utils;

import com.vp.plugin.ViewManager;
import pluginmain.enums.TextFormat;
import pluginmain.exceptions.NoChosenDiagramException;
import pluginmain.exceptions.NoChosenOptionException;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public class UserInputHandler {

    public static String showChooseDiagramDialog(List<String> diagrams, ViewManager viewManager) throws NoChosenDiagramException {
        String chosenDiagramName = (String) JOptionPane.showInputDialog(
                viewManager.getRootFrame(),
                "Select diagram for exporting",
                "Select diagram",
                JOptionPane.PLAIN_MESSAGE,
                null,
                diagrams.toArray(),
                diagrams.get(0));
        if ((chosenDiagramName == null)) {
            throw new NoChosenDiagramException();
        }
        return chosenDiagramName;
    }

    public static TextFormat showChooseTextFormatDialog(ViewManager viewManager, String message) throws NoChosenOptionException {
        TextFormat[] textFormats = TextFormat.values();
        if (textFormats.length != 0) {
            TextFormat chosenFormat = (TextFormat) JOptionPane.showInputDialog(
                    viewManager.getRootFrame(),
                    message,
                    "Select text format",
                    JOptionPane.PLAIN_MESSAGE,
                    null,
                    textFormats,
                    textFormats[0]);
            if ((chosenFormat == null)) {
                throw new NoChosenOptionException();
            }
            return chosenFormat;
        }
        throw new RuntimeException();

    }

    public static String chooseDirectory(ViewManager viewManager) throws FileNotFoundException {
        JFileChooser fileChooser = createFileChooser("Select output directory for EXPORT", JFileChooser.DIRECTORIES_ONLY);
        fileChooser.showOpenDialog(viewManager.getRootFrame());
        File selectedFile = fileChooser.getSelectedFile();
        if (selectedFile == null) {
            throw new FileNotFoundException();
        }
        if (!selectedFile.isDirectory()) {
            JOptionPane.showMessageDialog(viewManager.getRootFrame(), "Select existing directory please");
            return chooseDirectory(viewManager);
        }
        return selectedFile.getPath();
    }

    public static String chooseFile(ViewManager viewManager) throws FileNotFoundException {
        JFileChooser fileChooser = createFileChooser("Select file with defined diagram for IMPORT", JFileChooser.FILES_ONLY);
        fileChooser.showOpenDialog(viewManager.getRootFrame());
        File selectedFile = fileChooser.getSelectedFile();
        if (selectedFile == null) {
            throw new FileNotFoundException();
        }
        if (selectedFile.isDirectory()) {
            JOptionPane.showMessageDialog(viewManager.getRootFrame(), "Select existing file please");
            return chooseFile(viewManager);
        }
        return selectedFile.getPath();
    }

    public static void showMessageToUser(ViewManager viewManager, String message, String title) {
        JOptionPane.showMessageDialog(
                viewManager.getRootFrame(),
                message,
                title,
                JOptionPane.PLAIN_MESSAGE);
    }

    private static JFileChooser createFileChooser(String title, int typeOfFiles) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new java.io.File("."));
        fileChooser.setDialogTitle(title);
        fileChooser.setFileSelectionMode(typeOfFiles);
        fileChooser.setAcceptAllFileFilterUsed(false);
        return fileChooser;
    }

}
