package pluginmain.exports.filewriters.plantuml;

import com.vp.plugin.ViewManager;
import com.vp.plugin.diagram.LayoutOption$Orientation;
import com.vp.plugin.model.*;
import pluginmain.elements.common.*;
import pluginmain.elements.relationships.AssociationElement;
import pluginmain.elements.relationships.SimpleRelationshipElement;
import pluginmain.imports.ImportDiagramInfo;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collections;
import java.util.List;

public abstract class PlantUmlWriter {

    protected File outputFile;
    protected FileWriter fileWriter;
    protected ViewManager viewManager;
    protected List<Element> elements;

    public abstract void writeAsPlantUMLToFile() throws IOException;

    public void writeIndentation(int indentationLevel) throws IOException {
        for (int i = 0; i < indentationLevel; i++) {
            fileWriter.write("\t");
        }
    }

    public void writeLineBreak() throws IOException {
        fileWriter.write(System.getProperty("line.separator"));
    }

    public void writeEmptyLines(int count) throws IOException {
        for (int i = 0; i < count; i++) {
            writeLineBreak();
        }
    }

    public void writeVisibilityModifierAsSign(String visibility) throws IOException {
        switch (visibility) {
            case "public":
                fileWriter.write("+");
                break;
            case "private":
                fileWriter.write("-");
                break;
            case "protected":
                fileWriter.write("#");
                break;
            case "package":
                fileWriter.write("~");
                break;
            default:
                break;
        }
    }

    protected void writePackageHeadInfo(ChildrenBasedElement packageElement, int indentation) throws IOException {
        writeIndentation(indentation);
        fileWriter.write(packageElement.getShape() + " " + packageElement.getNameWithQuotesIfNeeded());
        if (packageElement.getAlias() != null && !packageElement.getAlias().equals(""))
            fileWriter.write(" as " + packageElement.getAlias());

        if (!packageElement.getModelType().equals("Package") && !packageElement.getModelType().equals("System"))
            fileWriter.write(" <<" + packageElement.getModelType() + ">>");
        writeStereotypes(packageElement);

        if (!packageElement.isBackgroundColorDefault())
            fileWriter.write(" " + packageElement.getBackgroundColor());
        fileWriter.write(" {");
        writeLineBreak();
    }

    protected void writeNote(NoteElement noteElement, int indentation) throws IOException {
        writeIndentation(indentation);
        fileWriter.write("note as ");
        fileWriter.write(noteElement.getAlias());

        if (!noteElement.isBackgroundColorDefault())
            fileWriter.write(" " + noteElement.getBackgroundColor());
        writeLineBreak();
        writeIndentation(indentation + 1);
        fileWriter.write(noteElement.getDescription());
        writeLineBreak();
        writeIndentation(indentation);
        fileWriter.write("end note");
        writeEmptyLines(2);
    }

    protected void writeStereotypes(HasStereotypeElement element) throws IOException {
        for (String stereotype : element.getStereotypes()) {
            fileWriter.write(" <<" + stereotype + ">>");
        }
    }


    protected void writeSimpleRelationship(SimpleRelationshipElement element, boolean isFirstRelationOrAssociation) throws IOException {
        IStereotype[] iStereotypes = element.getRelationship().toStereotypeModelArray();
        String stereotypesAsString = "";
        if (iStereotypes != null) {
            StringBuilder stereotypesAsStringBuilder = new StringBuilder();
            for (int i = 0; i < iStereotypes.length; i++) {
                String stereotype = iStereotypes[i].getName();
                stereotypesAsStringBuilder.insert(0, "<<" + stereotype + ">> ");
            }
            stereotypesAsString = stereotypesAsStringBuilder.toString();
        }


        if (isFirstRelationOrAssociation)
            writeLineBreak();

        fileWriter.write(element.getFromName());
        writeSimpleRelationshipArrow(element.getRelationship());
        fileWriter.write(element.getToName());

        if (!stereotypesAsString.equals("")) {
            fileWriter.write(" : " + stereotypesAsString);
            if (element.getName() != null && !element.getName().equals(""))
                fileWriter.write(element.getNameWithQuotesIfNeeded());
        } else if (element.getName() != null && !element.getName().equals(""))
            fileWriter.write(" : " + element.getName());
        writeLineBreak();

    }

    private void writeSimpleRelationshipArrow(ISimpleRelationship relationship) throws IOException {
        if (relationship instanceof IAnchor || relationship instanceof IConstraint)
            fileWriter.write(" .. ");
        else if (relationship instanceof IGeneralization)
            fileWriter.write(" <|-- ");
        else if (relationship instanceof IExtend)
            fileWriter.write(" <.. ");
        else if (relationship instanceof IRealization)
            fileWriter.write(" <|.. ");
        else
            fileWriter.write(" ..> ");
    }

    protected void writeAssociation(AssociationElement element, boolean isFirstRelationOrAssociation) throws IOException {
        if (isFirstRelationOrAssociation)
            writeLineBreak();

        fileWriter.write(element.getFromName());

        if (!element.getFromMultiplicity().equals(""))
            fileWriter.write(" \"" + element.getFromMultiplicity() + "\"");

        writeAssociationArrow(element);

        if (!element.getToMultiplicity().equals(""))
            fileWriter.write("\"" + element.getToMultiplicity() + "\" ");

        fileWriter.write(element.getToName());

        if (element.getNameWithQuotesIfNeeded() != null && !element.getNameWithQuotesIfNeeded().equals(""))
            fileWriter.write(" : " + element.getNameWithQuotesIfNeeded());
        writeLineBreak();
    }

    private void writeAssociationArrow(AssociationElement element) throws IOException {
        StringBuilder sb = new StringBuilder();
        sb.append(' ');
        determineArrowEnd(element.getFromAggregationKind(), element.getFromNavigable(), sb, true, element.getToNavigable() == 0);
        sb.append("--");
        determineArrowEnd(element.getToAggregationKind(), element.getToNavigable(), sb, false, element.getFromNavigable() == 0);
        sb.append(' ');
        fileWriter.write(sb.toString());
    }

    private StringBuilder determineArrowEnd(String aggregationKind, int navigable, StringBuilder sb, boolean isFrom, boolean oppositeIsNavigable) {
        switch (aggregationKind) {
            case "Shared":
                return sb.append('o');
            case "Composited":
                return sb.append('*');
            default:
                if (navigable == 0 && !oppositeIsNavigable)
                    if (isFrom)
                        return sb.append('<');
                    else
                        return sb.append('>');
                else if (navigable == 2)
                    return sb.append("x");
                else
                    return sb;
        }
    }

    protected void reorderWithCoordX(List<Element> elements) {
        Collections.sort(elements, new SortElementsByStartCoordX());
    }

    protected void writeDirection() throws IOException {
        LayoutOption$Orientation orientation = ImportDiagramInfo.getInstance().getOrientation();
        if (orientation != null && orientation.equals(LayoutOption$Orientation.TopToBottom)) {
            fileWriter.write("top to bottom direction");
        } else if (orientation != null && orientation.equals(LayoutOption$Orientation.LeftToRight)) {
            fileWriter.write("left to right direction");
        }

        writeLineBreak();
    }

    protected void writeSkinparams() throws IOException {
        for (String skinparam : ImportDiagramInfo.getInstance().getSkinparamOptions()) {
            fileWriter.write("skinparam " + skinparam);
            writeLineBreak();
        }
        if (ImportDiagramInfo.getInstance().getSkinparamOptions().size() != 0)
            writeLineBreak();
    }

    public void preparePlantUMLExportFile(String path) throws IOException {
        if (!path.endsWith(".puml"))
            path = path.concat(".puml");
        outputFile = new File(path);
        fileWriter = new FileWriter(outputFile);
        fileWriter.write("@startuml");
        writeLineBreak();
    }

    public void finishPlantUMLExportFile() throws IOException {
        fileWriter.write("@enduml");
        fileWriter.close();
    }

    public FileWriter getFileWriter() {
        return fileWriter;
    }
}
