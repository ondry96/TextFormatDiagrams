package pluginmain.exports.filewriters.plantuml;

import com.vp.plugin.ViewManager;
import pluginmain.elements.classdiagram.*;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.AssociationElement;
import pluginmain.elements.relationships.SimpleRelationshipElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ClassDiagramPlantUmlWriter extends PlantUmlWriter {

    public ClassDiagramPlantUmlWriter(String fullPath, ViewManager viewManager, List<Element> elements) throws IOException {
        preparePlantUMLExportFile(fullPath);
        this.viewManager = viewManager;
        this.elements = elements;
    }

    @Override
    public void writeAsPlantUMLToFile() throws IOException {
        reorderWithCoordX(elements);
        writeSkinparams();
        boolean isFirstRelationOrAssociation = true;

        for (Element element : elements) {
            switch (element.getElementType()) {
                case "ClassPackageElement":
                    writePackage((ClassPackageElement) element, 0);
                    break;
                case "ClassElement":
                    writeClass((ClassElement) element, 0);
                    break;
                case "NoteElement":
                    writeNote((NoteElement) element, 0);
                    break;
                case "AssociationElement":
                    writeAssociation((AssociationElement) element, isFirstRelationOrAssociation);
                    isFirstRelationOrAssociation = false;
                    break;
                case "SimpleRelationshipElement":
                    writeSimpleRelationship((SimpleRelationshipElement) element, isFirstRelationOrAssociation);
                    isFirstRelationOrAssociation = false;
                    break;
            }
        }

        for (Element element : elements) {
            if (element.getElementType().equals("AssociationClassElement")) {
                writeAssociationClass((AssociationClassElement) element);
            }
        }

        finishPlantUMLExportFile();
    }

    private void writeAssociationClass(AssociationClassElement element) throws IOException {
        fileWriter.write("(" + element.getAssociationFromName() + ", " + element.getAssociationToName() + ")");
        fileWriter.write(" .. ");
        fileWriter.write(element.getToClassName());
        writeLineBreak();
    }

    private void writePackage(ClassPackageElement classPackageElement, int indentation) throws IOException {
        writePackageHeadInfo(classPackageElement, indentation);
        List<Element> elements = new ArrayList<>();
        elements.addAll(classPackageElement.getClasses());
        elements.addAll(classPackageElement.getNotes());
        elements.addAll(classPackageElement.getPackages());
        reorderWithCoordX(elements);

        for (Element element : elements) {
            if (element instanceof ClassElement)
                writeClass((ClassElement) element, indentation + 1);
            else if (element instanceof NoteElement)
                writeNote((NoteElement) element, indentation + 1);
            else //package
                writePackage((ClassPackageElement) element, indentation + 1);
        }

        writeIndentation(indentation);
        fileWriter.write("}");
        writeEmptyLines(2);

    }

    private void writeClass(ClassElement classElement, int indentation) throws IOException {
        writeIndentation(indentation);
        fileWriter.write("class " + classElement.getNameWithQuotesIfNeeded());
        if (classElement.getAlias() != null && !classElement.getAlias().equals(""))
            fileWriter.write(" as " + classElement.getAlias());
        writeGenericTemplateParameters(classElement);
        writeStereotypes(classElement);
        if (!classElement.isBackgroundColorDefault())
            fileWriter.write(" " + classElement.getBackgroundColor());

        if (classElement.isHasBody()) {
            fileWriter.write(" {");
            writeLineBreak();
            if (classElement.isEnum()) {
                writeEnumerationLiterals(classElement, indentation + 1);
            }
            writeClassAttributes(classElement, indentation + 1);
            writeClassOperations(classElement, indentation + 1);

            writeIndentation(indentation);
            fileWriter.write("}");
            writeLineBreak();
        }
        writeLineBreak();
    }

    private void writeClassOperations(ClassElement classElement, int indentation) throws IOException {
        for (OperationElement operation : classElement.getOperations()) {
            writeIndentation(indentation);
            writeAbstractAndStaticModifiers(operation);
            writeVisibilityModifierAsSign(operation.getVisibility());
            if (operation.getName().contains("(") && operation.getName().contains(")"))
                fileWriter.write(operation.getName());
            else {
                fileWriter.write(operation.getName() + "(");
                if (operation.getParameters() != null)
                    fileWriter.write(operation.getParameters());
                fileWriter.write(")");
            }
            if (operation.getReturnType() != null)
                fileWriter.write(" : " + operation.getReturnType());
            writeLineBreak();
        }
    }

    private void writeClassAttributes(ClassElement classElement, int indentation) throws IOException {
        for (AttributeElement attribute : classElement.getAttributes()) {
            writeIndentation(indentation);
            if (attribute.getName().contains("(") && attribute.getName().contains(")"))
                fileWriter.write("{field} ");
            writeAbstractAndStaticModifiers(attribute);

            writeVisibilityModifierAsSign(attribute.getVisibility());
            fileWriter.write(attribute.getName());

            if (attribute.getType() != null)
                fileWriter.write(" : " + attribute.getType());

            if (attribute.getInitialValue() != null)
                fileWriter.write(" = " + attribute.getInitialValue());
            writeLineBreak();
        }
    }

    private void writeAbstractAndStaticModifiers(ResourceElement resource) throws IOException {
        if (resource.isAbstract())
            fileWriter.write("{abstract} ");
        if (resource.isStatic())
            fileWriter.write("{static} ");
    }

    private void writeEnumerationLiterals(ClassElement classElement, int indentation) throws IOException {
        for (String enumerationLiteral : classElement.getEnumerationLiterals()) {
            writeIndentation(indentation);
            fileWriter.write(enumerationLiteral);
            writeLineBreak();
        }
    }


    private void writeGenericTemplateParameters(ClassElement classElement) throws IOException {
        for (String parameter : classElement.getTemplateParameters()) {
            fileWriter.write(" <" + parameter + ">");
        }
    }


}
