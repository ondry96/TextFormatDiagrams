package pluginmain.exports.filewriters.plantuml;

import com.vp.plugin.ViewManager;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.AssociationElement;
import pluginmain.elements.relationships.SimpleRelationshipElement;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class UseCaseDiagramPlantUmlWriter extends PlantUmlWriter {

    public UseCaseDiagramPlantUmlWriter(String fullPath, ViewManager viewManager, ArrayList<Element> elements) throws IOException {
        preparePlantUMLExportFile(fullPath);
        this.viewManager = viewManager;
        this.elements = elements;
    }

    @Override
    public void writeAsPlantUMLToFile() throws IOException {
        reorderWithCoordX(elements);
        writeDirection();
        writeSkinparams();
        boolean isFirstRelationOrAssociation = true;

        for (Element element : elements) {
            switch (element.getElementType()) {
                case "UseCasePackageElement":
                    writePackage((UseCasePackageElement) element, 0);
                    break;
                case "UseCaseElement":
                    writeUseCase((UseCaseElement) element, 0);
                    break;
                case "ActorElement":
                    writeActor((ActorElement) element, 0);
                    break;
                case "NoteElement":
                    writeNote((NoteElement) element, 0);
                    break;
                case "AssociationElement":
                    writeAssociation((AssociationElement) element, isFirstRelationOrAssociation);
                    isFirstRelationOrAssociation = false;
                    break;
                case "SimpleRelationshipElement":
                    writeSimpleRelationship((SimpleRelationshipElement) element, isFirstRelationOrAssociation);
                    isFirstRelationOrAssociation = false;
                    break;
                default:
                    break;
            }
        }
        finishPlantUMLExportFile();
    }

    private void writePackage(UseCasePackageElement useCasePackageElement, int indentation) throws IOException {
        writePackageHeadInfo(useCasePackageElement, indentation);
        List<Element> elements = new ArrayList<>();
        elements.addAll(useCasePackageElement.getActors());
        elements.addAll(useCasePackageElement.getUseCases());
        elements.addAll(useCasePackageElement.getNotes());
        elements.addAll(useCasePackageElement.getPackages());
        reorderWithCoordX(elements);

        for (Element element : elements) {
            if (element instanceof ActorElement)
                writeActor((ActorElement) element, indentation + 1);
            else if (element instanceof NoteElement)
                writeNote((NoteElement) element, indentation + 1);
            else if (element instanceof UseCaseElement)
                writeUseCase((UseCaseElement) element, indentation + 1);
            else //package
                writePackage((UseCasePackageElement) element, indentation + 1);
        }

        writeIndentation(indentation);
        fileWriter.write("}");
        writeEmptyLines(2);

    }

    private void writeActor(ActorElement actorElement, int indentation) throws IOException {
        writeIndentation(indentation);
        fileWriter.write("actor ");
        if (actorElement.getAlias() != null) {
            fileWriter.write(":" + actorElement.getName().substring(1, actorElement.getName().length() - 1) + ":" + " as " + actorElement.getAlias());
        } else {
            fileWriter.write(actorElement.getName());
        }
        writeStereotypes(actorElement);
        if (!actorElement.isBackgroundColorDefault())
            fileWriter.write(" " + actorElement.getBackgroundColor());
        writeLineBreak();
    }

    private void writeUseCase(UseCaseElement useCaseElement, int indentation) throws IOException {
        writeIndentation(indentation);
        fileWriter.write("usecase ");
        if (useCaseElement.getAlias() != null) {
            fileWriter.write("(" + useCaseElement.getName().substring(1, useCaseElement.getName().length() - 1) + ")" + " as " + useCaseElement.getAlias());
        } else {
            fileWriter.write("(" + useCaseElement.getName() + ")");
        }
        writeStereotypes(useCaseElement);
        if (!useCaseElement.isBackgroundColorDefault())
            fileWriter.write(" " + useCaseElement.getBackgroundColor());

        writeLineBreak();
    }
}
