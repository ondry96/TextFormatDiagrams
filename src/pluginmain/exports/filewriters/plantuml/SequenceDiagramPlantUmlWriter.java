package pluginmain.exports.filewriters.plantuml;

import com.vp.plugin.ViewManager;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.SortMessagesByStartCoordY;
import pluginmain.elements.common.SortParticipantsByStartCoordX;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.GroupElement;
import pluginmain.elements.sequencediagram.MessageElement;
import pluginmain.elements.sequencediagram.ParticipantElement;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SequenceDiagramPlantUmlWriter extends PlantUmlWriter {

    public SequenceDiagramPlantUmlWriter(String fullPath, ViewManager viewManager, List<Element> elements) throws IOException {
        preparePlantUMLExportFile(fullPath);
        this.viewManager = viewManager;
        this.elements = elements;
    }

    @Override
    public void writeAsPlantUMLToFile() throws IOException {
        reorderParticipantsWithCoordX(elements);
        reorderMessagesWithCoordY(elements);

        writeSkinparams();
        writeAutonumber();

        for (Element element : elements) {
            switch (element.getElementType()) {
                case "MessageElement":
                    writeMessage((MessageElement) element, 0);
                    break;
                case "ParticipantElement":
                    if (((ParticipantElement) element).isCreatedByMessage()) //will be written before corresponding message
                        break;
                    else
                        writeParticipant((ParticipantElement) element, 0);
                    break;
            }
        }
        writeLineBreak();

        for (Element element : elements) {
            switch (element.getElementType()) {
                case "GroupElement":
                    writeGroup((GroupElement) element, 0);
                    break;
            }
        }

        finishPlantUMLExportFile();
    }

    private void writeParticipant(ParticipantElement participantElement, int indentation) throws IOException {
        writeIndentation(indentation);

        String participantType = processStereotypes(participantElement);

        fileWriter.write(participantType + " " + participantElement.getName());
        if (participantElement.getAlias() != null && !participantElement.getAlias().equals(""))
            fileWriter.write(" as " + participantElement.getAlias());
        writeStereotypes(participantElement);
        if (participantElement.getOrder() != 0)
            fileWriter.write(" order " + participantElement.getOrder());

        if (!participantElement.isBackgroundColorDefault())
            fileWriter.write(" " + participantElement.getBackgroundColor());
        writeLineBreak();

    }

    private String processStereotypes(ParticipantElement participantElement) {
        String participantType = "participant";
        ArrayList<String> stereotypes = participantElement.getStereotypes();
        if (stereotypes.contains("boundary"))
            participantType = "boundary";
        if (stereotypes.contains("control"))
            participantType = "control";
        if (stereotypes.contains("entity"))
            participantType = "entity";
        if (stereotypes.contains("actor"))
            participantType = "actor";

        if (!participantType.equals("participant"))
            stereotypes.remove(participantType);
        return participantType;
    }

    private void writeMessage(MessageElement messageElement, int indentation) throws IOException {
        if (messageElement.isCreateMessage()) { //participant should be written
            Optional<Element> participantOptional = elements.stream().filter(
                    element -> element instanceof ParticipantElement && element.getName().equals(messageElement.getToName())
            ).findFirst();
            if (participantOptional.isPresent()) {
                ParticipantElement participantElement = (ParticipantElement) participantOptional.get();
                String participantType = processStereotypes(participantElement);

                writeIndentation(indentation);
                fileWriter.write("create " + participantType + " " + participantElement.getName());
                if (participantElement.getAlias() != null && !participantElement.getAlias().equals(""))
                    fileWriter.write(" as " + participantElement.getAlias());
                writeStereotypes(participantElement);
                if (participantElement.getOrder() != 0)
                    fileWriter.write(" order " + participantElement.getOrder());

                if (!participantElement.isBackgroundColorDefault())
                    fileWriter.write(" " + participantElement.getBackgroundColor());
                writeLineBreak();
            }

        }

        writeIndentation(indentation);

        if (messageElement.getFromName() == null)
            fileWriter.write("[");
        else
            fileWriter.write(messageElement.getFromName() + " ");

        fileWriter.write("-");
        if (messageElement.isDotted())
            fileWriter.write("-");
        fileWriter.write(messageElement.getRightArrowPart());

        if (messageElement.getToName() == null)
            fileWriter.write("]");
        else
            fileWriter.write(" " + messageElement.getToName());

        if (messageElement.getName() != null)
            fileWriter.write(" : " + messageElement.getName());
        writeLineBreak();

    }

    private void writeGroup(GroupElement element, int indentation) throws IOException {
        int groupCasesCount = element.getGroupCases().size();

        for (GroupCaseElement groupCase : element.getGroupCases()) {
            groupCasesCount--;
            writeIndentation(indentation);
            fileWriter.write(groupCase.getType());
            if (groupCase.getConstraint() != null)
                fileWriter.write(" " + groupCase.getConstraint());
            writeLineBreak();

            reorderMessagesWithCoordYInMessageList(groupCase.getMessageElements());
            for (MessageElement messageElement : groupCase.getMessageElements()) {
                writeMessage(messageElement, indentation + 1);
            }

            for (GroupElement groupElement : groupCase.getGroupElements()) {
                writeGroup(groupElement, indentation + 1);
            }

            if (groupCasesCount == 0) {
                writeIndentation(indentation);
                fileWriter.write("end");
                writeLineBreak();
            }


        }
        writeLineBreak();
    }

    private void reorderParticipantsWithCoordX(List<Element> elements) {
        elements.sort(new SortParticipantsByStartCoordX());
    }

    private void reorderMessagesWithCoordY(List<Element> elements) {
        elements.sort(new SortMessagesByStartCoordY());
    }

    private void reorderMessagesWithCoordYInMessageList(ArrayList<MessageElement> messageElements) {
        messageElements.sort(new SortMessagesByStartCoordY());
    }

    private void writeAutonumber() throws IOException {
        fileWriter.write("autonumber");
        writeLineBreak();
    }

}
