package pluginmain.exports.diagramexporters;

import com.vp.plugin.ViewManager;
import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.diagram.IDiagramUIModel;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.model.*;
import pluginmain.elements.classdiagram.AssociationClassElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.common.IHasAlias;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.AssociationElement;
import pluginmain.elements.relationships.SimpleRelationshipElement;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.enums.TextFormat;
import pluginmain.exceptions.UnsupportedTextFormatException;
import pluginmain.exports.ExportDiagramInfo;
import pluginmain.exports.filewriters.plantuml.PlantUmlWriter;
import pluginmain.imports.toinnerstructureprocessors.CommonElementFinder;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import static pluginmain.imports.diagramcreators.DiagramCreator.hideAliasStereotype;
import static pluginmain.utils.UserInputHandler.showMessageToUser;

public abstract class DiagramExporter {

    protected ArrayList<Element> elements = new ArrayList<>();
    protected ArrayList<String> elementIds = new ArrayList<>();
    protected int notesCount = 0;
    private String foundAlias = "";

    protected abstract PlantUmlWriter createCorrectPlantUmlWriter(ViewManager viewManager, String chosenDirectoryPath) throws IOException;

    protected abstract void processNotMainPackageElement(IModelElement element, int nestingLevel, IDiagramElement diagramElement, ChildrenBasedElement packageElement);

    protected abstract ChildrenBasedElement createCorrectPackageElement(String name, String color, int nestingLevel, String shape, int startCoordX, String modelType);

    public void exportToDirectory(ViewManager viewManager, String chosenDirectoryPath, TextFormat chosenFormat) throws IOException, UnsupportedTextFormatException {
        switch (chosenFormat) {
            case PlantUml:
                PlantUmlWriter plantUmlWriter = createCorrectPlantUmlWriter(viewManager, chosenDirectoryPath);
                plantUmlWriter.writeAsPlantUMLToFile();
                break;
            default:
                throw new UnsupportedTextFormatException("This format is not supported");
        }
        showMessageToUser(viewManager, "Successfully exported to " + chosenFormat.toString(), "Successfully exported");
    }

    public void buildElementsHierarchy(IDiagramUIModel diagramUIModel) {
        Iterator iteratorForMainElements = diagramUIModel.diagramElementIterator();
        Iterator iteratorForRelationships = diagramUIModel.diagramElementIterator();
        buildMainElements(iteratorForMainElements, diagramUIModel);
        buildRelationshipElements(iteratorForRelationships);
    }

    protected void buildRelationshipElements(Iterator iteratorForRelationships) {
        while (iteratorForRelationships.hasNext()) {
            IDiagramElement element = (IDiagramElement) iteratorForRelationships.next();
            if (element.getModelElement() == null || element.getShapeType().equals("Containment")) {
                //case containment - impossible to handle from and to element's names, sorry about it - will not process
                continue;
            }
            IModelElement modelElement = element.getModelElement();
            String elementId = modelElement.getId();
            if (elementIds.contains(elementId))
                continue;
            else
                elementIds.add(elementId);

            processRelationshipElements(modelElement);

        }
    }


    protected void buildMainElements(Iterator iterator, IDiagramUIModel diagramUIModel) {
        List<IDiagramElement> diagramElementsInCurrentDiagram = Arrays.asList(diagramUIModel.toDiagramElementArray());
        ExportDiagramInfo.getInstance().setDiagramElementsInCurrentDiagram(diagramElementsInCurrentDiagram);

        Iterator iterator1 = diagramUIModel.diagramElementIterator();
        List<IDiagramElement> listOfDiagramElements = new ArrayList<>();
        while (iterator1.hasNext()) {
            listOfDiagramElements.add((IDiagramElement) iterator1.next());
        }


        while (iterator.hasNext()) {
            IDiagramElement diagramElement = (IDiagramElement) iterator.next();
            boolean hasParent = false;
            for (IDiagramElement parentElement : listOfDiagramElements) {
                List<IShapeUIModel> iShapeUIModels = Arrays.asList(parentElement.toChildArray());
                if (iShapeUIModels.contains(diagramElement))
                    hasParent = true;
            }

            if (hasParent)
                continue;
            IModelElement modelElement = diagramElement.getModelElement();
            if (modelElement == null || modelElement instanceof IRelationship || diagramElement.getShapeType().equals("Containment")) {
                continue;
            }

            IModelElement parentModel = modelElement.getParent();

            if (parentModel == null || parentModel.getModelType().equals("Frame") || !diagramElementsInCurrentDiagram.contains(parentModel)) {
                switch (modelElement.getModelType()) {
                    case "Package":
                    case "Model":
                    case "System":
                        processChildrenBaseElement((IHasChildrenBaseModelElement) modelElement, diagramElement, 0, null);
                        break;
                    default:
                        processNotMainPackageElement(modelElement, 0, diagramElement, null);
                        break;
                }
            }


            String elementId = modelElement.getId();
            if (elementIds.contains(elementId))
                continue;
            else
                elementIds.add(elementId);
        }
    }

    protected String colorToHex(Color backgroundColor) {
        return "#" + Integer.toHexString(backgroundColor.getRGB()).substring(2).toUpperCase();
    }

    protected void processChildrenBaseElement(IHasChildrenBaseModelElement childrenBaseModelElement, IDiagramElement diagramElement, int nestingLevel, ChildrenBasedElement parentPackage) {
        String modelType = childrenBaseModelElement.getModelType();

        ChildrenBasedElement packageElement;
        if (childrenBaseModelElement.getMasterView() == null) {
            IShapeUIModel diagramElementOfAuxView = getCorrectDiagramElementForAuxiliaryView(childrenBaseModelElement);
            if (diagramElementOfAuxView == null)
                return;

            String color = colorToHex(diagramElementOfAuxView.getFillColor().getColor1());
            String primitiveShapeType = determinePrimitiveShapeType(diagramElementOfAuxView);
            packageElement = createCorrectPackageElement(childrenBaseModelElement.getName(), color, nestingLevel,
                    primitiveShapeType, diagramElementOfAuxView.getX(), modelType);
        } else {
            String color = colorToHex(((IShapeUIModel) childrenBaseModelElement.getMasterView()).getFillColor().getColor1());
            String primitiveShapeType = determinePrimitiveShapeType(((IShapeUIModel) childrenBaseModelElement.getMasterView()));
            packageElement = createCorrectPackageElement(childrenBaseModelElement.getName(), color, nestingLevel,
                    primitiveShapeType, childrenBaseModelElement.getMasterView().getX(), modelType);
        }

        processStereotypesOfChildrenBaseModelElement(childrenBaseModelElement, packageElement);

        if (parentPackage == null)
            elements.add(packageElement);
        else
            parentPackage.getPackages().add(packageElement);

        for (int i = 0; i < diagramElement.childrenCount(); i++) {
            IShapeUIModel childByIndex = diagramElement.getChildAt(i);
            String elementId = childByIndex.getId();
            if (elementIds.contains(elementId))
                continue;
            else
                elementIds.add(elementId);
            processNotMainPackageElement(childByIndex.getModelElement(), nestingLevel + 1, childByIndex, packageElement);
        }

    }

    protected void processNote(INOTE iNote, int nestingLevel, ChildrenBasedElement parentPackage) {
        String alias;
        String description = iNote.getDescription();

        if (iNote.getName() == null || iNote.getName().equals("")) {
            List<IDiagramElement> diagramElementsInCurrentDiagram = ExportDiagramInfo.getInstance().getDiagramElementsInCurrentDiagram();

            boolean shouldCheckAllDiagramElementNames = true;
            alias = "N" + notesCount++;
            while (shouldCheckAllDiagramElementNames) {
                shouldCheckAllDiagramElementNames = false;
                for (IDiagramElement diagramElement : diagramElementsInCurrentDiagram) {
                    if (diagramElement.getModelElement() != null &&
                            diagramElement.getModelElement().getName() != null &&
                            diagramElement.getModelElement().getName().equals(alias)) {
                        alias = "N" + notesCount++;
                        shouldCheckAllDiagramElementNames = true;
                        break;
                    }
                }
            }

            iNote.addStereotype("$alias: " + alias);
        } else {
            alias = iNote.getName();
            int i = 0;
            while (CommonElementFinder.findNoteByAlias(elements, alias) != null) {
                alias += i++;
            }
        }
        NoteElement noteElement;

        if (iNote.getMasterView() == null) {
            IStereotype[] stereotypeIterator = iNote.toStereotypeModelArray();
            IShapeUIModel diagramElementOfAuxView = getCorrectDiagramElementForAuxiliaryViewAndHideAlias(iNote, stereotypeIterator);
            if (diagramElementOfAuxView == null)
                return;

            String color = colorToHex(diagramElementOfAuxView.getFillColor().getColor1());
            noteElement = new NoteElement(alias, description, color, nestingLevel, iNote.getId(), diagramElementOfAuxView.getX());
        } else {
            IStereotype[] stereotypeIterator = iNote.toStereotypeModelArray();
            hideAliasStereotype((IShapeUIModel) iNote.getMasterView(), stereotypeIterator);
            String color = colorToHex(((IShapeUIModel) iNote.getMasterView()).getFillColor().getColor1());
            noteElement = new NoteElement(alias, description, color, nestingLevel, iNote.getId(), iNote.getMasterView().getX());
        }


        if (parentPackage == null)
            elements.add(noteElement);
        else
            parentPackage.getNotes().add(noteElement);
    }

    protected IShapeUIModel getCorrectDiagramElementForAuxiliaryView(IModelElement modelElement) {
        IShapeUIModel diagramElementOfAuxView = null;
        for (IDiagramElement diagramElement : modelElement.getDiagramElements()) {
            if (ExportDiagramInfo.getInstance().getDiagramElementsInCurrentDiagram().contains(diagramElement)) {
                diagramElementOfAuxView = (IShapeUIModel) diagramElement;
                break;
            }
        }
        return diagramElementOfAuxView;
    }


    protected IShapeUIModel getCorrectDiagramElementForAuxiliaryViewAndHideAlias(IModelElement modelElement, IStereotype[] stereotypeIterator) {
        IShapeUIModel diagramElementOfAuxView = null;
        for (IDiagramElement diagramElement : modelElement.getDiagramElements()) {
            hideAliasStereotype((IShapeUIModel) diagramElement, stereotypeIterator);
            if (ExportDiagramInfo.getInstance().getDiagramElementsInCurrentDiagram().contains(diagramElement)) {
                diagramElementOfAuxView = (IShapeUIModel) diagramElement;
                break;
            }
        }
        return diagramElementOfAuxView;
    }

    protected void processStereotypesOfChildrenBaseModelElement(IHasChildrenBaseModelElement childrenBaseModelElement, ChildrenBasedElement packageElement) {
        switch (childrenBaseModelElement.getModelType()) {
            case "Package":
                processStereotypesOfPackage((IPackage) childrenBaseModelElement, packageElement);
                break;
            case "System":
                processStereotypesOfSystem((ISystem) childrenBaseModelElement, packageElement);
                break;
            case "Model":
                processStereotypesOfModel((IModel) childrenBaseModelElement, packageElement);
                break;
            case "CombinedFragment":
                processStereotypesOfCombinedFragment((ICombinedFragment) childrenBaseModelElement, packageElement);
                break;
            default:
                throw new RuntimeException("Unknown ChildrenBaseModelElement");
        }
    }

    private void processStereotypesOfCombinedFragment(ICombinedFragment iCombinedFragment, ChildrenBasedElement packageElement) {
        processStereotypesArray(iCombinedFragment.toStereotypeArray(), packageElement);
    }

    private void processStereotypesOfPackage(IPackage iPackage, ChildrenBasedElement packageElement) {
        processStereotypesArray(iPackage.toStereotypeArray(), packageElement);
    }

    private void processStereotypesOfSystem(ISystem iSystem, ChildrenBasedElement packageElement) {
        processStereotypesArray(iSystem.toStereotypeArray(), packageElement);
    }

    private void processStereotypesOfModel(IModel iModel, ChildrenBasedElement packageElement) {
        processStereotypesArray(iModel.toStereotypeArray(), packageElement);
    }

    private void processStereotypesArray(String[] stereotypes, ChildrenBasedElement packageElement) {
        if (stereotypes == null)
            return;
        for (String stereotype : stereotypes) {
            if (stereotype.startsWith("$alias: "))
                packageElement.setAlias(stereotype.substring(8));
            else
                packageElement.getStereotypes().add(stereotype);
        }
    }


    protected void findAliasInSubPackages(String idOfSearchedElement, ArrayList<ChildrenBasedElement> packageElements) {
        for (ChildrenBasedElement packageElement : packageElements) {
            List<NoteElement> onlyNotes = packageElement.getNotes()
                    .stream()
                    .filter(note -> note.getModelId().equals(idOfSearchedElement)).collect(Collectors.toList());
            for (NoteElement noteElement : onlyNotes) {
                if (noteElement.getModelId().equals(idOfSearchedElement))
                    foundAlias = noteElement.getAlias();
            }

            if (packageElement instanceof UseCasePackageElement) {
                List<ActorElement> onlyActors = ((UseCasePackageElement) packageElement).getActors()
                        .stream()
                        .filter(actor -> actor.getModelId().equals(idOfSearchedElement)).collect(Collectors.toList());
                for (ActorElement actorElement : onlyActors) {
                    if (actorElement.getModelId().equals(idOfSearchedElement))
                        foundAlias = actorElement.getAlias();
                }

                List<UseCaseElement> onlyUseCases = ((UseCasePackageElement) packageElement).getUseCases()
                        .stream()
                        .filter(useCase -> useCase.getModelId().equals(idOfSearchedElement)).collect(Collectors.toList());
                for (UseCaseElement useCaseElement : onlyUseCases) {
                    if (useCaseElement.getModelId().equals(idOfSearchedElement))
                        foundAlias = useCaseElement.getAlias();
                }
            }
            findAliasInSubPackages(idOfSearchedElement, packageElement.getPackages());
        }
    }

    protected String getAlias(String idOfSearchedElement) {
        Iterator<Element> iHasAliasIterator = elements.stream()
                .filter(elem -> elem instanceof IHasAlias).iterator();

        while (iHasAliasIterator.hasNext()) {
            IHasAlias elementWithAlias = (IHasAlias) iHasAliasIterator.next();
            if (elementWithAlias.getModelId().equals(idOfSearchedElement))
                return elementWithAlias.getAlias();
        }

        List<Element> onlyPackageElements = elements.stream()
                .filter(elem -> elem instanceof ChildrenBasedElement).collect(Collectors.toList());
        ArrayList<ChildrenBasedElement> outputPackageElements = new ArrayList<>();
        onlyPackageElements.forEach(elem -> outputPackageElements.add((ChildrenBasedElement) elem));

        findAliasInSubPackages(idOfSearchedElement, outputPackageElements);
        if (foundAlias.equals(""))
            throw new RuntimeException("Incorrectly saved element");
        return foundAlias;
    }

    protected void processRelationshipElements(IModelElement element) {
        if (element.getModelType().equals("Association")) {
            IAssociation iAssociation = (IAssociation) element;
            processAssociation(iAssociation);
        } else if (element.getModelType().equals("AssociationClass")) {
            IAssociationClass iAssociationClass = (IAssociationClass) element;
            processAssociationClass(iAssociationClass);
        } else {
            ISimpleRelationship relationship;
            try {
                relationship = (ISimpleRelationship) element;
            } catch (Exception e) {
                return;
            }
            String name = relationship.getName();
            processSimpleRelationship(relationship.getFrom(), relationship.getTo(), relationship, name);
        }
    }

    private void processAssociationClass(IAssociationClass iAssociationClass) {
        IAssociation fromAssociation = (IAssociation) iAssociationClass.getFrom();
        String assocFromName = fromAssociation.getFrom().getName();
        String assocToName = fromAssociation.getTo().getName();
        IModelElement toElement = iAssociationClass.getTo();

        AssociationClassElement element = new AssociationClassElement(iAssociationClass.getName(), assocFromName, assocToName, toElement.getName(), iAssociationClass.getMasterView().getX());
        elements.add(element);
    }

    private void processSimpleRelationship(IModelElement fromElement, IModelElement toElement, ISimpleRelationship relationship, String name) {
        final String fromName = getRelationshipEndName(fromElement);
        final String toName = getRelationshipEndName(toElement);
        SimpleRelationshipElement element = new SimpleRelationshipElement(name, fromName, toName, relationship);
        elements.add(element);
    }

    private String getRelationshipEndName(IModelElement endElement) {
        String alias = getElementAlias(endElement);
        String endName;
        if (alias != null)
            endName = alias;
        else
            endName = endElement.getName();


        if (endElement instanceof IUseCase && alias == null) {
            endName = "(" + endName + ")";
        } else if (endElement instanceof IActor && alias == null) {
            endName = ":" + endName + ":";
        } else if (endName.contains(" "))
            endName = "\"" + endName + "\"";

        return endName;

    }

    private String getElementAlias(IModelElement endElement) {
        IStereotype[] iStereotypesOfElement = endElement.toStereotypeModelArray();
        if (iStereotypesOfElement == null)
            return null;

        for (int i = 0; i < iStereotypesOfElement.length; i++) {
            IStereotype iStereotype = iStereotypesOfElement[i];
            if (iStereotype.getName().startsWith("$alias: "))
                return iStereotype.getName().substring(8);
        }
        return null;
    }

    private void processAssociation(IAssociation iAssociation) {
        IModelElement fromElement = iAssociation.getFrom();
        IModelElement toElement = iAssociation.getTo();
        IAssociationEnd fromEnd = (IAssociationEnd) iAssociation.getFromEnd();
        IAssociationEnd toEnd = (IAssociationEnd) iAssociation.getToEnd();

        String fromAggregationKind = fromEnd.getAggregationKind();
        String toAggregationKind = toEnd.getAggregationKind();

        String fromMultiplicity = "";
        String toMultiplicity = "";
        if (!fromEnd.getMultiplicity().equals("Unspecified"))
            fromMultiplicity = fromEnd.getMultiplicity();
        if (!toEnd.getMultiplicity().equals("Unspecified"))
            toMultiplicity = toEnd.getMultiplicity();

        int fromNavigable = fromEnd.getNavigable();
        int toNavigable = toEnd.getNavigable();

        final String fromName = getRelationshipEndName(fromElement);
        final String toName = getRelationshipEndName(toElement);

        AssociationElement associationElement = new AssociationElement(iAssociation.getName(), fromName,
                toName, fromAggregationKind, toAggregationKind, fromMultiplicity, toMultiplicity, fromNavigable, toNavigable);

        elements.add(associationElement);
    }

    private String determinePrimitiveShapeType(IShapeUIModel shapeUIModel) {
        int primitiveShapeTypeAsNumber = shapeUIModel.getPrimitiveShapeType();
        if (primitiveShapeTypeAsNumber == 1)
            return "rectangle";
        else
            return "package";
    }

}
