package pluginmain.exports.diagramexporters;

import com.vp.plugin.ViewManager;
import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.model.*;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;
import pluginmain.exports.filewriters.plantuml.PlantUmlWriter;
import pluginmain.exports.filewriters.plantuml.UseCaseDiagramPlantUmlWriter;

import java.io.IOException;

public class UseCaseDiagramExporter extends DiagramExporter {

    @Override
    protected PlantUmlWriter createCorrectPlantUmlWriter(ViewManager viewManager, String chosenDirectoryPath) throws IOException {
        return new UseCaseDiagramPlantUmlWriter(chosenDirectoryPath, viewManager, elements);
    }

    @Override
    protected ChildrenBasedElement createCorrectPackageElement(String name, String color, int nestingLevel, String shape, int startCoordX, String modelType) {
        return new UseCasePackageElement(name, color, nestingLevel, shape, startCoordX, modelType);
    }

    protected void processNotMainPackageElement(IModelElement element, int nestingLevel, IDiagramElement diagramElement, ChildrenBasedElement parentPackage) {
        switch (element.getModelType()) {
            case "UseCase":
                processUseCase((IUseCase) element, nestingLevel, (UseCasePackageElement) parentPackage);
                break;

            case "Actor":
                processActor((IActor) element, nestingLevel, (UseCasePackageElement) parentPackage);
                break;

            case "Package":
            case "System":
            case "Model":
                processChildrenBaseElement((IHasChildrenBaseModelElement) element, diagramElement, nestingLevel, parentPackage);
                break;

            case "NOTE":
                processNote((INOTE) element, nestingLevel, parentPackage);
                break;
        }
    }

    private void processUseCase(IUseCase iUseCase, int nestingLevel, UseCasePackageElement parentPackage) {
        String color = colorToHex(((IShapeUIModel) iUseCase.getMasterView()).getFillColor().getColor1());
        UseCaseElement useCaseElement = new UseCaseElement(iUseCase.getName(), color, nestingLevel, iUseCase.getId(), iUseCase.getMasterView().getX());
        processStereotypesOfUseCase(iUseCase, useCaseElement);

        if (parentPackage == null)
            elements.add(useCaseElement);
        else
            parentPackage.getUseCases().add(useCaseElement);
    }

    private void processActor(IActor iActor, int nestingLevel, UseCasePackageElement parentPackage) {
        String color = colorToHex(((IShapeUIModel) iActor.getMasterView()).getFillColor().getColor1());
        ActorElement actorElement = new ActorElement(iActor.getName(), color, nestingLevel, null, iActor.getId(), iActor.getMasterView().getX());
        processStereotypesOfActor(iActor, actorElement);

        if (parentPackage == null)
            elements.add(actorElement);
        else
            parentPackage.getActors().add(actorElement);
    }

    private void processStereotypesOfUseCase(IUseCase iUseCase, UseCaseElement useCaseElement) {
        String[] stereotypes = iUseCase.toStereotypeArray();
        if (stereotypes == null)
            return;
        for (String stereotype : stereotypes) {
            if (stereotype.startsWith("$alias: ")) {
                useCaseElement.setAlias(stereotype.substring(8));
                useCaseElement.setName("\"" + useCaseElement.getName() + "\"");
            } else if (!stereotype.equals("UseCase")) {
                useCaseElement.getStereotypes().add(stereotype);
            }
        }
    }

    private void processStereotypesOfActor(IActor iActor, ActorElement actorElement) {
        String[] stereotypes = iActor.toStereotypeArray();
        if (stereotypes == null)
            return;
        for (String stereotype : stereotypes) {
            if (stereotype.startsWith("$alias: ")) {
                actorElement.setAlias(stereotype.substring(8));
                actorElement.setName("\"" + actorElement.getName() + "\"");
            } else
                actorElement.getStereotypes().add(stereotype);
        }
    }

}
