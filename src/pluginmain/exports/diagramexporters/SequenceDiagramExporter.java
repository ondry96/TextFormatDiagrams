package pluginmain.exports.diagramexporters;

import com.vp.plugin.ViewManager;
import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.model.*;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.GroupElement;
import pluginmain.elements.sequencediagram.MessageElement;
import pluginmain.elements.sequencediagram.ParticipantElement;
import pluginmain.exports.filewriters.plantuml.PlantUmlWriter;
import pluginmain.exports.filewriters.plantuml.SequenceDiagramPlantUmlWriter;
import pluginmain.imports.ImportDiagramInfo;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SequenceDiagramExporter extends DiagramExporter {

    @Override
    protected PlantUmlWriter createCorrectPlantUmlWriter(ViewManager viewManager, String chosenDirectoryPath) throws IOException {
        return new SequenceDiagramPlantUmlWriter(chosenDirectoryPath, viewManager, elements);
    }

    @Override
    protected ChildrenBasedElement createCorrectPackageElement(String name, String color, int nestingLevel, String shape, int startCoordX, String modelType) {
        return new GroupCaseElement(name, nestingLevel, shape, startCoordX, modelType);
    }

    @Override
    protected void processNotMainPackageElement(IModelElement element, int nestingLevel, IDiagramElement diagramElement, ChildrenBasedElement parentElement) {
        switch (element.getModelType()) {
            case "CombinedFragment":
                processGroup((ICombinedFragment) element, nestingLevel, parentElement);
                break;
            case "InteractionLifeLine":
                processLifeline((IInteractionLifeLine) element, nestingLevel);
                break;

            case "InteractionActor":
                processActor((IInteractionActor) element, nestingLevel);
                break;

            case "Activation":
                break;

        }
    }

    @Override
    protected void processRelationshipElements(IModelElement element) {
        IMessage iMessage;
        try {
            iMessage = (IMessage) element;
        } catch (ClassCastException e) {
            return;
        }
        processMessage(iMessage, 0, null);
    }

    private void processOperand(IInteractionOperand element, int nestingLevel, ChildrenBasedElement parentGroup, String caseType) {
        String color = colorToHex(((IShapeUIModel) element.getMasterView()).getFillColor().getColor1());
        String primitiveShapeType = "operand";
        String modelType = element.getModelType();

        GroupCaseElement groupCaseElement = (GroupCaseElement) createCorrectPackageElement(element.getName(), color, nestingLevel,
                primitiveShapeType, element.getMasterView().getX(), modelType);

        String constraint = element.getGuard().getConstraint();
        if (constraint != null)
            constraint = constraint.replaceAll("\n|\r\n", " ");
        groupCaseElement.setConstraint(constraint);
        groupCaseElement.setType(caseType);

        if (element.toMessageArray() != null) {
            List<IMessage> iMessages = Arrays.asList(element.toMessageArray());
            iMessages.stream().forEach(iMessage -> processMessage(iMessage, nestingLevel + 1, groupCaseElement));
        }

        for (int i = 0; i < element.childCount(); i++) {
            IModelElement childByIndex = element.getChildByIndex(i);
            String elementId = childByIndex.getId();
            if (elementIds.contains(elementId))
                continue;
            else
                elementIds.add(elementId);
            processNotMainPackageElement(childByIndex, nestingLevel + 1, getCorrectDiagramElementForAuxiliaryView(element), groupCaseElement);
        }
        ((GroupElement) parentGroup).getGroupCases().add(groupCaseElement);
    }

    private void processMessage(IMessage iMessage, int nestingLevel, GroupCaseElement groupCaseElement) {
        MessageElement newMessage = new MessageElement();
        elementIds.add(iMessage.getId());
        newMessage.setNestingLevel(nestingLevel);
        newMessage.setyCoord(iMessage.getMasterView().getY());

        String nameOfTransitOperation = getNameOfTransitOperation(iMessage);

        if (iMessage.getFrom() != null) {
            if (iMessage.getFrom() instanceof IInteractionLifeLine) {
                ParticipantElement participantElement = new ParticipantElement();
                setParticipantNameWithBaseClassifierIfHasOne(participantElement, (IInteractionLifeLine) iMessage.getFrom());
                newMessage.setFromName(participantElement.getName());
            } else
                newMessage.setFromName(iMessage.getFrom().getName());

        }

        if (iMessage.getTo() != null) {
            if (iMessage.getTo() instanceof IInteractionLifeLine) {
                ParticipantElement participantElement = new ParticipantElement();
                setParticipantNameWithBaseClassifierIfHasOne(participantElement, (IInteractionLifeLine) iMessage.getTo());
                newMessage.setToName(participantElement.getName());
            } else
                newMessage.setToName(iMessage.getTo().getName());
        }

        if (iMessage.getName() != null) {
            if (nameOfTransitOperation != null)
                newMessage.setName(iMessage.getName() + " " + nameOfTransitOperation);
            else
                newMessage.setName(iMessage.getName());
        } else if (nameOfTransitOperation != null) {
            newMessage.setName(nameOfTransitOperation);
        }

        newMessage.setRightArrowPart(">");

        if (iMessage.isAsynchronous())
            newMessage.setRightArrowPart(">>");

        if (iMessage.getActionType() != null) {
            String type = iMessage.getActionType().getName();
            if (type.equals("Destroy"))
                newMessage.setRightArrowPart(">x");
            else if (type.equals("Return"))
                newMessage.setDotted(true);
        }

        if (iMessage.getType() == 2) {
            //it is create message
            Optional<Element> participantOptional = elements.stream().filter(
                    element -> {
                        if (element instanceof ParticipantElement)
                            return (((ParticipantElement) element).getAlias() == null && element.getName().equals(newMessage.getToName())) ||
                                    (((ParticipantElement) element).getAlias() != null && ((ParticipantElement) element).getAlias().equals(newMessage.getToName()));
                        else
                            return false;
                    }).findFirst();
            if (participantOptional.isPresent()) {
                ((ParticipantElement) participantOptional.get()).setCreatedByMessage(true);
            } else {
                //participant was not created yet
                ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage().add(newMessage.getToName());
            }
            newMessage.setCreateMessage(true);

        }

        if (groupCaseElement == null)
            elements.add(newMessage);
        else
            groupCaseElement.getMessageElements().add(newMessage);
    }

    private String getNameOfTransitOperation(IMessage iMessage) {
        IModelElement[] transitsFrom = (IModelElement[]) iMessage.getModelPropertyByName(IModel.PROP_TRANSIT_FROM).getValue();
        IModelElement[] transitsTo = (IModelElement[]) iMessage.getModelPropertyByName(IModel.PROP_TRANSIT_TO).getValue();

        if (transitsFrom.getClass().isArray()) {
            for (IModelElement element : transitsFrom) {
                if (element instanceof IOperation) {
                    return operationSignature((IOperation) element);
                }
            }
        }

        if (transitsTo.getClass().isArray()) {
            for (IModelElement element : transitsTo) {
                if (element instanceof IOperation) {
                    return operationSignature((IOperation) element);
                }
            }
        }
        return null;

    }

    private String operationSignature(IOperation operation) {
        StringBuilder signatureOfOperation = new StringBuilder();
        signatureOfOperation.append(operation.getName());

        int parameterCount = operation.parameterCount();
        signatureOfOperation.append("(");

        int i = 0;
        for (IParameter parameter : operation.toParameterArray()) {
            signatureOfOperation.append(parameter.getName());
            if (i != parameterCount - 1)
                signatureOfOperation.append(",");
        }

        signatureOfOperation.append(")");

        String returnTypeAsString = operation.getReturnTypeAsString();
        if (returnTypeAsString != null && !returnTypeAsString.equals(""))
            signatureOfOperation.append(" : ").append(returnTypeAsString);

        return signatureOfOperation.toString();
    }

    private void processGroup(ICombinedFragment iFragment, int nestingLevel, ChildrenBasedElement parentElement) {
        String color = colorToHex(((IShapeUIModel) iFragment.getMasterView()).getFillColor().getColor1());
        GroupElement groupElement = new GroupElement(iFragment.getName(), color, nestingLevel, iFragment.getMasterView().getX());

        boolean isFirstOperand = true;
        for (int i = 0; i < iFragment.childCount(); i++) {
            IModelElement childByIndex = iFragment.getChildByIndex(i);
            if (childByIndex instanceof IInteractionOperand) {
                if (isFirstOperand) {
                    processOperand((IInteractionOperand) childByIndex, nestingLevel, groupElement, iFragment.getInteractionOperator());
                    isFirstOperand = false;
                } else
                    processOperand((IInteractionOperand) childByIndex, nestingLevel, groupElement, "else");

            }
        }

        if (parentElement != null)
            ((GroupCaseElement) parentElement).getGroupElements().add(groupElement);
        else
            elements.add(groupElement);
    }

    private void processActor(IInteractionActor iInteractionActor, int nestingLevel) {
        String color = colorToHex(((IShapeUIModel) iInteractionActor.getMasterView()).getFillColor().getColor1());
        ParticipantElement participantElement = new ParticipantElement();
        participantElement.setName(iInteractionActor.getName());
        participantElement.setBackgroundColor(color);
        participantElement.setNestingLevel(nestingLevel);
        participantElement.setStartCoordX(iInteractionActor.getMasterView().getX());
        String[] stereotypes = iInteractionActor.toStereotypesArray();
        for (String stereotype : stereotypes) {
            participantElement.getStereotypes().add(stereotype);
        }
        participantElement.getStereotypes().add("actor");

        if (ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage().contains(participantElement.getName()))
            participantElement.setCreatedByMessage(true);

        elements.add(participantElement);
    }

    private void processLifeline(IInteractionLifeLine iInteractionLifeLine, int nestingLevel) {
        String color = colorToHex(((IShapeUIModel) iInteractionLifeLine.getMasterView()).getFillColor().getColor1());
        ParticipantElement participantElement = new ParticipantElement();
        setParticipantNameWithBaseClassifierIfHasOne(participantElement, iInteractionLifeLine);

        participantElement.setBackgroundColor(color);
        participantElement.setNestingLevel(nestingLevel);
        participantElement.setStartCoordX(iInteractionLifeLine.getMasterView().getX());
        String[] stereotypes = iInteractionLifeLine.toStereotypesArray();
        for (String stereotype : stereotypes) {
            participantElement.getStereotypes().add(stereotype);
        }

        if (ImportDiagramInfo.getInstance().getParticipantsCreatedByMessage().contains(participantElement.getName()))
            participantElement.setCreatedByMessage(true);

        elements.add(participantElement);
    }

    private void setParticipantNameWithBaseClassifierIfHasOne(ParticipantElement participantElement, IInteractionLifeLine iInteractionLifeLine) {
        String name = iInteractionLifeLine.getName();
        String baseClassifier = iInteractionLifeLine.getBaseClassifierAsString();
        boolean hasName = name != null && !name.equals("");
        boolean hasBaseClassifier = baseClassifier != null && !baseClassifier.equals("");
        if (hasName && hasBaseClassifier)
            participantElement.setName("\"" + name + ": " + baseClassifier + "\"");
        else if (hasName)
            if (name.contains(" "))
                participantElement.setName("\"" + name + "\"");
            else
                participantElement.setName(name);
        else if (hasBaseClassifier)
            participantElement.setName("\": " + baseClassifier + "\"");
    }

}
