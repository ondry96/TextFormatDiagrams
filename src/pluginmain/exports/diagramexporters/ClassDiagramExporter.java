package pluginmain.exports.diagramexporters;

import com.vp.plugin.ViewManager;
import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.diagram.IShapeUIModel;
import com.vp.plugin.model.*;
import pluginmain.elements.classdiagram.AttributeElement;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.classdiagram.OperationElement;
import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.exports.filewriters.plantuml.ClassDiagramPlantUmlWriter;
import pluginmain.exports.filewriters.plantuml.PlantUmlWriter;

import java.io.IOException;
import java.util.Iterator;

public class ClassDiagramExporter extends DiagramExporter {

    @Override
    protected PlantUmlWriter createCorrectPlantUmlWriter(ViewManager viewManager, String chosenDirectoryPath) throws IOException {
        return new ClassDiagramPlantUmlWriter(chosenDirectoryPath, viewManager, elements);
    }

    @Override
    protected ChildrenBasedElement createCorrectPackageElement(String name, String color, int nestingLevel, String shape, int startCoordX, String modelType) {
        return new ClassPackageElement(name, color, nestingLevel, shape, startCoordX, modelType);
    }

    @Override
    protected void processNotMainPackageElement(IModelElement element, int nestingLevel, IDiagramElement diagramElement, ChildrenBasedElement classPackageElement) {
        switch (element.getModelType()) {
            case "Class":
                processClass((IClass) element, nestingLevel, (ClassPackageElement) classPackageElement);
                break;

            case "Package":
            case "Model":
                processChildrenBaseElement((IHasChildrenBaseModelElement) element, diagramElement, nestingLevel, classPackageElement);
                break;

            case "NOTE":
                processNote((INOTE) element, nestingLevel, classPackageElement);
                break;
        }

    }

    private void processClass(IClass iClass, int nestingLevel, ClassPackageElement parentPackage) {
        ClassElement classElement;
        String color;
        boolean isEnum = iClass.hasStereotype("enumeration") || iClass.hasStereotype("Enumeration") ||
                iClass.hasStereotype("Enum") || iClass.hasStereotype("enum");
        if (iClass.getMasterView() == null) {
            IShapeUIModel diagramElementOfAuxView = getCorrectDiagramElementForAuxiliaryView(iClass);
            if (diagramElementOfAuxView == null)
                return;
            color = colorToHex(diagramElementOfAuxView.getFillColor().getColor1());
            classElement = new ClassElement(iClass.getName(), color, isEnum, iClass.getVisibility(), nestingLevel, diagramElementOfAuxView.getX());

        } else {
            color = colorToHex(((IShapeUIModel) iClass.getMasterView()).getFillColor().getColor1());
            classElement = new ClassElement(iClass.getName(), color, isEnum, iClass.getVisibility(), nestingLevel, iClass.getMasterView().getX());

        }

        processGenericsTemplateParameters(iClass, classElement);
        processStereotypes(iClass, classElement);
        if (classElement.isEnum()) {
            processEnumerationLiterals(iClass.enumerationLiteralIterator(), classElement);
        }
        processClassAttributes(iClass.attributeIterator(), classElement);
        processClassOperations(iClass.operationIterator(), classElement);

        if (parentPackage == null)
            elements.add(classElement);
        else
            parentPackage.getClasses().add(classElement);
    }

    private void processGenericsTemplateParameters(IClass iClass, ClassElement classElement) {
        ITemplateParameter[] templateParameters = iClass.toTemplateParameterArray();
        if (templateParameters == null)
            return;
        for (ITemplateParameter parameter : templateParameters) {
            classElement.getTemplateParameters().add(parameter.getName());
        }
    }

    private void processStereotypes(IClass iClass, ClassElement classElement) {
        String[] stereotypes = iClass.toStereotypeArray();
        if (stereotypes == null)
            return;
        for (String stereotype : stereotypes) {
            if (stereotype.startsWith("$alias: ")) {
                classElement.setAlias(stereotype.substring(8));
                classElement.setName("\"" + classElement.getName() + "\"");
            } else
                classElement.getStereotypes().add(stereotype);
        }
    }

    private void processEnumerationLiterals(Iterator enumerationLiteralIterator, ClassElement classElement) {
        while (enumerationLiteralIterator.hasNext()) {
            IEnumerationLiteral enumerationLiteral = (IEnumerationLiteral) enumerationLiteralIterator.next();
            classElement.getEnumerationLiterals().add(enumerationLiteral.getName());
            classElement.setHasBody(true);
        }
    }

    private void processClassOperations(Iterator operationIterator, ClassElement classElement) {
        while (operationIterator.hasNext()) {
            IOperation operation = (IOperation) operationIterator.next();
            boolean isStatic = operation.getScope().equals("classifier");
            String parameters = "";
            for (int i = 0; i < operation.parameterCount(); i++) {
                IParameter parameter = operation.getParameterByIndex(i);
                parameters = parameters.concat(parameter.getName());
                if (i != operation.parameterCount() - 1)
                    parameters = parameters.concat(", ");
            }
            OperationElement operationElement = new OperationElement(operation.getName(), operation.getVisibility(),
                    operation.isAbstract(), isStatic, operation.getReturnTypeAsString());

            if (!parameters.equals(""))
                operationElement.setParameters(parameters);
            classElement.getOperations().add(operationElement);
            classElement.setHasBody(true);
        }
    }

    private void processClassAttributes(Iterator attributeIterator, ClassElement classElement) {
        while (attributeIterator.hasNext()) {
            IAttribute attribute = (IAttribute) attributeIterator.next();
            boolean isStatic = attribute.getScope().equals("classifier");
            AttributeElement attributeElement = new AttributeElement(attribute.getName(), attribute.getVisibility(),
                    attribute.isAbstract(), isStatic, attribute.getTypeAsString(), attribute.getInitialValue());
            classElement.getAttributes().add(attributeElement);
            classElement.setHasBody(true);
        }
    }

}
