package pluginmain.exports;

import com.vp.plugin.diagram.IDiagramElement;

import java.util.ArrayList;
import java.util.List;

public class ExportDiagramInfo {


    private static ExportDiagramInfo instance = new ExportDiagramInfo();
    private List<IDiagramElement> diagramElementsInCurrentDiagram = new ArrayList<>();

    private ExportDiagramInfo() {
    }

    public static ExportDiagramInfo getInstance() {
        return instance;
    }

    public List<IDiagramElement> getDiagramElementsInCurrentDiagram() {
        return diagramElementsInCurrentDiagram;
    }

    public void setDiagramElementsInCurrentDiagram(List<IDiagramElement> diagramElementsInCurrentDiagram) {
        this.diagramElementsInCurrentDiagram = diagramElementsInCurrentDiagram;
    }

    public void reset() {
        diagramElementsInCurrentDiagram = new ArrayList<>();
    }
}
