package pluginmain.exceptions;

public class UnsupportedDiagramException extends Exception {
    public UnsupportedDiagramException() {
    }

    public UnsupportedDiagramException(String message) {
        super(message);
    }

    public UnsupportedDiagramException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedDiagramException(Throwable cause) {
        super(cause);
    }

    public UnsupportedDiagramException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
