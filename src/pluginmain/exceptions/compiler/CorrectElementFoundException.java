package pluginmain.exceptions.compiler;

import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.Element;

public class CorrectElementFoundException extends Throwable {

    private Element element;
    private ChildrenBasedElement parentPackageElement;

    public CorrectElementFoundException(Element element, ChildrenBasedElement packageClassPackageElement) {
        this.element = element;
        this.parentPackageElement = packageClassPackageElement;
    }

    public Element getElement() {
        return element;
    }

    public void setElement(Element element) {
        this.element = element;
    }

    public ChildrenBasedElement getParentPackageElement() {
        return parentPackageElement;
    }

    public void setParentPackageElement(ChildrenBasedElement parentPackageElement) {
        this.parentPackageElement = parentPackageElement;
    }
}
