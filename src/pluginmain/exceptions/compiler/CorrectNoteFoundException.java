package pluginmain.exceptions.compiler;

import pluginmain.elements.common.ChildrenBasedElement;

public class CorrectNoteFoundException extends Throwable {

    private int indexOfNote;
    private ChildrenBasedElement parentPackageElement;

    public CorrectNoteFoundException(int indexOfNote, ChildrenBasedElement packageClassPackageElement) {
        this.indexOfNote = indexOfNote;
        this.parentPackageElement = packageClassPackageElement;
    }

    public int getIndexOfNote() {
        return indexOfNote;
    }

    public void setIndexOfNote(int indexOfNote) {
        this.indexOfNote = indexOfNote;
    }

    public ChildrenBasedElement getParentPackageElement() {
        return parentPackageElement;
    }

    public void setParentPackageElement(ChildrenBasedElement parentPackageElement) {
        this.parentPackageElement = parentPackageElement;
    }
}
