package pluginmain.exceptions.compiler;

import pluginmain.elements.sequencediagram.GroupCaseElement;

public class CorrectGroupCaseFoundException extends Throwable {

    private GroupCaseElement foundGroupCase;

    public CorrectGroupCaseFoundException(GroupCaseElement foundGroupCase) {
        this.foundGroupCase = foundGroupCase;
    }

    public GroupCaseElement getFoundGroupCase() {
        return foundGroupCase;
    }

    public void setFoundGroupCase(GroupCaseElement foundGroupCase) {
        this.foundGroupCase = foundGroupCase;
    }
}
