package pluginmain.exceptions.compiler;

import pluginmain.elements.common.ChildrenBasedElement;

public class CorrectPackageFoundException extends Throwable {

    private ChildrenBasedElement packageClassPackageElement;

    public CorrectPackageFoundException(ChildrenBasedElement packageClassPackageElement) {
        this.packageClassPackageElement = packageClassPackageElement;
    }

    public ChildrenBasedElement getPackageElement() {
        return packageClassPackageElement;
    }

    public void setPackageElement(ChildrenBasedElement packageClassPackageElement) {
        this.packageClassPackageElement = packageClassPackageElement;
    }
}
