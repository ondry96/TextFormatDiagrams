package pluginmain.exceptions.compiler;

public class NameAndAliasStartsWithSpecialSymbolsException extends Throwable {

    public NameAndAliasStartsWithSpecialSymbolsException() {
    }

    public NameAndAliasStartsWithSpecialSymbolsException(String message) {
        super(message);
    }
}
