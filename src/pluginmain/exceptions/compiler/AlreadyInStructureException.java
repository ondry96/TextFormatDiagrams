package pluginmain.exceptions.compiler;

import pluginmain.elements.common.Element;

public class AlreadyInStructureException extends Throwable {
    private Element foundedClassByAlias;

    public AlreadyInStructureException(Element foundedClassByAlias) {
        this.foundedClassByAlias = foundedClassByAlias;
    }

    public AlreadyInStructureException(String message) {
        super(message);
    }

    public Element getFoundedClassByAlias() {
        return foundedClassByAlias;
    }
}
