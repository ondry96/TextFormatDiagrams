package pluginmain.exceptions.compiler;

public class CannotAttachNoteException extends Throwable {

    public CannotAttachNoteException() {
    }

    public CannotAttachNoteException(String message) {
        super(message);
    }

    public CannotAttachNoteException(String message, Throwable cause) {
        super(message, cause);
    }

    public CannotAttachNoteException(Throwable cause) {
        super(cause);
    }

    public CannotAttachNoteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}


