package pluginmain.exceptions.compiler;

public class SemanticMultipleKeywordException extends Exception {
    public SemanticMultipleKeywordException() {
    }

    public SemanticMultipleKeywordException(String message) {
        super(message);
    }

    public SemanticMultipleKeywordException(String message, Throwable cause) {
        super(message, cause);
    }

    public SemanticMultipleKeywordException(Throwable cause) {
        super(cause);
    }

    public SemanticMultipleKeywordException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
