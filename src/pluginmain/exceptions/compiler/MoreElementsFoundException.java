package pluginmain.exceptions.compiler;

public class MoreElementsFoundException extends Throwable {

    public MoreElementsFoundException() {
    }

    public MoreElementsFoundException(String message) {
        super(message);
    }

    public MoreElementsFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public MoreElementsFoundException(Throwable cause) {
        super(cause);
    }

    public MoreElementsFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
