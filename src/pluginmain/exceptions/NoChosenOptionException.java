package pluginmain.exceptions;

public class NoChosenOptionException extends Exception {
    public NoChosenOptionException() {
    }

    public NoChosenOptionException(String message) {
        super(message);
    }

    public NoChosenOptionException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoChosenOptionException(Throwable cause) {
        super(cause);
    }

    public NoChosenOptionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
