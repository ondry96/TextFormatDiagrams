package pluginmain.exceptions;

public class NoChosenDiagramException extends Exception {
    public NoChosenDiagramException() {
    }

    public NoChosenDiagramException(String message) {
        super(message);
    }

    public NoChosenDiagramException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoChosenDiagramException(Throwable cause) {
        super(cause);
    }

    public NoChosenDiagramException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
