package pluginmain.exceptions;

public class NoDiagramException extends Exception {
    public NoDiagramException() {
    }

    public NoDiagramException(String message) {
        super(message);
    }

    public NoDiagramException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoDiagramException(Throwable cause) {
        super(cause);
    }

    public NoDiagramException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
