package pluginmain.exceptions;

public class UnsupportedTextFormatException extends Exception {

    public UnsupportedTextFormatException() {
    }

    public UnsupportedTextFormatException(String message) {
        super(message);
    }

    public UnsupportedTextFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public UnsupportedTextFormatException(Throwable cause) {
        super(cause);
    }

    public UnsupportedTextFormatException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
