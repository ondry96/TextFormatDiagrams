package pluginmain.elements.classdiagram;

import pluginmain.elements.common.HasStereotypeElement;
import pluginmain.elements.common.IHasAlias;

import java.util.ArrayList;

public class ClassElement extends HasStereotypeElement implements IHasAlias {

    private ArrayList<String> templateParameters = new ArrayList<>();
    private ArrayList<String> enumerationLiterals = new ArrayList<>();
    private ArrayList<AttributeElement> attributes = new ArrayList<>();
    private ArrayList<OperationElement> operations = new ArrayList<>();
    private boolean isEnum;
    private String visibility;
    private boolean hasBody;
    private String alias;
    private String modelId;

    public ClassElement() {
    }

    public ClassElement(String name) {
        this.name = name;
    }

    public ClassElement(String name, String color, boolean isEnum, String visibility, int nestingLevel, int startCoordX) {
        this.name = name;
        this.backgroundColorInHex = color;
        this.isEnum = isEnum;
        this.visibility = visibility;
        this.nestingLevel = nestingLevel;
        this.hasBody = false;
        this.startCoordX = startCoordX;
    }

    public ArrayList<String> getTemplateParameters() {
        return templateParameters;
    }

    public void setTemplateParameters(ArrayList<String> templateParameters) {
        this.templateParameters = templateParameters;
    }

    public ArrayList<String> getEnumerationLiterals() {
        return enumerationLiterals;
    }

    public void setEnumerationLiterals(ArrayList<String> enumerationLiterals) {
        this.enumerationLiterals = enumerationLiterals;
    }

    public ArrayList<AttributeElement> getAttributes() {
        return attributes;
    }

    public void setAttributes(ArrayList<AttributeElement> attributes) {
        this.attributes = attributes;
    }

    public ArrayList<OperationElement> getOperations() {
        return operations;
    }

    public void setOperations(ArrayList<OperationElement> operations) {
        this.operations = operations;
    }

    public boolean isEnum() {
        return isEnum;
    }

    public void setEnum(boolean anEnum) {
        isEnum = anEnum;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public boolean isHasBody() {
        return hasBody;
    }

    public void setHasBody(boolean hasBody) {
        this.hasBody = hasBody;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
}
