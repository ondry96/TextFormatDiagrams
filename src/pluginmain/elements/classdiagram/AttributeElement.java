package pluginmain.elements.classdiagram;

public class AttributeElement extends ResourceElement {

    private String type;
    private String initialValue;

    public AttributeElement(String name, String visibility, boolean isAbstract, boolean isStatic, String typeAsString, String initialValue) {
        this.name = name;
        this.visibility = visibility;
        this.type = typeAsString;
        this.initialValue = initialValue;
        this.isAbstract = isAbstract;
        this.isStatic = isStatic;
        this.startCoordX = -1;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getInitialValue() {
        return initialValue;
    }

    public void setInitialValue(String initialValue) {
        this.initialValue = initialValue;
    }
}
