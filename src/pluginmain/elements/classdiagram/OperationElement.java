package pluginmain.elements.classdiagram;

public class OperationElement extends ResourceElement {

    private String returnType;
    private String parameters;

    public OperationElement(String name, String visibility, boolean isAbstract, boolean isStatic, String returnType) {
        this.name = name;
        this.visibility = visibility;
        this.isAbstract = isAbstract;
        this.isStatic = isStatic;
        this.returnType = returnType;
        this.startCoordX = -1;
    }

    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
        this.parameters = parameters;
    }

    public String getReturnType() {
        return returnType;
    }

    public void setReturnType(String returnType) {
        this.returnType = returnType;
    }
}
