package pluginmain.elements.classdiagram;

import pluginmain.elements.common.ChildrenBasedElement;

import java.util.ArrayList;

public class ClassPackageElement extends ChildrenBasedElement {

    private ArrayList<ClassElement> classes = new ArrayList<>();

    public ClassPackageElement(String name) {
        this.name = name;
    }

    public ClassPackageElement(String name, String colorInHex, String alias) {
        this.name = name;
        this.backgroundColorInHex = colorInHex;
        this.alias = alias;
    }

    public ClassPackageElement(String name, String colorInHex, int nestingLevel, String shape, int startCoordX, String modelType) {
        this.name = name;
        this.backgroundColorInHex = colorInHex;
        this.nestingLevel = nestingLevel;
        this.shape = shape;
        this.startCoordX = startCoordX;
        this.modelType = modelType;
    }

    public ArrayList<ClassElement> getClasses() {
        return classes;
    }

    public void setClasses(ArrayList<ClassElement> classes) {
        this.classes = classes;
    }

}
