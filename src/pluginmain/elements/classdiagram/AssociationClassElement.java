package pluginmain.elements.classdiagram;

import pluginmain.elements.common.Element;

public class AssociationClassElement extends Element {
    private String associationFromName;
    private String associationToName;
    private String toClassName;

    public AssociationClassElement(String name, String associationFromName, String associationToName, String toClassName, int startCoordX) {
        this.name = name;
        this.associationFromName = associationFromName;
        this.associationToName = associationToName;
        this.toClassName = toClassName;
        this.startCoordX = startCoordX;
    }

    public String getAssociationFromName() {
        return associationFromName;
    }

    public void setAssociationFromName(String associationFromName) {
        this.associationFromName = associationFromName;
    }

    public String getAssociationToName() {
        return associationToName;
    }

    public void setAssociationToName(String associationToName) {
        this.associationToName = associationToName;
    }

    public String getToClassName() {
        return toClassName;
    }

    public void setToClassName(String toClassName) {
        this.toClassName = toClassName;
    }
}
