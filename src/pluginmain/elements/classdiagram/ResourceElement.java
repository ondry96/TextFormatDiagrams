package pluginmain.elements.classdiagram;

import pluginmain.elements.common.Element;

public abstract class ResourceElement extends Element {
    protected String visibility;
    protected boolean isAbstract;
    protected boolean isStatic;

    public boolean isAbstract() {
        return isAbstract;
    }

    public void setAbstract(boolean anAbstract) {
        isAbstract = anAbstract;
    }

    public boolean isStatic() {
        return isStatic;
    }

    public void setStatic(boolean aStatic) {
        isStatic = aStatic;
    }

    public String getVisibility() {
        return visibility;
    }

    public void setVisibility(String visibility) {
        this.visibility = visibility;
    }
}
