package pluginmain.elements.sequencediagram;

import pluginmain.elements.relationships.RelationshipElement;

public class MessageElement extends RelationshipElement {

    private boolean isDotted;
    private String leftArrowPart;
    private String rightArrowPart;
    private boolean isCreateMessage = false;
    private int yCoord;


    public MessageElement() {
    }

    public int getyCoord() {
        return yCoord;
    }

    public void setyCoord(int yCoord) {
        this.yCoord = yCoord;
    }

    public boolean isCreateMessage() {
        return isCreateMessage;
    }

    public void setCreateMessage(boolean createMessage) {
        isCreateMessage = createMessage;
    }

    public boolean isDotted() {
        return isDotted;
    }

    public void setDotted(boolean dotted) {
        isDotted = dotted;
    }

    public String getLeftArrowPart() {
        return leftArrowPart;
    }

    public void setLeftArrowPart(String leftArrowPart) {
        this.leftArrowPart = leftArrowPart;
    }

    public String getRightArrowPart() {
        return rightArrowPart;
    }

    public void setRightArrowPart(String rightArrowPart) {
        this.rightArrowPart = rightArrowPart;
    }
}
