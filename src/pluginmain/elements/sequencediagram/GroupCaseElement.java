package pluginmain.elements.sequencediagram;

import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.IHasAlias;

import java.util.ArrayList;

public class GroupCaseElement extends ChildrenBasedElement implements IHasAlias {

    private String type;
    private String alias;
    private String constraint;
    private GroupElement parentGroup;
    private ArrayList<GroupElement> groupElements = new ArrayList<>();
    private ArrayList<MessageElement> messageElements = new ArrayList<>();

    public GroupCaseElement() {
    }

    public GroupCaseElement(String name, int nestingLevel, String shape, int startCoordX, String modelType, GroupElement parentGroup) {
        this.parentGroup = parentGroup;
        this.name = name;
        this.nestingLevel = nestingLevel;
        this.shape = shape;
        this.startCoordX = startCoordX;
        this.modelType = modelType;
    }

    public GroupCaseElement(String name, int nestingLevel, String shape, int startCoordX, String modelType) {
        this.name = name;
        this.nestingLevel = nestingLevel;
        this.shape = shape;
        this.startCoordX = startCoordX;
        this.modelType = modelType;
    }

    public String getConstraint() {
        return constraint;
    }

    public void setConstraint(String constraint) {
        this.constraint = constraint;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public GroupElement getParentGroup() {
        return parentGroup;
    }

    public void setParentGroup(GroupElement parentGroup) {
        this.parentGroup = parentGroup;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String getModelId() {
        return null;
    }

    public ArrayList<GroupElement> getGroupElements() {
        return groupElements;
    }

    public ArrayList<MessageElement> getMessageElements() {
        return messageElements;
    }
}
