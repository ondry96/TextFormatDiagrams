package pluginmain.elements.sequencediagram;

import pluginmain.elements.common.HasStereotypeElement;
import pluginmain.elements.common.IHasAlias;

import java.util.Objects;

public class ParticipantElement extends HasStereotypeElement implements IHasAlias {

    private String alias;
    private String modelId;
    private String baseClassifier;
    private int order = 0;
    private boolean createdByMessage = false;

    public ParticipantElement() {
    }

    public ParticipantElement(String name) {
        this.name = name;
    }

    public boolean isCreatedByMessage() {
        return createdByMessage;
    }

    public void setCreatedByMessage(boolean createdByMessage) {
        this.createdByMessage = createdByMessage;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getBaseClassifier() {
        return baseClassifier;
    }

    public void setBaseClassifier(String baseClassifier) {
        this.baseClassifier = baseClassifier;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String getModelId() {
        return modelId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ParticipantElement)) return false;
        ParticipantElement element = (ParticipantElement) o;
        return (Objects.equals(name, element.name) && alias == null && element.alias == null) ||
                (alias != null && element.alias != null && alias.equals(element.alias));
    }
}
