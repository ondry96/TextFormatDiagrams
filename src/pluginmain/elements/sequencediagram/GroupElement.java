package pluginmain.elements.sequencediagram;

import pluginmain.elements.common.ChildrenBasedElement;
import pluginmain.elements.common.IHasAlias;

import java.util.ArrayList;

public class GroupElement extends ChildrenBasedElement implements IHasAlias {

    private String alias;
    private ArrayList<GroupCaseElement> groupCases = new ArrayList<>();

    public GroupElement() {
    }

    public GroupElement(String name, String colorInHex, int nestingLevel, int startCoordX) {
        this.name = name;
        this.backgroundColorInHex = colorInHex;
        this.nestingLevel = nestingLevel;
        this.startCoordX = startCoordX;
    }

    public ArrayList<GroupCaseElement> getGroupCases() {
        return groupCases;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String getModelId() {
        return null;
    }
}
