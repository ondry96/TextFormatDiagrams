package pluginmain.elements.common;

import pluginmain.elements.sequencediagram.MessageElement;

import java.util.Comparator;

public class SortMessagesByStartCoordY implements Comparator<Element> {

    @Override
    public int compare(Element e1, Element e2) {
        if (e1 instanceof MessageElement && e2 instanceof MessageElement)
            return ((MessageElement) e1).getyCoord() - ((MessageElement) e2).getyCoord();
        else
            return 0;
    }
}
