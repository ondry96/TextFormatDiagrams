package pluginmain.elements.common;

import java.util.ArrayList;

public abstract class ChildrenBasedElement extends HasStereotypeElement implements IHasAlias {

    protected ArrayList<ChildrenBasedElement> packages = new ArrayList<>();
    protected ArrayList<NoteElement> notes = new ArrayList<>();
    protected String shape;
    protected String modelType;
    protected String alias;
    protected String modelId;


    public ArrayList<ChildrenBasedElement> getPackages() {
        return packages;
    }

    public void setPackages(ArrayList<ChildrenBasedElement> packages) {
        this.packages = packages;
    }

    public ArrayList<NoteElement> getNotes() {
        return notes;
    }

    public void setNotes(ArrayList<NoteElement> notes) {
        this.notes = notes;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public String getModelType() {
        return modelType;
    }

    public void setModelType(String modelType) {
        this.modelType = modelType;
    }

    @Override
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    @Override
    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
}
