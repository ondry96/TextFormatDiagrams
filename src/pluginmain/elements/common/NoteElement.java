package pluginmain.elements.common;

public class NoteElement extends Element implements IHasAlias {
    private String alias;
    private String description;
    private String modelId;

    public NoteElement(String alias, String description, String color, int nestingLevel, String modelId, int startCoordX) {
        this.alias = alias;
        this.description = description;
        this.nestingLevel = nestingLevel;
        this.backgroundColorInHex = color;
        this.modelId = modelId;
        this.startCoordX = startCoordX;
    }

    public NoteElement() {

    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
}
