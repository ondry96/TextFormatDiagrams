package pluginmain.elements.common;

import java.util.ArrayList;

public abstract class HasStereotypeElement extends Element {

    private ArrayList<String> stereotypes = new ArrayList<>();

    public ArrayList<String> getStereotypes() {
        return stereotypes;
    }

    public void setStereotypes(ArrayList<String> stereotypes) {
        this.stereotypes = stereotypes;
    }

}
