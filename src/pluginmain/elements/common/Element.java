package pluginmain.elements.common;

import pluginmain.elements.relationships.RelationshipElement;
import pluginmain.utils.StringModifier;

import java.util.Objects;

public abstract class Element {
    public static final String DEFAULT_COLOR = "#7ACFF5";
    protected String name;
    protected String fullName;
    protected int nestingLevel;
    protected String backgroundColorInHex;
    protected int startCoordX;

    public String getElementType() {
        return this.getClass().getSimpleName();
    }

    /**
     * Returns hexa code if it is not default VP color
     *
     * @return
     */
    public String getBackgroundColor() {
        return backgroundColorInHex;
    }

    public boolean isBackgroundColorDefault() {
        return backgroundColorInHex.equals(DEFAULT_COLOR);
    }

    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColorInHex = backgroundColor;
    }

    public int getNestingLevel() {
        return nestingLevel;
    }

    public void setNestingLevel(int nestingLevel) {
        this.nestingLevel = nestingLevel;
    }

    /**
     * Returns name with quotes if name has multiple words (and not relationship), if has only single word just name
     *
     * @return
     */
    public String getNameWithQuotesIfNeeded() {
        if (!(this instanceof RelationshipElement)) {
            return StringModifier.addQuotesToStartAndEndIfMoreWordsAndDontHaveThem(name);
        }
        return name;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public int getStartCoordX() {
        return startCoordX;
    }

    public void setStartCoordX(int startCoordX) {
        this.startCoordX = startCoordX;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Element)) return false;
        Element element = (Element) o;
        return Objects.equals(name, element.name) && Objects.equals(fullName, element.fullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, fullName);
    }
}

