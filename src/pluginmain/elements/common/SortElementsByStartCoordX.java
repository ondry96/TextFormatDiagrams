package pluginmain.elements.common;

import java.util.Comparator;

public class SortElementsByStartCoordX implements Comparator<Element> {

    @Override
    public int compare(Element e1, Element e2) {
        if (e1.getStartCoordX() == -1)
            return 1;
        if (e2.getStartCoordX() == -1)
            return -1;
        return e1.getStartCoordX() - e2.getStartCoordX();
    }
}
