package pluginmain.elements.common;

import pluginmain.elements.sequencediagram.ParticipantElement;

import java.util.Comparator;

public class SortParticipantsByStartCoordX implements Comparator<Element> {

    @Override
    public int compare(Element e1, Element e2) {
        if (e1 instanceof ParticipantElement && e2 instanceof ParticipantElement)
            return e1.getStartCoordX() - e2.getStartCoordX();
        else
            return 0;
    }
}
