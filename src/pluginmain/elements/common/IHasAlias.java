package pluginmain.elements.common;

public interface IHasAlias {

    String getAlias();

    String getModelId();
}
