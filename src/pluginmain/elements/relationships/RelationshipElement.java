package pluginmain.elements.relationships;

import pluginmain.elements.common.Element;

public abstract class RelationshipElement extends Element {
    protected String fromName;
    protected String toName;

    public String getFromName() {
        return fromName;
    }

    public void setFromName(String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(String toName) {
        this.toName = toName;
    }
}
