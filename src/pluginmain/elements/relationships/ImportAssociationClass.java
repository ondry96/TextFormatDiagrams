package pluginmain.elements.relationships;

import pluginmain.elements.common.Element;

public class ImportAssociationClass extends Element {

    private ImportAssociationElement association;
    private String toClassName;

    public ImportAssociationClass(ImportAssociationElement association, String toClassName, String name) {
        this.association = association;
        this.toClassName = toClassName;
        this.name = name;
    }

    public String getToClassName() {
        return toClassName;
    }

    public void setToClassName(String toClassName) {
        this.toClassName = toClassName;
    }

    public ImportAssociationElement getAssociation() {
        return association;
    }

    public void setAssociation(ImportAssociationElement association) {
        this.association = association;
    }
}
