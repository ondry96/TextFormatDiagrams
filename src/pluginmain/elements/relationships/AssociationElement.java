package pluginmain.elements.relationships;

public class AssociationElement extends RelationshipElement {

    private String fromAggregationKind;
    private String toAggregationKind;
    private String fromMultiplicity;
    private String toMultiplicity;
    private int fromNavigable;
    private int toNavigable;

    public AssociationElement() {
    }

    public AssociationElement(String name, String fromName, String toName, String fromAggregationKind, String toAggregationKind, String fromMultiplicity, String toMultiplicity, int fromNavigable, int toNavigable) {
        this.name = name;
        this.fromName = fromName;
        this.toName = toName;
        this.fromAggregationKind = fromAggregationKind;
        this.toAggregationKind = toAggregationKind;
        this.fromMultiplicity = fromMultiplicity;
        this.toMultiplicity = toMultiplicity;
        this.fromNavigable = fromNavigable;
        this.toNavigable = toNavigable;
        this.startCoordX = -1;
    }

    public String getFromAggregationKind() {
        return fromAggregationKind;
    }

    public void setFromAggregationKind(String fromAggregationKind) {
        this.fromAggregationKind = fromAggregationKind;
    }

    public String getToAggregationKind() {
        return toAggregationKind;
    }

    public void setToAggregationKind(String toAggregationKind) {
        this.toAggregationKind = toAggregationKind;
    }

    public String getFromMultiplicity() {
        return fromMultiplicity;
    }

    public void setFromMultiplicity(String fromMultiplicity) {
        this.fromMultiplicity = fromMultiplicity;
    }

    public String getToMultiplicity() {
        return toMultiplicity;
    }

    public void setToMultiplicity(String toMultiplicity) {
        this.toMultiplicity = toMultiplicity;
    }

    public int getFromNavigable() {
        return fromNavigable;
    }

    public void setFromNavigable(int fromNavigable) {
        this.fromNavigable = fromNavigable;
    }

    public int getToNavigable() {
        return toNavigable;
    }

    public void setToNavigable(int toNavigable) {
        this.toNavigable = toNavigable;
    }
}
