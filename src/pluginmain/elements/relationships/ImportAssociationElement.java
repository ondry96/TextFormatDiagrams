package pluginmain.elements.relationships;

public class ImportAssociationElement extends AssociationElement {
    private boolean isMiddleDotted;

    public ImportAssociationElement() {
        super();
    }

    public ImportAssociationElement(String name, String fromName, String toName, String fromAggregationKind, String toAggregationKind,
                                    String fromMultiplicity, String toMultiplicity, int fromNavigable, int toNavigable, boolean isMiddleDotted) {
        super(name, fromName, toName, fromAggregationKind, toAggregationKind, fromMultiplicity, toMultiplicity, fromNavigable, toNavigable);
        this.isMiddleDotted = isMiddleDotted;
    }

    public boolean isMiddleDotted() {
        return isMiddleDotted;
    }

    public void setMiddleDotted(boolean middleDotted) {
        isMiddleDotted = middleDotted;
    }
}
