package pluginmain.elements.relationships;

import com.vp.plugin.model.ISimpleRelationship;

public class SimpleRelationshipElement extends RelationshipElement {

    private ISimpleRelationship relationship;

    public SimpleRelationshipElement(String name, String fromClassName, String toClassName) {
        this.name = name;
        this.fromName = fromClassName;
        this.toName = toClassName;
        this.startCoordX = -1;
    }

    public SimpleRelationshipElement(String name, String fromClassName, String toClassName, ISimpleRelationship relationship) {
        this.name = name;
        this.fromName = fromClassName;
        this.toName = toClassName;
        this.relationship = relationship;
        this.startCoordX = -1;
    }

    public ISimpleRelationship getRelationship() {
        return relationship;
    }

    public void setRelationship(ISimpleRelationship relationship) {
        this.relationship = relationship;
    }
}
