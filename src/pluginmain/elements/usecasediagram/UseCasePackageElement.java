package pluginmain.elements.usecasediagram;

import pluginmain.elements.common.ChildrenBasedElement;

import java.util.ArrayList;

public class UseCasePackageElement extends ChildrenBasedElement {

    private ArrayList<UseCaseElement> useCases = new ArrayList<>();
    private ArrayList<ActorElement> actors = new ArrayList<>();

    public UseCasePackageElement(String name, String colorInHex, String alias) {
        this.name = name;
        this.backgroundColorInHex = colorInHex;
        this.alias = alias;
    }

    public UseCasePackageElement(String name, String colorInHex, int nestingLevel, String shape, int startCoordX, String modelType) {
        this.name = name;
        this.backgroundColorInHex = colorInHex;
        this.nestingLevel = nestingLevel;
        this.shape = shape;
        this.startCoordX = startCoordX;
        this.modelType = modelType;
    }

    public ArrayList<UseCaseElement> getUseCases() {
        return useCases;
    }

    public void setUseCases(ArrayList<UseCaseElement> useCases) {
        this.useCases = useCases;
    }

    public ArrayList<ActorElement> getActors() {
        return actors;
    }

    public void setActors(ArrayList<ActorElement> actors) {
        this.actors = actors;
    }
}
