package pluginmain.elements.usecasediagram;

import pluginmain.elements.common.HasStereotypeElement;
import pluginmain.elements.common.IHasAlias;

public class ActorElement extends HasStereotypeElement implements IHasAlias {

    private String alias;
    private String modelId;

    public ActorElement() {
    }

    public ActorElement(String name, String color, int nestingLevel, String alias, String modelId, int startCoordX) {
        this.name = name;
        this.backgroundColorInHex = color;
        this.nestingLevel = nestingLevel;
        this.alias = alias;
        this.modelId = modelId;
        this.startCoordX = startCoordX;
    }

    public ActorElement(String name) {
        this.name = name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
}
