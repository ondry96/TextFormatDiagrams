package pluginmain.elements.usecasediagram;

import pluginmain.elements.common.HasStereotypeElement;
import pluginmain.elements.common.IHasAlias;

public class UseCaseElement extends HasStereotypeElement implements IHasAlias {

    private String alias;
    private String modelId;

    public UseCaseElement() {
    }

    public UseCaseElement(String name) {
        this.name = name;
    }

    public UseCaseElement(String name, String color, int nestingLevel, String modelId, int startCoordX) {
        this.name = name;
        this.backgroundColorInHex = color;
        this.nestingLevel = nestingLevel;
        this.modelId = modelId;
        this.startCoordX = startCoordX;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
}
