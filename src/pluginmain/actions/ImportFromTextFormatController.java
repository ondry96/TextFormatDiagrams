package pluginmain.actions;

import antlr.visitors.IDiagramType;
import antlr.visitors.PlantUMLBaseDiagramVisitor;
import com.vp.plugin.ApplicationManager;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.ProjectManager;
import com.vp.plugin.ViewManager;
import com.vp.plugin.action.VPAction;
import com.vp.plugin.action.VPActionController;
import com.vp.plugin.diagram.IClassDiagramUIModel;
import com.vp.plugin.diagram.IDiagramElement;
import com.vp.plugin.diagram.IDiagramUIModel;
import com.vp.plugin.diagram.LayoutOption$Hierarchical;
import com.vp.plugin.diagram.shape.IClassUIModel;
import com.vp.plugin.diagram.shape.ICombinedFragmentUIModel;
import com.vp.plugin.diagram.shape.IInteractionLifeLineUIModel;
import com.vp.plugin.model.IInteractionLifeLine;
import com.vp.plugin.model.IModel;
import com.vp.plugin.model.property.ITransitProperty;
import org.apache.commons.io.FilenameUtils;
import pluginmain.enums.TextFormat;
import pluginmain.exceptions.NoChosenOptionException;
import pluginmain.exceptions.UnsupportedTextFormatException;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.imports.diagramcreators.DiagramCreator;
import pluginmain.imports.diagramimporters.DiagramImporter;
import pluginmain.imports.diagramimporters.PlantUMLDiagramImporter;

import java.io.File;
import java.io.FileNotFoundException;

import static pluginmain.utils.UserInputHandler.*;

public class ImportFromTextFormatController implements VPActionController {

    private String chosenFilePath = null;
    private String diagramName = null;
    private TextFormat chosenFormat = TextFormat.PlantUml;

    public void update(VPAction vpAction) {

    }

    public void performAction(VPAction vpAction) {
        chosenFilePath = null;
        diagramName = null;
        ViewManager viewManager = ApplicationManager.instance().getViewManager();
        DiagramImporter diagramImporter = null;
        try {
            retrieveDataFromUser(viewManager);
            diagramImporter = createCorrectDiagramImporter(chosenFormat);
        } catch (FileNotFoundException e) {
            showMessageToUser(viewManager, "Selected file was not found or it cannot be read.", "File not found or cannot be read");
            return;
        } catch (NoChosenOptionException e) {
            return;
        } catch (UnsupportedTextFormatException e) {
            showMessageToUser(viewManager, e.getMessage(), "Unsupported text format");
            return;
        }

        IDiagramType iDiagramType = diagramImporter.importToInnerStructure(viewManager, chosenFormat, chosenFilePath, diagramName);
        if (iDiagramType == null)
            return;

        DiagramCreator diagramCreator = iDiagramType.getCreator();
        diagramCreator.setInnerStructure(iDiagramType.getElements());
        diagramCreator.setErrors(iDiagramType.getErrorsInGrammar());


        String diagramType = diagramCreator.getDiagramType();
        IDiagramUIModel createdDiagram = diagramCreator.createDiagram(diagramType);

        String errors = processErrorsOfVisitor(iDiagramType.getVisitor());
        if (!errors.isEmpty()) {
            createdDiagram.delete();
            showMessageToUser(viewManager, "Could not create. Following errors occurred while creating diagram:\n" + errors, "Errors occurred");
            return;
        }

        String infoAboutRenaming = processInfoAboutRenaming(diagramCreator);
        if (!infoAboutRenaming.isEmpty()) {
            showMessageToUser(viewManager, "Some elements already existed in another diagram. They were renamed:\n" + infoAboutRenaming, "Elements renamed");
        }
        openAndLayoutDiagram(createdDiagram, diagramType);

    }

    private DiagramImporter createCorrectDiagramImporter(TextFormat chosenFormat) throws UnsupportedTextFormatException {
        DiagramImporter diagramImporter;
        switch (chosenFormat) {
            case PlantUml:
                diagramImporter = new PlantUMLDiagramImporter();
                break;
            default:
                throw new UnsupportedTextFormatException("This format is not supported");
        }
        return diagramImporter;
    }

    private String processInfoAboutRenaming(DiagramCreator diagramCreator) {
        StringBuilder renamesStringBuilder = new StringBuilder();

        for (String renamed : diagramCreator.usedRenames) {
            renamesStringBuilder.append(renamed).append("\n");
        }
        return renamesStringBuilder.toString();
    }

    private String processErrorsOfVisitor(PlantUMLBaseDiagramVisitor pumlVisitor) {
        StringBuilder errorsStringBuilder = new StringBuilder();
        if (pumlVisitor.getErrorsInProgram().size() != 0) {
            errorsStringBuilder.append("Errors in program:\n");
            for (String errorInProgram : pumlVisitor.getErrorsInProgram()) {
                errorsStringBuilder.append(errorInProgram).append("\n");
            }
        }
        if (pumlVisitor.getErrorsInGrammar().size() != 0) {
            errorsStringBuilder.append("Errors in grammar:\n");
            for (String errorInProgram : pumlVisitor.getErrorsInGrammar()) {
                errorsStringBuilder.append(errorInProgram).append("\n");
            }
        }
        return errorsStringBuilder.toString();
    }

    private void openAndLayoutDiagram(IDiagramUIModel createdDiagram, String diagramType) {
        DiagramManager diagramManager = ApplicationManager.instance().getDiagramManager();

        if (diagramType.equals(DiagramManager.DIAGRAM_TYPE_INTERACTION_DIAGRAM)) {
            diagramManager.openDiagram(createdDiagram);
            diagramManager.autoLayout(createdDiagram);
            finalizeLayout(createdDiagram);

        } else {
            LayoutOption$Hierarchical layoutOption;
            layoutOption = (LayoutOption$Hierarchical) diagramManager.createHierarchicalLayoutOption();
            layoutOption.setOrientation(ImportDiagramInfo.getInstance().getOrientation());
            layoutOption.setMinimumConnectorDistance(50);
            layoutOption.setMinimumShapeDistance(80);
            layoutOption.setMinimumLayerDistance(100);
            diagramManager.openAndLayoutDiagram(createdDiagram, layoutOption);

        }
    }

    private void finalizeLayout(IDiagramUIModel createdDiagram) {
        IDiagramElement[] iDiagramElements = createdDiagram.toDiagramElementArray();
        for (IDiagramElement diagramElement : iDiagramElements) {
            diagramElement.resetCaption();
            diagramElement.resetCaptionSize();
            if (diagramElement instanceof IInteractionLifeLineUIModel) {
                IInteractionLifeLine element = (IInteractionLifeLine) diagramElement.getModelElement();
                if (element.getName().contains(":") && !element.getName().equals("")) {
                    int firstColon = element.getName().indexOf(":");
                    element.setBaseClassifier(element.getName().substring(firstColon + 1).trim());
                    if (firstColon == 0)
                        element.setName("");
                    else
                        element.setName(element.getName().substring(0, firstColon).trim());

                    createTransitToInteractionLifeLineIfNeeded(element, element.getBaseClassifierAsString());

                } else {
                    element.setName(element.getName().trim());
                    createTransitToInteractionLifeLineIfNeeded(element, element.getName());
                }
            } else if (diagramElement instanceof ICombinedFragmentUIModel) {
                diagramElement.setY(diagramElement.getY() + 30);
                ((ICombinedFragmentUIModel) diagramElement).fitSize();
            }
        }
    }

    private void createTransitToInteractionLifeLineIfNeeded(IInteractionLifeLine element, String name) {
        ProjectManager projectManager = ApplicationManager.instance().getProjectManager();
        IDiagramUIModel[] iDiagramUIModels = projectManager.getProject().toDiagramArray();

        for (IDiagramUIModel diagramUIModel : iDiagramUIModels) {
            if (diagramUIModel instanceof IClassDiagramUIModel) {
                for (IDiagramElement diagramElement : diagramUIModel.toDiagramElementArray()) {
                    if (diagramElement instanceof IClassUIModel && diagramElement.getModelElement().getName().equals(name)) {
                        ITransitProperty transitProp = (ITransitProperty) element.getModelPropertyByName(IModel.PROP_TRANSIT_FROM);
                        transitProp.addValue(diagramElement.getModelElement());
                        return;
                    }
                }
            }
        }
    }

    private void retrieveDataFromUser(ViewManager viewManager)
            throws FileNotFoundException, NoChosenOptionException {
        chosenFilePath = chooseFile(viewManager);
        ProjectManager projectManager = ApplicationManager.instance().getProjectManager();
        IDiagramUIModel[] iDiagramUIModels = projectManager.getProject().toDiagramArray();

        String processedFileName = chosenFilePath;
        String removedExtension = "";
        File f = new File(processedFileName);
        int pos = f.getName().lastIndexOf(".");
        if (pos != -1) {
            removedExtension = f.getName().substring(pos);
            processedFileName = "imp-" + f.getName().substring(0, pos);
        } else {
            processedFileName = "imp-" + f.getName();
        }

        changeFilenameIfExists(iDiagramUIModels, processedFileName, removedExtension, f);

        chosenFormat = showChooseTextFormatDialog(viewManager, "Select text format of input file for importing");
    }

    private void changeFilenameIfExists(IDiagramUIModel[] iDiagramUIModels, String processedFileName, String removedExtension, File f) {
        for (IDiagramUIModel diagramUIModel : iDiagramUIModels) {
            if (diagramUIModel.getName().equals(processedFileName)) {

                char lastCharacter = processedFileName.charAt(processedFileName.length() - 1);
                if (Character.isDigit(lastCharacter)) {
                    int numericValueOfLastCharacter = Character.getNumericValue(lastCharacter);
                    if (numericValueOfLastCharacter != 9)
                        diagramName = FilenameUtils.getPath(f.getName()) + processedFileName.substring(4, processedFileName.length() - 1) + ++numericValueOfLastCharacter + removedExtension;
                    else
                        diagramName = FilenameUtils.getPath(f.getName()) + processedFileName.substring(4) + "1" + removedExtension;
                } else {
                    diagramName = FilenameUtils.getPath(f.getName()) + processedFileName.substring(4) + "1" + removedExtension;
                }
                changeFilenameIfExists(iDiagramUIModels, "imp-" + diagramName.substring(0, diagramName.lastIndexOf(".")), removedExtension, f);
            }
        }
    }


}
