package pluginmain.actions;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.ViewManager;
import com.vp.plugin.diagram.IDiagramUIModel;
import pluginmain.exceptions.NoChosenOptionException;
import pluginmain.exceptions.NoDiagramException;
import pluginmain.utils.UserInputHandler;

import java.io.FileNotFoundException;

import static pluginmain.utils.UserInputHandler.showChooseTextFormatDialog;

public class ExportActiveDiagramToTextFormatController extends ExportToTextFormatController {

    protected void retrieveDataFromUser(ViewManager viewManager, IDiagramUIModel[] iDiagramUIModels)
            throws FileNotFoundException, NoChosenOptionException, NoDiagramException {
        IDiagramUIModel activeDiagram = ApplicationManager.instance().getDiagramManager().getActiveDiagram();
        if (activeDiagram == null)
            throw new NoDiagramException("No active diagram to export.");
        chosenDirectoryPath = UserInputHandler.chooseDirectory(viewManager);

        chosenDiagramName = activeDiagram.getName();
        chosenFormat = showChooseTextFormatDialog(viewManager, "Select output text format for exporting");
    }

}
