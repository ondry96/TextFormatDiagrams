package pluginmain.actions;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.ViewManager;
import com.vp.plugin.action.VPAction;
import com.vp.plugin.action.VPContext;
import com.vp.plugin.action.VPContextActionController;
import com.vp.plugin.diagram.IDiagramUIModel;
import pluginmain.exceptions.*;
import pluginmain.exports.diagramexporters.DiagramExporter;
import pluginmain.utils.UserInputHandler;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import static pluginmain.utils.UserInputHandler.showChooseTextFormatDialog;
import static pluginmain.utils.UserInputHandler.showMessageToUser;

public class ExportDiagramFromToolbarController extends ExportToTextFormatController implements VPContextActionController {
    @Override
    public void performAction(VPAction vpAction, VPContext vpContext, ActionEvent actionEvent) {
        ViewManager viewManager = ApplicationManager.instance().getViewManager();
        try {
            IDiagramUIModel diagramToExport = retrieveDataFromUser(viewManager, vpContext);
            String modelType = diagramToExport.getType();
            DiagramExporter exportCreator = createDiagramExportCreatorInstance(modelType);
            exportCreator.buildElementsHierarchy(diagramToExport);
            chosenDirectoryPath = chosenDirectoryPath + File.separator + "exp-" + diagramToExport.getName();
            exportCreator.exportToDirectory(viewManager, chosenDirectoryPath, chosenFormat);
        } catch (UnsupportedDiagramException e) {
            showMessageToUser(viewManager, e.getMessage(), "Unsupported diagram");
        } catch (NoDiagramException e) {
            showMessageToUser(viewManager, e.getMessage(), "No diagram found");
        } catch (FileNotFoundException | NoChosenDiagramException | NoChosenOptionException ignored) {
            //not needed to show message to user
        } catch (UnsupportedTextFormatException e) {
            showMessageToUser(viewManager, e.getMessage(), "Unsupported text format");
        } catch (IOException e) {
            showMessageToUser(viewManager, "Could not write output to file", "Incorrect file");
        }
    }

    private IDiagramUIModel retrieveDataFromUser(ViewManager viewManager, VPContext vpContext)
            throws FileNotFoundException, NoDiagramException, NoChosenDiagramException, NoChosenOptionException {
        chosenDirectoryPath = UserInputHandler.chooseDirectory(viewManager);

        IDiagramUIModel diagram = vpContext.getDiagram();
        if (diagram == null)
            throw new NoDiagramException("No diagram to export found.");

        chosenDiagramName = diagram.getName();
        chosenFormat = showChooseTextFormatDialog(viewManager, "Select output text format for exporting");
        return diagram;
    }

    @Override
    public void update(VPAction vpAction, VPContext vpContext) {

    }
}
