package pluginmain.actions;

import com.vp.plugin.ApplicationManager;
import com.vp.plugin.ProjectManager;
import com.vp.plugin.ViewManager;
import com.vp.plugin.action.VPAction;
import com.vp.plugin.action.VPActionController;
import com.vp.plugin.diagram.IDiagramUIModel;
import pluginmain.enums.TextFormat;
import pluginmain.exceptions.*;
import pluginmain.exports.diagramexporters.ClassDiagramExporter;
import pluginmain.exports.diagramexporters.DiagramExporter;
import pluginmain.exports.diagramexporters.SequenceDiagramExporter;
import pluginmain.exports.diagramexporters.UseCaseDiagramExporter;
import pluginmain.utils.UserInputHandler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static pluginmain.utils.UserInputHandler.*;

public class ExportToTextFormatController implements VPActionController {

    protected String chosenDiagramName = null;
    protected TextFormat chosenFormat = null;
    protected String chosenDirectoryPath = null;

    public void performAction(VPAction vpAction) {
        ViewManager viewManager = ApplicationManager.instance().getViewManager();
        ProjectManager projectManager = ApplicationManager.instance().getProjectManager();
        IDiagramUIModel[] iDiagramUIModels = projectManager.getProject().toDiagramArray();
        try {
            retrieveDataFromUser(viewManager, iDiagramUIModels);
            IDiagramUIModel diagramToExport = getDiagramToExport(iDiagramUIModels);
            String modelType = diagramToExport.getType();
            DiagramExporter exportCreator = createDiagramExportCreatorInstance(modelType);
            exportCreator.buildElementsHierarchy(diagramToExport);
            chosenDirectoryPath = chosenDirectoryPath + File.separator + "exp-" + diagramToExport.getName();
            exportCreator.exportToDirectory(viewManager, chosenDirectoryPath, chosenFormat);
        } catch (UnsupportedDiagramException e) {
            showMessageToUser(viewManager, e.getMessage(), "Unsupported diagram");
        } catch (NoDiagramException e) {
            showMessageToUser(viewManager, e.getMessage(), "No diagram found");
        } catch (FileNotFoundException | NoChosenDiagramException | NoChosenOptionException ignored) {
            //not needed to show message to user
        } catch (UnsupportedTextFormatException e) {
            showMessageToUser(viewManager, e.getMessage(), "Unsupported text format");
        } catch (IOException e) {
            showMessageToUser(viewManager, "Could not write output to file", "Incorrect file");
        }
    }

    protected DiagramExporter createDiagramExportCreatorInstance(String modelType) throws UnsupportedDiagramException {
        switch (modelType) {
            case "UseCaseDiagram":
                return new UseCaseDiagramExporter();
            case "ClassDiagram":
                return new ClassDiagramExporter();
            case "InteractionDiagram":
                return new SequenceDiagramExporter();
            default:
                throw new UnsupportedDiagramException("This diagram is not supported.");
        }
    }

    protected IDiagramUIModel getDiagramToExport(IDiagramUIModel[] diagrams) throws RuntimeException {
        for (int i = 0; i < diagrams.length; i++) {
            if (diagrams[i].getName().equals(chosenDiagramName))
                return diagrams[i];
        }
        throw new RuntimeException();
    }

    protected void retrieveDataFromUser(ViewManager viewManager, IDiagramUIModel[] iDiagramUIModels)
            throws FileNotFoundException, NoDiagramException, NoChosenDiagramException, NoChosenOptionException {
        chosenDirectoryPath = UserInputHandler.chooseDirectory(viewManager);
        List<String> diagrams = retrieveDiagramNames(iDiagramUIModels);
        if (diagrams.isEmpty())
            throw new NoDiagramException("No diagram to export found.");

        chosenDiagramName = showChooseDiagramDialog(diagrams, viewManager);
        chosenFormat = showChooseTextFormatDialog(viewManager, "Select output text format for exporting");
    }

    protected List<String> retrieveDiagramNames(IDiagramUIModel[] iDiagramUIModels) {
        List<String> diagrams = new ArrayList<>();
        for (int i = 0; i < iDiagramUIModels.length; i++) {
            diagrams.add(iDiagramUIModels[i].getName());
        }
        return diagrams;
    }

    public void update(VPAction vpAction) {
    }

}
