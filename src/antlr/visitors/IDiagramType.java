package antlr.visitors;

import pluginmain.elements.common.Element;
import pluginmain.imports.diagramcreators.DiagramCreator;

import java.util.List;

public interface IDiagramType {
    List<String> getErrorsInGrammar();

    List<Element> getElements();

    DiagramCreator getCreator();

    PlantUMLBaseDiagramVisitor getVisitor();
}
