package antlr.visitors;

import antlr.plantumlgrammar.PlantUMLParser;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.diagram.LayoutOption$Orientation;
import org.antlr.v4.runtime.tree.ParseTree;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.exceptions.UnsupportedDiagramException;
import pluginmain.exceptions.compiler.*;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.imports.toinnerstructureprocessors.classdiagram.ClassDiagramElementFinder;

import java.util.ArrayList;
import java.util.List;

import static pluginmain.imports.toinnerstructureprocessors.CommonElementFinder.findPackageByNameOrAlias;

public class PlantUMLClassDiagramVisitor extends PlantUMLBaseDiagramVisitor {

    private String currentPackageNameOrAlias;
    private List<String> currentPackageStructure = new ArrayList<>();

    public PlantUMLClassDiagramVisitor() {
        this.currentPackageStructure.add(null);
    }

    @Override
    public Object visitClassDiagram(PlantUMLParser.ClassDiagramContext ctx) {
        ImportDiagramInfo.getInstance().reset();
        return super.visitClassDiagram(ctx);
    }

    @Override
    public Object visitClassRule(PlantUMLParser.ClassRuleContext ctx) {
        try {
            facadeProcessor.classProcessor.processClass(ctx, elements, currentPackageNameOrAlias);
        } catch (SyntaxException | NameAndAliasStartsWithSpecialSymbolsException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (PackageNotFoundException | MoreElementsFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        return null;
    }

    @Override
    public Object visitAddResourceToClassRule(PlantUMLParser.AddResourceToClassRuleContext ctx) {
        String className = ctx.getChild(0).getText();
        ParseTree line = ctx.getChild(2);
        try {
            ClassElement classElement = ClassDiagramElementFinder.findClassByNameOrAlias(elements, className);
            if (classElement == null) {
                if (currentPackageNameOrAlias == null) {
                    classElement = new ClassElement(className);
                    elements.add(classElement);
                } else {
                    ClassPackageElement foundedPackage = (ClassPackageElement) findPackageByNameOrAlias(elements, currentPackageNameOrAlias, currentPackageNameOrAlias);
                    classElement = new ClassElement(className);
                    foundedPackage.getClasses().add(classElement);
                }
            }
            ImportDiagramInfo.getInstance().setLastSavedElement(classElement, classElement.getAlias() != null); //even if not saved (already exists)!

            facadeProcessor.classProcessor.processBodyLine(classElement, line);
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (PackageNotFoundException | MoreElementsFoundException e) {
            errorsInProgram.add(e.getMessage());
        }
        return null;
    }


    @Override
    public Object visitClassPackageRule(PlantUMLParser.ClassPackageRuleContext ctx) {
        try {
            currentPackageNameOrAlias = currentPackageStructure.get(currentPackageStructure.size() - 1);
            currentPackageNameOrAlias = facadeProcessor.packageProcessor.processPackage(ctx, elements, currentPackageNameOrAlias, DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM);
            currentPackageStructure.add(currentPackageNameOrAlias);
        } catch (PackageNotFoundException | MoreElementsFoundException | UnsupportedDiagramException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        super.visitClassPackageRule(ctx);
        currentPackageStructure.remove(currentPackageStructure.size() - 1);
        currentPackageNameOrAlias = currentPackageStructure.get(currentPackageStructure.size() - 1);
        return null;
    }

    @Override
    public Object visitClassDiagramRelationRule(PlantUMLParser.ClassDiagramRelationRuleContext ctx) {

        try {
            facadeProcessor.relationProcessor.processRelation(ctx, elements, currentPackageNameOrAlias, DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM);
        } catch (PackageNotFoundException | MoreElementsFoundException | UnsupportedDiagramException e) {
            errorsInProgram.add(e.getMessage());
            e.printStackTrace();
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        }
        return null;
    }

    @Override
    public Object visitNoteRule(PlantUMLParser.NoteRuleContext ctx) {
        try {
            facadeProcessor.noteProcessor.processNote(ctx, elements, currentPackageNameOrAlias, DiagramManager.DIAGRAM_TYPE_CLASS_DIAGRAM);
        } catch (PackageNotFoundException | MoreElementsFoundException | UnsupportedDiagramException e) {
            errorsInProgram.add(e.getMessage());
            e.printStackTrace();
        } catch (CannotAttachNoteException | AlreadyInStructureException e) {
            errorsInGrammar.add(e.getMessage());
        }

        return null;
    }

    @Override
    public Object visitTopToBottomDirection(PlantUMLParser.TopToBottomDirectionContext ctx) {
        ImportDiagramInfo.getInstance().setOrientation(LayoutOption$Orientation.LeftToRight);
        return null;
    }

    @Override
    public Object visitLeftToRightDirection(PlantUMLParser.LeftToRightDirectionContext ctx) {
        ImportDiagramInfo.getInstance().setOrientation(LayoutOption$Orientation.TopToBottom);
        return null;
    }

}
