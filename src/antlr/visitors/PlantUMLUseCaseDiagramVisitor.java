package antlr.visitors;

import antlr.plantumlgrammar.PlantUMLParser;
import com.vp.plugin.DiagramManager;
import com.vp.plugin.diagram.LayoutOption$Orientation;
import pluginmain.exceptions.UnsupportedDiagramException;
import pluginmain.exceptions.compiler.*;
import pluginmain.imports.ImportDiagramInfo;

import java.util.ArrayList;
import java.util.List;

public class PlantUMLUseCaseDiagramVisitor extends PlantUMLBaseDiagramVisitor {

    private String currentPackageNameOrAlias;
    private List<String> currentPackageStructure = new ArrayList<>();

    public PlantUMLUseCaseDiagramVisitor() {
        this.currentPackageStructure.add(null);
    }

    @Override
    public Object visitUseCaseDiagram(PlantUMLParser.UseCaseDiagramContext ctx) {
        ImportDiagramInfo.getInstance().reset();
        ImportDiagramInfo.getInstance().setOrientation(LayoutOption$Orientation.TopToBottom);
        return super.visitUseCaseDiagram(ctx);
    }

    @Override
    public Object visitUseCasePackageRule(PlantUMLParser.UseCasePackageRuleContext ctx) {
        try {
            currentPackageNameOrAlias = currentPackageStructure.get(currentPackageStructure.size() - 1);
            currentPackageNameOrAlias = facadeProcessor.packageProcessor.processPackage(ctx, elements, currentPackageNameOrAlias, DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM);
            currentPackageStructure.add(currentPackageNameOrAlias);
        } catch (PackageNotFoundException | MoreElementsFoundException | UnsupportedDiagramException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        super.visitUseCasePackageRule(ctx);
        currentPackageStructure.remove(currentPackageStructure.size() - 1);
        currentPackageNameOrAlias = currentPackageStructure.get(currentPackageStructure.size() - 1);
        return null;
    }

    @Override
    public Object visitActorRule(PlantUMLParser.ActorRuleContext ctx) {
        try {
            facadeProcessor.actorProcessor.processActor(ctx, elements, currentPackageNameOrAlias);
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (MoreElementsFoundException | PackageNotFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        return null;
    }

    @Override
    public Object visitUseCaseRule(PlantUMLParser.UseCaseRuleContext ctx) {
        try {
            facadeProcessor.useCaseProcessor.processUseCase(ctx, elements, currentPackageNameOrAlias);
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (MoreElementsFoundException | PackageNotFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        return null;
    }

    @Override
    public Object visitUseCaseDiagramRelationRule(PlantUMLParser.UseCaseDiagramRelationRuleContext ctx) {
        try {
            facadeProcessor.relationProcessor.processRelation(ctx, elements, currentPackageNameOrAlias, DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM);
        } catch (PackageNotFoundException | MoreElementsFoundException | UnsupportedDiagramException e) {
            errorsInProgram.add(e.getMessage());
            e.printStackTrace();
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        }
        return null;
    }

    @Override
    public Object visitNoteRule(PlantUMLParser.NoteRuleContext ctx) {
        try {
            facadeProcessor.noteProcessor.processNote(ctx, elements, currentPackageNameOrAlias, DiagramManager.DIAGRAM_TYPE_USE_CASE_DIAGRAM);
        } catch (PackageNotFoundException | MoreElementsFoundException | UnsupportedDiagramException e) {
            errorsInProgram.add(e.getMessage());
            e.printStackTrace();
        } catch (CannotAttachNoteException | AlreadyInStructureException e) {
            errorsInGrammar.add(e.getMessage());
        }

        return null;
    }

    @Override
    public Object visitTopToBottomDirection(PlantUMLParser.TopToBottomDirectionContext ctx) {
        ImportDiagramInfo.getInstance().setOrientation(LayoutOption$Orientation.TopToBottom);
        return null;
    }

    @Override
    public Object visitLeftToRightDirection(PlantUMLParser.LeftToRightDirectionContext ctx) {
        ImportDiagramInfo.getInstance().setOrientation(LayoutOption$Orientation.LeftToRight);
        return null;
    }
}
