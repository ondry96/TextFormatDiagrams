package antlr.visitors;

import antlr.plantumlgrammar.PlantUMLBaseVisitor;
import antlr.plantumlgrammar.PlantUMLParser;
import pluginmain.elements.common.Element;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.imports.toinnerstructureprocessors.CommonProcessor;
import pluginmain.imports.toinnerstructureprocessors.FacadeProcessor;

import java.util.ArrayList;
import java.util.List;

public abstract class PlantUMLBaseDiagramVisitor extends PlantUMLBaseVisitor<Object> {

    protected final FacadeProcessor facadeProcessor = new FacadeProcessor();
    protected List<String> errorsInGrammar = new ArrayList<>();
    protected List<String> errorsInProgram = new ArrayList<>();
    protected List<Element> elements = new ArrayList<>();

    @Override
    public Object visitSkinparamRule(PlantUMLParser.SkinparamRuleContext ctx) {
        String skinparam = CommonProcessor.processIdentifierWithSpace(ctx.getChild(1));
        if (!ImportDiagramInfo.getInstance().getSkinparamOptions().contains(skinparam))
            ImportDiagramInfo.getInstance().getSkinparamOptions().add(skinparam);
        return null;
    }


    public List<Element> getElements() {
        return elements;
    }

    public List<String> getErrorsInGrammar() {
        return errorsInGrammar;
    }

    public List<String> getErrorsInProgram() {
        return errorsInProgram;
    }

}
