package antlr.visitors;

import antlr.plantumlgrammar.PlantUMLParser;
import pluginmain.exceptions.compiler.MoreElementsFoundException;
import pluginmain.exceptions.compiler.SyntaxException;
import pluginmain.imports.ImportDiagramInfo;

import java.util.ArrayList;
import java.util.List;

public class PlantUMLSequenceDiagramVisitor extends PlantUMLBaseDiagramVisitor {


    protected String currentGroupCaseAlias;
    protected List<String> currentGroupCasesStructure = new ArrayList<>();

    public PlantUMLSequenceDiagramVisitor() {
        this.currentGroupCasesStructure.add(null);
    }

    @Override
    public Object visitSequenceDiagram(PlantUMLParser.SequenceDiagramContext ctx) {
        ImportDiagramInfo.getInstance().reset();
        return super.visitSequenceDiagram(ctx);
    }

    @Override
    public Object visitParticipantRule(PlantUMLParser.ParticipantRuleContext ctx) {
        try {
            facadeProcessor.participantProcessor.processParticipant(ctx, elements);
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (MoreElementsFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        return null;
    }

    @Override
    public Object visitGroupRule(PlantUMLParser.GroupRuleContext ctx) {
        try {
            currentGroupCaseAlias = currentGroupCasesStructure.get(currentGroupCasesStructure.size() - 1);
            currentGroupCaseAlias = facadeProcessor.groupProcessor.processGroup(ctx, elements, currentGroupCaseAlias);
            currentGroupCasesStructure.add(currentGroupCaseAlias);
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (MoreElementsFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        super.visitGroupRule(ctx);
        currentGroupCasesStructure.remove(currentGroupCasesStructure.size() - 1);
        currentGroupCaseAlias = currentGroupCasesStructure.get(currentGroupCasesStructure.size() - 1);

        return null;
    }

    @Override
    public Object visitElseGroupBranch(PlantUMLParser.ElseGroupBranchContext ctx) {
        try {
            currentGroupCaseAlias = currentGroupCasesStructure.get(currentGroupCasesStructure.size() - 1);
            currentGroupCaseAlias = facadeProcessor.groupProcessor.processElseGroupBranch(ctx, elements, currentGroupCaseAlias);
            currentGroupCasesStructure.add(currentGroupCaseAlias);

        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        } catch (MoreElementsFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        }
        super.visitElseGroupBranch(ctx);
        currentGroupCasesStructure.remove(currentGroupCasesStructure.size() - 1);
        currentGroupCaseAlias = currentGroupCasesStructure.get(currentGroupCasesStructure.size() - 1);

        return null;
    }

    @Override
    public Object visitMessageRule(PlantUMLParser.MessageRuleContext ctx) {
        try {
            facadeProcessor.messageProcessor.processMessage(ctx, elements, currentGroupCaseAlias);
        } catch (MoreElementsFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        }

        return null;
    }


    @Override
    public Object visitCreateRule(PlantUMLParser.CreateRuleContext ctx) {
        try {
            facadeProcessor.messageProcessor.processCreateMessage(ctx, elements);
        } catch (MoreElementsFoundException e) {
            e.printStackTrace();
            errorsInProgram.add(e.getMessage());
        } catch (SyntaxException e) {
            errorsInGrammar.add(e.getMessage());
        }

        return super.visitCreateRule(ctx);
    }

}
