package antlr.visitors;

import antlr.plantumlgrammar.PlantUMLBaseVisitor;
import antlr.plantumlgrammar.PlantUMLParser;
import pluginmain.elements.common.Element;
import pluginmain.imports.ImportDiagramInfo;
import pluginmain.imports.diagramcreators.ClassDiagramCreator;
import pluginmain.imports.diagramcreators.DiagramCreator;
import pluginmain.imports.diagramcreators.SequenceDiagramCreator;
import pluginmain.imports.diagramcreators.UseCaseDiagramCreator;

import java.util.List;

public class PlantUMLDiagramTypeVisitor extends PlantUMLBaseVisitor<Object> implements IDiagramType {

    private PlantUMLBaseDiagramVisitor visitor;
    private DiagramCreator creator;
    private String diagramName;

    public PlantUMLDiagramTypeVisitor(String diagramName) {
        this.diagramName = diagramName;
    }

    @Override
    public Object visitClassDiagram(PlantUMLParser.ClassDiagramContext ctx) {
        ImportDiagramInfo.getInstance().reset();
        visitor = new PlantUMLClassDiagramVisitor();
        creator = new ClassDiagramCreator(diagramName);
        return null;
    }

    @Override
    public Object visitUseCaseDiagram(PlantUMLParser.UseCaseDiagramContext ctx) {
        ImportDiagramInfo.getInstance().reset();
        visitor = new PlantUMLUseCaseDiagramVisitor();
        creator = new UseCaseDiagramCreator(diagramName);
        return null;
    }

    @Override
    public Object visitSequenceDiagram(PlantUMLParser.SequenceDiagramContext ctx) {
        ImportDiagramInfo.getInstance().reset();
        visitor = new PlantUMLSequenceDiagramVisitor();
        creator = new SequenceDiagramCreator(diagramName);
        return null;
    }

    public PlantUMLBaseDiagramVisitor getVisitor() {
        return visitor;
    }

    @Override
    public List<String> getErrorsInGrammar() {
        return visitor.getErrorsInGrammar();
    }

    @Override
    public List<Element> getElements() {
        return visitor.getElements();
    }

    public DiagramCreator getCreator() {
        return creator;
    }

}
