// Generated from C:/Users/lukas/Desktop/SKOLA/2MIT-zima/DIP/TextFormatDiagrams/src/antlr/plantumlgrammar\PlantUML.g4 by ANTLR 4.9.1
package antlr.plantumlgrammar;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link PlantUMLParser}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 *            operations with no return type.
 */
public interface PlantUMLVisitor<T> extends ParseTreeVisitor<T> {
    /**
     * Visit a parse tree produced by {@link PlantUMLParser#uml}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUml(PlantUMLParser.UmlContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classDiagram}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassDiagram(PlantUMLParser.ClassDiagramContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCaseDiagram}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCaseDiagram(PlantUMLParser.UseCaseDiagramContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#sequenceDiagram}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSequenceDiagram(PlantUMLParser.SequenceDiagramContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classDiagramLine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassDiagramLine(PlantUMLParser.ClassDiagramLineContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCaseDiagramLine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCaseDiagramLine(PlantUMLParser.UseCaseDiagramLineContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#sequenceDiagramLine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSequenceDiagramLine(PlantUMLParser.SequenceDiagramLineContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#noteRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNoteRule(PlantUMLParser.NoteRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#skinparamRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSkinparamRule(PlantUMLParser.SkinparamRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#autonumberRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAutonumberRule(PlantUMLParser.AutonumberRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassRule(PlantUMLParser.ClassRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#addResourceToClassRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAddResourceToClassRule(PlantUMLParser.AddResourceToClassRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classPackageRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassPackageRule(PlantUMLParser.ClassPackageRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classDiagramRelationRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassDiagramRelationRule(PlantUMLParser.ClassDiagramRelationRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classTypeWithAbstract}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassTypeWithAbstract(PlantUMLParser.ClassTypeWithAbstractContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classBody}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassBody(PlantUMLParser.ClassBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classPackageBody}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassPackageBody(PlantUMLParser.ClassPackageBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classBodyLine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassBodyLine(PlantUMLParser.ClassBodyLineContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#classPackageBodyLine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitClassPackageBodyLine(PlantUMLParser.ClassPackageBodyLineContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#hideRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitHideRule(PlantUMLParser.HideRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#showRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitShowRule(PlantUMLParser.ShowRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#actorRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitActorRule(PlantUMLParser.ActorRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCaseRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCaseRule(PlantUMLParser.UseCaseRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCasePackageRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCasePackageRule(PlantUMLParser.UseCasePackageRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCaseDiagramRelationRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCaseDiagramRelationRule(PlantUMLParser.UseCaseDiagramRelationRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCasePackageBody}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCasePackageBody(PlantUMLParser.UseCasePackageBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCasePackageBodyLine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCasePackageBodyLine(PlantUMLParser.UseCasePackageBodyLineContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#participantRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitParticipantRule(PlantUMLParser.ParticipantRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#messageRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMessageRule(PlantUMLParser.MessageRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#groupRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGroupRule(PlantUMLParser.GroupRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#createRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitCreateRule(PlantUMLParser.CreateRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#returnRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitReturnRule(PlantUMLParser.ReturnRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#activateRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitActivateRule(PlantUMLParser.ActivateRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#deactivateRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDeactivateRule(PlantUMLParser.DeactivateRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#sequenceNoteRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSequenceNoteRule(PlantUMLParser.SequenceNoteRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#refRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitRefRule(PlantUMLParser.RefRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#groupCase}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGroupCase(PlantUMLParser.GroupCaseContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#elseGroupBranch}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitElseGroupBranch(PlantUMLParser.ElseGroupBranchContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#order}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitOrder(PlantUMLParser.OrderContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#topToBottomDirection}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTopToBottomDirection(PlantUMLParser.TopToBottomDirectionContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#leftToRightDirection}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitLeftToRightDirection(PlantUMLParser.LeftToRightDirectionContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#basicAlias}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBasicAlias(PlantUMLParser.BasicAliasContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#useCaseAlias}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitUseCaseAlias(PlantUMLParser.UseCaseAliasContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#actorAlias}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitActorAlias(PlantUMLParser.ActorAliasContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#generics}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGenerics(PlantUMLParser.GenericsContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#stereotypes}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStereotypes(PlantUMLParser.StereotypesContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#identificator}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentificator(PlantUMLParser.IdentificatorContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#identificator_with_space}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentificator_with_space(PlantUMLParser.Identificator_with_spaceContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#dentificator_with_space_and_special_chars_without_angle_brackets}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDentificator_with_space_and_special_chars_without_angle_brackets(PlantUMLParser.Dentificator_with_space_and_special_chars_without_angle_bracketsContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#identificator_with_space_and_special_chars}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentificator_with_space_and_special_chars(PlantUMLParser.Identificator_with_space_and_special_charsContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#identificator_of_note}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIdentificator_of_note(PlantUMLParser.Identificator_of_noteContext ctx);

    /**
     * Visit a parse tree produced by {@link PlantUMLParser#keyword}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitKeyword(PlantUMLParser.KeywordContext ctx);
}
