// Generated from C:/Users/lukas/Desktop/SKOLA/2MIT-zima/DIP/TextFormatDiagrams/src/antlr/plantumlgrammar\PlantUML.g4 by ANTLR 4.9.1
package antlr.plantumlgrammar;

import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.ATN;
import org.antlr.v4.runtime.atn.ATNDeserializer;
import org.antlr.v4.runtime.atn.ParserATNSimulator;
import org.antlr.v4.runtime.atn.PredictionContextCache;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.tree.ParseTreeVisitor;
import org.antlr.v4.runtime.tree.TerminalNode;

import java.util.List;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class PlantUMLParser extends Parser {
	public static final int
			T__0 = 1, T__1 = 2, T__2 = 3, T__3 = 4, COMMENT = 5, ARROW_DOTTED = 6, ARROW_DASHED = 7,
			ARROW_SEQUENCE_DIAGRAM = 8, CLASS_TYPE_KEYWORD = 9, CLASS_ANOTHER_TYPES_KEYWORD = 10,
			ABSTRACT_KEYWORD = 11, EXTENDS_KEYWORD = 12, IMPLEMENTS_KEYWORD = 13, FIELD_KEYWORD = 14,
			METHOD_KEYWORD = 15, STATIC_KEYWORD = 16, CLASSIFIER_KEYWORD = 17, PACKAGE_KEYWORD = 18,
			NAMESPACE_KEYWORD = 19, RECTANGLE_KEYWORD = 20, USECASE_KEYWORD = 21, ACTOR_KEYWORD = 22,
			SYSTEM_KEYWORD = 23, ENTITY_KEYWORD = 24, TOP_TO_BOTTOM_DIRECTION_KEYWORD = 25,
			LEFT_TO_RIGHT_DIRECTION_KEYWORD = 26, ORDER_KEYWORD = 27, PARTICIPANT_TYPE_KEYWORD = 28,
			ELSE_KEYWORD = 29, GROUP_TYPE_KEYWORD = 30, CREATE_KEYWORD = 31, RETURN_KEYWORD = 32,
			OVER_KEYWORD = 33, REF_KEYWORD = 34, ACTIVATE_KEYWORD = 35, DEACTIVATE_KEYWORD = 36,
			HIDE_KEYWORD = 37, SHOW_KEYWORD = 38, NOTE_KEYWORD = 39, SKINPARAM_KEYWORD = 40,
			AUTONUMBER_KEYWORD = 41, DIRECTION_KEYWORD = 42, OF_KEYWORD = 43, END_KEYWORD = 44,
			AS_KEYWORD = 45, ARROW_MIDDLE_DOTTED = 46, ARROW_MIDDLE_DASHED = 47, OPENED_ANGLE_BRACKET = 48,
			CLOSED_ANGLE_BRACKET = 49, DOUBLE_OPENED_ANGLE_BRACKET = 50, DOUBLE_CLOSED_ANGLE_BRACKET = 51,
			OPENED_CURLY_BRACKET = 52, CLOSED_CURLY_BRACKET = 53, COLON = 54, DASH = 55, VISIBILITY_SIGN = 56,
			HEX_COLOR = 57, NEWLINE = 58, IDENTIFICATOR_CLOSED_IN_COLONS = 59, IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES = 60,
			IDENTIFICATOR_CLOSED_IN_BRACKETS = 61, IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES = 62,
			QUOTED_IDENTIFICATOR = 63, QUOTED_IDENTIFICATOR_WITH_NEW_LINES = 64, IDENTIFICATOR = 65,
			WS = 66, SPACE = 67, QUOTE = 68, DOT = 69;
	public static final int
			RULE_uml = 0, RULE_classDiagram = 1, RULE_useCaseDiagram = 2, RULE_sequenceDiagram = 3,
			RULE_classDiagramLine = 4, RULE_useCaseDiagramLine = 5, RULE_sequenceDiagramLine = 6,
			RULE_noteRule = 7, RULE_skinparamRule = 8, RULE_autonumberRule = 9, RULE_classRule = 10,
			RULE_addResourceToClassRule = 11, RULE_classPackageRule = 12, RULE_classDiagramRelationRule = 13,
			RULE_classTypeWithAbstract = 14, RULE_classBody = 15, RULE_classPackageBody = 16,
			RULE_classBodyLine = 17, RULE_classPackageBodyLine = 18, RULE_hideRule = 19,
			RULE_showRule = 20, RULE_actorRule = 21, RULE_useCaseRule = 22, RULE_useCasePackageRule = 23,
			RULE_useCaseDiagramRelationRule = 24, RULE_useCasePackageBody = 25, RULE_useCasePackageBodyLine = 26,
			RULE_participantRule = 27, RULE_messageRule = 28, RULE_groupRule = 29,
			RULE_createRule = 30, RULE_returnRule = 31, RULE_activateRule = 32, RULE_deactivateRule = 33,
			RULE_sequenceNoteRule = 34, RULE_refRule = 35, RULE_groupCase = 36, RULE_elseGroupBranch = 37,
			RULE_order = 38, RULE_topToBottomDirection = 39, RULE_leftToRightDirection = 40,
			RULE_basicAlias = 41, RULE_useCaseAlias = 42, RULE_actorAlias = 43, RULE_generics = 44,
			RULE_stereotypes = 45, RULE_identificator = 46, RULE_identificator_with_space = 47,
			RULE_dentificator_with_space_and_special_chars_without_angle_brackets = 48,
			RULE_identificator_with_space_and_special_chars = 49, RULE_identificator_of_note = 50,
			RULE_keyword = 51;
	public static final String[] ruleNames = makeRuleNames();
	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	public static final String _serializedATN =
			"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3G\u0337\4\2\t\2\4" +
					"\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4\13\t" +
					"\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22\t\22" +
					"\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30\4\31\t\31" +
					"\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37\4 \t \4!" +
					"\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t*\4+\t+\4" +
					",\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63\4\64\t" +
					"\64\4\65\t\65\3\2\7\2l\n\2\f\2\16\2o\13\2\3\2\3\2\6\2s\n\2\r\2\16\2t\3" +
					"\2\3\2\3\2\5\2z\n\2\3\2\3\2\7\2~\n\2\f\2\16\2\u0081\13\2\3\2\3\2\3\3\3" +
					"\3\3\3\3\3\3\3\3\3\5\3\u008b\n\3\3\3\6\3\u008e\n\3\r\3\16\3\u008f\7\3" +
					"\u0092\n\3\f\3\16\3\u0095\13\3\3\4\3\4\3\4\3\4\5\4\u009b\n\4\3\4\6\4\u009e" +
					"\n\4\r\4\16\4\u009f\7\4\u00a2\n\4\f\4\16\4\u00a5\13\4\3\5\3\5\3\5\5\5" +
					"\u00aa\n\5\3\5\6\5\u00ad\n\5\r\5\16\5\u00ae\7\5\u00b1\n\5\f\5\16\5\u00b4" +
					"\13\5\3\6\3\6\3\6\3\6\3\6\5\6\u00bb\n\6\3\7\3\7\3\7\3\7\3\7\5\7\u00c2" +
					"\n\7\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\3\b\5\b\u00cd\n\b\3\t\3\t\3\t\3\t" +
					"\5\t\u00d3\n\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\5\t\u00dc\n\t\5\t\u00de\n\t" +
					"\3\t\5\t\u00e1\n\t\3\t\3\t\5\t\u00e5\n\t\3\t\7\t\u00e8\n\t\f\t\16\t\u00eb" +
					"\13\t\3\t\3\t\3\t\3\t\3\t\5\t\u00f2\n\t\3\t\3\t\3\t\5\t\u00f7\n\t\3\t" +
					"\7\t\u00fa\n\t\f\t\16\t\u00fd\13\t\3\t\5\t\u0100\n\t\3\t\3\t\5\t\u0104" +
					"\n\t\3\t\3\t\5\t\u0108\n\t\3\t\7\t\u010b\n\t\f\t\16\t\u010e\13\t\3\t\3" +
					"\t\3\t\3\t\3\t\5\t\u0115\n\t\5\t\u0117\n\t\5\t\u0119\n\t\3\n\3\n\3\n\3" +
					"\13\3\13\3\f\3\f\3\f\5\f\u0123\n\f\3\f\5\f\u0126\n\f\3\f\7\f\u0129\n\f" +
					"\f\f\16\f\u012c\13\f\3\f\5\f\u012f\n\f\3\f\3\f\5\f\u0133\n\f\3\f\3\f\5" +
					"\f\u0137\n\f\3\f\5\f\u013a\n\f\3\r\3\r\3\r\3\r\3\16\3\16\3\16\5\16\u0143" +
					"\n\16\3\16\7\16\u0146\n\16\f\16\16\16\u0149\13\16\3\16\5\16\u014c\n\16" +
					"\3\16\5\16\u014f\n\16\3\17\3\17\5\17\u0153\n\17\3\17\5\17\u0156\n\17\3" +
					"\17\3\17\5\17\u015a\n\17\3\17\3\17\3\17\3\17\5\17\u0160\n\17\5\17\u0162" +
					"\n\17\3\20\3\20\3\20\3\20\3\20\5\20\u0169\n\20\5\20\u016b\n\20\3\21\3" +
					"\21\6\21\u016f\n\21\r\21\16\21\u0170\3\21\3\21\6\21\u0175\n\21\r\21\16" +
					"\21\u0176\7\21\u0179\n\21\f\21\16\21\u017c\13\21\3\21\3\21\3\22\3\22\6" +
					"\22\u0182\n\22\r\22\16\22\u0183\3\22\3\22\6\22\u0188\n\22\r\22\16\22\u0189" +
					"\7\22\u018c\n\22\f\22\16\22\u018f\13\22\3\22\3\22\3\23\7\23\u0194\n\23" +
					"\f\23\16\23\u0197\13\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3\23\3" +
					"\23\3\23\3\23\3\23\3\23\3\23\3\23\5\23\u01a9\n\23\7\23\u01ab\n\23\f\23" +
					"\16\23\u01ae\13\23\3\23\7\23\u01b1\n\23\f\23\16\23\u01b4\13\23\3\24\3" +
					"\24\3\24\3\24\3\24\3\24\3\24\3\24\5\24\u01be\n\24\3\25\3\25\3\25\3\26" +
					"\3\26\3\26\3\27\5\27\u01c7\n\27\3\27\3\27\3\27\5\27\u01cc\n\27\3\27\5" +
					"\27\u01cf\n\27\3\27\7\27\u01d2\n\27\f\27\16\27\u01d5\13\27\3\27\5\27\u01d8" +
					"\n\27\3\30\5\30\u01db\n\30\3\30\3\30\3\30\5\30\u01e0\n\30\3\30\5\30\u01e3" +
					"\n\30\3\30\7\30\u01e6\n\30\f\30\16\30\u01e9\13\30\3\30\5\30\u01ec\n\30" +
					"\3\31\3\31\3\31\5\31\u01f1\n\31\3\31\7\31\u01f4\n\31\f\31\16\31\u01f7" +
					"\13\31\3\31\5\31\u01fa\n\31\3\31\5\31\u01fd\n\31\3\32\3\32\3\32\5\32\u0202" +
					"\n\32\3\32\5\32\u0205\n\32\3\32\3\32\5\32\u0209\n\32\3\32\3\32\3\32\5" +
					"\32\u020e\n\32\3\32\3\32\3\32\5\32\u0213\n\32\5\32\u0215\n\32\3\33\3\33" +
					"\6\33\u0219\n\33\r\33\16\33\u021a\3\33\3\33\6\33\u021f\n\33\r\33\16\33" +
					"\u0220\7\33\u0223\n\33\f\33\16\33\u0226\13\33\3\33\3\33\3\34\3\34\3\34" +
					"\3\34\3\34\3\34\3\34\3\34\5\34\u0232\n\34\3\35\3\35\3\35\5\35\u0237\n" +
					"\35\3\35\7\35\u023a\n\35\f\35\16\35\u023d\13\35\3\35\5\35\u0240\n\35\3" +
					"\35\5\35\u0243\n\35\3\36\3\36\5\36\u0247\n\36\5\36\u0249\n\36\3\36\3\36" +
					"\3\36\5\36\u024e\n\36\5\36\u0250\n\36\3\36\3\36\3\36\5\36\u0255\n\36\3" +
					"\36\3\36\5\36\u0259\n\36\3\37\3\37\5\37\u025d\n\37\3\37\3\37\3\37\7\37" +
					"\u0262\n\37\f\37\16\37\u0265\13\37\3\37\3\37\5\37\u0269\n\37\3 \3 \5 " +
					"\u026d\n \3 \3 \5 \u0271\n \3 \7 \u0274\n \f \16 \u0277\13 \3 \5 \u027a" +
					"\n \3 \5 \u027d\n \3 \3 \3 \3!\3!\3!\3\"\3\"\3\"\3#\3#\3#\3$\3$\3$\3$" +
					"\3%\3%\3%\3%\3%\5%\u0294\n%\3%\7%\u0297\n%\f%\16%\u029a\13%\3%\3%\5%\u029e" +
					"\n%\3%\3%\3%\5%\u02a3\n%\5%\u02a5\n%\3&\6&\u02a8\n&\r&\16&\u02a9\3&\3" +
					"&\3&\3&\3&\3&\3&\3&\3&\5&\u02b5\n&\3&\6&\u02b8\n&\r&\16&\u02b9\7&\u02bc" +
					"\n&\f&\16&\u02bf\13&\3\'\3\'\5\'\u02c3\n\'\3\'\3\'\3(\3(\3(\3)\3)\3*\3" +
					"*\3+\3+\3+\3,\3,\3,\3,\3,\3,\3,\5,\u02d8\n,\3-\3-\3-\3-\3-\3-\3-\5-\u02e1" +
					"\n-\3.\3.\3.\3.\3/\3/\3/\3/\3\60\3\60\3\60\5\60\u02ee\n\60\3\61\3\61\3" +
					"\61\3\61\6\61\u02f4\n\61\r\61\16\61\u02f5\3\62\3\62\3\62\3\62\3\62\3\62" +
					"\3\62\3\62\3\62\6\62\u0301\n\62\r\62\16\62\u0302\3\63\3\63\3\63\3\63\3" +
					"\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\3\63\6\63\u0316" +
					"\n\63\r\63\16\63\u0317\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\3" +
					"\64\3\64\3\64\3\64\3\64\3\64\3\64\3\64\6\64\u032b\n\64\r\64\16\64\u032c" +
					"\3\64\7\64\u0330\n\64\f\64\16\64\u0333\13\64\3\65\3\65\3\65\2\2\66\2\4" +
					"\6\b\n\f\16\20\22\24\26\30\32\34\36 \"$&(*,.\60\62\64\668:<>@BDFHJLNP" +
					"RTVXZ\\^`bdfh\2\13\3\2\24\26\3\2?@\3\2\b\t\4\2\24\24\26\26\3\2=>\5\2\30" +
					"\30\32\32\36\36\3\2\t\n\4\2))..\4\2\13\31\33/\2\u03e7\2m\3\2\2\2\4\u0093" +
					"\3\2\2\2\6\u00a3\3\2\2\2\b\u00b2\3\2\2\2\n\u00ba\3\2\2\2\f\u00c1\3\2\2" +
					"\2\16\u00cc\3\2\2\2\20\u00ce\3\2\2\2\22\u011a\3\2\2\2\24\u011d\3\2\2\2" +
					"\26\u011f\3\2\2\2\30\u013b\3\2\2\2\32\u013f\3\2\2\2\34\u0152\3\2\2\2\36" +
					"\u016a\3\2\2\2 \u016c\3\2\2\2\"\u017f\3\2\2\2$\u01ac\3\2\2\2&\u01bd\3" +
					"\2\2\2(\u01bf\3\2\2\2*\u01c2\3\2\2\2,\u01c6\3\2\2\2.\u01da\3\2\2\2\60" +
					"\u01ed\3\2\2\2\62\u0201\3\2\2\2\64\u0216\3\2\2\2\66\u0231\3\2\2\28\u0233" +
					"\3\2\2\2:\u0248\3\2\2\2<\u025a\3\2\2\2>\u026a\3\2\2\2@\u0281\3\2\2\2B" +
					"\u0284\3\2\2\2D\u0287\3\2\2\2F\u028a\3\2\2\2H\u028e\3\2\2\2J\u02a7\3\2" +
					"\2\2L\u02c0\3\2\2\2N\u02c6\3\2\2\2P\u02c9\3\2\2\2R\u02cb\3\2\2\2T\u02cd" +
					"\3\2\2\2V\u02d0\3\2\2\2X\u02d9\3\2\2\2Z\u02e2\3\2\2\2\\\u02e6\3\2\2\2" +
					"^\u02ed\3\2\2\2`\u02f3\3\2\2\2b\u0300\3\2\2\2d\u0315\3\2\2\2f\u0331\3" +
					"\2\2\2h\u0334\3\2\2\2jl\7<\2\2kj\3\2\2\2lo\3\2\2\2mk\3\2\2\2mn\3\2\2\2" +
					"np\3\2\2\2om\3\2\2\2pr\7\3\2\2qs\7<\2\2rq\3\2\2\2st\3\2\2\2tr\3\2\2\2" +
					"tu\3\2\2\2uy\3\2\2\2vz\5\b\5\2wz\5\4\3\2xz\5\6\4\2yv\3\2\2\2yw\3\2\2\2" +
					"yx\3\2\2\2z{\3\2\2\2{\177\7\4\2\2|~\7<\2\2}|\3\2\2\2~\u0081\3\2\2\2\177" +
					"}\3\2\2\2\177\u0080\3\2\2\2\u0080\u0082\3\2\2\2\u0081\177\3\2\2\2\u0082" +
					"\u0083\7\2\2\3\u0083\3\3\2\2\2\u0084\u008b\5(\25\2\u0085\u008b\5*\26\2" +
					"\u0086\u008b\5P)\2\u0087\u008b\5R*\2\u0088\u008b\5\22\n\2\u0089\u008b" +
					"\5\n\6\2\u008a\u0084\3\2\2\2\u008a\u0085\3\2\2\2\u008a\u0086\3\2\2\2\u008a" +
					"\u0087\3\2\2\2\u008a\u0088\3\2\2\2\u008a\u0089\3\2\2\2\u008b\u008d\3\2" +
					"\2\2\u008c\u008e\7<\2\2\u008d\u008c\3\2\2\2\u008e\u008f\3\2\2\2\u008f" +
					"\u008d\3\2\2\2\u008f\u0090\3\2\2\2\u0090\u0092\3\2\2\2\u0091\u008a\3\2" +
					"\2\2\u0092\u0095\3\2\2\2\u0093\u0091\3\2\2\2\u0093\u0094\3\2\2\2\u0094" +
					"\5\3\2\2\2\u0095\u0093\3\2\2\2\u0096\u009b\5P)\2\u0097\u009b\5R*\2\u0098" +
					"\u009b\5\22\n\2\u0099\u009b\5\f\7\2\u009a\u0096\3\2\2\2\u009a\u0097\3" +
					"\2\2\2\u009a\u0098\3\2\2\2\u009a\u0099\3\2\2\2\u009b\u009d\3\2\2\2\u009c" +
					"\u009e\7<\2\2\u009d\u009c\3\2\2\2\u009e\u009f\3\2\2\2\u009f\u009d\3\2" +
					"\2\2\u009f\u00a0\3\2\2\2\u00a0\u00a2\3\2\2\2\u00a1\u009a\3\2\2\2\u00a2" +
					"\u00a5\3\2\2\2\u00a3\u00a1\3\2\2\2\u00a3\u00a4\3\2\2\2\u00a4\7\3\2\2\2" +
					"\u00a5\u00a3\3\2\2\2\u00a6\u00aa\5\22\n\2\u00a7\u00aa\5\24\13\2\u00a8" +
					"\u00aa\5\16\b\2\u00a9\u00a6\3\2\2\2\u00a9\u00a7\3\2\2\2\u00a9\u00a8\3" +
					"\2\2\2\u00aa\u00ac\3\2\2\2\u00ab\u00ad\7<\2\2\u00ac\u00ab\3\2\2\2\u00ad" +
					"\u00ae\3\2\2\2\u00ae\u00ac\3\2\2\2\u00ae\u00af\3\2\2\2\u00af\u00b1\3\2" +
					"\2\2\u00b0\u00a9\3\2\2\2\u00b1\u00b4\3\2\2\2\u00b2\u00b0\3\2\2\2\u00b2" +
					"\u00b3\3\2\2\2\u00b3\t\3\2\2\2\u00b4\u00b2\3\2\2\2\u00b5\u00bb\5\26\f" +
					"\2\u00b6\u00bb\5\30\r\2\u00b7\u00bb\5\32\16\2\u00b8\u00bb\5\34\17\2\u00b9" +
					"\u00bb\5\20\t\2\u00ba\u00b5\3\2\2\2\u00ba\u00b6\3\2\2\2\u00ba\u00b7\3" +
					"\2\2\2\u00ba\u00b8\3\2\2\2\u00ba\u00b9\3\2\2\2\u00bb\13\3\2\2\2\u00bc" +
					"\u00c2\5,\27\2\u00bd\u00c2\5.\30\2\u00be\u00c2\5\60\31\2\u00bf\u00c2\5" +
					"\62\32\2\u00c0\u00c2\5\20\t\2\u00c1\u00bc\3\2\2\2\u00c1\u00bd\3\2\2\2" +
					"\u00c1\u00be\3\2\2\2\u00c1\u00bf\3\2\2\2\u00c1\u00c0\3\2\2\2\u00c2\r\3" +
					"\2\2\2\u00c3\u00cd\58\35\2\u00c4\u00cd\5:\36\2\u00c5\u00cd\5<\37\2\u00c6" +
					"\u00cd\5H%\2\u00c7\u00cd\5> \2\u00c8\u00cd\5@!\2\u00c9\u00cd\5B\"\2\u00ca" +
					"\u00cd\5D#\2\u00cb\u00cd\5F$\2\u00cc\u00c3\3\2\2\2\u00cc\u00c4\3\2\2\2" +
					"\u00cc\u00c5\3\2\2\2\u00cc\u00c6\3\2\2\2\u00cc\u00c7\3\2\2\2\u00cc\u00c8" +
					"\3\2\2\2\u00cc\u00c9\3\2\2\2\u00cc\u00ca\3\2\2\2\u00cc\u00cb\3\2\2\2\u00cd" +
					"\17\3\2\2\2\u00ce\u0118\7)\2\2\u00cf\u00d0\7A\2\2\u00d0\u00d2\5T+\2\u00d1" +
					"\u00d3\7;\2\2\u00d2\u00d1\3\2\2\2\u00d2\u00d3\3\2\2\2\u00d3\u0119\3\2" +
					"\2\2\u00d4\u00dd\7,\2\2\u00d5\u00db\7-\2\2\u00d6\u00dc\7>\2\2\u00d7\u00dc" +
					"\7=\2\2\u00d8\u00dc\7@\2\2\u00d9\u00dc\7?\2\2\u00da\u00dc\5^\60\2\u00db" +
					"\u00d6\3\2\2\2\u00db\u00d7\3\2\2\2\u00db\u00d8\3\2\2\2\u00db\u00d9\3\2" +
					"\2\2\u00db\u00da\3\2\2\2\u00dc\u00de\3\2\2\2\u00dd\u00d5\3\2\2\2\u00dd" +
					"\u00de\3\2\2\2\u00de\u00e0\3\2\2\2\u00df\u00e1\7;\2\2\u00e0\u00df\3\2" +
					"\2\2\u00e0\u00e1\3\2\2\2\u00e1\u00ff\3\2\2\2\u00e2\u00e4\7<\2\2\u00e3" +
					"\u00e5\5f\64\2\u00e4\u00e3\3\2\2\2\u00e4\u00e5\3\2\2\2\u00e5\u00e9\3\2" +
					"\2\2\u00e6\u00e8\7<\2\2\u00e7\u00e6\3\2\2\2\u00e8\u00eb\3\2\2\2\u00e9" +
					"\u00e7\3\2\2\2\u00e9\u00ea\3\2\2\2\u00ea\u00ec\3\2\2\2\u00eb\u00e9\3\2" +
					"\2\2\u00ec\u00ed\7.\2\2\u00ed\u0100\7)\2\2\u00ee\u00f1\78\2\2\u00ef\u00f2" +
					"\5d\63\2\u00f0\u00f2\7A\2\2\u00f1\u00ef\3\2\2\2\u00f1\u00f0\3\2\2\2\u00f2" +
					"\u0100\3\2\2\2\u00f3\u00f4\7\66\2\2\u00f4\u00f6\7<\2\2\u00f5\u00f7\5f" +
					"\64\2\u00f6\u00f5\3\2\2\2\u00f6\u00f7\3\2\2\2\u00f7\u00fb\3\2\2\2\u00f8" +
					"\u00fa\7<\2\2\u00f9\u00f8\3\2\2\2\u00fa\u00fd\3\2\2\2\u00fb\u00f9\3\2" +
					"\2\2\u00fb\u00fc\3\2\2\2\u00fc\u00fe\3\2\2\2\u00fd\u00fb\3\2\2\2\u00fe" +
					"\u0100\7\67\2\2\u00ff\u00e2\3\2\2\2\u00ff\u00ee\3\2\2\2\u00ff\u00f3\3" +
					"\2\2\2\u0100\u0119\3\2\2\2\u0101\u0103\5T+\2\u0102\u0104\7;\2\2\u0103" +
					"\u0102\3\2\2\2\u0103\u0104\3\2\2\2\u0104\u0116\3\2\2\2\u0105\u0107\7<" +
					"\2\2\u0106\u0108\5f\64\2\u0107\u0106\3\2\2\2\u0107\u0108\3\2\2\2\u0108" +
					"\u010c\3\2\2\2\u0109\u010b\7<\2\2\u010a\u0109\3\2\2\2\u010b\u010e\3\2" +
					"\2\2\u010c\u010a\3\2\2\2\u010c\u010d\3\2\2\2\u010d\u010f\3\2\2\2\u010e" +
					"\u010c\3\2\2\2\u010f\u0110\7.\2\2\u0110\u0117\7)\2\2\u0111\u0114\78\2" +
					"\2\u0112\u0115\5d\63\2\u0113\u0115\7A\2\2\u0114\u0112\3\2\2\2\u0114\u0113" +
					"\3\2\2\2\u0115\u0117\3\2\2\2\u0116\u0105\3\2\2\2\u0116\u0111\3\2\2\2\u0117" +
					"\u0119\3\2\2\2\u0118\u00cf\3\2\2\2\u0118\u00d4\3\2\2\2\u0118\u0101\3\2" +
					"\2\2\u0119\21\3\2\2\2\u011a\u011b\7*\2\2\u011b\u011c\5d\63\2\u011c\23" +
					"\3\2\2\2\u011d\u011e\7+\2\2\u011e\25\3\2\2\2\u011f\u0120\5\36\20\2\u0120" +
					"\u0122\5^\60\2\u0121\u0123\5T+\2\u0122\u0121\3\2\2\2\u0122\u0123\3\2\2" +
					"\2\u0123\u0125\3\2\2\2\u0124\u0126\5Z.\2\u0125\u0124\3\2\2\2\u0125\u0126" +
					"\3\2\2\2\u0126\u012a\3\2\2\2\u0127\u0129\5\\/\2\u0128\u0127\3\2\2\2\u0129" +
					"\u012c\3\2\2\2\u012a\u0128\3\2\2\2\u012a\u012b\3\2\2\2\u012b\u012e\3\2" +
					"\2\2\u012c\u012a\3\2\2\2\u012d\u012f\7;\2\2\u012e\u012d\3\2\2\2\u012e" +
					"\u012f\3\2\2\2\u012f\u0132\3\2\2\2\u0130\u0131\7\16\2\2\u0131\u0133\5" +
					"^\60\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2\2\2\u0133\u0136\3\2\2\2\u0134" +
					"\u0135\7\17\2\2\u0135\u0137\5^\60\2\u0136\u0134\3\2\2\2\u0136\u0137\3" +
					"\2\2\2\u0137\u0139\3\2\2\2\u0138\u013a\5 \21\2\u0139\u0138\3\2\2\2\u0139" +
					"\u013a\3\2\2\2\u013a\27\3\2\2\2\u013b\u013c\5^\60\2\u013c\u013d\78\2\2" +
					"\u013d\u013e\5$\23\2\u013e\31\3\2\2\2\u013f\u0140\t\2\2\2\u0140\u0142" +
					"\5^\60\2\u0141\u0143\5T+\2\u0142\u0141\3\2\2\2\u0142\u0143\3\2\2\2\u0143" +
					"\u0147\3\2\2\2\u0144\u0146\5\\/\2\u0145\u0144\3\2\2\2\u0146\u0149\3\2" +
					"\2\2\u0147\u0145\3\2\2\2\u0147\u0148\3\2\2\2\u0148\u014b\3\2\2\2\u0149" +
					"\u0147\3\2\2\2\u014a\u014c\7;\2\2\u014b\u014a\3\2\2\2\u014b\u014c\3\2" +
					"\2\2\u014c\u014e\3\2\2\2\u014d\u014f\5\"\22\2\u014e\u014d\3\2\2\2\u014e" +
					"\u014f\3\2\2\2\u014f\33\3\2\2\2\u0150\u0153\t\3\2\2\u0151\u0153\5^\60" +
					"\2\u0152\u0150\3\2\2\2\u0152\u0151\3\2\2\2\u0153\u0155\3\2\2\2\u0154\u0156" +
					"\7A\2\2\u0155\u0154\3\2\2\2\u0155\u0156\3\2\2\2\u0156\u0157\3\2\2\2\u0157" +
					"\u0159\t\4\2\2\u0158\u015a\7A\2\2\u0159\u0158\3\2\2\2\u0159\u015a\3\2" +
					"\2\2\u015a\u015b\3\2\2\2\u015b\u0161\5^\60\2\u015c\u015f\78\2\2\u015d" +
					"\u0160\5d\63\2\u015e\u0160\7A\2\2\u015f\u015d\3\2\2\2\u015f\u015e\3\2" +
					"\2\2\u0160\u0162\3\2\2\2\u0161\u015c\3\2\2\2\u0161\u0162\3\2\2\2\u0162" +
					"\35\3\2\2\2\u0163\u016b\7\13\2\2\u0164\u016b\7\f\2\2\u0165\u016b\7\32" +
					"\2\2\u0166\u0168\7\r\2\2\u0167\u0169\7\13\2\2\u0168\u0167\3\2\2\2\u0168" +
					"\u0169\3\2\2\2\u0169\u016b\3\2\2\2\u016a\u0163\3\2\2\2\u016a\u0164\3\2" +
					"\2\2\u016a\u0165\3\2\2\2\u016a\u0166\3\2\2\2\u016b\37\3\2\2\2\u016c\u016e" +
					"\7\66\2\2\u016d\u016f\7<\2\2\u016e\u016d\3\2\2\2\u016f\u0170\3\2\2\2\u0170" +
					"\u016e\3\2\2\2\u0170\u0171\3\2\2\2\u0171\u017a\3\2\2\2\u0172\u0174\5$" +
					"\23\2\u0173\u0175\7<\2\2\u0174\u0173\3\2\2\2\u0175\u0176\3\2\2\2\u0176" +
					"\u0174\3\2\2\2\u0176\u0177\3\2\2\2\u0177\u0179\3\2\2\2\u0178\u0172\3\2" +
					"\2\2\u0179\u017c\3\2\2\2\u017a\u0178\3\2\2\2\u017a\u017b\3\2\2\2\u017b" +
					"\u017d\3\2\2\2\u017c\u017a\3\2\2\2\u017d\u017e\7\67\2\2\u017e!\3\2\2\2" +
					"\u017f\u0181\7\66\2\2\u0180\u0182\7<\2\2\u0181\u0180\3\2\2\2\u0182\u0183" +
					"\3\2\2\2\u0183\u0181\3\2\2\2\u0183\u0184\3\2\2\2\u0184\u018d\3\2\2\2\u0185" +
					"\u0187\5&\24\2\u0186\u0188\7<\2\2\u0187\u0186\3\2\2\2\u0188\u0189\3\2" +
					"\2\2\u0189\u0187\3\2\2\2\u0189\u018a\3\2\2\2\u018a\u018c\3\2\2\2\u018b" +
					"\u0185\3\2\2\2\u018c\u018f\3\2\2\2\u018d\u018b\3\2\2\2\u018d\u018e\3\2" +
					"\2\2\u018e\u0190\3\2\2\2\u018f\u018d\3\2\2\2\u0190\u0191\7\67\2\2\u0191" +
					"#\3\2\2\2\u0192\u0194\5d\63\2\u0193\u0192\3\2\2\2\u0194\u0197\3\2\2\2" +
					"\u0195\u0193\3\2\2\2\u0195\u0196\3\2\2\2\u0196\u01a8\3\2\2\2\u0197\u0195" +
					"\3\2\2\2\u0198\u01a9\7:\2\2\u0199\u019a\7\66\2\2\u019a\u019b\7\20\2\2" +
					"\u019b\u01a9\7\67\2\2\u019c\u019d\7\66\2\2\u019d\u019e\7\21\2\2\u019e" +
					"\u01a9\7\67\2\2\u019f\u01a0\7\66\2\2\u01a0\u01a1\7\r\2\2\u01a1\u01a9\7" +
					"\67\2\2\u01a2\u01a3\7\66\2\2\u01a3\u01a4\7\23\2\2\u01a4\u01a9\7\67\2\2" +
					"\u01a5\u01a6\7\66\2\2\u01a6\u01a7\7\22\2\2\u01a7\u01a9\7\67\2\2\u01a8" +
					"\u0198\3\2\2\2\u01a8\u0199\3\2\2\2\u01a8\u019c\3\2\2\2\u01a8\u019f\3\2" +
					"\2\2\u01a8\u01a2\3\2\2\2\u01a8\u01a5\3\2\2\2\u01a9\u01ab\3\2\2\2\u01aa" +
					"\u0195\3\2\2\2\u01ab\u01ae\3\2\2\2\u01ac\u01aa\3\2\2\2\u01ac\u01ad\3\2" +
					"\2\2\u01ad\u01b2\3\2\2\2\u01ae\u01ac\3\2\2\2\u01af\u01b1\5d\63\2\u01b0" +
					"\u01af\3\2\2\2\u01b1\u01b4\3\2\2\2\u01b2\u01b0\3\2\2\2\u01b2\u01b3\3\2" +
					"\2\2\u01b3%\3\2\2\2\u01b4\u01b2\3\2\2\2\u01b5\u01be\5P)\2\u01b6\u01be" +
					"\5R*\2\u01b7\u01be\5\22\n\2\u01b8\u01be\5\26\f\2\u01b9\u01be\5\20\t\2" +
					"\u01ba\u01be\5\30\r\2\u01bb\u01be\5\32\16\2\u01bc\u01be\5\34\17\2\u01bd" +
					"\u01b5\3\2\2\2\u01bd\u01b6\3\2\2\2\u01bd\u01b7\3\2\2\2\u01bd\u01b8\3\2" +
					"\2\2\u01bd\u01b9\3\2\2\2\u01bd\u01ba\3\2\2\2\u01bd\u01bb\3\2\2\2\u01bd" +
					"\u01bc\3\2\2\2\u01be\'\3\2\2\2\u01bf\u01c0\7\'\2\2\u01c0\u01c1\5d\63\2" +
					"\u01c1)\3\2\2\2\u01c2\u01c3\7(\2\2\u01c3\u01c4\5d\63\2\u01c4+\3\2\2\2" +
					"\u01c5\u01c7\7\30\2\2\u01c6\u01c5\3\2\2\2\u01c6\u01c7\3\2\2\2\u01c7\u01cb" +
					"\3\2\2\2\u01c8\u01cc\7>\2\2\u01c9\u01cc\7=\2\2\u01ca\u01cc\5^\60\2\u01cb" +
					"\u01c8\3\2\2\2\u01cb\u01c9\3\2\2\2\u01cb\u01ca\3\2\2\2\u01cc\u01ce\3\2" +
					"\2\2\u01cd\u01cf\5X-\2\u01ce\u01cd\3\2\2\2\u01ce\u01cf\3\2\2\2\u01cf\u01d3" +
					"\3\2\2\2\u01d0\u01d2\5\\/\2\u01d1\u01d0\3\2\2\2\u01d2\u01d5\3\2\2\2\u01d3" +
					"\u01d1\3\2\2\2\u01d3\u01d4\3\2\2\2\u01d4\u01d7\3\2\2\2\u01d5\u01d3\3\2" +
					"\2\2\u01d6\u01d8\7;\2\2\u01d7\u01d6\3\2\2\2\u01d7\u01d8\3\2\2\2\u01d8" +
					"-\3\2\2\2\u01d9\u01db\7\27\2\2\u01da\u01d9\3\2\2\2\u01da\u01db\3\2\2\2" +
					"\u01db\u01df\3\2\2\2\u01dc\u01e0\7@\2\2\u01dd\u01e0\7?\2\2\u01de\u01e0" +
					"\5^\60\2\u01df\u01dc\3\2\2\2\u01df\u01dd\3\2\2\2\u01df\u01de\3\2\2\2\u01e0" +
					"\u01e2\3\2\2\2\u01e1\u01e3\5V,\2\u01e2\u01e1\3\2\2\2\u01e2\u01e3\3\2\2" +
					"\2\u01e3\u01e7\3\2\2\2\u01e4\u01e6\5\\/\2\u01e5\u01e4\3\2\2\2\u01e6\u01e9" +
					"\3\2\2\2\u01e7\u01e5\3\2\2\2\u01e7\u01e8\3\2\2\2\u01e8\u01eb\3\2\2\2\u01e9" +
					"\u01e7\3\2\2\2\u01ea\u01ec\7;\2\2\u01eb\u01ea\3\2\2\2\u01eb\u01ec\3\2" +
					"\2\2\u01ec/\3\2\2\2\u01ed\u01ee\t\5\2\2\u01ee\u01f0\5^\60\2\u01ef\u01f1" +
					"\5T+\2\u01f0\u01ef\3\2\2\2\u01f0\u01f1\3\2\2\2\u01f1\u01f5\3\2\2\2\u01f2" +
					"\u01f4\5\\/\2\u01f3\u01f2\3\2\2\2\u01f4\u01f7\3\2\2\2\u01f5\u01f3\3\2" +
					"\2\2\u01f5\u01f6\3\2\2\2\u01f6\u01f9\3\2\2\2\u01f7\u01f5\3\2\2\2\u01f8" +
					"\u01fa\7;\2\2\u01f9\u01f8\3\2\2\2\u01f9\u01fa\3\2\2\2\u01fa\u01fc\3\2" +
					"\2\2\u01fb\u01fd\5\64\33\2\u01fc\u01fb\3\2\2\2\u01fc\u01fd\3\2\2\2\u01fd" +
					"\61\3\2\2\2\u01fe\u0202\t\3\2\2\u01ff\u0202\t\6\2\2\u0200\u0202\5^\60" +
					"\2\u0201\u01fe\3\2\2\2\u0201\u01ff\3\2\2\2\u0201\u0200\3\2\2\2\u0202\u0204" +
					"\3\2\2\2\u0203\u0205\7A\2\2\u0204\u0203\3\2\2\2\u0204\u0205\3\2\2\2\u0205" +
					"\u0206\3\2\2\2\u0206\u0208\t\4\2\2\u0207\u0209\7A\2\2\u0208\u0207\3\2" +
					"\2\2\u0208\u0209\3\2\2\2\u0209\u020d\3\2\2\2\u020a\u020e\t\3\2\2\u020b" +
					"\u020e\t\6\2\2\u020c\u020e\5^\60\2\u020d\u020a\3\2\2\2\u020d\u020b\3\2" +
					"\2\2\u020d\u020c\3\2\2\2\u020e\u0214\3\2\2\2\u020f\u0212\78\2\2\u0210" +
					"\u0213\5d\63\2\u0211\u0213\7A\2\2\u0212\u0210\3\2\2\2\u0212\u0211\3\2" +
					"\2\2\u0213\u0215\3\2\2\2\u0214\u020f\3\2\2\2\u0214\u0215\3\2\2\2\u0215" +
					"\63\3\2\2\2\u0216\u0218\7\66\2\2\u0217\u0219\7<\2\2\u0218\u0217\3\2\2" +
					"\2\u0219\u021a\3\2\2\2\u021a\u0218\3\2\2\2\u021a\u021b\3\2\2\2\u021b\u0224" +
					"\3\2\2\2\u021c\u021e\5\66\34\2\u021d\u021f\7<\2\2\u021e\u021d\3\2\2\2" +
					"\u021f\u0220\3\2\2\2\u0220\u021e\3\2\2\2\u0220\u0221\3\2\2\2\u0221\u0223" +
					"\3\2\2\2\u0222\u021c\3\2\2\2\u0223\u0226\3\2\2\2\u0224\u0222\3\2\2\2\u0224" +
					"\u0225\3\2\2\2\u0225\u0227\3\2\2\2\u0226\u0224\3\2\2\2\u0227\u0228\7\67" +
					"\2\2\u0228\65\3\2\2\2\u0229\u0232\5P)\2\u022a\u0232\5R*\2\u022b\u0232" +
					"\5\22\n\2\u022c\u0232\5,\27\2\u022d\u0232\5.\30\2\u022e\u0232\5\20\t\2" +
					"\u022f\u0232\5\60\31\2\u0230\u0232\5\62\32\2\u0231\u0229\3\2\2\2\u0231" +
					"\u022a\3\2\2\2\u0231\u022b\3\2\2\2\u0231\u022c\3\2\2\2\u0231\u022d\3\2" +
					"\2\2\u0231\u022e\3\2\2\2\u0231\u022f\3\2\2\2\u0231\u0230\3\2\2\2\u0232" +
					"\67\3\2\2\2\u0233\u0234\t\7\2\2\u0234\u0236\5^\60\2\u0235\u0237\5T+\2" +
					"\u0236\u0235\3\2\2\2\u0236\u0237\3\2\2\2\u0237\u023b\3\2\2\2\u0238\u023a" +
					"\5\\/\2\u0239\u0238\3\2\2\2\u023a\u023d\3\2\2\2\u023b\u0239\3\2\2\2\u023b" +
					"\u023c\3\2\2\2\u023c\u023f\3\2\2\2\u023d\u023b\3\2\2\2\u023e\u0240\5N" +
					"(\2\u023f\u023e\3\2\2\2\u023f\u0240\3\2\2\2\u0240\u0242\3\2\2\2\u0241" +
					"\u0243\7;\2\2\u0242\u0241\3\2\2\2\u0242\u0243\3\2\2\2\u02439\3\2\2\2\u0244" +
					"\u0246\5^\60\2\u0245\u0247\5T+\2\u0246\u0245\3\2\2\2\u0246\u0247\3\2\2" +
					"\2\u0247\u0249\3\2\2\2\u0248\u0244\3\2\2\2\u0248\u0249\3\2\2\2\u0249\u024a" +
					"\3\2\2\2\u024a\u024f\t\b\2\2\u024b\u024d\5^\60\2\u024c\u024e\5T+\2\u024d" +
					"\u024c\3\2\2\2\u024d\u024e\3\2\2\2\u024e\u0250\3\2\2\2\u024f\u024b\3\2" +
					"\2\2\u024f\u0250\3\2\2\2\u0250\u0258\3\2\2\2\u0251\u0254\78\2\2\u0252" +
					"\u0255\5d\63\2\u0253\u0255\7A\2\2\u0254\u0252\3\2\2\2\u0254\u0253\3\2" +
					"\2\2\u0255\u0259\3\2\2\2\u0256\u0257\t\6\2\2\u0257\u0259\5d\63\2\u0258" +
					"\u0251\3\2\2\2\u0258\u0256\3\2\2\2\u0258\u0259\3\2\2\2\u0259;\3\2\2\2" +
					"\u025a\u025c\7 \2\2\u025b\u025d\5d\63\2\u025c\u025b\3\2\2\2\u025c\u025d" +
					"\3\2\2\2\u025d\u025e\3\2\2\2\u025e\u025f\5J&\2\u025f\u0263\3\2\2\2\u0260" +
					"\u0262\5L\'\2\u0261\u0260\3\2\2\2\u0262\u0265\3\2\2\2\u0263\u0261\3\2" +
					"\2\2\u0263\u0264\3\2\2\2\u0264\u0266\3\2\2\2\u0265\u0263\3\2\2\2\u0266" +
					"\u0268\7.\2\2\u0267\u0269\5`\61\2\u0268\u0267\3\2\2\2\u0268\u0269\3\2" +
					"\2\2\u0269=\3\2\2\2\u026a\u026c\7!\2\2\u026b\u026d\t\7\2\2\u026c\u026b" +
					"\3\2\2\2\u026c\u026d\3\2\2\2\u026d\u026e\3\2\2\2\u026e\u0270\5^\60\2\u026f" +
					"\u0271\5T+\2\u0270\u026f\3\2\2\2\u0270\u0271\3\2\2\2\u0271\u0275\3\2\2" +
					"\2\u0272\u0274\5\\/\2\u0273\u0272\3\2\2\2\u0274\u0277\3\2\2\2\u0275\u0273" +
					"\3\2\2\2\u0275\u0276\3\2\2\2\u0276\u0279\3\2\2\2\u0277\u0275\3\2\2\2\u0278" +
					"\u027a\5N(\2\u0279\u0278\3\2\2\2\u0279\u027a\3\2\2\2\u027a\u027c\3\2\2" +
					"\2\u027b\u027d\7;\2\2\u027c\u027b\3\2\2\2\u027c\u027d\3\2\2\2\u027d\u027e" +
					"\3\2\2\2\u027e\u027f\7<\2\2\u027f\u0280\5:\36\2\u0280?\3\2\2\2\u0281\u0282" +
					"\7\"\2\2\u0282\u0283\5`\61\2\u0283A\3\2\2\2\u0284\u0285\7%\2\2\u0285\u0286" +
					"\5`\61\2\u0286C\3\2\2\2\u0287\u0288\7&\2\2\u0288\u0289\5`\61\2\u0289E" +
					"\3\2\2\2\u028a\u028b\7)\2\2\u028b\u028c\7#\2\2\u028c\u028d\5`\61\2\u028d" +
					"G\3\2\2\2\u028e\u028f\7$\2\2\u028f\u0290\7#\2\2\u0290\u02a4\5^\60\2\u0291" +
					"\u0293\7<\2\2\u0292\u0294\5f\64\2\u0293\u0292\3\2\2\2\u0293\u0294\3\2" +
					"\2\2\u0294\u0298\3\2\2\2\u0295\u0297\7<\2\2\u0296\u0295\3\2\2\2\u0297" +
					"\u029a\3\2\2\2\u0298\u0296\3\2\2\2\u0298\u0299\3\2\2\2\u0299\u029b\3\2" +
					"\2\2\u029a\u0298\3\2\2\2\u029b\u029d\7.\2\2\u029c\u029e\7$\2\2\u029d\u029c" +
					"\3\2\2\2\u029d\u029e\3\2\2\2\u029e\u02a5\3\2\2\2\u029f\u02a2\78\2\2\u02a0" +
					"\u02a3\5d\63\2\u02a1\u02a3\7A\2\2\u02a2\u02a0\3\2\2\2\u02a2\u02a1\3\2" +
					"\2\2\u02a3\u02a5\3\2\2\2\u02a4\u0291\3\2\2\2\u02a4\u029f\3\2\2\2\u02a5" +
					"I\3\2\2\2\u02a6\u02a8\7<\2\2\u02a7\u02a6\3\2\2\2\u02a8\u02a9\3\2\2\2\u02a9" +
					"\u02a7\3\2\2\2\u02a9\u02aa\3\2\2\2\u02aa\u02bd\3\2\2\2\u02ab\u02b5\5:" +
					"\36\2\u02ac\u02b5\58\35\2\u02ad\u02b5\5<\37\2\u02ae\u02b5\5H%\2\u02af" +
					"\u02b5\5> \2\u02b0\u02b5\5@!\2\u02b1\u02b5\5B\"\2\u02b2\u02b5\5D#\2\u02b3" +
					"\u02b5\5F$\2\u02b4\u02ab\3\2\2\2\u02b4\u02ac\3\2\2\2\u02b4\u02ad\3\2\2" +
					"\2\u02b4\u02ae\3\2\2\2\u02b4\u02af\3\2\2\2\u02b4\u02b0\3\2\2\2\u02b4\u02b1" +
					"\3\2\2\2\u02b4\u02b2\3\2\2\2\u02b4\u02b3\3\2\2\2\u02b5\u02b7\3\2\2\2\u02b6" +
					"\u02b8\7<\2\2\u02b7\u02b6\3\2\2\2\u02b8\u02b9\3\2\2\2\u02b9\u02b7\3\2" +
					"\2\2\u02b9\u02ba\3\2\2\2\u02ba\u02bc\3\2\2\2\u02bb\u02b4\3\2\2\2\u02bc" +
					"\u02bf\3\2\2\2\u02bd\u02bb\3\2\2\2\u02bd\u02be\3\2\2\2\u02beK\3\2\2\2" +
					"\u02bf\u02bd\3\2\2\2\u02c0\u02c2\7\37\2\2\u02c1\u02c3\5d\63\2\u02c2\u02c1" +
					"\3\2\2\2\u02c2\u02c3\3\2\2\2\u02c3\u02c4\3\2\2\2\u02c4\u02c5\5J&\2\u02c5" +
					"M\3\2\2\2\u02c6\u02c7\7\35\2\2\u02c7\u02c8\5^\60\2\u02c8O\3\2\2\2\u02c9" +
					"\u02ca\7\33\2\2\u02caQ\3\2\2\2\u02cb\u02cc\7\34\2\2\u02ccS\3\2\2\2\u02cd" +
					"\u02ce\7/\2\2\u02ce\u02cf\5^\60\2\u02cfU\3\2\2\2\u02d0\u02d7\7/\2\2\u02d1" +
					"\u02d8\5h\65\2\u02d2\u02d8\7?\2\2\u02d3\u02d8\7@\2\2\u02d4\u02d8\7C\2" +
					"\2\u02d5\u02d8\7B\2\2\u02d6\u02d8\7A\2\2\u02d7\u02d1\3\2\2\2\u02d7\u02d2" +
					"\3\2\2\2\u02d7\u02d3\3\2\2\2\u02d7\u02d4\3\2\2\2\u02d7\u02d5\3\2\2\2\u02d7" +
					"\u02d6\3\2\2\2\u02d8W\3\2\2\2\u02d9\u02e0\7/\2\2\u02da\u02e1\5h\65\2\u02db" +
					"\u02e1\7=\2\2\u02dc\u02e1\7>\2\2\u02dd\u02e1\7C\2\2\u02de\u02e1\7B\2\2" +
					"\u02df\u02e1\7A\2\2\u02e0\u02da\3\2\2\2\u02e0\u02db\3\2\2\2\u02e0\u02dc" +
					"\3\2\2\2\u02e0\u02dd\3\2\2\2\u02e0\u02de\3\2\2\2\u02e0\u02df\3\2\2\2\u02e1" +
					"Y\3\2\2\2\u02e2\u02e3\7\62\2\2\u02e3\u02e4\5b\62\2\u02e4\u02e5\7\63\2" +
					"\2\u02e5[\3\2\2\2\u02e6\u02e7\7\64\2\2\u02e7\u02e8\5b\62\2\u02e8\u02e9" +
					"\7\65\2\2\u02e9]\3\2\2\2\u02ea\u02ee\5h\65\2\u02eb\u02ee\7C\2\2\u02ec" +
					"\u02ee\7A\2\2\u02ed\u02ea\3\2\2\2\u02ed\u02eb\3\2\2\2\u02ed\u02ec\3\2" +
					"\2\2\u02ee_\3\2\2\2\u02ef\u02f4\5h\65\2\u02f0\u02f4\7C\2\2\u02f1\u02f4" +
					"\7E\2\2\u02f2\u02f4\78\2\2\u02f3\u02ef\3\2\2\2\u02f3\u02f0\3\2\2\2\u02f3" +
					"\u02f1\3\2\2\2\u02f3\u02f2\3\2\2\2\u02f4\u02f5\3\2\2\2\u02f5\u02f3\3\2" +
					"\2\2\u02f5\u02f6\3\2\2\2\u02f6a\3\2\2\2\u02f7\u0301\5h\65\2\u02f8\u0301" +
					"\7C\2\2\u02f9\u0301\7E\2\2\u02fa\u0301\78\2\2\u02fb\u0301\7:\2\2\u02fc" +
					"\u0301\7F\2\2\u02fd\u0301\7G\2\2\u02fe\u0301\7\5\2\2\u02ff\u0301\7\6\2" +
					"\2\u0300\u02f7\3\2\2\2\u0300\u02f8\3\2\2\2\u0300\u02f9\3\2\2\2\u0300\u02fa" +
					"\3\2\2\2\u0300\u02fb\3\2\2\2\u0300\u02fc\3\2\2\2\u0300\u02fd\3\2\2\2\u0300" +
					"\u02fe\3\2\2\2\u0300\u02ff\3\2\2\2\u0301\u0302\3\2\2\2\u0302\u0300\3\2" +
					"\2\2\u0302\u0303\3\2\2\2\u0303c\3\2\2\2\u0304\u0316\5h\65\2\u0305\u0316" +
					"\7C\2\2\u0306\u0316\7E\2\2\u0307\u0316\78\2\2\u0308\u0316\7\63\2\2\u0309" +
					"\u0316\7\62\2\2\u030a\u0316\7\65\2\2\u030b\u0316\7\64\2\2\u030c\u0316" +
					"\7?\2\2\u030d\u0316\7@\2\2\u030e\u0316\7>\2\2\u030f\u0316\7=\2\2\u0310" +
					"\u0316\7:\2\2\u0311\u0316\7F\2\2\u0312\u0316\7G\2\2\u0313\u0316\7\5\2" +
					"\2\u0314\u0316\7\6\2\2\u0315\u0304\3\2\2\2\u0315\u0305\3\2\2\2\u0315\u0306" +
					"\3\2\2\2\u0315\u0307\3\2\2\2\u0315\u0308\3\2\2\2\u0315\u0309\3\2\2\2\u0315" +
					"\u030a\3\2\2\2\u0315\u030b\3\2\2\2\u0315\u030c\3\2\2\2\u0315\u030d\3\2" +
					"\2\2\u0315\u030e\3\2\2\2\u0315\u030f\3\2\2\2\u0315\u0310\3\2\2\2\u0315" +
					"\u0311\3\2\2\2\u0315\u0312\3\2\2\2\u0315\u0313\3\2\2\2\u0315\u0314\3\2" +
					"\2\2\u0316\u0317\3\2\2\2\u0317\u0315\3\2\2\2\u0317\u0318\3\2\2\2\u0318" +
					"e\3\2\2\2\u0319\u032b\5h\65\2\u031a\u032b\7C\2\2\u031b\u032b\7E\2\2\u031c" +
					"\u032b\78\2\2\u031d\u032b\7\63\2\2\u031e\u032b\7\62\2\2\u031f\u032b\7" +
					"\65\2\2\u0320\u032b\7\64\2\2\u0321\u032b\7?\2\2\u0322\u032b\7@\2\2\u0323" +
					"\u032b\7>\2\2\u0324\u032b\7=\2\2\u0325\u032b\7:\2\2\u0326\u032b\7F\2\2" +
					"\u0327\u032b\7G\2\2\u0328\u032b\7\5\2\2\u0329\u032b\7\6\2\2\u032a\u0319" +
					"\3\2\2\2\u032a\u031a\3\2\2\2\u032a\u031b\3\2\2\2\u032a\u031c\3\2\2\2\u032a" +
					"\u031d\3\2\2\2\u032a\u031e\3\2\2\2\u032a\u031f\3\2\2\2\u032a\u0320\3\2" +
					"\2\2\u032a\u0321\3\2\2\2\u032a\u0322\3\2\2\2\u032a\u0323\3\2\2\2\u032a" +
					"\u0324\3\2\2\2\u032a\u0325\3\2\2\2\u032a\u0326\3\2\2\2\u032a\u0327\3\2" +
					"\2\2\u032a\u0328\3\2\2\2\u032a\u0329\3\2\2\2\u032b\u032c\3\2\2\2\u032c" +
					"\u032a\3\2\2\2\u032c\u032d\3\2\2\2\u032d\u032e\3\2\2\2\u032e\u0330\n\t" +
					"\2\2\u032f\u032a\3\2\2\2\u0330\u0333\3\2\2\2\u0331\u032f\3\2\2\2\u0331" +
					"\u0332\3\2\2\2\u0332g\3\2\2\2\u0333\u0331\3\2\2\2\u0334\u0335\t\n\2\2" +
					"\u0335i\3\2\2\2\177mty\177\u008a\u008f\u0093\u009a\u009f\u00a3\u00a9\u00ae" +
					"\u00b2\u00ba\u00c1\u00cc\u00d2\u00db\u00dd\u00e0\u00e4\u00e9\u00f1\u00f6" +
					"\u00fb\u00ff\u0103\u0107\u010c\u0114\u0116\u0118\u0122\u0125\u012a\u012e" +
					"\u0132\u0136\u0139\u0142\u0147\u014b\u014e\u0152\u0155\u0159\u015f\u0161" +
					"\u0168\u016a\u0170\u0176\u017a\u0183\u0189\u018d\u0195\u01a8\u01ac\u01b2" +
					"\u01bd\u01c6\u01cb\u01ce\u01d3\u01d7\u01da\u01df\u01e2\u01e7\u01eb\u01f0" +
					"\u01f5\u01f9\u01fc\u0201\u0204\u0208\u020d\u0212\u0214\u021a\u0220\u0224" +
					"\u0231\u0236\u023b\u023f\u0242\u0246\u0248\u024d\u024f\u0254\u0258\u025c" +
					"\u0263\u0268\u026c\u0270\u0275\u0279\u027c\u0293\u0298\u029d\u02a2\u02a4" +
					"\u02a9\u02b4\u02b9\u02bd\u02c2\u02d7\u02e0\u02ed\u02f3\u02f5\u0300\u0302" +
					"\u0315\u0317\u032a\u032c\u0331";
	public static final ATN _ATN =
			new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
			new PredictionContextCache();
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	static {
		RuntimeMetaData.checkVersion("4.9.1", RuntimeMetaData.VERSION);
	}

	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	public PlantUMLParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this, _ATN, _decisionToDFA, _sharedContextCache);
	}

	private static String[] makeRuleNames() {
		return new String[]{
				"uml", "classDiagram", "useCaseDiagram", "sequenceDiagram", "classDiagramLine",
				"useCaseDiagramLine", "sequenceDiagramLine", "noteRule", "skinparamRule",
				"autonumberRule", "classRule", "addResourceToClassRule", "classPackageRule",
				"classDiagramRelationRule", "classTypeWithAbstract", "classBody", "classPackageBody",
				"classBodyLine", "classPackageBodyLine", "hideRule", "showRule", "actorRule",
				"useCaseRule", "useCasePackageRule", "useCaseDiagramRelationRule", "useCasePackageBody",
				"useCasePackageBodyLine", "participantRule", "messageRule", "groupRule",
				"createRule", "returnRule", "activateRule", "deactivateRule", "sequenceNoteRule",
				"refRule", "groupCase", "elseGroupBranch", "order", "topToBottomDirection",
				"leftToRightDirection", "basicAlias", "useCaseAlias", "actorAlias", "generics",
				"stereotypes", "identificator", "identificator_with_space", "dentificator_with_space_and_special_chars_without_angle_brackets",
				"identificator_with_space_and_special_chars", "identificator_of_note",
				"keyword"
		};
	}

	private static String[] makeLiteralNames() {
		return new String[]{
				null, "'@startuml'", "'@enduml'", "'['", "']'", null, null, null, null,
				"'class'", null, "'abstract'", "'extends'", "'implements'", "'field'",
				"'method'", "'static'", "'classifier'", "'package'", "'namespace'", "'rectangle'",
				"'usecase'", "'actor'", "'system'", "'entity'", "'top to bottom direction'",
				"'left to right direction'", "'order'", null, "'else'", null, "'create'",
				"'return'", "'over'", "'ref'", "'activate'", "'deactivate'", "'hide'",
				"'show'", "'note'", "'skinparam'", "'autonumber'", null, "'of'", "'end'",
				"'as'", null, null, "'<'", "'>'", "'<<'", "'>>'", "'{'", "'}'", "':'",
				"'-'"
		};
	}

	private static String[] makeSymbolicNames() {
		return new String[]{
				null, null, null, null, null, "COMMENT", "ARROW_DOTTED", "ARROW_DASHED",
				"ARROW_SEQUENCE_DIAGRAM", "CLASS_TYPE_KEYWORD", "CLASS_ANOTHER_TYPES_KEYWORD",
				"ABSTRACT_KEYWORD", "EXTENDS_KEYWORD", "IMPLEMENTS_KEYWORD", "FIELD_KEYWORD",
				"METHOD_KEYWORD", "STATIC_KEYWORD", "CLASSIFIER_KEYWORD", "PACKAGE_KEYWORD",
				"NAMESPACE_KEYWORD", "RECTANGLE_KEYWORD", "USECASE_KEYWORD", "ACTOR_KEYWORD",
				"SYSTEM_KEYWORD", "ENTITY_KEYWORD", "TOP_TO_BOTTOM_DIRECTION_KEYWORD",
				"LEFT_TO_RIGHT_DIRECTION_KEYWORD", "ORDER_KEYWORD", "PARTICIPANT_TYPE_KEYWORD",
				"ELSE_KEYWORD", "GROUP_TYPE_KEYWORD", "CREATE_KEYWORD", "RETURN_KEYWORD",
				"OVER_KEYWORD", "REF_KEYWORD", "ACTIVATE_KEYWORD", "DEACTIVATE_KEYWORD",
				"HIDE_KEYWORD", "SHOW_KEYWORD", "NOTE_KEYWORD", "SKINPARAM_KEYWORD",
				"AUTONUMBER_KEYWORD", "DIRECTION_KEYWORD", "OF_KEYWORD", "END_KEYWORD",
				"AS_KEYWORD", "ARROW_MIDDLE_DOTTED", "ARROW_MIDDLE_DASHED", "OPENED_ANGLE_BRACKET",
				"CLOSED_ANGLE_BRACKET", "DOUBLE_OPENED_ANGLE_BRACKET", "DOUBLE_CLOSED_ANGLE_BRACKET",
				"OPENED_CURLY_BRACKET", "CLOSED_CURLY_BRACKET", "COLON", "DASH", "VISIBILITY_SIGN",
				"HEX_COLOR", "NEWLINE", "IDENTIFICATOR_CLOSED_IN_COLONS", "IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES",
				"IDENTIFICATOR_CLOSED_IN_BRACKETS", "IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES",
				"QUOTED_IDENTIFICATOR", "QUOTED_IDENTIFICATOR_WITH_NEW_LINES", "IDENTIFICATOR",
				"WS", "SPACE", "QUOTE", "DOT"
		};
	}

	@Override
	public String getGrammarFileName() {
		return "PlantUML.g4";
	}

	@Override
	public String[] getRuleNames() {
		return ruleNames;
	}

	@Override
	public String getSerializedATN() {
		return _serializedATN;
	}

	@Override
	public ATN getATN() {
		return _ATN; }

	public final UmlContext uml() throws RecognitionException {
		UmlContext _localctx = new UmlContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_uml);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(107);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == NEWLINE) {
					{
						{
							setState(104);
							match(NEWLINE);
						}
					}
					setState(109);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(110);
				match(T__0);
				setState(112);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						{
							setState(111);
							match(NEWLINE);
						}
					}
					setState(114);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while (_la == NEWLINE);
				setState(119);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 2, _ctx)) {
					case 1: {
						setState(116);
						sequenceDiagram();
					}
					break;
					case 2: {
						setState(117);
						classDiagram();
					}
					break;
					case 3: {
						setState(118);
						useCaseDiagram();
					}
					break;
				}
				setState(121);
				match(T__1);
				setState(125);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == NEWLINE) {
					{
						{
							setState(122);
							match(NEWLINE);
						}
					}
					setState(127);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(128);
				match(EOF);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassDiagramContext classDiagram() throws RecognitionException {
		ClassDiagramContext _localctx = new ClassDiagramContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_classDiagram);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(145);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (ENTITY_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES - 9)) | (1L << (QUOTED_IDENTIFICATOR - 9)) | (1L << (IDENTIFICATOR - 9)))) != 0)) {
					{
						{
							setState(136);
							_errHandler.sync(this);
							switch (getInterpreter().adaptivePredict(_input, 4, _ctx)) {
								case 1: {
									setState(130);
									hideRule();
								}
								break;
								case 2: {
									setState(131);
									showRule();
								}
								break;
								case 3: {
									setState(132);
									topToBottomDirection();
								}
								break;
								case 4: {
									setState(133);
									leftToRightDirection();
								}
								break;
								case 5: {
									setState(134);
									skinparamRule();
								}
								break;
								case 6: {
									setState(135);
									classDiagramLine();
								}
								break;
							}
							setState(139);
							_errHandler.sync(this);
							_la = _input.LA(1);
							do {
								{
									{
										setState(138);
										match(NEWLINE);
									}
								}
								setState(141);
								_errHandler.sync(this);
								_la = _input.LA(1);
							} while (_la == NEWLINE);
						}
					}
					setState(147);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCaseDiagramContext useCaseDiagram() throws RecognitionException {
		UseCaseDiagramContext _localctx = new UseCaseDiagramContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_useCaseDiagram);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(161);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_COLONS - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES - 9)) | (1L << (QUOTED_IDENTIFICATOR - 9)) | (1L << (IDENTIFICATOR - 9)))) != 0)) {
					{
						{
							setState(152);
							_errHandler.sync(this);
							switch (getInterpreter().adaptivePredict(_input, 7, _ctx)) {
								case 1: {
									setState(148);
									topToBottomDirection();
								}
								break;
								case 2: {
									setState(149);
									leftToRightDirection();
								}
								break;
								case 3: {
									setState(150);
									skinparamRule();
								}
								break;
								case 4: {
									setState(151);
									useCaseDiagramLine();
								}
								break;
							}
							setState(155);
							_errHandler.sync(this);
							_la = _input.LA(1);
							do {
								{
									{
										setState(154);
										match(NEWLINE);
									}
								}
								setState(157);
								_errHandler.sync(this);
								_la = _input.LA(1);
							} while (_la == NEWLINE);
						}
					}
					setState(163);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final SequenceDiagramContext sequenceDiagram() throws RecognitionException {
		SequenceDiagramContext _localctx = new SequenceDiagramContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_sequenceDiagram);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(176);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 7)) & ~0x3f) == 0 && ((1L << (_la - 7)) & ((1L << (ARROW_DASHED - 7)) | (1L << (ARROW_SEQUENCE_DIAGRAM - 7)) | (1L << (CLASS_TYPE_KEYWORD - 7)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 7)) | (1L << (ABSTRACT_KEYWORD - 7)) | (1L << (EXTENDS_KEYWORD - 7)) | (1L << (IMPLEMENTS_KEYWORD - 7)) | (1L << (FIELD_KEYWORD - 7)) | (1L << (METHOD_KEYWORD - 7)) | (1L << (STATIC_KEYWORD - 7)) | (1L << (CLASSIFIER_KEYWORD - 7)) | (1L << (PACKAGE_KEYWORD - 7)) | (1L << (NAMESPACE_KEYWORD - 7)) | (1L << (RECTANGLE_KEYWORD - 7)) | (1L << (USECASE_KEYWORD - 7)) | (1L << (ACTOR_KEYWORD - 7)) | (1L << (SYSTEM_KEYWORD - 7)) | (1L << (ENTITY_KEYWORD - 7)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 7)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 7)) | (1L << (ORDER_KEYWORD - 7)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 7)) | (1L << (ELSE_KEYWORD - 7)) | (1L << (GROUP_TYPE_KEYWORD - 7)) | (1L << (CREATE_KEYWORD - 7)) | (1L << (RETURN_KEYWORD - 7)) | (1L << (OVER_KEYWORD - 7)) | (1L << (REF_KEYWORD - 7)) | (1L << (ACTIVATE_KEYWORD - 7)) | (1L << (DEACTIVATE_KEYWORD - 7)) | (1L << (HIDE_KEYWORD - 7)) | (1L << (SHOW_KEYWORD - 7)) | (1L << (NOTE_KEYWORD - 7)) | (1L << (SKINPARAM_KEYWORD - 7)) | (1L << (AUTONUMBER_KEYWORD - 7)) | (1L << (DIRECTION_KEYWORD - 7)) | (1L << (OF_KEYWORD - 7)) | (1L << (END_KEYWORD - 7)) | (1L << (AS_KEYWORD - 7)) | (1L << (QUOTED_IDENTIFICATOR - 7)) | (1L << (IDENTIFICATOR - 7)))) != 0)) {
					{
						{
							setState(167);
							_errHandler.sync(this);
							switch (getInterpreter().adaptivePredict(_input, 10, _ctx)) {
								case 1: {
									setState(164);
									skinparamRule();
								}
								break;
								case 2: {
									setState(165);
									autonumberRule();
								}
								break;
								case 3: {
									setState(166);
									sequenceDiagramLine();
								}
								break;
							}
							setState(170);
							_errHandler.sync(this);
							_la = _input.LA(1);
							do {
								{
									{
										setState(169);
										match(NEWLINE);
									}
								}
								setState(172);
								_errHandler.sync(this);
								_la = _input.LA(1);
							} while (_la == NEWLINE);
						}
					}
					setState(178);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassDiagramLineContext classDiagramLine() throws RecognitionException {
		ClassDiagramLineContext _localctx = new ClassDiagramLineContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_classDiagramLine);
		try {
			setState(184);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 13, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(179);
					classRule();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(180);
					addResourceToClassRule();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					setState(181);
					classPackageRule();
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(182);
					classDiagramRelationRule();
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(183);
					noteRule();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCaseDiagramLineContext useCaseDiagramLine() throws RecognitionException {
		UseCaseDiagramLineContext _localctx = new UseCaseDiagramLineContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_useCaseDiagramLine);
		try {
			setState(191);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 14, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(186);
					actorRule();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(187);
					useCaseRule();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					setState(188);
					useCasePackageRule();
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(189);
					useCaseDiagramRelationRule();
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(190);
					noteRule();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final SequenceDiagramLineContext sequenceDiagramLine() throws RecognitionException {
		SequenceDiagramLineContext _localctx = new SequenceDiagramLineContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_sequenceDiagramLine);
		try {
			setState(202);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 15, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(193);
					participantRule();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(194);
					messageRule();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					setState(195);
					groupRule();
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(196);
					refRule();
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(197);
					createRule();
				}
				break;
				case 6:
					enterOuterAlt(_localctx, 6);
				{
					setState(198);
					returnRule();
				}
				break;
				case 7:
					enterOuterAlt(_localctx, 7);
				{
					setState(199);
					activateRule();
				}
				break;
				case 8:
					enterOuterAlt(_localctx, 8);
				{
					setState(200);
					deactivateRule();
				}
				break;
				case 9:
					enterOuterAlt(_localctx, 9);
				{
					setState(201);
					sequenceNoteRule();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final NoteRuleContext noteRule() throws RecognitionException {
		NoteRuleContext _localctx = new NoteRuleContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_noteRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(204);
				match(NOTE_KEYWORD);
				setState(278);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case QUOTED_IDENTIFICATOR: {
						{
							setState(205);
							match(QUOTED_IDENTIFICATOR);
							setState(206);
							basicAlias();
							setState(208);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if (_la == HEX_COLOR) {
								{
									setState(207);
									match(HEX_COLOR);
								}
							}

						}
					}
					break;
					case DIRECTION_KEYWORD: {
						{
							setState(210);
							match(DIRECTION_KEYWORD);
							setState(219);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if (_la == OF_KEYWORD) {
								{
									setState(211);
									match(OF_KEYWORD);
									setState(217);
									_errHandler.sync(this);
									switch (_input.LA(1)) {
										case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
											setState(212);
											match(IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
										}
										break;
										case IDENTIFICATOR_CLOSED_IN_COLONS: {
											setState(213);
											match(IDENTIFICATOR_CLOSED_IN_COLONS);
										}
										break;
										case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
											setState(214);
											match(IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
										}
										break;
										case IDENTIFICATOR_CLOSED_IN_BRACKETS: {
											setState(215);
											match(IDENTIFICATOR_CLOSED_IN_BRACKETS);
										}
										break;
										case CLASS_TYPE_KEYWORD:
										case CLASS_ANOTHER_TYPES_KEYWORD:
										case ABSTRACT_KEYWORD:
										case EXTENDS_KEYWORD:
										case IMPLEMENTS_KEYWORD:
										case FIELD_KEYWORD:
										case METHOD_KEYWORD:
										case STATIC_KEYWORD:
										case CLASSIFIER_KEYWORD:
										case PACKAGE_KEYWORD:
										case NAMESPACE_KEYWORD:
										case RECTANGLE_KEYWORD:
										case USECASE_KEYWORD:
										case ACTOR_KEYWORD:
										case SYSTEM_KEYWORD:
										case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
										case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
										case ORDER_KEYWORD:
										case PARTICIPANT_TYPE_KEYWORD:
										case ELSE_KEYWORD:
										case GROUP_TYPE_KEYWORD:
										case CREATE_KEYWORD:
										case RETURN_KEYWORD:
										case OVER_KEYWORD:
										case REF_KEYWORD:
										case ACTIVATE_KEYWORD:
										case DEACTIVATE_KEYWORD:
										case HIDE_KEYWORD:
										case SHOW_KEYWORD:
										case NOTE_KEYWORD:
										case SKINPARAM_KEYWORD:
										case AUTONUMBER_KEYWORD:
										case DIRECTION_KEYWORD:
										case OF_KEYWORD:
										case END_KEYWORD:
										case AS_KEYWORD:
										case QUOTED_IDENTIFICATOR:
										case IDENTIFICATOR: {
											setState(216);
											identificator();
										}
										break;
										default:
											throw new NoViableAltException(this);
									}
								}
							}

							setState(222);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if (_la == HEX_COLOR) {
								{
									setState(221);
									match(HEX_COLOR);
								}
							}

							setState(253);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
								case NEWLINE: {
									{
										setState(224);
										match(NEWLINE);
										{
											setState(226);
											_errHandler.sync(this);
											switch (getInterpreter().adaptivePredict(_input, 20, _ctx)) {
												case 1: {
													setState(225);
													identificator_of_note();
												}
												break;
											}
											setState(231);
											_errHandler.sync(this);
											_la = _input.LA(1);
											while (_la == NEWLINE) {
												{
													{
														setState(228);
														match(NEWLINE);
													}
												}
												setState(233);
												_errHandler.sync(this);
												_la = _input.LA(1);
											}
										}
										setState(234);
										match(END_KEYWORD);
										setState(235);
										match(NOTE_KEYWORD);
									}
								}
								break;
								case COLON: {
									{
										setState(236);
										match(COLON);
										setState(239);
										_errHandler.sync(this);
										switch (_input.LA(1)) {
											case T__2:
											case T__3:
											case CLASS_TYPE_KEYWORD:
											case CLASS_ANOTHER_TYPES_KEYWORD:
											case ABSTRACT_KEYWORD:
											case EXTENDS_KEYWORD:
											case IMPLEMENTS_KEYWORD:
											case FIELD_KEYWORD:
											case METHOD_KEYWORD:
											case STATIC_KEYWORD:
											case CLASSIFIER_KEYWORD:
											case PACKAGE_KEYWORD:
											case NAMESPACE_KEYWORD:
											case RECTANGLE_KEYWORD:
											case USECASE_KEYWORD:
											case ACTOR_KEYWORD:
											case SYSTEM_KEYWORD:
											case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
											case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
											case ORDER_KEYWORD:
											case PARTICIPANT_TYPE_KEYWORD:
											case ELSE_KEYWORD:
											case GROUP_TYPE_KEYWORD:
											case CREATE_KEYWORD:
											case RETURN_KEYWORD:
											case OVER_KEYWORD:
											case REF_KEYWORD:
											case ACTIVATE_KEYWORD:
											case DEACTIVATE_KEYWORD:
											case HIDE_KEYWORD:
											case SHOW_KEYWORD:
											case NOTE_KEYWORD:
											case SKINPARAM_KEYWORD:
											case AUTONUMBER_KEYWORD:
											case DIRECTION_KEYWORD:
											case OF_KEYWORD:
											case END_KEYWORD:
											case AS_KEYWORD:
											case OPENED_ANGLE_BRACKET:
											case CLOSED_ANGLE_BRACKET:
											case DOUBLE_OPENED_ANGLE_BRACKET:
											case DOUBLE_CLOSED_ANGLE_BRACKET:
											case COLON:
											case VISIBILITY_SIGN:
											case IDENTIFICATOR_CLOSED_IN_COLONS:
											case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES:
											case IDENTIFICATOR_CLOSED_IN_BRACKETS:
											case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES:
											case IDENTIFICATOR:
											case SPACE:
											case QUOTE:
											case DOT: {
												setState(237);
												identificator_with_space_and_special_chars();
											}
											break;
											case QUOTED_IDENTIFICATOR: {
												setState(238);
												match(QUOTED_IDENTIFICATOR);
											}
											break;
											default:
												throw new NoViableAltException(this);
										}
									}
								}
								break;
								case OPENED_CURLY_BRACKET: {
									{
										setState(241);
										match(OPENED_CURLY_BRACKET);
										setState(242);
										match(NEWLINE);
										{
											setState(244);
											_errHandler.sync(this);
											switch (getInterpreter().adaptivePredict(_input, 23, _ctx)) {
												case 1: {
													setState(243);
													identificator_of_note();
												}
												break;
											}
											setState(249);
											_errHandler.sync(this);
											_la = _input.LA(1);
											while (_la == NEWLINE) {
												{
													{
														setState(246);
														match(NEWLINE);
													}
												}
												setState(251);
												_errHandler.sync(this);
												_la = _input.LA(1);
											}
										}
										setState(252);
										match(CLOSED_CURLY_BRACKET);
									}
								}
								break;
								default:
									throw new NoViableAltException(this);
							}
						}
					}
					break;
					case AS_KEYWORD: {
						{
							setState(255);
							basicAlias();
							setState(257);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if (_la == HEX_COLOR) {
								{
									setState(256);
									match(HEX_COLOR);
								}
							}

							setState(276);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
								case NEWLINE: {
									{
										setState(259);
										match(NEWLINE);
										{
											setState(261);
											_errHandler.sync(this);
											switch (getInterpreter().adaptivePredict(_input, 27, _ctx)) {
												case 1: {
													setState(260);
													identificator_of_note();
												}
												break;
											}
											setState(266);
											_errHandler.sync(this);
											_la = _input.LA(1);
											while (_la == NEWLINE) {
												{
													{
														setState(263);
														match(NEWLINE);
													}
												}
												setState(268);
												_errHandler.sync(this);
												_la = _input.LA(1);
											}
										}
										setState(269);
										match(END_KEYWORD);
										setState(270);
										match(NOTE_KEYWORD);
									}
								}
								break;
								case COLON: {
									{
										setState(271);
										match(COLON);
										setState(274);
										_errHandler.sync(this);
										switch (_input.LA(1)) {
											case T__2:
											case T__3:
											case CLASS_TYPE_KEYWORD:
											case CLASS_ANOTHER_TYPES_KEYWORD:
											case ABSTRACT_KEYWORD:
											case EXTENDS_KEYWORD:
											case IMPLEMENTS_KEYWORD:
											case FIELD_KEYWORD:
											case METHOD_KEYWORD:
											case STATIC_KEYWORD:
											case CLASSIFIER_KEYWORD:
											case PACKAGE_KEYWORD:
											case NAMESPACE_KEYWORD:
											case RECTANGLE_KEYWORD:
											case USECASE_KEYWORD:
											case ACTOR_KEYWORD:
											case SYSTEM_KEYWORD:
											case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
											case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
											case ORDER_KEYWORD:
											case PARTICIPANT_TYPE_KEYWORD:
											case ELSE_KEYWORD:
											case GROUP_TYPE_KEYWORD:
											case CREATE_KEYWORD:
											case RETURN_KEYWORD:
											case OVER_KEYWORD:
											case REF_KEYWORD:
											case ACTIVATE_KEYWORD:
											case DEACTIVATE_KEYWORD:
											case HIDE_KEYWORD:
											case SHOW_KEYWORD:
											case NOTE_KEYWORD:
											case SKINPARAM_KEYWORD:
											case AUTONUMBER_KEYWORD:
											case DIRECTION_KEYWORD:
											case OF_KEYWORD:
											case END_KEYWORD:
											case AS_KEYWORD:
											case OPENED_ANGLE_BRACKET:
											case CLOSED_ANGLE_BRACKET:
											case DOUBLE_OPENED_ANGLE_BRACKET:
											case DOUBLE_CLOSED_ANGLE_BRACKET:
											case COLON:
											case VISIBILITY_SIGN:
											case IDENTIFICATOR_CLOSED_IN_COLONS:
											case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES:
											case IDENTIFICATOR_CLOSED_IN_BRACKETS:
											case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES:
											case IDENTIFICATOR:
											case SPACE:
											case QUOTE:
											case DOT: {
												setState(272);
												identificator_with_space_and_special_chars();
											}
											break;
											case QUOTED_IDENTIFICATOR: {
												setState(273);
												match(QUOTED_IDENTIFICATOR);
											}
											break;
											default:
												throw new NoViableAltException(this);
										}
									}
								}
								break;
								default:
									throw new NoViableAltException(this);
							}
						}
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final SkinparamRuleContext skinparamRule() throws RecognitionException {
		SkinparamRuleContext _localctx = new SkinparamRuleContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_skinparamRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(280);
				match(SKINPARAM_KEYWORD);
				setState(281);
				identificator_with_space_and_special_chars();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final AutonumberRuleContext autonumberRule() throws RecognitionException {
		AutonumberRuleContext _localctx = new AutonumberRuleContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_autonumberRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(283);
				match(AUTONUMBER_KEYWORD);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassRuleContext classRule() throws RecognitionException {
		ClassRuleContext _localctx = new ClassRuleContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_classRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(285);
				classTypeWithAbstract();
				setState(286);
				identificator();
				setState(288);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == AS_KEYWORD) {
					{
						setState(287);
						basicAlias();
					}
				}

				setState(291);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == OPENED_ANGLE_BRACKET) {
					{
						setState(290);
						generics();
					}
				}

				setState(296);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
					{
						{
							setState(293);
							stereotypes();
						}
					}
					setState(298);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(300);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == HEX_COLOR) {
					{
						setState(299);
						match(HEX_COLOR);
					}
				}

				setState(304);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == EXTENDS_KEYWORD) {
					{
						setState(302);
						match(EXTENDS_KEYWORD);
						setState(303);
						identificator();
					}
				}

				setState(308);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == IMPLEMENTS_KEYWORD) {
					{
						setState(306);
						match(IMPLEMENTS_KEYWORD);
						setState(307);
						identificator();
					}
				}

				setState(311);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == OPENED_CURLY_BRACKET) {
					{
						setState(310);
						classBody();
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final AddResourceToClassRuleContext addResourceToClassRule() throws RecognitionException {
		AddResourceToClassRuleContext _localctx = new AddResourceToClassRuleContext(_ctx, getState());
		enterRule(_localctx, 22, RULE_addResourceToClassRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(313);
				identificator();
				setState(314);
				match(COLON);
				setState(315);
				classBodyLine();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassPackageRuleContext classPackageRule() throws RecognitionException {
		ClassPackageRuleContext _localctx = new ClassPackageRuleContext(_ctx, getState());
		enterRule(_localctx, 24, RULE_classPackageRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(317);
				_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(318);
				identificator();
				setState(320);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == AS_KEYWORD) {
					{
						setState(319);
						basicAlias();
					}
				}

				setState(325);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
					{
						{
							setState(322);
							stereotypes();
						}
					}
					setState(327);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(329);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == HEX_COLOR) {
					{
						setState(328);
						match(HEX_COLOR);
					}
				}

				setState(332);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == OPENED_CURLY_BRACKET) {
					{
						setState(331);
						classPackageBody();
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassDiagramRelationRuleContext classDiagramRelationRule() throws RecognitionException {
		ClassDiagramRelationRuleContext _localctx = new ClassDiagramRelationRuleContext(_ctx, getState());
		enterRule(_localctx, 26, RULE_classDiagramRelationRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(336);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case IDENTIFICATOR_CLOSED_IN_BRACKETS:
					case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
						setState(334);
						_la = _input.LA(1);
						if (!(_la == IDENTIFICATOR_CLOSED_IN_BRACKETS || _la == IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
					break;
					case CLASS_TYPE_KEYWORD:
					case CLASS_ANOTHER_TYPES_KEYWORD:
					case ABSTRACT_KEYWORD:
					case EXTENDS_KEYWORD:
					case IMPLEMENTS_KEYWORD:
					case FIELD_KEYWORD:
					case METHOD_KEYWORD:
					case STATIC_KEYWORD:
					case CLASSIFIER_KEYWORD:
					case PACKAGE_KEYWORD:
					case NAMESPACE_KEYWORD:
					case RECTANGLE_KEYWORD:
					case USECASE_KEYWORD:
					case ACTOR_KEYWORD:
					case SYSTEM_KEYWORD:
					case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
					case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
					case ORDER_KEYWORD:
					case PARTICIPANT_TYPE_KEYWORD:
					case ELSE_KEYWORD:
					case GROUP_TYPE_KEYWORD:
					case CREATE_KEYWORD:
					case RETURN_KEYWORD:
					case OVER_KEYWORD:
					case REF_KEYWORD:
					case ACTIVATE_KEYWORD:
					case DEACTIVATE_KEYWORD:
					case HIDE_KEYWORD:
					case SHOW_KEYWORD:
					case NOTE_KEYWORD:
					case SKINPARAM_KEYWORD:
					case AUTONUMBER_KEYWORD:
					case DIRECTION_KEYWORD:
					case OF_KEYWORD:
					case END_KEYWORD:
					case AS_KEYWORD:
					case QUOTED_IDENTIFICATOR:
					case IDENTIFICATOR: {
						setState(335);
						identificator();
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
				setState(339);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == QUOTED_IDENTIFICATOR) {
					{
						setState(338);
						match(QUOTED_IDENTIFICATOR);
					}
				}

				setState(341);
				_la = _input.LA(1);
				if (!(_la == ARROW_DOTTED || _la == ARROW_DASHED)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(343);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 45, _ctx)) {
					case 1: {
						setState(342);
						match(QUOTED_IDENTIFICATOR);
					}
					break;
				}
				setState(345);
				identificator();
				setState(351);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == COLON) {
					{
						setState(346);
						match(COLON);
						setState(349);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
							case T__2:
							case T__3:
							case CLASS_TYPE_KEYWORD:
							case CLASS_ANOTHER_TYPES_KEYWORD:
							case ABSTRACT_KEYWORD:
							case EXTENDS_KEYWORD:
							case IMPLEMENTS_KEYWORD:
							case FIELD_KEYWORD:
							case METHOD_KEYWORD:
							case STATIC_KEYWORD:
							case CLASSIFIER_KEYWORD:
							case PACKAGE_KEYWORD:
							case NAMESPACE_KEYWORD:
							case RECTANGLE_KEYWORD:
							case USECASE_KEYWORD:
							case ACTOR_KEYWORD:
							case SYSTEM_KEYWORD:
							case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
							case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
							case ORDER_KEYWORD:
							case PARTICIPANT_TYPE_KEYWORD:
							case ELSE_KEYWORD:
							case GROUP_TYPE_KEYWORD:
							case CREATE_KEYWORD:
							case RETURN_KEYWORD:
							case OVER_KEYWORD:
							case REF_KEYWORD:
							case ACTIVATE_KEYWORD:
							case DEACTIVATE_KEYWORD:
							case HIDE_KEYWORD:
							case SHOW_KEYWORD:
							case NOTE_KEYWORD:
							case SKINPARAM_KEYWORD:
							case AUTONUMBER_KEYWORD:
							case DIRECTION_KEYWORD:
							case OF_KEYWORD:
							case END_KEYWORD:
							case AS_KEYWORD:
							case OPENED_ANGLE_BRACKET:
							case CLOSED_ANGLE_BRACKET:
							case DOUBLE_OPENED_ANGLE_BRACKET:
							case DOUBLE_CLOSED_ANGLE_BRACKET:
							case COLON:
							case VISIBILITY_SIGN:
							case IDENTIFICATOR_CLOSED_IN_COLONS:
							case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES:
							case IDENTIFICATOR_CLOSED_IN_BRACKETS:
							case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES:
							case IDENTIFICATOR:
							case SPACE:
							case QUOTE:
							case DOT: {
								setState(347);
								identificator_with_space_and_special_chars();
							}
							break;
							case QUOTED_IDENTIFICATOR: {
								setState(348);
								match(QUOTED_IDENTIFICATOR);
							}
							break;
							default:
								throw new NoViableAltException(this);
						}
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassTypeWithAbstractContext classTypeWithAbstract() throws RecognitionException {
		ClassTypeWithAbstractContext _localctx = new ClassTypeWithAbstractContext(_ctx, getState());
		enterRule(_localctx, 28, RULE_classTypeWithAbstract);
		try {
			setState(360);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
				case CLASS_TYPE_KEYWORD:
					enterOuterAlt(_localctx, 1);
				{
					setState(353);
					match(CLASS_TYPE_KEYWORD);
				}
				break;
				case CLASS_ANOTHER_TYPES_KEYWORD:
					enterOuterAlt(_localctx, 2);
				{
					setState(354);
					match(CLASS_ANOTHER_TYPES_KEYWORD);
				}
				break;
				case ENTITY_KEYWORD:
					enterOuterAlt(_localctx, 3);
				{
					setState(355);
					match(ENTITY_KEYWORD);
				}
				break;
				case ABSTRACT_KEYWORD:
					enterOuterAlt(_localctx, 4);
				{
					setState(356);
					match(ABSTRACT_KEYWORD);
					setState(358);
					_errHandler.sync(this);
					switch (getInterpreter().adaptivePredict(_input, 48, _ctx)) {
						case 1: {
							setState(357);
							match(CLASS_TYPE_KEYWORD);
						}
						break;
					}
				}
				break;
				default:
					throw new NoViableAltException(this);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassBodyContext classBody() throws RecognitionException {
		ClassBodyContext _localctx = new ClassBodyContext(_ctx, getState());
		enterRule(_localctx, 30, RULE_classBody);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(362);
				match(OPENED_CURLY_BRACKET);
				setState(364);
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
						case 1: {
							{
								setState(363);
								match(NEWLINE);
							}
						}
						break;
						default:
							throw new NoViableAltException(this);
					}
					setState(366);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 50, _ctx);
				} while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
				setState(376);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << CLASS_TYPE_KEYWORD) | (1L << CLASS_ANOTHER_TYPES_KEYWORD) | (1L << ABSTRACT_KEYWORD) | (1L << EXTENDS_KEYWORD) | (1L << IMPLEMENTS_KEYWORD) | (1L << FIELD_KEYWORD) | (1L << METHOD_KEYWORD) | (1L << STATIC_KEYWORD) | (1L << CLASSIFIER_KEYWORD) | (1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD) | (1L << USECASE_KEYWORD) | (1L << ACTOR_KEYWORD) | (1L << SYSTEM_KEYWORD) | (1L << TOP_TO_BOTTOM_DIRECTION_KEYWORD) | (1L << LEFT_TO_RIGHT_DIRECTION_KEYWORD) | (1L << ORDER_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD) | (1L << ELSE_KEYWORD) | (1L << GROUP_TYPE_KEYWORD) | (1L << CREATE_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << OVER_KEYWORD) | (1L << REF_KEYWORD) | (1L << ACTIVATE_KEYWORD) | (1L << DEACTIVATE_KEYWORD) | (1L << HIDE_KEYWORD) | (1L << SHOW_KEYWORD) | (1L << NOTE_KEYWORD) | (1L << SKINPARAM_KEYWORD) | (1L << AUTONUMBER_KEYWORD) | (1L << DIRECTION_KEYWORD) | (1L << OF_KEYWORD) | (1L << END_KEYWORD) | (1L << AS_KEYWORD) | (1L << OPENED_ANGLE_BRACKET) | (1L << CLOSED_ANGLE_BRACKET) | (1L << DOUBLE_OPENED_ANGLE_BRACKET) | (1L << DOUBLE_CLOSED_ANGLE_BRACKET) | (1L << OPENED_CURLY_BRACKET) | (1L << COLON) | (1L << VISIBILITY_SIGN) | (1L << NEWLINE) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (IDENTIFICATOR - 65)) | (1L << (SPACE - 65)) | (1L << (QUOTE - 65)) | (1L << (DOT - 65)))) != 0)) {
					{
						{
							setState(368);
							classBodyLine();
							setState(370);
							_errHandler.sync(this);
							_alt = 1;
							do {
								switch (_alt) {
									case 1: {
										{
											setState(369);
											match(NEWLINE);
										}
									}
									break;
									default:
										throw new NoViableAltException(this);
								}
								setState(372);
								_errHandler.sync(this);
								_alt = getInterpreter().adaptivePredict(_input, 51, _ctx);
							} while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
						}
					}
					setState(378);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(379);
				match(CLOSED_CURLY_BRACKET);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassPackageBodyContext classPackageBody() throws RecognitionException {
		ClassPackageBodyContext _localctx = new ClassPackageBodyContext(_ctx, getState());
		enterRule(_localctx, 32, RULE_classPackageBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(381);
				match(OPENED_CURLY_BRACKET);
				setState(383);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						{
							setState(382);
							match(NEWLINE);
						}
					}
					setState(385);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while (_la == NEWLINE);
				setState(395);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (ENTITY_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES - 9)) | (1L << (QUOTED_IDENTIFICATOR - 9)) | (1L << (IDENTIFICATOR - 9)))) != 0)) {
					{
						{
							setState(387);
							classPackageBodyLine();
							setState(389);
							_errHandler.sync(this);
							_la = _input.LA(1);
							do {
								{
									{
										setState(388);
										match(NEWLINE);
									}
								}
								setState(391);
								_errHandler.sync(this);
								_la = _input.LA(1);
							} while (_la == NEWLINE);
						}
					}
					setState(397);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(398);
				match(CLOSED_CURLY_BRACKET);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassBodyLineContext classBodyLine() throws RecognitionException {
		ClassBodyLineContext _localctx = new ClassBodyLineContext(_ctx, getState());
		enterRule(_localctx, 34, RULE_classBodyLine);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(426);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 58, _ctx);
				while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
					if (_alt == 1) {
						{
							{
								setState(403);
								_errHandler.sync(this);
								_alt = getInterpreter().adaptivePredict(_input, 56, _ctx);
								while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
									if (_alt == 1) {
										{
											{
												setState(400);
												identificator_with_space_and_special_chars();
											}
										}
									}
									setState(405);
									_errHandler.sync(this);
									_alt = getInterpreter().adaptivePredict(_input, 56, _ctx);
								}
								setState(422);
								_errHandler.sync(this);
								switch (getInterpreter().adaptivePredict(_input, 57, _ctx)) {
									case 1: {
										setState(406);
										match(VISIBILITY_SIGN);
									}
									break;
									case 2: {
										setState(407);
										match(OPENED_CURLY_BRACKET);
										setState(408);
										match(FIELD_KEYWORD);
										setState(409);
										match(CLOSED_CURLY_BRACKET);
									}
									break;
									case 3: {
										setState(410);
										match(OPENED_CURLY_BRACKET);
										setState(411);
										match(METHOD_KEYWORD);
										setState(412);
										match(CLOSED_CURLY_BRACKET);
									}
									break;
									case 4: {
										setState(413);
										match(OPENED_CURLY_BRACKET);
										setState(414);
										match(ABSTRACT_KEYWORD);
										setState(415);
										match(CLOSED_CURLY_BRACKET);
									}
									break;
									case 5: {
										setState(416);
										match(OPENED_CURLY_BRACKET);
										setState(417);
										match(CLASSIFIER_KEYWORD);
										setState(418);
										match(CLOSED_CURLY_BRACKET);
									}
									break;
									case 6: {
										setState(419);
										match(OPENED_CURLY_BRACKET);
										setState(420);
										match(STATIC_KEYWORD);
										setState(421);
										match(CLOSED_CURLY_BRACKET);
									}
									break;
								}
							}
						}
					}
					setState(428);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 58, _ctx);
				}
				setState(432);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << CLASS_TYPE_KEYWORD) | (1L << CLASS_ANOTHER_TYPES_KEYWORD) | (1L << ABSTRACT_KEYWORD) | (1L << EXTENDS_KEYWORD) | (1L << IMPLEMENTS_KEYWORD) | (1L << FIELD_KEYWORD) | (1L << METHOD_KEYWORD) | (1L << STATIC_KEYWORD) | (1L << CLASSIFIER_KEYWORD) | (1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD) | (1L << USECASE_KEYWORD) | (1L << ACTOR_KEYWORD) | (1L << SYSTEM_KEYWORD) | (1L << TOP_TO_BOTTOM_DIRECTION_KEYWORD) | (1L << LEFT_TO_RIGHT_DIRECTION_KEYWORD) | (1L << ORDER_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD) | (1L << ELSE_KEYWORD) | (1L << GROUP_TYPE_KEYWORD) | (1L << CREATE_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << OVER_KEYWORD) | (1L << REF_KEYWORD) | (1L << ACTIVATE_KEYWORD) | (1L << DEACTIVATE_KEYWORD) | (1L << HIDE_KEYWORD) | (1L << SHOW_KEYWORD) | (1L << NOTE_KEYWORD) | (1L << SKINPARAM_KEYWORD) | (1L << AUTONUMBER_KEYWORD) | (1L << DIRECTION_KEYWORD) | (1L << OF_KEYWORD) | (1L << END_KEYWORD) | (1L << AS_KEYWORD) | (1L << OPENED_ANGLE_BRACKET) | (1L << CLOSED_ANGLE_BRACKET) | (1L << DOUBLE_OPENED_ANGLE_BRACKET) | (1L << DOUBLE_CLOSED_ANGLE_BRACKET) | (1L << COLON) | (1L << VISIBILITY_SIGN) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (IDENTIFICATOR - 65)) | (1L << (SPACE - 65)) | (1L << (QUOTE - 65)) | (1L << (DOT - 65)))) != 0)) {
					{
						{
							setState(429);
							identificator_with_space_and_special_chars();
						}
					}
					setState(434);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ClassPackageBodyLineContext classPackageBodyLine() throws RecognitionException {
		ClassPackageBodyLineContext _localctx = new ClassPackageBodyLineContext(_ctx, getState());
		enterRule(_localctx, 36, RULE_classPackageBodyLine);
		try {
			setState(443);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 60, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(435);
					topToBottomDirection();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(436);
					leftToRightDirection();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					setState(437);
					skinparamRule();
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(438);
					classRule();
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(439);
					noteRule();
				}
				break;
				case 6:
					enterOuterAlt(_localctx, 6);
				{
					setState(440);
					addResourceToClassRule();
				}
				break;
				case 7:
					enterOuterAlt(_localctx, 7);
				{
					setState(441);
					classPackageRule();
				}
				break;
				case 8:
					enterOuterAlt(_localctx, 8);
				{
					setState(442);
					classDiagramRelationRule();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final HideRuleContext hideRule() throws RecognitionException {
		HideRuleContext _localctx = new HideRuleContext(_ctx, getState());
		enterRule(_localctx, 38, RULE_hideRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(445);
				match(HIDE_KEYWORD);
				setState(446);
				identificator_with_space_and_special_chars();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ShowRuleContext showRule() throws RecognitionException {
		ShowRuleContext _localctx = new ShowRuleContext(_ctx, getState());
		enterRule(_localctx, 40, RULE_showRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(448);
				match(SHOW_KEYWORD);
				setState(449);
				identificator_with_space_and_special_chars();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ActorRuleContext actorRule() throws RecognitionException {
		ActorRuleContext _localctx = new ActorRuleContext(_ctx, getState());
		enterRule(_localctx, 42, RULE_actorRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				{
					setState(452);
					_errHandler.sync(this);
					switch (getInterpreter().adaptivePredict(_input, 61, _ctx)) {
						case 1: {
							setState(451);
							match(ACTOR_KEYWORD);
						}
						break;
					}
					setState(457);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
						case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
							setState(454);
							match(IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
						}
						break;
						case IDENTIFICATOR_CLOSED_IN_COLONS: {
							setState(455);
							match(IDENTIFICATOR_CLOSED_IN_COLONS);
						}
						break;
						case CLASS_TYPE_KEYWORD:
						case CLASS_ANOTHER_TYPES_KEYWORD:
						case ABSTRACT_KEYWORD:
						case EXTENDS_KEYWORD:
						case IMPLEMENTS_KEYWORD:
						case FIELD_KEYWORD:
						case METHOD_KEYWORD:
						case STATIC_KEYWORD:
						case CLASSIFIER_KEYWORD:
						case PACKAGE_KEYWORD:
						case NAMESPACE_KEYWORD:
						case RECTANGLE_KEYWORD:
						case USECASE_KEYWORD:
						case ACTOR_KEYWORD:
						case SYSTEM_KEYWORD:
						case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
						case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
						case ORDER_KEYWORD:
						case PARTICIPANT_TYPE_KEYWORD:
						case ELSE_KEYWORD:
						case GROUP_TYPE_KEYWORD:
						case CREATE_KEYWORD:
						case RETURN_KEYWORD:
						case OVER_KEYWORD:
						case REF_KEYWORD:
						case ACTIVATE_KEYWORD:
						case DEACTIVATE_KEYWORD:
						case HIDE_KEYWORD:
						case SHOW_KEYWORD:
						case NOTE_KEYWORD:
						case SKINPARAM_KEYWORD:
						case AUTONUMBER_KEYWORD:
						case DIRECTION_KEYWORD:
						case OF_KEYWORD:
						case END_KEYWORD:
						case AS_KEYWORD:
						case QUOTED_IDENTIFICATOR:
						case IDENTIFICATOR: {
							setState(456);
							identificator();
						}
						break;
						default:
							throw new NoViableAltException(this);
					}
					setState(460);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la == AS_KEYWORD) {
						{
							setState(459);
							actorAlias();
						}
					}

					setState(465);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
						{
							{
								setState(462);
								stereotypes();
							}
						}
						setState(467);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(469);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la == HEX_COLOR) {
						{
							setState(468);
							match(HEX_COLOR);
						}
					}

				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCaseRuleContext useCaseRule() throws RecognitionException {
		UseCaseRuleContext _localctx = new UseCaseRuleContext(_ctx, getState());
		enterRule(_localctx, 44, RULE_useCaseRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				{
					setState(472);
					_errHandler.sync(this);
					switch (getInterpreter().adaptivePredict(_input, 66, _ctx)) {
						case 1: {
							setState(471);
							match(USECASE_KEYWORD);
						}
						break;
					}
					setState(477);
					_errHandler.sync(this);
					switch (_input.LA(1)) {
						case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
							setState(474);
							match(IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
						}
						break;
						case IDENTIFICATOR_CLOSED_IN_BRACKETS: {
							setState(475);
							match(IDENTIFICATOR_CLOSED_IN_BRACKETS);
						}
						break;
						case CLASS_TYPE_KEYWORD:
						case CLASS_ANOTHER_TYPES_KEYWORD:
						case ABSTRACT_KEYWORD:
						case EXTENDS_KEYWORD:
						case IMPLEMENTS_KEYWORD:
						case FIELD_KEYWORD:
						case METHOD_KEYWORD:
						case STATIC_KEYWORD:
						case CLASSIFIER_KEYWORD:
						case PACKAGE_KEYWORD:
						case NAMESPACE_KEYWORD:
						case RECTANGLE_KEYWORD:
						case USECASE_KEYWORD:
						case ACTOR_KEYWORD:
						case SYSTEM_KEYWORD:
						case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
						case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
						case ORDER_KEYWORD:
						case PARTICIPANT_TYPE_KEYWORD:
						case ELSE_KEYWORD:
						case GROUP_TYPE_KEYWORD:
						case CREATE_KEYWORD:
						case RETURN_KEYWORD:
						case OVER_KEYWORD:
						case REF_KEYWORD:
						case ACTIVATE_KEYWORD:
						case DEACTIVATE_KEYWORD:
						case HIDE_KEYWORD:
						case SHOW_KEYWORD:
						case NOTE_KEYWORD:
						case SKINPARAM_KEYWORD:
						case AUTONUMBER_KEYWORD:
						case DIRECTION_KEYWORD:
						case OF_KEYWORD:
						case END_KEYWORD:
						case AS_KEYWORD:
						case QUOTED_IDENTIFICATOR:
						case IDENTIFICATOR: {
							setState(476);
							identificator();
						}
						break;
						default:
							throw new NoViableAltException(this);
					}
					setState(480);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la == AS_KEYWORD) {
						{
							setState(479);
							useCaseAlias();
						}
					}

					setState(485);
					_errHandler.sync(this);
					_la = _input.LA(1);
					while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
						{
							{
								setState(482);
								stereotypes();
							}
						}
						setState(487);
						_errHandler.sync(this);
						_la = _input.LA(1);
					}
					setState(489);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if (_la == HEX_COLOR) {
						{
							setState(488);
							match(HEX_COLOR);
						}
					}

				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCasePackageRuleContext useCasePackageRule() throws RecognitionException {
		UseCasePackageRuleContext _localctx = new UseCasePackageRuleContext(_ctx, getState());
		enterRule(_localctx, 46, RULE_useCasePackageRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(491);
				_la = _input.LA(1);
				if (!(_la == PACKAGE_KEYWORD || _la == RECTANGLE_KEYWORD)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(492);
				identificator();
				setState(494);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == AS_KEYWORD) {
					{
						setState(493);
						basicAlias();
					}
				}

				setState(499);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
					{
						{
							setState(496);
							stereotypes();
						}
					}
					setState(501);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(503);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == HEX_COLOR) {
					{
						setState(502);
						match(HEX_COLOR);
					}
				}

				setState(506);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == OPENED_CURLY_BRACKET) {
					{
						setState(505);
						useCasePackageBody();
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCaseDiagramRelationRuleContext useCaseDiagramRelationRule() throws RecognitionException {
		UseCaseDiagramRelationRuleContext _localctx = new UseCaseDiagramRelationRuleContext(_ctx, getState());
		enterRule(_localctx, 48, RULE_useCaseDiagramRelationRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(511);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case IDENTIFICATOR_CLOSED_IN_BRACKETS:
					case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
						setState(508);
						_la = _input.LA(1);
						if (!(_la == IDENTIFICATOR_CLOSED_IN_BRACKETS || _la == IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_COLONS:
					case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
						setState(509);
						_la = _input.LA(1);
						if (!(_la == IDENTIFICATOR_CLOSED_IN_COLONS || _la == IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
					break;
					case CLASS_TYPE_KEYWORD:
					case CLASS_ANOTHER_TYPES_KEYWORD:
					case ABSTRACT_KEYWORD:
					case EXTENDS_KEYWORD:
					case IMPLEMENTS_KEYWORD:
					case FIELD_KEYWORD:
					case METHOD_KEYWORD:
					case STATIC_KEYWORD:
					case CLASSIFIER_KEYWORD:
					case PACKAGE_KEYWORD:
					case NAMESPACE_KEYWORD:
					case RECTANGLE_KEYWORD:
					case USECASE_KEYWORD:
					case ACTOR_KEYWORD:
					case SYSTEM_KEYWORD:
					case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
					case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
					case ORDER_KEYWORD:
					case PARTICIPANT_TYPE_KEYWORD:
					case ELSE_KEYWORD:
					case GROUP_TYPE_KEYWORD:
					case CREATE_KEYWORD:
					case RETURN_KEYWORD:
					case OVER_KEYWORD:
					case REF_KEYWORD:
					case ACTIVATE_KEYWORD:
					case DEACTIVATE_KEYWORD:
					case HIDE_KEYWORD:
					case SHOW_KEYWORD:
					case NOTE_KEYWORD:
					case SKINPARAM_KEYWORD:
					case AUTONUMBER_KEYWORD:
					case DIRECTION_KEYWORD:
					case OF_KEYWORD:
					case END_KEYWORD:
					case AS_KEYWORD:
					case QUOTED_IDENTIFICATOR:
					case IDENTIFICATOR: {
						setState(510);
						identificator();
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
				setState(514);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == QUOTED_IDENTIFICATOR) {
					{
						setState(513);
						match(QUOTED_IDENTIFICATOR);
					}
				}

				setState(516);
				_la = _input.LA(1);
				if (!(_la == ARROW_DOTTED || _la == ARROW_DASHED)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(518);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 77, _ctx)) {
					case 1: {
						setState(517);
						match(QUOTED_IDENTIFICATOR);
					}
					break;
				}
				setState(523);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case IDENTIFICATOR_CLOSED_IN_BRACKETS:
					case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
						setState(520);
						_la = _input.LA(1);
						if (!(_la == IDENTIFICATOR_CLOSED_IN_BRACKETS || _la == IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_COLONS:
					case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
						setState(521);
						_la = _input.LA(1);
						if (!(_la == IDENTIFICATOR_CLOSED_IN_COLONS || _la == IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES)) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
					break;
					case CLASS_TYPE_KEYWORD:
					case CLASS_ANOTHER_TYPES_KEYWORD:
					case ABSTRACT_KEYWORD:
					case EXTENDS_KEYWORD:
					case IMPLEMENTS_KEYWORD:
					case FIELD_KEYWORD:
					case METHOD_KEYWORD:
					case STATIC_KEYWORD:
					case CLASSIFIER_KEYWORD:
					case PACKAGE_KEYWORD:
					case NAMESPACE_KEYWORD:
					case RECTANGLE_KEYWORD:
					case USECASE_KEYWORD:
					case ACTOR_KEYWORD:
					case SYSTEM_KEYWORD:
					case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
					case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
					case ORDER_KEYWORD:
					case PARTICIPANT_TYPE_KEYWORD:
					case ELSE_KEYWORD:
					case GROUP_TYPE_KEYWORD:
					case CREATE_KEYWORD:
					case RETURN_KEYWORD:
					case OVER_KEYWORD:
					case REF_KEYWORD:
					case ACTIVATE_KEYWORD:
					case DEACTIVATE_KEYWORD:
					case HIDE_KEYWORD:
					case SHOW_KEYWORD:
					case NOTE_KEYWORD:
					case SKINPARAM_KEYWORD:
					case AUTONUMBER_KEYWORD:
					case DIRECTION_KEYWORD:
					case OF_KEYWORD:
					case END_KEYWORD:
					case AS_KEYWORD:
					case QUOTED_IDENTIFICATOR:
					case IDENTIFICATOR: {
						setState(522);
						identificator();
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
				setState(530);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == COLON) {
					{
						setState(525);
						match(COLON);
						setState(528);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
							case T__2:
							case T__3:
							case CLASS_TYPE_KEYWORD:
							case CLASS_ANOTHER_TYPES_KEYWORD:
							case ABSTRACT_KEYWORD:
							case EXTENDS_KEYWORD:
							case IMPLEMENTS_KEYWORD:
							case FIELD_KEYWORD:
							case METHOD_KEYWORD:
							case STATIC_KEYWORD:
							case CLASSIFIER_KEYWORD:
							case PACKAGE_KEYWORD:
							case NAMESPACE_KEYWORD:
							case RECTANGLE_KEYWORD:
							case USECASE_KEYWORD:
							case ACTOR_KEYWORD:
							case SYSTEM_KEYWORD:
							case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
							case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
							case ORDER_KEYWORD:
							case PARTICIPANT_TYPE_KEYWORD:
							case ELSE_KEYWORD:
							case GROUP_TYPE_KEYWORD:
							case CREATE_KEYWORD:
							case RETURN_KEYWORD:
							case OVER_KEYWORD:
							case REF_KEYWORD:
							case ACTIVATE_KEYWORD:
							case DEACTIVATE_KEYWORD:
							case HIDE_KEYWORD:
							case SHOW_KEYWORD:
							case NOTE_KEYWORD:
							case SKINPARAM_KEYWORD:
							case AUTONUMBER_KEYWORD:
							case DIRECTION_KEYWORD:
							case OF_KEYWORD:
							case END_KEYWORD:
							case AS_KEYWORD:
							case OPENED_ANGLE_BRACKET:
							case CLOSED_ANGLE_BRACKET:
							case DOUBLE_OPENED_ANGLE_BRACKET:
							case DOUBLE_CLOSED_ANGLE_BRACKET:
							case COLON:
							case VISIBILITY_SIGN:
							case IDENTIFICATOR_CLOSED_IN_COLONS:
							case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES:
							case IDENTIFICATOR_CLOSED_IN_BRACKETS:
							case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES:
							case IDENTIFICATOR:
							case SPACE:
							case QUOTE:
							case DOT: {
								setState(526);
								identificator_with_space_and_special_chars();
							}
							break;
							case QUOTED_IDENTIFICATOR: {
								setState(527);
								match(QUOTED_IDENTIFICATOR);
							}
							break;
							default:
								throw new NoViableAltException(this);
						}
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCasePackageBodyContext useCasePackageBody() throws RecognitionException {
		UseCasePackageBodyContext _localctx = new UseCasePackageBodyContext(_ctx, getState());
		enterRule(_localctx, 50, RULE_useCasePackageBody);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(532);
				match(OPENED_CURLY_BRACKET);
				setState(534);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						{
							setState(533);
							match(NEWLINE);
						}
					}
					setState(536);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while (_la == NEWLINE);
				setState(546);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_COLONS - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS - 9)) | (1L << (IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES - 9)) | (1L << (QUOTED_IDENTIFICATOR - 9)) | (1L << (IDENTIFICATOR - 9)))) != 0)) {
					{
						{
							setState(538);
							useCasePackageBodyLine();
							setState(540);
							_errHandler.sync(this);
							_la = _input.LA(1);
							do {
								{
									{
										setState(539);
										match(NEWLINE);
									}
								}
								setState(542);
								_errHandler.sync(this);
								_la = _input.LA(1);
							} while (_la == NEWLINE);
						}
					}
					setState(548);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(549);
				match(CLOSED_CURLY_BRACKET);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCasePackageBodyLineContext useCasePackageBodyLine() throws RecognitionException {
		UseCasePackageBodyLineContext _localctx = new UseCasePackageBodyLineContext(_ctx, getState());
		enterRule(_localctx, 52, RULE_useCasePackageBodyLine);
		try {
			setState(559);
			_errHandler.sync(this);
			switch (getInterpreter().adaptivePredict(_input, 84, _ctx)) {
				case 1:
					enterOuterAlt(_localctx, 1);
				{
					setState(551);
					topToBottomDirection();
				}
				break;
				case 2:
					enterOuterAlt(_localctx, 2);
				{
					setState(552);
					leftToRightDirection();
				}
				break;
				case 3:
					enterOuterAlt(_localctx, 3);
				{
					setState(553);
					skinparamRule();
				}
				break;
				case 4:
					enterOuterAlt(_localctx, 4);
				{
					setState(554);
					actorRule();
				}
				break;
				case 5:
					enterOuterAlt(_localctx, 5);
				{
					setState(555);
					useCaseRule();
				}
				break;
				case 6:
					enterOuterAlt(_localctx, 6);
				{
					setState(556);
					noteRule();
				}
				break;
				case 7:
					enterOuterAlt(_localctx, 7);
				{
					setState(557);
					useCasePackageRule();
				}
				break;
				case 8:
					enterOuterAlt(_localctx, 8);
				{
					setState(558);
					useCaseDiagramRelationRule();
				}
				break;
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ParticipantRuleContext participantRule() throws RecognitionException {
		ParticipantRuleContext _localctx = new ParticipantRuleContext(_ctx, getState());
		enterRule(_localctx, 54, RULE_participantRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(561);
				_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ACTOR_KEYWORD) | (1L << ENTITY_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(562);
				identificator();
				setState(564);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == AS_KEYWORD) {
					{
						setState(563);
						basicAlias();
					}
				}

				setState(569);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
					{
						{
							setState(566);
							stereotypes();
						}
					}
					setState(571);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(573);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == ORDER_KEYWORD) {
					{
						setState(572);
						order();
					}
				}

				setState(576);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == HEX_COLOR) {
					{
						setState(575);
						match(HEX_COLOR);
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final MessageRuleContext messageRule() throws RecognitionException {
		MessageRuleContext _localctx = new MessageRuleContext(_ctx, getState());
		enterRule(_localctx, 56, RULE_messageRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(582);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (QUOTED_IDENTIFICATOR - 9)) | (1L << (IDENTIFICATOR - 9)))) != 0)) {
					{
						setState(578);
						identificator();
						setState(580);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la == AS_KEYWORD) {
							{
								setState(579);
								basicAlias();
							}
						}

					}
				}

				setState(584);
				_la = _input.LA(1);
				if (!(_la == ARROW_DASHED || _la == ARROW_SEQUENCE_DIAGRAM)) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
				setState(589);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (QUOTED_IDENTIFICATOR - 9)) | (1L << (IDENTIFICATOR - 9)))) != 0)) {
					{
						setState(585);
						identificator();
						setState(587);
						_errHandler.sync(this);
						_la = _input.LA(1);
						if (_la == AS_KEYWORD) {
							{
								setState(586);
								basicAlias();
							}
						}

					}
				}

				setState(598);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case COLON: {
						{
							setState(591);
							match(COLON);
							setState(594);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
								case T__2:
								case T__3:
								case CLASS_TYPE_KEYWORD:
								case CLASS_ANOTHER_TYPES_KEYWORD:
								case ABSTRACT_KEYWORD:
								case EXTENDS_KEYWORD:
								case IMPLEMENTS_KEYWORD:
								case FIELD_KEYWORD:
								case METHOD_KEYWORD:
								case STATIC_KEYWORD:
								case CLASSIFIER_KEYWORD:
								case PACKAGE_KEYWORD:
								case NAMESPACE_KEYWORD:
								case RECTANGLE_KEYWORD:
								case USECASE_KEYWORD:
								case ACTOR_KEYWORD:
								case SYSTEM_KEYWORD:
								case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
								case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
								case ORDER_KEYWORD:
								case PARTICIPANT_TYPE_KEYWORD:
								case ELSE_KEYWORD:
								case GROUP_TYPE_KEYWORD:
								case CREATE_KEYWORD:
								case RETURN_KEYWORD:
								case OVER_KEYWORD:
								case REF_KEYWORD:
								case ACTIVATE_KEYWORD:
								case DEACTIVATE_KEYWORD:
								case HIDE_KEYWORD:
								case SHOW_KEYWORD:
								case NOTE_KEYWORD:
								case SKINPARAM_KEYWORD:
								case AUTONUMBER_KEYWORD:
								case DIRECTION_KEYWORD:
								case OF_KEYWORD:
								case END_KEYWORD:
								case AS_KEYWORD:
								case OPENED_ANGLE_BRACKET:
								case CLOSED_ANGLE_BRACKET:
								case DOUBLE_OPENED_ANGLE_BRACKET:
								case DOUBLE_CLOSED_ANGLE_BRACKET:
								case COLON:
								case VISIBILITY_SIGN:
								case IDENTIFICATOR_CLOSED_IN_COLONS:
								case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES:
								case IDENTIFICATOR_CLOSED_IN_BRACKETS:
								case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES:
								case IDENTIFICATOR:
								case SPACE:
								case QUOTE:
								case DOT: {
									setState(592);
									identificator_with_space_and_special_chars();
								}
								break;
								case QUOTED_IDENTIFICATOR: {
									setState(593);
									match(QUOTED_IDENTIFICATOR);
								}
								break;
								default:
									throw new NoViableAltException(this);
							}
						}
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_COLONS:
					case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
						{
							setState(596);
							_la = _input.LA(1);
							if (!(_la == IDENTIFICATOR_CLOSED_IN_COLONS || _la == IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES)) {
								_errHandler.recoverInline(this);
							} else {
								if (_input.LA(1) == Token.EOF) matchedEOF = true;
								_errHandler.reportMatch(this);
								consume();
							}
							setState(597);
							identificator_with_space_and_special_chars();
						}
					}
					break;
					case NEWLINE:
						break;
					default:
						break;
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final GroupRuleContext groupRule() throws RecognitionException {
		GroupRuleContext _localctx = new GroupRuleContext(_ctx, getState());
		enterRule(_localctx, 58, RULE_groupRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				{
					{
						setState(600);
						match(GROUP_TYPE_KEYWORD);
					}
					setState(602);
					_errHandler.sync(this);
					_la = _input.LA(1);
					if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << CLASS_TYPE_KEYWORD) | (1L << CLASS_ANOTHER_TYPES_KEYWORD) | (1L << ABSTRACT_KEYWORD) | (1L << EXTENDS_KEYWORD) | (1L << IMPLEMENTS_KEYWORD) | (1L << FIELD_KEYWORD) | (1L << METHOD_KEYWORD) | (1L << STATIC_KEYWORD) | (1L << CLASSIFIER_KEYWORD) | (1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD) | (1L << USECASE_KEYWORD) | (1L << ACTOR_KEYWORD) | (1L << SYSTEM_KEYWORD) | (1L << TOP_TO_BOTTOM_DIRECTION_KEYWORD) | (1L << LEFT_TO_RIGHT_DIRECTION_KEYWORD) | (1L << ORDER_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD) | (1L << ELSE_KEYWORD) | (1L << GROUP_TYPE_KEYWORD) | (1L << CREATE_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << OVER_KEYWORD) | (1L << REF_KEYWORD) | (1L << ACTIVATE_KEYWORD) | (1L << DEACTIVATE_KEYWORD) | (1L << HIDE_KEYWORD) | (1L << SHOW_KEYWORD) | (1L << NOTE_KEYWORD) | (1L << SKINPARAM_KEYWORD) | (1L << AUTONUMBER_KEYWORD) | (1L << DIRECTION_KEYWORD) | (1L << OF_KEYWORD) | (1L << END_KEYWORD) | (1L << AS_KEYWORD) | (1L << OPENED_ANGLE_BRACKET) | (1L << CLOSED_ANGLE_BRACKET) | (1L << DOUBLE_OPENED_ANGLE_BRACKET) | (1L << DOUBLE_CLOSED_ANGLE_BRACKET) | (1L << COLON) | (1L << VISIBILITY_SIGN) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (IDENTIFICATOR - 65)) | (1L << (SPACE - 65)) | (1L << (QUOTE - 65)) | (1L << (DOT - 65)))) != 0)) {
						{
							setState(601);
							identificator_with_space_and_special_chars();
						}
					}

					setState(604);
					groupCase();
				}
				setState(609);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == ELSE_KEYWORD) {
					{
						{
							setState(606);
							elseGroupBranch();
						}
					}
					setState(611);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(612);
				match(END_KEYWORD);
				setState(614);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (COLON - 9)) | (1L << (IDENTIFICATOR - 9)) | (1L << (SPACE - 9)))) != 0)) {
					{
						setState(613);
						identificator_with_space();
					}
				}

			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final CreateRuleContext createRule() throws RecognitionException {
		CreateRuleContext _localctx = new CreateRuleContext(_ctx, getState());
		enterRule(_localctx, 60, RULE_createRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(616);
				match(CREATE_KEYWORD);
				setState(618);
				_errHandler.sync(this);
				switch (getInterpreter().adaptivePredict(_input, 98, _ctx)) {
					case 1: {
						setState(617);
						_la = _input.LA(1);
						if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << ACTOR_KEYWORD) | (1L << ENTITY_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD))) != 0))) {
							_errHandler.recoverInline(this);
						} else {
							if (_input.LA(1) == Token.EOF) matchedEOF = true;
							_errHandler.reportMatch(this);
							consume();
						}
					}
					break;
				}
				setState(620);
				identificator();
				setState(622);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == AS_KEYWORD) {
					{
						setState(621);
						basicAlias();
					}
				}

				setState(627);
				_errHandler.sync(this);
				_la = _input.LA(1);
				while (_la == DOUBLE_OPENED_ANGLE_BRACKET) {
					{
						{
							setState(624);
							stereotypes();
						}
					}
					setState(629);
					_errHandler.sync(this);
					_la = _input.LA(1);
				}
				setState(631);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == ORDER_KEYWORD) {
					{
						setState(630);
						order();
					}
				}

				setState(634);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if (_la == HEX_COLOR) {
					{
						setState(633);
						match(HEX_COLOR);
					}
				}

				setState(636);
				match(NEWLINE);
				setState(637);
				messageRule();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ReturnRuleContext returnRule() throws RecognitionException {
		ReturnRuleContext _localctx = new ReturnRuleContext(_ctx, getState());
		enterRule(_localctx, 62, RULE_returnRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(639);
				match(RETURN_KEYWORD);
				setState(640);
				identificator_with_space();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ActivateRuleContext activateRule() throws RecognitionException {
		ActivateRuleContext _localctx = new ActivateRuleContext(_ctx, getState());
		enterRule(_localctx, 64, RULE_activateRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(642);
				match(ACTIVATE_KEYWORD);
				setState(643);
				identificator_with_space();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final DeactivateRuleContext deactivateRule() throws RecognitionException {
		DeactivateRuleContext _localctx = new DeactivateRuleContext(_ctx, getState());
		enterRule(_localctx, 66, RULE_deactivateRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(645);
				match(DEACTIVATE_KEYWORD);
				setState(646);
				identificator_with_space();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final SequenceNoteRuleContext sequenceNoteRule() throws RecognitionException {
		SequenceNoteRuleContext _localctx = new SequenceNoteRuleContext(_ctx, getState());
		enterRule(_localctx, 68, RULE_sequenceNoteRule);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(648);
				match(NOTE_KEYWORD);
				setState(649);
				match(OVER_KEYWORD);
				setState(650);
				identificator_with_space();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final RefRuleContext refRule() throws RecognitionException {
		RefRuleContext _localctx = new RefRuleContext(_ctx, getState());
		enterRule(_localctx, 70, RULE_refRule);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(652);
				match(REF_KEYWORD);
				setState(653);
				match(OVER_KEYWORD);
				setState(654);
				identificator();
				setState(674);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case NEWLINE: {
						{
							setState(655);
							match(NEWLINE);
							{
								setState(657);
								_errHandler.sync(this);
								switch (getInterpreter().adaptivePredict(_input, 103, _ctx)) {
									case 1: {
										setState(656);
										identificator_of_note();
									}
									break;
								}
								setState(662);
								_errHandler.sync(this);
								_la = _input.LA(1);
								while (_la == NEWLINE) {
									{
										{
											setState(659);
											match(NEWLINE);
										}
									}
									setState(664);
									_errHandler.sync(this);
									_la = _input.LA(1);
								}
							}
							setState(665);
							match(END_KEYWORD);
							setState(667);
							_errHandler.sync(this);
							_la = _input.LA(1);
							if (_la == REF_KEYWORD) {
								{
									setState(666);
									match(REF_KEYWORD);
								}
							}

						}
					}
					break;
					case COLON: {
						{
							setState(669);
							match(COLON);
							setState(672);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
								case T__2:
								case T__3:
								case CLASS_TYPE_KEYWORD:
								case CLASS_ANOTHER_TYPES_KEYWORD:
								case ABSTRACT_KEYWORD:
								case EXTENDS_KEYWORD:
								case IMPLEMENTS_KEYWORD:
								case FIELD_KEYWORD:
								case METHOD_KEYWORD:
								case STATIC_KEYWORD:
								case CLASSIFIER_KEYWORD:
								case PACKAGE_KEYWORD:
								case NAMESPACE_KEYWORD:
								case RECTANGLE_KEYWORD:
								case USECASE_KEYWORD:
								case ACTOR_KEYWORD:
								case SYSTEM_KEYWORD:
								case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
								case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
								case ORDER_KEYWORD:
								case PARTICIPANT_TYPE_KEYWORD:
								case ELSE_KEYWORD:
								case GROUP_TYPE_KEYWORD:
								case CREATE_KEYWORD:
								case RETURN_KEYWORD:
								case OVER_KEYWORD:
								case REF_KEYWORD:
								case ACTIVATE_KEYWORD:
								case DEACTIVATE_KEYWORD:
								case HIDE_KEYWORD:
								case SHOW_KEYWORD:
								case NOTE_KEYWORD:
								case SKINPARAM_KEYWORD:
								case AUTONUMBER_KEYWORD:
								case DIRECTION_KEYWORD:
								case OF_KEYWORD:
								case END_KEYWORD:
								case AS_KEYWORD:
								case OPENED_ANGLE_BRACKET:
								case CLOSED_ANGLE_BRACKET:
								case DOUBLE_OPENED_ANGLE_BRACKET:
								case DOUBLE_CLOSED_ANGLE_BRACKET:
								case COLON:
								case VISIBILITY_SIGN:
								case IDENTIFICATOR_CLOSED_IN_COLONS:
								case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES:
								case IDENTIFICATOR_CLOSED_IN_BRACKETS:
								case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES:
								case IDENTIFICATOR:
								case SPACE:
								case QUOTE:
								case DOT: {
									setState(670);
									identificator_with_space_and_special_chars();
								}
								break;
								case QUOTED_IDENTIFICATOR: {
									setState(671);
									match(QUOTED_IDENTIFICATOR);
								}
								break;
								default:
									throw new NoViableAltException(this);
							}
						}
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final GroupCaseContext groupCase() throws RecognitionException {
		GroupCaseContext _localctx = new GroupCaseContext(_ctx, getState());
		enterRule(_localctx, 72, RULE_groupCase);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(677);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						{
							setState(676);
							match(NEWLINE);
						}
					}
					setState(679);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while (_la == NEWLINE);
				setState(699);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 111, _ctx);
				while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
					if (_alt == 1) {
						{
							{
								setState(690);
								_errHandler.sync(this);
								switch (getInterpreter().adaptivePredict(_input, 109, _ctx)) {
									case 1: {
										setState(681);
										messageRule();
									}
									break;
									case 2: {
										setState(682);
										participantRule();
									}
									break;
									case 3: {
										setState(683);
										groupRule();
									}
									break;
									case 4: {
										setState(684);
										refRule();
									}
									break;
									case 5: {
										setState(685);
										createRule();
									}
									break;
									case 6: {
										setState(686);
										returnRule();
									}
									break;
									case 7: {
										setState(687);
										activateRule();
									}
									break;
									case 8: {
										setState(688);
										deactivateRule();
									}
									break;
									case 9: {
										setState(689);
										sequenceNoteRule();
									}
									break;
								}
								setState(693);
								_errHandler.sync(this);
								_la = _input.LA(1);
								do {
									{
										{
											setState(692);
											match(NEWLINE);
										}
									}
									setState(695);
									_errHandler.sync(this);
									_la = _input.LA(1);
								} while (_la == NEWLINE);
							}
						}
					}
					setState(701);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 111, _ctx);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ElseGroupBranchContext elseGroupBranch() throws RecognitionException {
		ElseGroupBranchContext _localctx = new ElseGroupBranchContext(_ctx, getState());
		enterRule(_localctx, 74, RULE_elseGroupBranch);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(702);
				match(ELSE_KEYWORD);
				setState(704);
				_errHandler.sync(this);
				_la = _input.LA(1);
				if ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << CLASS_TYPE_KEYWORD) | (1L << CLASS_ANOTHER_TYPES_KEYWORD) | (1L << ABSTRACT_KEYWORD) | (1L << EXTENDS_KEYWORD) | (1L << IMPLEMENTS_KEYWORD) | (1L << FIELD_KEYWORD) | (1L << METHOD_KEYWORD) | (1L << STATIC_KEYWORD) | (1L << CLASSIFIER_KEYWORD) | (1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD) | (1L << USECASE_KEYWORD) | (1L << ACTOR_KEYWORD) | (1L << SYSTEM_KEYWORD) | (1L << TOP_TO_BOTTOM_DIRECTION_KEYWORD) | (1L << LEFT_TO_RIGHT_DIRECTION_KEYWORD) | (1L << ORDER_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD) | (1L << ELSE_KEYWORD) | (1L << GROUP_TYPE_KEYWORD) | (1L << CREATE_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << OVER_KEYWORD) | (1L << REF_KEYWORD) | (1L << ACTIVATE_KEYWORD) | (1L << DEACTIVATE_KEYWORD) | (1L << HIDE_KEYWORD) | (1L << SHOW_KEYWORD) | (1L << NOTE_KEYWORD) | (1L << SKINPARAM_KEYWORD) | (1L << AUTONUMBER_KEYWORD) | (1L << DIRECTION_KEYWORD) | (1L << OF_KEYWORD) | (1L << END_KEYWORD) | (1L << AS_KEYWORD) | (1L << OPENED_ANGLE_BRACKET) | (1L << CLOSED_ANGLE_BRACKET) | (1L << DOUBLE_OPENED_ANGLE_BRACKET) | (1L << DOUBLE_CLOSED_ANGLE_BRACKET) | (1L << COLON) | (1L << VISIBILITY_SIGN) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS) | (1L << IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS) | (1L << IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (IDENTIFICATOR - 65)) | (1L << (SPACE - 65)) | (1L << (QUOTE - 65)) | (1L << (DOT - 65)))) != 0)) {
					{
						setState(703);
						identificator_with_space_and_special_chars();
					}
				}

				setState(706);
				groupCase();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final OrderContext order() throws RecognitionException {
		OrderContext _localctx = new OrderContext(_ctx, getState());
		enterRule(_localctx, 76, RULE_order);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(708);
				match(ORDER_KEYWORD);
				setState(709);
				identificator();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final TopToBottomDirectionContext topToBottomDirection() throws RecognitionException {
		TopToBottomDirectionContext _localctx = new TopToBottomDirectionContext(_ctx, getState());
		enterRule(_localctx, 78, RULE_topToBottomDirection);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(711);
				match(TOP_TO_BOTTOM_DIRECTION_KEYWORD);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final LeftToRightDirectionContext leftToRightDirection() throws RecognitionException {
		LeftToRightDirectionContext _localctx = new LeftToRightDirectionContext(_ctx, getState());
		enterRule(_localctx, 80, RULE_leftToRightDirection);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(713);
				match(LEFT_TO_RIGHT_DIRECTION_KEYWORD);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final BasicAliasContext basicAlias() throws RecognitionException {
		BasicAliasContext _localctx = new BasicAliasContext(_ctx, getState());
		enterRule(_localctx, 82, RULE_basicAlias);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(715);
				match(AS_KEYWORD);
				setState(716);
				identificator();
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final UseCaseAliasContext useCaseAlias() throws RecognitionException {
		UseCaseAliasContext _localctx = new UseCaseAliasContext(_ctx, getState());
		enterRule(_localctx, 84, RULE_useCaseAlias);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(718);
				match(AS_KEYWORD);
				setState(725);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case CLASS_TYPE_KEYWORD:
					case CLASS_ANOTHER_TYPES_KEYWORD:
					case ABSTRACT_KEYWORD:
					case EXTENDS_KEYWORD:
					case IMPLEMENTS_KEYWORD:
					case FIELD_KEYWORD:
					case METHOD_KEYWORD:
					case STATIC_KEYWORD:
					case CLASSIFIER_KEYWORD:
					case PACKAGE_KEYWORD:
					case NAMESPACE_KEYWORD:
					case RECTANGLE_KEYWORD:
					case USECASE_KEYWORD:
					case ACTOR_KEYWORD:
					case SYSTEM_KEYWORD:
					case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
					case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
					case ORDER_KEYWORD:
					case PARTICIPANT_TYPE_KEYWORD:
					case ELSE_KEYWORD:
					case GROUP_TYPE_KEYWORD:
					case CREATE_KEYWORD:
					case RETURN_KEYWORD:
					case OVER_KEYWORD:
					case REF_KEYWORD:
					case ACTIVATE_KEYWORD:
					case DEACTIVATE_KEYWORD:
					case HIDE_KEYWORD:
					case SHOW_KEYWORD:
					case NOTE_KEYWORD:
					case SKINPARAM_KEYWORD:
					case AUTONUMBER_KEYWORD:
					case DIRECTION_KEYWORD:
					case OF_KEYWORD:
					case END_KEYWORD:
					case AS_KEYWORD: {
						setState(719);
						keyword();
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_BRACKETS: {
						setState(720);
						match(IDENTIFICATOR_CLOSED_IN_BRACKETS);
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
						setState(721);
						match(IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
					}
					break;
					case IDENTIFICATOR: {
						setState(722);
						match(IDENTIFICATOR);
					}
					break;
					case QUOTED_IDENTIFICATOR_WITH_NEW_LINES: {
						setState(723);
						match(QUOTED_IDENTIFICATOR_WITH_NEW_LINES);
					}
					break;
					case QUOTED_IDENTIFICATOR: {
						setState(724);
						match(QUOTED_IDENTIFICATOR);
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final ActorAliasContext actorAlias() throws RecognitionException {
		ActorAliasContext _localctx = new ActorAliasContext(_ctx, getState());
		enterRule(_localctx, 86, RULE_actorAlias);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(727);
				match(AS_KEYWORD);
				setState(734);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case CLASS_TYPE_KEYWORD:
					case CLASS_ANOTHER_TYPES_KEYWORD:
					case ABSTRACT_KEYWORD:
					case EXTENDS_KEYWORD:
					case IMPLEMENTS_KEYWORD:
					case FIELD_KEYWORD:
					case METHOD_KEYWORD:
					case STATIC_KEYWORD:
					case CLASSIFIER_KEYWORD:
					case PACKAGE_KEYWORD:
					case NAMESPACE_KEYWORD:
					case RECTANGLE_KEYWORD:
					case USECASE_KEYWORD:
					case ACTOR_KEYWORD:
					case SYSTEM_KEYWORD:
					case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
					case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
					case ORDER_KEYWORD:
					case PARTICIPANT_TYPE_KEYWORD:
					case ELSE_KEYWORD:
					case GROUP_TYPE_KEYWORD:
					case CREATE_KEYWORD:
					case RETURN_KEYWORD:
					case OVER_KEYWORD:
					case REF_KEYWORD:
					case ACTIVATE_KEYWORD:
					case DEACTIVATE_KEYWORD:
					case HIDE_KEYWORD:
					case SHOW_KEYWORD:
					case NOTE_KEYWORD:
					case SKINPARAM_KEYWORD:
					case AUTONUMBER_KEYWORD:
					case DIRECTION_KEYWORD:
					case OF_KEYWORD:
					case END_KEYWORD:
					case AS_KEYWORD: {
						setState(728);
						keyword();
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_COLONS: {
						setState(729);
						match(IDENTIFICATOR_CLOSED_IN_COLONS);
					}
					break;
					case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
						setState(730);
						match(IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
					}
					break;
					case IDENTIFICATOR: {
						setState(731);
						match(IDENTIFICATOR);
					}
					break;
					case QUOTED_IDENTIFICATOR_WITH_NEW_LINES: {
						setState(732);
						match(QUOTED_IDENTIFICATOR_WITH_NEW_LINES);
					}
					break;
					case QUOTED_IDENTIFICATOR: {
						setState(733);
						match(QUOTED_IDENTIFICATOR);
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final GenericsContext generics() throws RecognitionException {
		GenericsContext _localctx = new GenericsContext(_ctx, getState());
		enterRule(_localctx, 88, RULE_generics);
		try {
			enterOuterAlt(_localctx, 1);
			{
				{
					setState(736);
					match(OPENED_ANGLE_BRACKET);
					setState(737);
					dentificator_with_space_and_special_chars_without_angle_brackets();
					setState(738);
					match(CLOSED_ANGLE_BRACKET);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final StereotypesContext stereotypes() throws RecognitionException {
		StereotypesContext _localctx = new StereotypesContext(_ctx, getState());
		enterRule(_localctx, 90, RULE_stereotypes);
		try {
			enterOuterAlt(_localctx, 1);
			{
				{
					setState(740);
					match(DOUBLE_OPENED_ANGLE_BRACKET);
					setState(741);
					dentificator_with_space_and_special_chars_without_angle_brackets();
					setState(742);
					match(DOUBLE_CLOSED_ANGLE_BRACKET);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final IdentificatorContext identificator() throws RecognitionException {
		IdentificatorContext _localctx = new IdentificatorContext(_ctx, getState());
		enterRule(_localctx, 92, RULE_identificator);
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(747);
				_errHandler.sync(this);
				switch (_input.LA(1)) {
					case CLASS_TYPE_KEYWORD:
					case CLASS_ANOTHER_TYPES_KEYWORD:
					case ABSTRACT_KEYWORD:
					case EXTENDS_KEYWORD:
					case IMPLEMENTS_KEYWORD:
					case FIELD_KEYWORD:
					case METHOD_KEYWORD:
					case STATIC_KEYWORD:
					case CLASSIFIER_KEYWORD:
					case PACKAGE_KEYWORD:
					case NAMESPACE_KEYWORD:
					case RECTANGLE_KEYWORD:
					case USECASE_KEYWORD:
					case ACTOR_KEYWORD:
					case SYSTEM_KEYWORD:
					case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
					case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
					case ORDER_KEYWORD:
					case PARTICIPANT_TYPE_KEYWORD:
					case ELSE_KEYWORD:
					case GROUP_TYPE_KEYWORD:
					case CREATE_KEYWORD:
					case RETURN_KEYWORD:
					case OVER_KEYWORD:
					case REF_KEYWORD:
					case ACTIVATE_KEYWORD:
					case DEACTIVATE_KEYWORD:
					case HIDE_KEYWORD:
					case SHOW_KEYWORD:
					case NOTE_KEYWORD:
					case SKINPARAM_KEYWORD:
					case AUTONUMBER_KEYWORD:
					case DIRECTION_KEYWORD:
					case OF_KEYWORD:
					case END_KEYWORD:
					case AS_KEYWORD: {
						setState(744);
						keyword();
					}
					break;
					case IDENTIFICATOR: {
						setState(745);
						match(IDENTIFICATOR);
					}
					break;
					case QUOTED_IDENTIFICATOR: {
						setState(746);
						match(QUOTED_IDENTIFICATOR);
					}
					break;
					default:
						throw new NoViableAltException(this);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final Identificator_with_spaceContext identificator_with_space() throws RecognitionException {
		Identificator_with_spaceContext _localctx = new Identificator_with_spaceContext(_ctx, getState());
		enterRule(_localctx, 94, RULE_identificator_with_space);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(753);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						setState(753);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
							case CLASS_TYPE_KEYWORD:
							case CLASS_ANOTHER_TYPES_KEYWORD:
							case ABSTRACT_KEYWORD:
							case EXTENDS_KEYWORD:
							case IMPLEMENTS_KEYWORD:
							case FIELD_KEYWORD:
							case METHOD_KEYWORD:
							case STATIC_KEYWORD:
							case CLASSIFIER_KEYWORD:
							case PACKAGE_KEYWORD:
							case NAMESPACE_KEYWORD:
							case RECTANGLE_KEYWORD:
							case USECASE_KEYWORD:
							case ACTOR_KEYWORD:
							case SYSTEM_KEYWORD:
							case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
							case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
							case ORDER_KEYWORD:
							case PARTICIPANT_TYPE_KEYWORD:
							case ELSE_KEYWORD:
							case GROUP_TYPE_KEYWORD:
							case CREATE_KEYWORD:
							case RETURN_KEYWORD:
							case OVER_KEYWORD:
							case REF_KEYWORD:
							case ACTIVATE_KEYWORD:
							case DEACTIVATE_KEYWORD:
							case HIDE_KEYWORD:
							case SHOW_KEYWORD:
							case NOTE_KEYWORD:
							case SKINPARAM_KEYWORD:
							case AUTONUMBER_KEYWORD:
							case DIRECTION_KEYWORD:
							case OF_KEYWORD:
							case END_KEYWORD:
							case AS_KEYWORD: {
								setState(749);
								keyword();
							}
							break;
							case IDENTIFICATOR: {
								setState(750);
								match(IDENTIFICATOR);
							}
							break;
							case SPACE: {
								setState(751);
								match(SPACE);
							}
							break;
							case COLON: {
								setState(752);
								match(COLON);
							}
							break;
							default:
								throw new NoViableAltException(this);
						}
					}
					setState(755);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while (((((_la - 9)) & ~0x3f) == 0 && ((1L << (_la - 9)) & ((1L << (CLASS_TYPE_KEYWORD - 9)) | (1L << (CLASS_ANOTHER_TYPES_KEYWORD - 9)) | (1L << (ABSTRACT_KEYWORD - 9)) | (1L << (EXTENDS_KEYWORD - 9)) | (1L << (IMPLEMENTS_KEYWORD - 9)) | (1L << (FIELD_KEYWORD - 9)) | (1L << (METHOD_KEYWORD - 9)) | (1L << (STATIC_KEYWORD - 9)) | (1L << (CLASSIFIER_KEYWORD - 9)) | (1L << (PACKAGE_KEYWORD - 9)) | (1L << (NAMESPACE_KEYWORD - 9)) | (1L << (RECTANGLE_KEYWORD - 9)) | (1L << (USECASE_KEYWORD - 9)) | (1L << (ACTOR_KEYWORD - 9)) | (1L << (SYSTEM_KEYWORD - 9)) | (1L << (TOP_TO_BOTTOM_DIRECTION_KEYWORD - 9)) | (1L << (LEFT_TO_RIGHT_DIRECTION_KEYWORD - 9)) | (1L << (ORDER_KEYWORD - 9)) | (1L << (PARTICIPANT_TYPE_KEYWORD - 9)) | (1L << (ELSE_KEYWORD - 9)) | (1L << (GROUP_TYPE_KEYWORD - 9)) | (1L << (CREATE_KEYWORD - 9)) | (1L << (RETURN_KEYWORD - 9)) | (1L << (OVER_KEYWORD - 9)) | (1L << (REF_KEYWORD - 9)) | (1L << (ACTIVATE_KEYWORD - 9)) | (1L << (DEACTIVATE_KEYWORD - 9)) | (1L << (HIDE_KEYWORD - 9)) | (1L << (SHOW_KEYWORD - 9)) | (1L << (NOTE_KEYWORD - 9)) | (1L << (SKINPARAM_KEYWORD - 9)) | (1L << (AUTONUMBER_KEYWORD - 9)) | (1L << (DIRECTION_KEYWORD - 9)) | (1L << (OF_KEYWORD - 9)) | (1L << (END_KEYWORD - 9)) | (1L << (AS_KEYWORD - 9)) | (1L << (COLON - 9)) | (1L << (IDENTIFICATOR - 9)) | (1L << (SPACE - 9)))) != 0));
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final Dentificator_with_space_and_special_chars_without_angle_bracketsContext dentificator_with_space_and_special_chars_without_angle_brackets() throws RecognitionException {
		Dentificator_with_space_and_special_chars_without_angle_bracketsContext _localctx = new Dentificator_with_space_and_special_chars_without_angle_bracketsContext(_ctx, getState());
		enterRule(_localctx, 96, RULE_dentificator_with_space_and_special_chars_without_angle_brackets);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(766);
				_errHandler.sync(this);
				_la = _input.LA(1);
				do {
					{
						setState(766);
						_errHandler.sync(this);
						switch (_input.LA(1)) {
							case CLASS_TYPE_KEYWORD:
							case CLASS_ANOTHER_TYPES_KEYWORD:
							case ABSTRACT_KEYWORD:
							case EXTENDS_KEYWORD:
							case IMPLEMENTS_KEYWORD:
							case FIELD_KEYWORD:
							case METHOD_KEYWORD:
							case STATIC_KEYWORD:
							case CLASSIFIER_KEYWORD:
							case PACKAGE_KEYWORD:
							case NAMESPACE_KEYWORD:
							case RECTANGLE_KEYWORD:
							case USECASE_KEYWORD:
							case ACTOR_KEYWORD:
							case SYSTEM_KEYWORD:
							case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
							case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
							case ORDER_KEYWORD:
							case PARTICIPANT_TYPE_KEYWORD:
							case ELSE_KEYWORD:
							case GROUP_TYPE_KEYWORD:
							case CREATE_KEYWORD:
							case RETURN_KEYWORD:
							case OVER_KEYWORD:
							case REF_KEYWORD:
							case ACTIVATE_KEYWORD:
							case DEACTIVATE_KEYWORD:
							case HIDE_KEYWORD:
							case SHOW_KEYWORD:
							case NOTE_KEYWORD:
							case SKINPARAM_KEYWORD:
							case AUTONUMBER_KEYWORD:
							case DIRECTION_KEYWORD:
							case OF_KEYWORD:
							case END_KEYWORD:
							case AS_KEYWORD: {
								setState(757);
								keyword();
							}
							break;
							case IDENTIFICATOR: {
								setState(758);
								match(IDENTIFICATOR);
							}
							break;
							case SPACE: {
								setState(759);
								match(SPACE);
							}
							break;
							case COLON: {
								setState(760);
								match(COLON);
							}
							break;
							case VISIBILITY_SIGN: {
								setState(761);
								match(VISIBILITY_SIGN);
							}
							break;
							case QUOTE: {
								setState(762);
								match(QUOTE);
							}
							break;
							case DOT: {
								setState(763);
								match(DOT);
							}
							break;
							case T__2: {
								setState(764);
								match(T__2);
							}
							break;
							case T__3: {
								setState(765);
								match(T__3);
							}
							break;
							default:
								throw new NoViableAltException(this);
						}
					}
					setState(768);
					_errHandler.sync(this);
					_la = _input.LA(1);
				} while ((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << T__2) | (1L << T__3) | (1L << CLASS_TYPE_KEYWORD) | (1L << CLASS_ANOTHER_TYPES_KEYWORD) | (1L << ABSTRACT_KEYWORD) | (1L << EXTENDS_KEYWORD) | (1L << IMPLEMENTS_KEYWORD) | (1L << FIELD_KEYWORD) | (1L << METHOD_KEYWORD) | (1L << STATIC_KEYWORD) | (1L << CLASSIFIER_KEYWORD) | (1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD) | (1L << USECASE_KEYWORD) | (1L << ACTOR_KEYWORD) | (1L << SYSTEM_KEYWORD) | (1L << TOP_TO_BOTTOM_DIRECTION_KEYWORD) | (1L << LEFT_TO_RIGHT_DIRECTION_KEYWORD) | (1L << ORDER_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD) | (1L << ELSE_KEYWORD) | (1L << GROUP_TYPE_KEYWORD) | (1L << CREATE_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << OVER_KEYWORD) | (1L << REF_KEYWORD) | (1L << ACTIVATE_KEYWORD) | (1L << DEACTIVATE_KEYWORD) | (1L << HIDE_KEYWORD) | (1L << SHOW_KEYWORD) | (1L << NOTE_KEYWORD) | (1L << SKINPARAM_KEYWORD) | (1L << AUTONUMBER_KEYWORD) | (1L << DIRECTION_KEYWORD) | (1L << OF_KEYWORD) | (1L << END_KEYWORD) | (1L << AS_KEYWORD) | (1L << COLON) | (1L << VISIBILITY_SIGN))) != 0) || ((((_la - 65)) & ~0x3f) == 0 && ((1L << (_la - 65)) & ((1L << (IDENTIFICATOR - 65)) | (1L << (SPACE - 65)) | (1L << (QUOTE - 65)) | (1L << (DOT - 65)))) != 0));
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() throws RecognitionException {
		Identificator_with_space_and_special_charsContext _localctx = new Identificator_with_space_and_special_charsContext(_ctx, getState());
		enterRule(_localctx, 98, RULE_identificator_with_space_and_special_chars);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(787);
				_errHandler.sync(this);
				_alt = 1;
				do {
					switch (_alt) {
						case 1: {
							setState(787);
							_errHandler.sync(this);
							switch (_input.LA(1)) {
								case CLASS_TYPE_KEYWORD:
								case CLASS_ANOTHER_TYPES_KEYWORD:
								case ABSTRACT_KEYWORD:
								case EXTENDS_KEYWORD:
								case IMPLEMENTS_KEYWORD:
								case FIELD_KEYWORD:
								case METHOD_KEYWORD:
								case STATIC_KEYWORD:
								case CLASSIFIER_KEYWORD:
								case PACKAGE_KEYWORD:
								case NAMESPACE_KEYWORD:
								case RECTANGLE_KEYWORD:
								case USECASE_KEYWORD:
								case ACTOR_KEYWORD:
								case SYSTEM_KEYWORD:
								case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
								case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
								case ORDER_KEYWORD:
								case PARTICIPANT_TYPE_KEYWORD:
								case ELSE_KEYWORD:
								case GROUP_TYPE_KEYWORD:
								case CREATE_KEYWORD:
								case RETURN_KEYWORD:
								case OVER_KEYWORD:
								case REF_KEYWORD:
								case ACTIVATE_KEYWORD:
								case DEACTIVATE_KEYWORD:
								case HIDE_KEYWORD:
								case SHOW_KEYWORD:
								case NOTE_KEYWORD:
								case SKINPARAM_KEYWORD:
								case AUTONUMBER_KEYWORD:
								case DIRECTION_KEYWORD:
								case OF_KEYWORD:
								case END_KEYWORD:
								case AS_KEYWORD: {
									setState(770);
									keyword();
								}
								break;
								case IDENTIFICATOR: {
									setState(771);
									match(IDENTIFICATOR);
								}
								break;
								case SPACE: {
									setState(772);
									match(SPACE);
								}
								break;
								case COLON: {
									setState(773);
									match(COLON);
								}
								break;
								case CLOSED_ANGLE_BRACKET: {
									setState(774);
									match(CLOSED_ANGLE_BRACKET);
								}
								break;
								case OPENED_ANGLE_BRACKET: {
									setState(775);
									match(OPENED_ANGLE_BRACKET);
								}
								break;
								case DOUBLE_CLOSED_ANGLE_BRACKET: {
									setState(776);
									match(DOUBLE_CLOSED_ANGLE_BRACKET);
								}
								break;
								case DOUBLE_OPENED_ANGLE_BRACKET: {
									setState(777);
									match(DOUBLE_OPENED_ANGLE_BRACKET);
								}
								break;
								case IDENTIFICATOR_CLOSED_IN_BRACKETS: {
									setState(778);
									match(IDENTIFICATOR_CLOSED_IN_BRACKETS);
								}
								break;
								case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
									setState(779);
									match(IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
								}
								break;
								case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
									setState(780);
									match(IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
								}
								break;
								case IDENTIFICATOR_CLOSED_IN_COLONS: {
									setState(781);
									match(IDENTIFICATOR_CLOSED_IN_COLONS);
								}
								break;
								case VISIBILITY_SIGN: {
									setState(782);
									match(VISIBILITY_SIGN);
								}
								break;
								case QUOTE: {
									setState(783);
									match(QUOTE);
								}
								break;
								case DOT: {
									setState(784);
									match(DOT);
								}
								break;
								case T__2: {
									setState(785);
									match(T__2);
								}
								break;
								case T__3: {
									setState(786);
									match(T__3);
								}
								break;
								default:
									throw new NoViableAltException(this);
							}
						}
						break;
						default:
							throw new NoViableAltException(this);
					}
					setState(789);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 121, _ctx);
				} while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final Identificator_of_noteContext identificator_of_note() throws RecognitionException {
		Identificator_of_noteContext _localctx = new Identificator_of_noteContext(_ctx, getState());
		enterRule(_localctx, 100, RULE_identificator_of_note);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
				setState(815);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input, 124, _ctx);
				while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER) {
					if (_alt == 1) {
						{
							{
								{
									setState(808);
									_errHandler.sync(this);
									_alt = 1;
									do {
										switch (_alt) {
											case 1: {
												setState(808);
												_errHandler.sync(this);
												switch (_input.LA(1)) {
													case CLASS_TYPE_KEYWORD:
													case CLASS_ANOTHER_TYPES_KEYWORD:
													case ABSTRACT_KEYWORD:
													case EXTENDS_KEYWORD:
													case IMPLEMENTS_KEYWORD:
													case FIELD_KEYWORD:
													case METHOD_KEYWORD:
													case STATIC_KEYWORD:
													case CLASSIFIER_KEYWORD:
													case PACKAGE_KEYWORD:
													case NAMESPACE_KEYWORD:
													case RECTANGLE_KEYWORD:
													case USECASE_KEYWORD:
													case ACTOR_KEYWORD:
													case SYSTEM_KEYWORD:
													case TOP_TO_BOTTOM_DIRECTION_KEYWORD:
													case LEFT_TO_RIGHT_DIRECTION_KEYWORD:
													case ORDER_KEYWORD:
													case PARTICIPANT_TYPE_KEYWORD:
													case ELSE_KEYWORD:
													case GROUP_TYPE_KEYWORD:
													case CREATE_KEYWORD:
													case RETURN_KEYWORD:
													case OVER_KEYWORD:
													case REF_KEYWORD:
													case ACTIVATE_KEYWORD:
													case DEACTIVATE_KEYWORD:
													case HIDE_KEYWORD:
													case SHOW_KEYWORD:
													case NOTE_KEYWORD:
													case SKINPARAM_KEYWORD:
													case AUTONUMBER_KEYWORD:
													case DIRECTION_KEYWORD:
													case OF_KEYWORD:
													case END_KEYWORD:
													case AS_KEYWORD: {
														setState(791);
														keyword();
													}
													break;
													case IDENTIFICATOR: {
														setState(792);
														match(IDENTIFICATOR);
													}
													break;
													case SPACE: {
														setState(793);
														match(SPACE);
													}
													break;
													case COLON: {
														setState(794);
														match(COLON);
													}
													break;
													case CLOSED_ANGLE_BRACKET: {
														setState(795);
														match(CLOSED_ANGLE_BRACKET);
													}
													break;
													case OPENED_ANGLE_BRACKET: {
														setState(796);
														match(OPENED_ANGLE_BRACKET);
													}
													break;
													case DOUBLE_CLOSED_ANGLE_BRACKET: {
														setState(797);
														match(DOUBLE_CLOSED_ANGLE_BRACKET);
													}
													break;
													case DOUBLE_OPENED_ANGLE_BRACKET: {
														setState(798);
														match(DOUBLE_OPENED_ANGLE_BRACKET);
													}
													break;
													case IDENTIFICATOR_CLOSED_IN_BRACKETS: {
														setState(799);
														match(IDENTIFICATOR_CLOSED_IN_BRACKETS);
													}
													break;
													case IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: {
														setState(800);
														match(IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
													}
													break;
													case IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: {
														setState(801);
														match(IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
													}
													break;
													case IDENTIFICATOR_CLOSED_IN_COLONS: {
														setState(802);
														match(IDENTIFICATOR_CLOSED_IN_COLONS);
													}
													break;
													case VISIBILITY_SIGN: {
														setState(803);
														match(VISIBILITY_SIGN);
													}
													break;
													case QUOTE: {
														setState(804);
														match(QUOTE);
													}
													break;
													case DOT: {
														setState(805);
														match(DOT);
													}
													break;
													case T__2: {
														setState(806);
														match(T__2);
													}
													break;
													case T__3: {
														setState(807);
														match(T__3);
													}
													break;
													default:
														throw new NoViableAltException(this);
												}
											}
											break;
											default:
												throw new NoViableAltException(this);
										}
										setState(810);
										_errHandler.sync(this);
										_alt = getInterpreter().adaptivePredict(_input, 123, _ctx);
									} while (_alt != 2 && _alt != org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER);
								}
								setState(812);
								_la = _input.LA(1);
								if (_la <= 0 || (_la == NOTE_KEYWORD || _la == END_KEYWORD)) {
									_errHandler.recoverInline(this);
								} else {
									if (_input.LA(1) == Token.EOF) matchedEOF = true;
									_errHandler.reportMatch(this);
									consume();
								}
							}
						}
					}
					setState(817);
					_errHandler.sync(this);
					_alt = getInterpreter().adaptivePredict(_input, 124, _ctx);
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public final KeywordContext keyword() throws RecognitionException {
		KeywordContext _localctx = new KeywordContext(_ctx, getState());
		enterRule(_localctx, 102, RULE_keyword);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
				setState(818);
				_la = _input.LA(1);
				if (!((((_la) & ~0x3f) == 0 && ((1L << _la) & ((1L << CLASS_TYPE_KEYWORD) | (1L << CLASS_ANOTHER_TYPES_KEYWORD) | (1L << ABSTRACT_KEYWORD) | (1L << EXTENDS_KEYWORD) | (1L << IMPLEMENTS_KEYWORD) | (1L << FIELD_KEYWORD) | (1L << METHOD_KEYWORD) | (1L << STATIC_KEYWORD) | (1L << CLASSIFIER_KEYWORD) | (1L << PACKAGE_KEYWORD) | (1L << NAMESPACE_KEYWORD) | (1L << RECTANGLE_KEYWORD) | (1L << USECASE_KEYWORD) | (1L << ACTOR_KEYWORD) | (1L << SYSTEM_KEYWORD) | (1L << TOP_TO_BOTTOM_DIRECTION_KEYWORD) | (1L << LEFT_TO_RIGHT_DIRECTION_KEYWORD) | (1L << ORDER_KEYWORD) | (1L << PARTICIPANT_TYPE_KEYWORD) | (1L << ELSE_KEYWORD) | (1L << GROUP_TYPE_KEYWORD) | (1L << CREATE_KEYWORD) | (1L << RETURN_KEYWORD) | (1L << OVER_KEYWORD) | (1L << REF_KEYWORD) | (1L << ACTIVATE_KEYWORD) | (1L << DEACTIVATE_KEYWORD) | (1L << HIDE_KEYWORD) | (1L << SHOW_KEYWORD) | (1L << NOTE_KEYWORD) | (1L << SKINPARAM_KEYWORD) | (1L << AUTONUMBER_KEYWORD) | (1L << DIRECTION_KEYWORD) | (1L << OF_KEYWORD) | (1L << END_KEYWORD) | (1L << AS_KEYWORD))) != 0))) {
					_errHandler.recoverInline(this);
				} else {
					if (_input.LA(1) == Token.EOF) matchedEOF = true;
					_errHandler.reportMatch(this);
					consume();
				}
			}
		} catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		} finally {
			exitRule();
		}
		return _localctx;
	}

	public static class UmlContext extends ParserRuleContext {
		public UmlContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode EOF() {
			return getToken(PlantUMLParser.EOF, 0);
		}

		public SequenceDiagramContext sequenceDiagram() {
			return getRuleContext(SequenceDiagramContext.class, 0);
		}

		public ClassDiagramContext classDiagram() {
			return getRuleContext(ClassDiagramContext.class, 0);
		}

		public UseCaseDiagramContext useCaseDiagram() {
			return getRuleContext(UseCaseDiagramContext.class, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_uml;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitUml(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassDiagramContext extends ParserRuleContext {
		public ClassDiagramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<HideRuleContext> hideRule() {
			return getRuleContexts(HideRuleContext.class);
		}

		public HideRuleContext hideRule(int i) {
			return getRuleContext(HideRuleContext.class, i);
		}

		public List<ShowRuleContext> showRule() {
			return getRuleContexts(ShowRuleContext.class);
		}

		public ShowRuleContext showRule(int i) {
			return getRuleContext(ShowRuleContext.class, i);
		}

		public List<TopToBottomDirectionContext> topToBottomDirection() {
			return getRuleContexts(TopToBottomDirectionContext.class);
		}

		public TopToBottomDirectionContext topToBottomDirection(int i) {
			return getRuleContext(TopToBottomDirectionContext.class, i);
		}

		public List<LeftToRightDirectionContext> leftToRightDirection() {
			return getRuleContexts(LeftToRightDirectionContext.class);
		}

		public LeftToRightDirectionContext leftToRightDirection(int i) {
			return getRuleContext(LeftToRightDirectionContext.class, i);
		}

		public List<SkinparamRuleContext> skinparamRule() {
			return getRuleContexts(SkinparamRuleContext.class);
		}

		public SkinparamRuleContext skinparamRule(int i) {
			return getRuleContext(SkinparamRuleContext.class, i);
		}

		public List<ClassDiagramLineContext> classDiagramLine() {
			return getRuleContexts(ClassDiagramLineContext.class);
		}

		public ClassDiagramLineContext classDiagramLine(int i) {
			return getRuleContext(ClassDiagramLineContext.class, i);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classDiagram;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassDiagram(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCaseDiagramContext extends ParserRuleContext {
		public UseCaseDiagramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<TopToBottomDirectionContext> topToBottomDirection() {
			return getRuleContexts(TopToBottomDirectionContext.class);
		}

		public TopToBottomDirectionContext topToBottomDirection(int i) {
			return getRuleContext(TopToBottomDirectionContext.class, i);
		}

		public List<LeftToRightDirectionContext> leftToRightDirection() {
			return getRuleContexts(LeftToRightDirectionContext.class);
		}

		public LeftToRightDirectionContext leftToRightDirection(int i) {
			return getRuleContext(LeftToRightDirectionContext.class, i);
		}

		public List<SkinparamRuleContext> skinparamRule() {
			return getRuleContexts(SkinparamRuleContext.class);
		}

		public SkinparamRuleContext skinparamRule(int i) {
			return getRuleContext(SkinparamRuleContext.class, i);
		}

		public List<UseCaseDiagramLineContext> useCaseDiagramLine() {
			return getRuleContexts(UseCaseDiagramLineContext.class);
		}

		public UseCaseDiagramLineContext useCaseDiagramLine(int i) {
			return getRuleContext(UseCaseDiagramLineContext.class, i);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCaseDiagram;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCaseDiagram(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class SequenceDiagramContext extends ParserRuleContext {
		public SequenceDiagramContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<SkinparamRuleContext> skinparamRule() {
			return getRuleContexts(SkinparamRuleContext.class);
		}

		public SkinparamRuleContext skinparamRule(int i) {
			return getRuleContext(SkinparamRuleContext.class, i);
		}

		public List<AutonumberRuleContext> autonumberRule() {
			return getRuleContexts(AutonumberRuleContext.class);
		}

		public AutonumberRuleContext autonumberRule(int i) {
			return getRuleContext(AutonumberRuleContext.class, i);
		}

		public List<SequenceDiagramLineContext> sequenceDiagramLine() {
			return getRuleContexts(SequenceDiagramLineContext.class);
		}

		public SequenceDiagramLineContext sequenceDiagramLine(int i) {
			return getRuleContext(SequenceDiagramLineContext.class, i);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_sequenceDiagram;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitSequenceDiagram(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassDiagramLineContext extends ParserRuleContext {
		public ClassDiagramLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public ClassRuleContext classRule() {
			return getRuleContext(ClassRuleContext.class, 0);
		}

		public AddResourceToClassRuleContext addResourceToClassRule() {
			return getRuleContext(AddResourceToClassRuleContext.class, 0);
		}

		public ClassPackageRuleContext classPackageRule() {
			return getRuleContext(ClassPackageRuleContext.class, 0);
		}

		public ClassDiagramRelationRuleContext classDiagramRelationRule() {
			return getRuleContext(ClassDiagramRelationRuleContext.class, 0);
		}

		public NoteRuleContext noteRule() {
			return getRuleContext(NoteRuleContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classDiagramLine;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassDiagramLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCaseDiagramLineContext extends ParserRuleContext {
		public UseCaseDiagramLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public ActorRuleContext actorRule() {
			return getRuleContext(ActorRuleContext.class, 0);
		}

		public UseCaseRuleContext useCaseRule() {
			return getRuleContext(UseCaseRuleContext.class, 0);
		}

		public UseCasePackageRuleContext useCasePackageRule() {
			return getRuleContext(UseCasePackageRuleContext.class, 0);
		}

		public UseCaseDiagramRelationRuleContext useCaseDiagramRelationRule() {
			return getRuleContext(UseCaseDiagramRelationRuleContext.class, 0);
		}

		public NoteRuleContext noteRule() {
			return getRuleContext(NoteRuleContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCaseDiagramLine;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCaseDiagramLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class SequenceDiagramLineContext extends ParserRuleContext {
		public SequenceDiagramLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public ParticipantRuleContext participantRule() {
			return getRuleContext(ParticipantRuleContext.class, 0);
		}

		public MessageRuleContext messageRule() {
			return getRuleContext(MessageRuleContext.class, 0);
		}

		public GroupRuleContext groupRule() {
			return getRuleContext(GroupRuleContext.class, 0);
		}

		public RefRuleContext refRule() {
			return getRuleContext(RefRuleContext.class, 0);
		}

		public CreateRuleContext createRule() {
			return getRuleContext(CreateRuleContext.class, 0);
		}

		public ReturnRuleContext returnRule() {
			return getRuleContext(ReturnRuleContext.class, 0);
		}

		public ActivateRuleContext activateRule() {
			return getRuleContext(ActivateRuleContext.class, 0);
		}

		public DeactivateRuleContext deactivateRule() {
			return getRuleContext(DeactivateRuleContext.class, 0);
		}

		public SequenceNoteRuleContext sequenceNoteRule() {
			return getRuleContext(SequenceNoteRuleContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_sequenceDiagramLine;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitSequenceDiagramLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class NoteRuleContext extends ParserRuleContext {
		public NoteRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<TerminalNode> NOTE_KEYWORD() {
			return getTokens(PlantUMLParser.NOTE_KEYWORD);
		}

		public TerminalNode NOTE_KEYWORD(int i) {
			return getToken(PlantUMLParser.NOTE_KEYWORD, i);
		}

		public TerminalNode QUOTED_IDENTIFICATOR() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, 0);
		}

		public BasicAliasContext basicAlias() {
			return getRuleContext(BasicAliasContext.class, 0);
		}

		public TerminalNode DIRECTION_KEYWORD() {
			return getToken(PlantUMLParser.DIRECTION_KEYWORD, 0);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		public TerminalNode OF_KEYWORD() {
			return getToken(PlantUMLParser.OF_KEYWORD, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		public TerminalNode END_KEYWORD() {
			return getToken(PlantUMLParser.END_KEYWORD, 0);
		}

		public TerminalNode COLON() {
			return getToken(PlantUMLParser.COLON, 0);
		}

		public TerminalNode OPENED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.OPENED_CURLY_BRACKET, 0);
		}

		public TerminalNode CLOSED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.CLOSED_CURLY_BRACKET, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		public Identificator_of_noteContext identificator_of_note() {
			return getRuleContext(Identificator_of_noteContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_noteRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitNoteRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class SkinparamRuleContext extends ParserRuleContext {
		public SkinparamRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode SKINPARAM_KEYWORD() {
			return getToken(PlantUMLParser.SKINPARAM_KEYWORD, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_skinparamRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitSkinparamRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class AutonumberRuleContext extends ParserRuleContext {
		public AutonumberRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode AUTONUMBER_KEYWORD() {
			return getToken(PlantUMLParser.AUTONUMBER_KEYWORD, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_autonumberRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitAutonumberRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassRuleContext extends ParserRuleContext {
		public ClassRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public ClassTypeWithAbstractContext classTypeWithAbstract() {
			return getRuleContext(ClassTypeWithAbstractContext.class, 0);
		}

		public List<IdentificatorContext> identificator() {
			return getRuleContexts(IdentificatorContext.class);
		}

		public IdentificatorContext identificator(int i) {
			return getRuleContext(IdentificatorContext.class, i);
		}

		public BasicAliasContext basicAlias() {
			return getRuleContext(BasicAliasContext.class, 0);
		}

		public GenericsContext generics() {
			return getRuleContext(GenericsContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		public TerminalNode EXTENDS_KEYWORD() {
			return getToken(PlantUMLParser.EXTENDS_KEYWORD, 0);
		}

		public TerminalNode IMPLEMENTS_KEYWORD() {
			return getToken(PlantUMLParser.IMPLEMENTS_KEYWORD, 0);
		}

		public ClassBodyContext classBody() {
			return getRuleContext(ClassBodyContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class AddResourceToClassRuleContext extends ParserRuleContext {
		public AddResourceToClassRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode COLON() {
			return getToken(PlantUMLParser.COLON, 0);
		}

		public ClassBodyLineContext classBodyLine() {
			return getRuleContext(ClassBodyLineContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_addResourceToClassRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitAddResourceToClassRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassPackageRuleContext extends ParserRuleContext {
		public ClassPackageRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode PACKAGE_KEYWORD() {
			return getToken(PlantUMLParser.PACKAGE_KEYWORD, 0);
		}

		public TerminalNode NAMESPACE_KEYWORD() {
			return getToken(PlantUMLParser.NAMESPACE_KEYWORD, 0);
		}

		public TerminalNode RECTANGLE_KEYWORD() {
			return getToken(PlantUMLParser.RECTANGLE_KEYWORD, 0);
		}

		public BasicAliasContext basicAlias() {
			return getRuleContext(BasicAliasContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		public ClassPackageBodyContext classPackageBody() {
			return getRuleContext(ClassPackageBodyContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classPackageRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassPackageRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassDiagramRelationRuleContext extends ParserRuleContext {
		public ClassDiagramRelationRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<IdentificatorContext> identificator() {
			return getRuleContexts(IdentificatorContext.class);
		}

		public IdentificatorContext identificator(int i) {
			return getRuleContext(IdentificatorContext.class, i);
		}

		public TerminalNode ARROW_DOTTED() {
			return getToken(PlantUMLParser.ARROW_DOTTED, 0);
		}

		public TerminalNode ARROW_DASHED() {
			return getToken(PlantUMLParser.ARROW_DASHED, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, 0);
		}

		public List<TerminalNode> QUOTED_IDENTIFICATOR() {
			return getTokens(PlantUMLParser.QUOTED_IDENTIFICATOR);
		}

		public TerminalNode QUOTED_IDENTIFICATOR(int i) {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, i);
		}

		public TerminalNode COLON() {
			return getToken(PlantUMLParser.COLON, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classDiagramRelationRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassDiagramRelationRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassTypeWithAbstractContext extends ParserRuleContext {
		public ClassTypeWithAbstractContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode CLASS_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.CLASS_TYPE_KEYWORD, 0);
		}

		public TerminalNode CLASS_ANOTHER_TYPES_KEYWORD() {
			return getToken(PlantUMLParser.CLASS_ANOTHER_TYPES_KEYWORD, 0);
		}

		public TerminalNode ENTITY_KEYWORD() {
			return getToken(PlantUMLParser.ENTITY_KEYWORD, 0);
		}

		public TerminalNode ABSTRACT_KEYWORD() {
			return getToken(PlantUMLParser.ABSTRACT_KEYWORD, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classTypeWithAbstract;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassTypeWithAbstract(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassBodyContext extends ParserRuleContext {
		public ClassBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode OPENED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.OPENED_CURLY_BRACKET, 0);
		}

		public TerminalNode CLOSED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.CLOSED_CURLY_BRACKET, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		public List<ClassBodyLineContext> classBodyLine() {
			return getRuleContexts(ClassBodyLineContext.class);
		}

		public ClassBodyLineContext classBodyLine(int i) {
			return getRuleContext(ClassBodyLineContext.class, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classBody;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassPackageBodyContext extends ParserRuleContext {
		public ClassPackageBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode OPENED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.OPENED_CURLY_BRACKET, 0);
		}

		public TerminalNode CLOSED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.CLOSED_CURLY_BRACKET, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		public List<ClassPackageBodyLineContext> classPackageBodyLine() {
			return getRuleContexts(ClassPackageBodyLineContext.class);
		}

		public ClassPackageBodyLineContext classPackageBodyLine(int i) {
			return getRuleContext(ClassPackageBodyLineContext.class, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classPackageBody;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassPackageBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassBodyLineContext extends ParserRuleContext {
		public ClassBodyLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<Identificator_with_space_and_special_charsContext> identificator_with_space_and_special_chars() {
			return getRuleContexts(Identificator_with_space_and_special_charsContext.class);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars(int i) {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, i);
		}

		public List<TerminalNode> VISIBILITY_SIGN() {
			return getTokens(PlantUMLParser.VISIBILITY_SIGN);
		}

		public TerminalNode VISIBILITY_SIGN(int i) {
			return getToken(PlantUMLParser.VISIBILITY_SIGN, i);
		}

		public List<TerminalNode> OPENED_CURLY_BRACKET() {
			return getTokens(PlantUMLParser.OPENED_CURLY_BRACKET);
		}

		public TerminalNode OPENED_CURLY_BRACKET(int i) {
			return getToken(PlantUMLParser.OPENED_CURLY_BRACKET, i);
		}

		public List<TerminalNode> FIELD_KEYWORD() {
			return getTokens(PlantUMLParser.FIELD_KEYWORD);
		}

		public TerminalNode FIELD_KEYWORD(int i) {
			return getToken(PlantUMLParser.FIELD_KEYWORD, i);
		}

		public List<TerminalNode> CLOSED_CURLY_BRACKET() {
			return getTokens(PlantUMLParser.CLOSED_CURLY_BRACKET);
		}

		public TerminalNode CLOSED_CURLY_BRACKET(int i) {
			return getToken(PlantUMLParser.CLOSED_CURLY_BRACKET, i);
		}

		public List<TerminalNode> METHOD_KEYWORD() {
			return getTokens(PlantUMLParser.METHOD_KEYWORD);
		}

		public TerminalNode METHOD_KEYWORD(int i) {
			return getToken(PlantUMLParser.METHOD_KEYWORD, i);
		}

		public List<TerminalNode> ABSTRACT_KEYWORD() {
			return getTokens(PlantUMLParser.ABSTRACT_KEYWORD);
		}

		public TerminalNode ABSTRACT_KEYWORD(int i) {
			return getToken(PlantUMLParser.ABSTRACT_KEYWORD, i);
		}

		public List<TerminalNode> CLASSIFIER_KEYWORD() {
			return getTokens(PlantUMLParser.CLASSIFIER_KEYWORD);
		}

		public TerminalNode CLASSIFIER_KEYWORD(int i) {
			return getToken(PlantUMLParser.CLASSIFIER_KEYWORD, i);
		}

		public List<TerminalNode> STATIC_KEYWORD() {
			return getTokens(PlantUMLParser.STATIC_KEYWORD);
		}

		public TerminalNode STATIC_KEYWORD(int i) {
			return getToken(PlantUMLParser.STATIC_KEYWORD, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classBodyLine;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassBodyLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ClassPackageBodyLineContext extends ParserRuleContext {
		public ClassPackageBodyLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TopToBottomDirectionContext topToBottomDirection() {
			return getRuleContext(TopToBottomDirectionContext.class, 0);
		}

		public LeftToRightDirectionContext leftToRightDirection() {
			return getRuleContext(LeftToRightDirectionContext.class, 0);
		}

		public SkinparamRuleContext skinparamRule() {
			return getRuleContext(SkinparamRuleContext.class, 0);
		}

		public ClassRuleContext classRule() {
			return getRuleContext(ClassRuleContext.class, 0);
		}

		public NoteRuleContext noteRule() {
			return getRuleContext(NoteRuleContext.class, 0);
		}

		public AddResourceToClassRuleContext addResourceToClassRule() {
			return getRuleContext(AddResourceToClassRuleContext.class, 0);
		}

		public ClassPackageRuleContext classPackageRule() {
			return getRuleContext(ClassPackageRuleContext.class, 0);
		}

		public ClassDiagramRelationRuleContext classDiagramRelationRule() {
			return getRuleContext(ClassDiagramRelationRuleContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_classPackageBodyLine;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitClassPackageBodyLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class HideRuleContext extends ParserRuleContext {
		public HideRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode HIDE_KEYWORD() {
			return getToken(PlantUMLParser.HIDE_KEYWORD, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_hideRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitHideRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ShowRuleContext extends ParserRuleContext {
		public ShowRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode SHOW_KEYWORD() {
			return getToken(PlantUMLParser.SHOW_KEYWORD, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_showRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitShowRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ActorRuleContext extends ParserRuleContext {
		public ActorRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode ACTOR_KEYWORD() {
			return getToken(PlantUMLParser.ACTOR_KEYWORD, 0);
		}

		public ActorAliasContext actorAlias() {
			return getRuleContext(ActorAliasContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_actorRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitActorRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCaseRuleContext extends ParserRuleContext {
		public UseCaseRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode USECASE_KEYWORD() {
			return getToken(PlantUMLParser.USECASE_KEYWORD, 0);
		}

		public UseCaseAliasContext useCaseAlias() {
			return getRuleContext(UseCaseAliasContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCaseRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCaseRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCasePackageRuleContext extends ParserRuleContext {
		public UseCasePackageRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode PACKAGE_KEYWORD() {
			return getToken(PlantUMLParser.PACKAGE_KEYWORD, 0);
		}

		public TerminalNode RECTANGLE_KEYWORD() {
			return getToken(PlantUMLParser.RECTANGLE_KEYWORD, 0);
		}

		public BasicAliasContext basicAlias() {
			return getRuleContext(BasicAliasContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		public UseCasePackageBodyContext useCasePackageBody() {
			return getRuleContext(UseCasePackageBodyContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCasePackageRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCasePackageRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCaseDiagramRelationRuleContext extends ParserRuleContext {
		public UseCaseDiagramRelationRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode ARROW_DOTTED() {
			return getToken(PlantUMLParser.ARROW_DOTTED, 0);
		}

		public TerminalNode ARROW_DASHED() {
			return getToken(PlantUMLParser.ARROW_DASHED, 0);
		}

		public List<IdentificatorContext> identificator() {
			return getRuleContexts(IdentificatorContext.class);
		}

		public IdentificatorContext identificator(int i) {
			return getRuleContext(IdentificatorContext.class, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, i);
		}

		public List<TerminalNode> QUOTED_IDENTIFICATOR() {
			return getTokens(PlantUMLParser.QUOTED_IDENTIFICATOR);
		}

		public TerminalNode QUOTED_IDENTIFICATOR(int i) {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, i);
		}

		public TerminalNode COLON() {
			return getToken(PlantUMLParser.COLON, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCaseDiagramRelationRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCaseDiagramRelationRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCasePackageBodyContext extends ParserRuleContext {
		public UseCasePackageBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode OPENED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.OPENED_CURLY_BRACKET, 0);
		}

		public TerminalNode CLOSED_CURLY_BRACKET() {
			return getToken(PlantUMLParser.CLOSED_CURLY_BRACKET, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		public List<UseCasePackageBodyLineContext> useCasePackageBodyLine() {
			return getRuleContexts(UseCasePackageBodyLineContext.class);
		}

		public UseCasePackageBodyLineContext useCasePackageBodyLine(int i) {
			return getRuleContext(UseCasePackageBodyLineContext.class, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCasePackageBody;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCasePackageBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCasePackageBodyLineContext extends ParserRuleContext {
		public UseCasePackageBodyLineContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TopToBottomDirectionContext topToBottomDirection() {
			return getRuleContext(TopToBottomDirectionContext.class, 0);
		}

		public LeftToRightDirectionContext leftToRightDirection() {
			return getRuleContext(LeftToRightDirectionContext.class, 0);
		}

		public SkinparamRuleContext skinparamRule() {
			return getRuleContext(SkinparamRuleContext.class, 0);
		}

		public ActorRuleContext actorRule() {
			return getRuleContext(ActorRuleContext.class, 0);
		}

		public UseCaseRuleContext useCaseRule() {
			return getRuleContext(UseCaseRuleContext.class, 0);
		}

		public NoteRuleContext noteRule() {
			return getRuleContext(NoteRuleContext.class, 0);
		}

		public UseCasePackageRuleContext useCasePackageRule() {
			return getRuleContext(UseCasePackageRuleContext.class, 0);
		}

		public UseCaseDiagramRelationRuleContext useCaseDiagramRelationRule() {
			return getRuleContext(UseCaseDiagramRelationRuleContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCasePackageBodyLine;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCasePackageBodyLine(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ParticipantRuleContext extends ParserRuleContext {
		public ParticipantRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode PARTICIPANT_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.PARTICIPANT_TYPE_KEYWORD, 0);
		}

		public TerminalNode ENTITY_KEYWORD() {
			return getToken(PlantUMLParser.ENTITY_KEYWORD, 0);
		}

		public TerminalNode ACTOR_KEYWORD() {
			return getToken(PlantUMLParser.ACTOR_KEYWORD, 0);
		}

		public BasicAliasContext basicAlias() {
			return getRuleContext(BasicAliasContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public OrderContext order() {
			return getRuleContext(OrderContext.class, 0);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_participantRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitParticipantRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class MessageRuleContext extends ParserRuleContext {
		public MessageRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode ARROW_SEQUENCE_DIAGRAM() {
			return getToken(PlantUMLParser.ARROW_SEQUENCE_DIAGRAM, 0);
		}

		public TerminalNode ARROW_DASHED() {
			return getToken(PlantUMLParser.ARROW_DASHED, 0);
		}

		public List<IdentificatorContext> identificator() {
			return getRuleContexts(IdentificatorContext.class);
		}

		public IdentificatorContext identificator(int i) {
			return getRuleContext(IdentificatorContext.class, i);
		}

		public TerminalNode COLON() {
			return getToken(PlantUMLParser.COLON, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		public List<BasicAliasContext> basicAlias() {
			return getRuleContexts(BasicAliasContext.class);
		}

		public BasicAliasContext basicAlias(int i) {
			return getRuleContext(BasicAliasContext.class, i);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_messageRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitMessageRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class GroupRuleContext extends ParserRuleContext {
		public GroupRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode END_KEYWORD() {
			return getToken(PlantUMLParser.END_KEYWORD, 0);
		}

		public GroupCaseContext groupCase() {
			return getRuleContext(GroupCaseContext.class, 0);
		}

		public List<ElseGroupBranchContext> elseGroupBranch() {
			return getRuleContexts(ElseGroupBranchContext.class);
		}

		public ElseGroupBranchContext elseGroupBranch(int i) {
			return getRuleContext(ElseGroupBranchContext.class, i);
		}

		public Identificator_with_spaceContext identificator_with_space() {
			return getRuleContext(Identificator_with_spaceContext.class, 0);
		}

		public TerminalNode GROUP_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.GROUP_TYPE_KEYWORD, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_groupRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitGroupRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class CreateRuleContext extends ParserRuleContext {
		public CreateRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode CREATE_KEYWORD() {
			return getToken(PlantUMLParser.CREATE_KEYWORD, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public TerminalNode NEWLINE() {
			return getToken(PlantUMLParser.NEWLINE, 0);
		}

		public MessageRuleContext messageRule() {
			return getRuleContext(MessageRuleContext.class, 0);
		}

		public BasicAliasContext basicAlias() {
			return getRuleContext(BasicAliasContext.class, 0);
		}

		public List<StereotypesContext> stereotypes() {
			return getRuleContexts(StereotypesContext.class);
		}

		public StereotypesContext stereotypes(int i) {
			return getRuleContext(StereotypesContext.class, i);
		}

		public OrderContext order() {
			return getRuleContext(OrderContext.class, 0);
		}

		public TerminalNode HEX_COLOR() {
			return getToken(PlantUMLParser.HEX_COLOR, 0);
		}

		public TerminalNode PARTICIPANT_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.PARTICIPANT_TYPE_KEYWORD, 0);
		}

		public TerminalNode ENTITY_KEYWORD() {
			return getToken(PlantUMLParser.ENTITY_KEYWORD, 0);
		}

		public TerminalNode ACTOR_KEYWORD() {
			return getToken(PlantUMLParser.ACTOR_KEYWORD, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_createRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitCreateRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ReturnRuleContext extends ParserRuleContext {
		public ReturnRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode RETURN_KEYWORD() {
			return getToken(PlantUMLParser.RETURN_KEYWORD, 0);
		}

		public Identificator_with_spaceContext identificator_with_space() {
			return getRuleContext(Identificator_with_spaceContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_returnRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitReturnRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ActivateRuleContext extends ParserRuleContext {
		public ActivateRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode ACTIVATE_KEYWORD() {
			return getToken(PlantUMLParser.ACTIVATE_KEYWORD, 0);
		}

		public Identificator_with_spaceContext identificator_with_space() {
			return getRuleContext(Identificator_with_spaceContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_activateRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitActivateRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class DeactivateRuleContext extends ParserRuleContext {
		public DeactivateRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode DEACTIVATE_KEYWORD() {
			return getToken(PlantUMLParser.DEACTIVATE_KEYWORD, 0);
		}

		public Identificator_with_spaceContext identificator_with_space() {
			return getRuleContext(Identificator_with_spaceContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_deactivateRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitDeactivateRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class SequenceNoteRuleContext extends ParserRuleContext {
		public SequenceNoteRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode NOTE_KEYWORD() {
			return getToken(PlantUMLParser.NOTE_KEYWORD, 0);
		}

		public TerminalNode OVER_KEYWORD() {
			return getToken(PlantUMLParser.OVER_KEYWORD, 0);
		}

		public Identificator_with_spaceContext identificator_with_space() {
			return getRuleContext(Identificator_with_spaceContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_sequenceNoteRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitSequenceNoteRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class RefRuleContext extends ParserRuleContext {
		public RefRuleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<TerminalNode> REF_KEYWORD() {
			return getTokens(PlantUMLParser.REF_KEYWORD);
		}

		public TerminalNode REF_KEYWORD(int i) {
			return getToken(PlantUMLParser.REF_KEYWORD, i);
		}

		public TerminalNode OVER_KEYWORD() {
			return getToken(PlantUMLParser.OVER_KEYWORD, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		public TerminalNode END_KEYWORD() {
			return getToken(PlantUMLParser.END_KEYWORD, 0);
		}

		public TerminalNode COLON() {
			return getToken(PlantUMLParser.COLON, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, 0);
		}

		public Identificator_of_noteContext identificator_of_note() {
			return getRuleContext(Identificator_of_noteContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_refRule;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitRefRule(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class GroupCaseContext extends ParserRuleContext {
		public GroupCaseContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<TerminalNode> NEWLINE() {
			return getTokens(PlantUMLParser.NEWLINE);
		}

		public TerminalNode NEWLINE(int i) {
			return getToken(PlantUMLParser.NEWLINE, i);
		}

		public List<MessageRuleContext> messageRule() {
			return getRuleContexts(MessageRuleContext.class);
		}

		public MessageRuleContext messageRule(int i) {
			return getRuleContext(MessageRuleContext.class, i);
		}

		public List<ParticipantRuleContext> participantRule() {
			return getRuleContexts(ParticipantRuleContext.class);
		}

		public ParticipantRuleContext participantRule(int i) {
			return getRuleContext(ParticipantRuleContext.class, i);
		}

		public List<GroupRuleContext> groupRule() {
			return getRuleContexts(GroupRuleContext.class);
		}

		public GroupRuleContext groupRule(int i) {
			return getRuleContext(GroupRuleContext.class, i);
		}

		public List<RefRuleContext> refRule() {
			return getRuleContexts(RefRuleContext.class);
		}

		public RefRuleContext refRule(int i) {
			return getRuleContext(RefRuleContext.class, i);
		}

		public List<CreateRuleContext> createRule() {
			return getRuleContexts(CreateRuleContext.class);
		}

		public CreateRuleContext createRule(int i) {
			return getRuleContext(CreateRuleContext.class, i);
		}

		public List<ReturnRuleContext> returnRule() {
			return getRuleContexts(ReturnRuleContext.class);
		}

		public ReturnRuleContext returnRule(int i) {
			return getRuleContext(ReturnRuleContext.class, i);
		}

		public List<ActivateRuleContext> activateRule() {
			return getRuleContexts(ActivateRuleContext.class);
		}

		public ActivateRuleContext activateRule(int i) {
			return getRuleContext(ActivateRuleContext.class, i);
		}

		public List<DeactivateRuleContext> deactivateRule() {
			return getRuleContexts(DeactivateRuleContext.class);
		}

		public DeactivateRuleContext deactivateRule(int i) {
			return getRuleContext(DeactivateRuleContext.class, i);
		}

		public List<SequenceNoteRuleContext> sequenceNoteRule() {
			return getRuleContexts(SequenceNoteRuleContext.class);
		}

		public SequenceNoteRuleContext sequenceNoteRule(int i) {
			return getRuleContext(SequenceNoteRuleContext.class, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_groupCase;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitGroupCase(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ElseGroupBranchContext extends ParserRuleContext {
		public ElseGroupBranchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode ELSE_KEYWORD() {
			return getToken(PlantUMLParser.ELSE_KEYWORD, 0);
		}

		public GroupCaseContext groupCase() {
			return getRuleContext(GroupCaseContext.class, 0);
		}

		public Identificator_with_space_and_special_charsContext identificator_with_space_and_special_chars() {
			return getRuleContext(Identificator_with_space_and_special_charsContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_elseGroupBranch;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitElseGroupBranch(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class OrderContext extends ParserRuleContext {
		public OrderContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode ORDER_KEYWORD() {
			return getToken(PlantUMLParser.ORDER_KEYWORD, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_order;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitOrder(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class TopToBottomDirectionContext extends ParserRuleContext {
		public TopToBottomDirectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode TOP_TO_BOTTOM_DIRECTION_KEYWORD() {
			return getToken(PlantUMLParser.TOP_TO_BOTTOM_DIRECTION_KEYWORD, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_topToBottomDirection;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitTopToBottomDirection(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class LeftToRightDirectionContext extends ParserRuleContext {
		public LeftToRightDirectionContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode LEFT_TO_RIGHT_DIRECTION_KEYWORD() {
			return getToken(PlantUMLParser.LEFT_TO_RIGHT_DIRECTION_KEYWORD, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_leftToRightDirection;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitLeftToRightDirection(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class BasicAliasContext extends ParserRuleContext {
		public BasicAliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode AS_KEYWORD() {
			return getToken(PlantUMLParser.AS_KEYWORD, 0);
		}

		public IdentificatorContext identificator() {
			return getRuleContext(IdentificatorContext.class, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_basicAlias;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitBasicAlias(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class UseCaseAliasContext extends ParserRuleContext {
		public UseCaseAliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode AS_KEYWORD() {
			return getToken(PlantUMLParser.AS_KEYWORD, 0);
		}

		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR() {
			return getToken(PlantUMLParser.IDENTIFICATOR, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR_WITH_NEW_LINES() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR_WITH_NEW_LINES, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_useCaseAlias;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitUseCaseAlias(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class ActorAliasContext extends ParserRuleContext {
		public ActorAliasContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode AS_KEYWORD() {
			return getToken(PlantUMLParser.AS_KEYWORD, 0);
		}

		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, 0);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, 0);
		}

		public TerminalNode IDENTIFICATOR() {
			return getToken(PlantUMLParser.IDENTIFICATOR, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR_WITH_NEW_LINES() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR_WITH_NEW_LINES, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_actorAlias;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitActorAlias(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class GenericsContext extends ParserRuleContext {
		public GenericsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode OPENED_ANGLE_BRACKET() {
			return getToken(PlantUMLParser.OPENED_ANGLE_BRACKET, 0);
		}

		public Dentificator_with_space_and_special_chars_without_angle_bracketsContext dentificator_with_space_and_special_chars_without_angle_brackets() {
			return getRuleContext(Dentificator_with_space_and_special_chars_without_angle_bracketsContext.class, 0);
		}

		public TerminalNode CLOSED_ANGLE_BRACKET() {
			return getToken(PlantUMLParser.CLOSED_ANGLE_BRACKET, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_generics;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitGenerics(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class StereotypesContext extends ParserRuleContext {
		public StereotypesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode DOUBLE_OPENED_ANGLE_BRACKET() {
			return getToken(PlantUMLParser.DOUBLE_OPENED_ANGLE_BRACKET, 0);
		}

		public Dentificator_with_space_and_special_chars_without_angle_bracketsContext dentificator_with_space_and_special_chars_without_angle_brackets() {
			return getRuleContext(Dentificator_with_space_and_special_chars_without_angle_bracketsContext.class, 0);
		}

		public TerminalNode DOUBLE_CLOSED_ANGLE_BRACKET() {
			return getToken(PlantUMLParser.DOUBLE_CLOSED_ANGLE_BRACKET, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_stereotypes;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitStereotypes(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class IdentificatorContext extends ParserRuleContext {
		public IdentificatorContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public KeywordContext keyword() {
			return getRuleContext(KeywordContext.class, 0);
		}

		public TerminalNode IDENTIFICATOR() {
			return getToken(PlantUMLParser.IDENTIFICATOR, 0);
		}

		public TerminalNode QUOTED_IDENTIFICATOR() {
			return getToken(PlantUMLParser.QUOTED_IDENTIFICATOR, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_identificator;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitIdentificator(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class Identificator_with_spaceContext extends ParserRuleContext {
		public Identificator_with_spaceContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<KeywordContext> keyword() {
			return getRuleContexts(KeywordContext.class);
		}

		public KeywordContext keyword(int i) {
			return getRuleContext(KeywordContext.class, i);
		}

		public List<TerminalNode> IDENTIFICATOR() {
			return getTokens(PlantUMLParser.IDENTIFICATOR);
		}

		public TerminalNode IDENTIFICATOR(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR, i);
		}

		public List<TerminalNode> SPACE() {
			return getTokens(PlantUMLParser.SPACE);
		}

		public TerminalNode SPACE(int i) {
			return getToken(PlantUMLParser.SPACE, i);
		}

		public List<TerminalNode> COLON() {
			return getTokens(PlantUMLParser.COLON);
		}

		public TerminalNode COLON(int i) {
			return getToken(PlantUMLParser.COLON, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_identificator_with_space;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitIdentificator_with_space(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class Dentificator_with_space_and_special_chars_without_angle_bracketsContext extends ParserRuleContext {
		public Dentificator_with_space_and_special_chars_without_angle_bracketsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<KeywordContext> keyword() {
			return getRuleContexts(KeywordContext.class);
		}

		public KeywordContext keyword(int i) {
			return getRuleContext(KeywordContext.class, i);
		}

		public List<TerminalNode> IDENTIFICATOR() {
			return getTokens(PlantUMLParser.IDENTIFICATOR);
		}

		public TerminalNode IDENTIFICATOR(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR, i);
		}

		public List<TerminalNode> SPACE() {
			return getTokens(PlantUMLParser.SPACE);
		}

		public TerminalNode SPACE(int i) {
			return getToken(PlantUMLParser.SPACE, i);
		}

		public List<TerminalNode> COLON() {
			return getTokens(PlantUMLParser.COLON);
		}

		public TerminalNode COLON(int i) {
			return getToken(PlantUMLParser.COLON, i);
		}

		public List<TerminalNode> VISIBILITY_SIGN() {
			return getTokens(PlantUMLParser.VISIBILITY_SIGN);
		}

		public TerminalNode VISIBILITY_SIGN(int i) {
			return getToken(PlantUMLParser.VISIBILITY_SIGN, i);
		}

		public List<TerminalNode> QUOTE() {
			return getTokens(PlantUMLParser.QUOTE);
		}

		public TerminalNode QUOTE(int i) {
			return getToken(PlantUMLParser.QUOTE, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(PlantUMLParser.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(PlantUMLParser.DOT, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_dentificator_with_space_and_special_chars_without_angle_brackets;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitDentificator_with_space_and_special_chars_without_angle_brackets(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class Identificator_with_space_and_special_charsContext extends ParserRuleContext {
		public Identificator_with_space_and_special_charsContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<KeywordContext> keyword() {
			return getRuleContexts(KeywordContext.class);
		}

		public KeywordContext keyword(int i) {
			return getRuleContext(KeywordContext.class, i);
		}

		public List<TerminalNode> IDENTIFICATOR() {
			return getTokens(PlantUMLParser.IDENTIFICATOR);
		}

		public TerminalNode IDENTIFICATOR(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR, i);
		}

		public List<TerminalNode> SPACE() {
			return getTokens(PlantUMLParser.SPACE);
		}

		public TerminalNode SPACE(int i) {
			return getToken(PlantUMLParser.SPACE, i);
		}

		public List<TerminalNode> COLON() {
			return getTokens(PlantUMLParser.COLON);
		}

		public TerminalNode COLON(int i) {
			return getToken(PlantUMLParser.COLON, i);
		}

		public List<TerminalNode> CLOSED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.CLOSED_ANGLE_BRACKET);
		}

		public TerminalNode CLOSED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.CLOSED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> OPENED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.OPENED_ANGLE_BRACKET);
		}

		public TerminalNode OPENED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.OPENED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> DOUBLE_CLOSED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.DOUBLE_CLOSED_ANGLE_BRACKET);
		}

		public TerminalNode DOUBLE_CLOSED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.DOUBLE_CLOSED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> DOUBLE_OPENED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.DOUBLE_OPENED_ANGLE_BRACKET);
		}

		public TerminalNode DOUBLE_OPENED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.DOUBLE_OPENED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, i);
		}

		public List<TerminalNode> VISIBILITY_SIGN() {
			return getTokens(PlantUMLParser.VISIBILITY_SIGN);
		}

		public TerminalNode VISIBILITY_SIGN(int i) {
			return getToken(PlantUMLParser.VISIBILITY_SIGN, i);
		}

		public List<TerminalNode> QUOTE() {
			return getTokens(PlantUMLParser.QUOTE);
		}

		public TerminalNode QUOTE(int i) {
			return getToken(PlantUMLParser.QUOTE, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(PlantUMLParser.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(PlantUMLParser.DOT, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_identificator_with_space_and_special_chars;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitIdentificator_with_space_and_special_chars(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class Identificator_of_noteContext extends ParserRuleContext {
		public Identificator_of_noteContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public List<TerminalNode> END_KEYWORD() {
			return getTokens(PlantUMLParser.END_KEYWORD);
		}

		public TerminalNode END_KEYWORD(int i) {
			return getToken(PlantUMLParser.END_KEYWORD, i);
		}

		public List<TerminalNode> NOTE_KEYWORD() {
			return getTokens(PlantUMLParser.NOTE_KEYWORD);
		}

		public TerminalNode NOTE_KEYWORD(int i) {
			return getToken(PlantUMLParser.NOTE_KEYWORD, i);
		}

		public List<KeywordContext> keyword() {
			return getRuleContexts(KeywordContext.class);
		}

		public KeywordContext keyword(int i) {
			return getRuleContext(KeywordContext.class, i);
		}

		public List<TerminalNode> IDENTIFICATOR() {
			return getTokens(PlantUMLParser.IDENTIFICATOR);
		}

		public TerminalNode IDENTIFICATOR(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR, i);
		}

		public List<TerminalNode> SPACE() {
			return getTokens(PlantUMLParser.SPACE);
		}

		public TerminalNode SPACE(int i) {
			return getToken(PlantUMLParser.SPACE, i);
		}

		public List<TerminalNode> COLON() {
			return getTokens(PlantUMLParser.COLON);
		}

		public TerminalNode COLON(int i) {
			return getToken(PlantUMLParser.COLON, i);
		}

		public List<TerminalNode> CLOSED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.CLOSED_ANGLE_BRACKET);
		}

		public TerminalNode CLOSED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.CLOSED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> OPENED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.OPENED_ANGLE_BRACKET);
		}

		public TerminalNode OPENED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.OPENED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> DOUBLE_CLOSED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.DOUBLE_CLOSED_ANGLE_BRACKET);
		}

		public TerminalNode DOUBLE_CLOSED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.DOUBLE_CLOSED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> DOUBLE_OPENED_ANGLE_BRACKET() {
			return getTokens(PlantUMLParser.DOUBLE_OPENED_ANGLE_BRACKET);
		}

		public TerminalNode DOUBLE_OPENED_ANGLE_BRACKET(int i) {
			return getToken(PlantUMLParser.DOUBLE_OPENED_ANGLE_BRACKET, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_BRACKETS() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES, i);
		}

		public List<TerminalNode> IDENTIFICATOR_CLOSED_IN_COLONS() {
			return getTokens(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS);
		}

		public TerminalNode IDENTIFICATOR_CLOSED_IN_COLONS(int i) {
			return getToken(PlantUMLParser.IDENTIFICATOR_CLOSED_IN_COLONS, i);
		}

		public List<TerminalNode> VISIBILITY_SIGN() {
			return getTokens(PlantUMLParser.VISIBILITY_SIGN);
		}

		public TerminalNode VISIBILITY_SIGN(int i) {
			return getToken(PlantUMLParser.VISIBILITY_SIGN, i);
		}

		public List<TerminalNode> QUOTE() {
			return getTokens(PlantUMLParser.QUOTE);
		}

		public TerminalNode QUOTE(int i) {
			return getToken(PlantUMLParser.QUOTE, i);
		}

		public List<TerminalNode> DOT() {
			return getTokens(PlantUMLParser.DOT);
		}

		public TerminalNode DOT(int i) {
			return getToken(PlantUMLParser.DOT, i);
		}

		@Override
		public int getRuleIndex() {
			return RULE_identificator_of_note;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor)
				return ((PlantUMLVisitor<? extends T>) visitor).visitIdentificator_of_note(this);
			else return visitor.visitChildren(this);
		}
	}

	public static class KeywordContext extends ParserRuleContext {
		public KeywordContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}

		public TerminalNode ABSTRACT_KEYWORD() {
			return getToken(PlantUMLParser.ABSTRACT_KEYWORD, 0);
		}

		public TerminalNode CLASS_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.CLASS_TYPE_KEYWORD, 0);
		}

		public TerminalNode EXTENDS_KEYWORD() {
			return getToken(PlantUMLParser.EXTENDS_KEYWORD, 0);
		}

		public TerminalNode IMPLEMENTS_KEYWORD() {
			return getToken(PlantUMLParser.IMPLEMENTS_KEYWORD, 0);
		}

		public TerminalNode CLASS_ANOTHER_TYPES_KEYWORD() {
			return getToken(PlantUMLParser.CLASS_ANOTHER_TYPES_KEYWORD, 0);
		}

		public TerminalNode FIELD_KEYWORD() {
			return getToken(PlantUMLParser.FIELD_KEYWORD, 0);
		}

		public TerminalNode METHOD_KEYWORD() {
			return getToken(PlantUMLParser.METHOD_KEYWORD, 0);
		}

		public TerminalNode STATIC_KEYWORD() {
			return getToken(PlantUMLParser.STATIC_KEYWORD, 0);
		}

		public TerminalNode CLASSIFIER_KEYWORD() {
			return getToken(PlantUMLParser.CLASSIFIER_KEYWORD, 0);
		}

		public TerminalNode PACKAGE_KEYWORD() {
			return getToken(PlantUMLParser.PACKAGE_KEYWORD, 0);
		}

		public TerminalNode NAMESPACE_KEYWORD() {
			return getToken(PlantUMLParser.NAMESPACE_KEYWORD, 0);
		}

		public TerminalNode NOTE_KEYWORD() {
			return getToken(PlantUMLParser.NOTE_KEYWORD, 0);
		}

		public TerminalNode DIRECTION_KEYWORD() {
			return getToken(PlantUMLParser.DIRECTION_KEYWORD, 0);
		}

		public TerminalNode OF_KEYWORD() {
			return getToken(PlantUMLParser.OF_KEYWORD, 0);
		}

		public TerminalNode END_KEYWORD() {
			return getToken(PlantUMLParser.END_KEYWORD, 0);
		}

		public TerminalNode AS_KEYWORD() {
			return getToken(PlantUMLParser.AS_KEYWORD, 0);
		}

		public TerminalNode ACTOR_KEYWORD() {
			return getToken(PlantUMLParser.ACTOR_KEYWORD, 0);
		}

		public TerminalNode USECASE_KEYWORD() {
			return getToken(PlantUMLParser.USECASE_KEYWORD, 0);
		}

		public TerminalNode SYSTEM_KEYWORD() {
			return getToken(PlantUMLParser.SYSTEM_KEYWORD, 0);
		}

		public TerminalNode RECTANGLE_KEYWORD() {
			return getToken(PlantUMLParser.RECTANGLE_KEYWORD, 0);
		}

		public TerminalNode SKINPARAM_KEYWORD() {
			return getToken(PlantUMLParser.SKINPARAM_KEYWORD, 0);
		}

		public TerminalNode LEFT_TO_RIGHT_DIRECTION_KEYWORD() {
			return getToken(PlantUMLParser.LEFT_TO_RIGHT_DIRECTION_KEYWORD, 0);
		}

		public TerminalNode TOP_TO_BOTTOM_DIRECTION_KEYWORD() {
			return getToken(PlantUMLParser.TOP_TO_BOTTOM_DIRECTION_KEYWORD, 0);
		}

		public TerminalNode PARTICIPANT_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.PARTICIPANT_TYPE_KEYWORD, 0);
		}

		public TerminalNode ORDER_KEYWORD() {
			return getToken(PlantUMLParser.ORDER_KEYWORD, 0);
		}

		public TerminalNode ELSE_KEYWORD() {
			return getToken(PlantUMLParser.ELSE_KEYWORD, 0);
		}

		public TerminalNode GROUP_TYPE_KEYWORD() {
			return getToken(PlantUMLParser.GROUP_TYPE_KEYWORD, 0);
		}

		public TerminalNode CREATE_KEYWORD() {
			return getToken(PlantUMLParser.CREATE_KEYWORD, 0);
		}

		public TerminalNode RETURN_KEYWORD() {
			return getToken(PlantUMLParser.RETURN_KEYWORD, 0);
		}

		public TerminalNode REF_KEYWORD() {
			return getToken(PlantUMLParser.REF_KEYWORD, 0);
		}

		public TerminalNode OVER_KEYWORD() {
			return getToken(PlantUMLParser.OVER_KEYWORD, 0);
		}

		public TerminalNode ACTIVATE_KEYWORD() {
			return getToken(PlantUMLParser.ACTIVATE_KEYWORD, 0);
		}

		public TerminalNode DEACTIVATE_KEYWORD() {
			return getToken(PlantUMLParser.DEACTIVATE_KEYWORD, 0);
		}

		public TerminalNode AUTONUMBER_KEYWORD() {
			return getToken(PlantUMLParser.AUTONUMBER_KEYWORD, 0);
		}

		public TerminalNode HIDE_KEYWORD() {
			return getToken(PlantUMLParser.HIDE_KEYWORD, 0);
		}

		public TerminalNode SHOW_KEYWORD() {
			return getToken(PlantUMLParser.SHOW_KEYWORD, 0);
		}

		@Override
		public int getRuleIndex() {
			return RULE_keyword;
		}

		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if (visitor instanceof PlantUMLVisitor) return ((PlantUMLVisitor<? extends T>) visitor).visitKeyword(this);
			else return visitor.visitChildren(this);
		}
	}
}
