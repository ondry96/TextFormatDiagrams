grammar PlantUML;

uml:
    NEWLINE*
    '@startuml' NEWLINE+
    (
     sequenceDiagram |
     classDiagram |
     useCaseDiagram
    )
    '@enduml' NEWLINE*
    EOF
    ;

classDiagram: ((hideRule |
                showRule |
                topToBottomDirection |
                leftToRightDirection |
                skinparamRule |
                classDiagramLine) NEWLINE+)*;

useCaseDiagram: ((topToBottomDirection |
                  leftToRightDirection |
                  skinparamRule |
                  useCaseDiagramLine) NEWLINE+)*;

sequenceDiagram: ((skinparamRule |
                   autonumberRule |
                   sequenceDiagramLine) NEWLINE+)*;

classDiagramLine: classRule |
                  addResourceToClassRule |
                  classPackageRule |
                  classDiagramRelationRule |
                  noteRule;

useCaseDiagramLine : actorRule |
                     useCaseRule |
                     useCasePackageRule |
                     useCaseDiagramRelationRule |
                     noteRule;

sequenceDiagramLine : participantRule |
                      messageRule |
                      groupRule |
                      refRule |
                      createRule |
                      returnRule |
                      activateRule |
                      deactivateRule |
                      sequenceNoteRule
                      ;

//COMMON TO ALL DIAGRAMS
noteRule: NOTE_KEYWORD (
        (QUOTED_IDENTIFICATOR basicAlias HEX_COLOR?) |
        (DIRECTION_KEYWORD (OF_KEYWORD (IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_COLONS | IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_BRACKETS | identificator))? HEX_COLOR? (
                (NEWLINE ((identificator_of_note)? NEWLINE*) END_KEYWORD NOTE_KEYWORD) |
                (COLON (identificator_with_space_and_special_chars | QUOTED_IDENTIFICATOR)) |
                (OPENED_CURLY_BRACKET NEWLINE ((identificator_of_note)? NEWLINE*) CLOSED_CURLY_BRACKET)
            )
        ) |
        (basicAlias HEX_COLOR? ((NEWLINE ((identificator_of_note)? NEWLINE*) END_KEYWORD NOTE_KEYWORD) | (COLON (identificator_with_space_and_special_chars | QUOTED_IDENTIFICATOR))))
    );

skinparamRule : SKINPARAM_KEYWORD identificator_with_space_and_special_chars;
autonumberRule: AUTONUMBER_KEYWORD;
//CLASS DIAGRAM
classRule: classTypeWithAbstract identificator basicAlias? generics? stereotypes* HEX_COLOR? (EXTENDS_KEYWORD identificator)? (IMPLEMENTS_KEYWORD identificator)? classBody?;
addResourceToClassRule: identificator COLON classBodyLine;
classPackageRule: (PACKAGE_KEYWORD | NAMESPACE_KEYWORD | RECTANGLE_KEYWORD) identificator basicAlias? stereotypes* HEX_COLOR? classPackageBody?;
classDiagramRelationRule: ((IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_BRACKETS) | identificator) QUOTED_IDENTIFICATOR?  (ARROW_DOTTED | ARROW_DASHED)  QUOTED_IDENTIFICATOR?  identificator (COLON (identificator_with_space_and_special_chars | QUOTED_IDENTIFICATOR))? ;
classTypeWithAbstract: CLASS_TYPE_KEYWORD | CLASS_ANOTHER_TYPES_KEYWORD | ENTITY_KEYWORD | ABSTRACT_KEYWORD CLASS_TYPE_KEYWORD?;

classBody:
    OPENED_CURLY_BRACKET NEWLINE+
    (classBodyLine NEWLINE+)*
    CLOSED_CURLY_BRACKET;

classPackageBody:
    OPENED_CURLY_BRACKET NEWLINE+
    (classPackageBodyLine NEWLINE+)*
    CLOSED_CURLY_BRACKET;

classBodyLine :
    (identificator_with_space_and_special_chars* (VISIBILITY_SIGN |
     OPENED_CURLY_BRACKET FIELD_KEYWORD CLOSED_CURLY_BRACKET  |
     OPENED_CURLY_BRACKET METHOD_KEYWORD CLOSED_CURLY_BRACKET |
     OPENED_CURLY_BRACKET ABSTRACT_KEYWORD CLOSED_CURLY_BRACKET |
     OPENED_CURLY_BRACKET CLASSIFIER_KEYWORD CLOSED_CURLY_BRACKET |
     OPENED_CURLY_BRACKET STATIC_KEYWORD CLOSED_CURLY_BRACKET))* identificator_with_space_and_special_chars*;

classPackageBodyLine :
    topToBottomDirection |
    leftToRightDirection |
    skinparamRule |
    classRule |
    noteRule |
    addResourceToClassRule |
    classPackageRule |
    classDiagramRelationRule
    ;

hideRule: HIDE_KEYWORD identificator_with_space_and_special_chars;
showRule: SHOW_KEYWORD identificator_with_space_and_special_chars;

//USECASE DIAGRAM
actorRule : (ACTOR_KEYWORD? (IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_COLONS | identificator) actorAlias? stereotypes* HEX_COLOR?);

useCaseRule: (USECASE_KEYWORD? (IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_BRACKETS | identificator) useCaseAlias? stereotypes* HEX_COLOR?);

useCasePackageRule: (PACKAGE_KEYWORD | RECTANGLE_KEYWORD) identificator basicAlias? stereotypes* HEX_COLOR? useCasePackageBody?;

useCaseDiagramRelationRule:
        ((IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_BRACKETS) | (IDENTIFICATOR_CLOSED_IN_COLONS | IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) | identificator)
        QUOTED_IDENTIFICATOR? (ARROW_DOTTED | ARROW_DASHED)  QUOTED_IDENTIFICATOR?
        ((IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_BRACKETS) | (IDENTIFICATOR_CLOSED_IN_COLONS | IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) | identificator)
        (COLON (identificator_with_space_and_special_chars | QUOTED_IDENTIFICATOR))?
        ;

useCasePackageBody:
    OPENED_CURLY_BRACKET NEWLINE+
    (useCasePackageBodyLine NEWLINE+)*
    CLOSED_CURLY_BRACKET;

useCasePackageBodyLine: topToBottomDirection |
                        leftToRightDirection |
                        skinparamRule |
                        actorRule |
                        useCaseRule |
                        noteRule |
                        useCasePackageRule |
                        useCaseDiagramRelationRule
                        ;

//SEQUENCE DIAGRAM
participantRule: (PARTICIPANT_TYPE_KEYWORD | ENTITY_KEYWORD | ACTOR_KEYWORD) identificator basicAlias? stereotypes* order? HEX_COLOR? ;
messageRule: (identificator basicAlias?)? (ARROW_SEQUENCE_DIAGRAM | ARROW_DASHED)  (identificator basicAlias?)?
    ((COLON (identificator_with_space_and_special_chars | QUOTED_IDENTIFICATOR)) |
    ((IDENTIFICATOR_CLOSED_IN_COLONS | IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES) identificator_with_space_and_special_chars))? ;
groupRule: ((GROUP_TYPE_KEYWORD) identificator_with_space_and_special_chars? groupCase) elseGroupBranch* END_KEYWORD identificator_with_space?;
createRule : CREATE_KEYWORD (PARTICIPANT_TYPE_KEYWORD | ENTITY_KEYWORD | ACTOR_KEYWORD)? identificator basicAlias? stereotypes* order? HEX_COLOR? NEWLINE messageRule;
returnRule : RETURN_KEYWORD identificator_with_space;
activateRule : ACTIVATE_KEYWORD identificator_with_space;
deactivateRule : DEACTIVATE_KEYWORD identificator_with_space;
sequenceNoteRule : NOTE_KEYWORD OVER_KEYWORD identificator_with_space;

refRule : REF_KEYWORD OVER_KEYWORD identificator ((NEWLINE ((identificator_of_note)? NEWLINE*) END_KEYWORD REF_KEYWORD?) | (COLON (identificator_with_space_and_special_chars | QUOTED_IDENTIFICATOR)));

groupCase:
        NEWLINE+ ((
            messageRule |
            participantRule |
            groupRule |
            refRule |
            createRule |
            returnRule |
            activateRule |
            deactivateRule |
            sequenceNoteRule
        )NEWLINE+)*;

elseGroupBranch: ELSE_KEYWORD identificator_with_space_and_special_chars? groupCase;
order: ORDER_KEYWORD identificator;

//OTHER PARTS
topToBottomDirection: TOP_TO_BOTTOM_DIRECTION_KEYWORD;
leftToRightDirection: LEFT_TO_RIGHT_DIRECTION_KEYWORD;

basicAlias: AS_KEYWORD identificator;
useCaseAlias: AS_KEYWORD
    (keyword | IDENTIFICATOR_CLOSED_IN_BRACKETS | IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES | IDENTIFICATOR | QUOTED_IDENTIFICATOR_WITH_NEW_LINES | QUOTED_IDENTIFICATOR);
actorAlias: AS_KEYWORD
    (keyword | IDENTIFICATOR_CLOSED_IN_COLONS | IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES | IDENTIFICATOR | QUOTED_IDENTIFICATOR_WITH_NEW_LINES | QUOTED_IDENTIFICATOR);

generics: (OPENED_ANGLE_BRACKET dentificator_with_space_and_special_chars_without_angle_brackets CLOSED_ANGLE_BRACKET);
stereotypes: (DOUBLE_OPENED_ANGLE_BRACKET dentificator_with_space_and_special_chars_without_angle_brackets DOUBLE_CLOSED_ANGLE_BRACKET);

identificator : (keyword | IDENTIFICATOR | QUOTED_IDENTIFICATOR);
identificator_with_space: (keyword | IDENTIFICATOR | SPACE | COLON)+;
dentificator_with_space_and_special_chars_without_angle_brackets:
    (keyword | IDENTIFICATOR | SPACE | COLON | VISIBILITY_SIGN | QUOTE | DOT | '[' | ']')+;
identificator_with_space_and_special_chars:
    (keyword | IDENTIFICATOR | SPACE | COLON | CLOSED_ANGLE_BRACKET | OPENED_ANGLE_BRACKET |
     DOUBLE_CLOSED_ANGLE_BRACKET | DOUBLE_OPENED_ANGLE_BRACKET | IDENTIFICATOR_CLOSED_IN_BRACKETS | IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES |
      IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_COLONS | VISIBILITY_SIGN | QUOTE | DOT | '[' | ']')+;

identificator_of_note: (((keyword | IDENTIFICATOR | SPACE | COLON | CLOSED_ANGLE_BRACKET | OPENED_ANGLE_BRACKET |
     DOUBLE_CLOSED_ANGLE_BRACKET | DOUBLE_OPENED_ANGLE_BRACKET | IDENTIFICATOR_CLOSED_IN_BRACKETS | IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES |
      IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES | IDENTIFICATOR_CLOSED_IN_COLONS | VISIBILITY_SIGN | QUOTE | DOT | '[' | ']')+)~(END_KEYWORD | NOTE_KEYWORD))*;

keyword: ABSTRACT_KEYWORD
       | CLASS_TYPE_KEYWORD
       | EXTENDS_KEYWORD
       | IMPLEMENTS_KEYWORD
       | CLASS_ANOTHER_TYPES_KEYWORD
       | FIELD_KEYWORD
       | METHOD_KEYWORD
       | STATIC_KEYWORD
       | CLASSIFIER_KEYWORD
       | PACKAGE_KEYWORD
       | NAMESPACE_KEYWORD
       | NOTE_KEYWORD
       | DIRECTION_KEYWORD
       | OF_KEYWORD
       | END_KEYWORD
       | AS_KEYWORD
       | ACTOR_KEYWORD
       | USECASE_KEYWORD
       | SYSTEM_KEYWORD
       | RECTANGLE_KEYWORD
       | SKINPARAM_KEYWORD
       | LEFT_TO_RIGHT_DIRECTION_KEYWORD
       | TOP_TO_BOTTOM_DIRECTION_KEYWORD
       | PARTICIPANT_TYPE_KEYWORD
       | ORDER_KEYWORD
       | ELSE_KEYWORD
       | GROUP_TYPE_KEYWORD
       | CREATE_KEYWORD
       | RETURN_KEYWORD
       | REF_KEYWORD
       | OVER_KEYWORD
       | ACTIVATE_KEYWORD
       | DEACTIVATE_KEYWORD
       | AUTONUMBER_KEYWORD
       | HIDE_KEYWORD
       | SHOW_KEYWORD;

//TOKENS
COMMENT : (
            '\'' .*? ('\n' | '\r\n' | '\r') |
            '/\'' .*?  '\'/'
          ) -> channel(HIDDEN);


ARROW_DOTTED: (OPENED_ANGLE_BRACKET '|'? | '*' | 'o' | '#' | 'x' | CLOSED_CURLY_BRACKET | '+' | '^')?
       ARROW_MIDDLE_DOTTED ( '|'? CLOSED_ANGLE_BRACKET | '*' | 'o' | '#' | 'x' | OPENED_CURLY_BRACKET | '+' | '^')?;

ARROW_DASHED: (OPENED_ANGLE_BRACKET '|'? | '*' | 'o' | '#' | 'x' | CLOSED_CURLY_BRACKET | '+' | '^')?
       ARROW_MIDDLE_DASHED ( '|'? CLOSED_ANGLE_BRACKET | '*' | 'o' | '#' | 'x' | OPENED_CURLY_BRACKET | '+' | '^')?;

ARROW_SEQUENCE_DIAGRAM:  ('[' | '?')?  ('o' | 'x' | ('x' OPENED_ANGLE_BRACKET) | ('o'? (OPENED_ANGLE_BRACKET | (OPENED_ANGLE_BRACKET OPENED_ANGLE_BRACKET) | '\\\\' | '//' | '\\' | '/')))?
       ARROW_MIDDLE_DASHED ('o' | 'x' | (CLOSED_ANGLE_BRACKET 'x' )  | ((CLOSED_ANGLE_BRACKET | (CLOSED_ANGLE_BRACKET CLOSED_ANGLE_BRACKET) | '\\\\' | '//' | '\\' | '/') 'o'?) )? (']' | '?')?;

//class diagram
CLASS_TYPE_KEYWORD : 'class';
CLASS_ANOTHER_TYPES_KEYWORD:
         'enum' |
         'interface' |
         'annotation' |
         'diamond' |
         'circle' |
         '<>' |
         '()'
         ;

ABSTRACT_KEYWORD : 'abstract';
EXTENDS_KEYWORD : 'extends';
IMPLEMENTS_KEYWORD : 'implements';
FIELD_KEYWORD : 'field';
METHOD_KEYWORD : 'method';
STATIC_KEYWORD : 'static';
CLASSIFIER_KEYWORD : 'classifier';
PACKAGE_KEYWORD : 'package';
NAMESPACE_KEYWORD : 'namespace';
RECTANGLE_KEYWORD: 'rectangle';

//use case diagram
USECASE_KEYWORD : 'usecase';
ACTOR_KEYWORD : 'actor';
SYSTEM_KEYWORD : 'system';
ENTITY_KEYWORD: 'entity';

TOP_TO_BOTTOM_DIRECTION_KEYWORD: 'top to bottom direction';
LEFT_TO_RIGHT_DIRECTION_KEYWORD: 'left to right direction';

//sequence diagram
ORDER_KEYWORD : 'order';
PARTICIPANT_TYPE_KEYWORD :
        'participant' |
        'boundary' |
        'control' |
        'database' |
        'collections' |
        'queue';
ELSE_KEYWORD: 'else';
GROUP_TYPE_KEYWORD:
        'alt' |
        'opt' |
        'loop' |
        'par' |
        'break' |
        'critical' |
        'group';
CREATE_KEYWORD: 'create';
RETURN_KEYWORD: 'return';
OVER_KEYWORD: 'over';
REF_KEYWORD: 'ref';
ACTIVATE_KEYWORD: 'activate';
DEACTIVATE_KEYWORD: 'deactivate';
HIDE_KEYWORD: 'hide';
SHOW_KEYWORD: 'show';

//common
NOTE_KEYWORD : 'note';
SKINPARAM_KEYWORD : 'skinparam';
AUTONUMBER_KEYWORD: 'autonumber';

DIRECTION_KEYWORD:
    'left' |
    'right' |
    'bottom' |
    'top';
OF_KEYWORD : 'of';
END_KEYWORD : 'end';
AS_KEYWORD : 'as';


ARROW_MIDDLE_DOTTED: DOT+;
ARROW_MIDDLE_DASHED: DASH+;

OPENED_ANGLE_BRACKET : '<';
CLOSED_ANGLE_BRACKET : '>';
DOUBLE_OPENED_ANGLE_BRACKET : '<<';
DOUBLE_CLOSED_ANGLE_BRACKET : '>>';
OPENED_CURLY_BRACKET : '{';
CLOSED_CURLY_BRACKET : '}';

COLON : ':';
DASH : '-';
VISIBILITY_SIGN : [+#~] | DASH;



HEX_COLOR : '#' (HEX_NUMBER | DIGIT) (HEX_NUMBER | DIGIT) (HEX_NUMBER | DIGIT) (HEX_NUMBER | DIGIT) (HEX_NUMBER | DIGIT) (HEX_NUMBER | DIGIT);

NEWLINE : '\r' '\n' | '\n' | '\r';


IDENTIFICATOR_CLOSED_IN_COLONS: COLON (((DIGIT | NONDIGIT | DOT | SPECIAL_CHARACTER | VISIBILITY_SIGN)+)) COLON;
IDENTIFICATOR_CLOSED_IN_COLONS_WITH_SPACES: COLON (((DIGIT | SPACE | NONDIGIT | DOT | SPECIAL_CHARACTER | VISIBILITY_SIGN)+)) COLON;

IDENTIFICATOR_CLOSED_IN_BRACKETS: '(' (((DIGIT | NONDIGIT | DOT | SPECIAL_CHARACTER_WITHOUT_CLASSIC_BRACKETS | VISIBILITY_SIGN)+)) ')';
IDENTIFICATOR_CLOSED_IN_BRACKETS_WITH_SPACES: '(' (((DIGIT | SPACE | NONDIGIT | DOT | SPECIAL_CHARACTER_WITHOUT_CLASSIC_BRACKETS | VISIBILITY_SIGN)+)) ')';

QUOTED_IDENTIFICATOR: QUOTE (DIGIT | NONDIGIT | SPACE | DOT | SPECIAL_CHARACTER | VISIBILITY_SIGN | COLON | '[' | ']')+ QUOTE;
QUOTED_IDENTIFICATOR_WITH_NEW_LINES: QUOTE (DIGIT | NONDIGIT | SPACE | DOT | SPECIAL_CHARACTER | VISIBILITY_SIGN | NEWLINE)+ QUOTE;


//also with "": e.g. "First Class"
IDENTIFICATOR : (DIGIT | NONDIGIT | DOT | SPECIAL_CHARACTER | VISIBILITY_SIGN)+;


//=========================================================
// Fragments
//=========================================================
fragment HEX_NUMBER : [a-fA-F];
fragment NONDIGIT : [a-zA-Z];
fragment SPACE : ' ';
fragment DIGIT :  [0-9];
fragment UNSIGNED_INTEGER : DIGIT+;
fragment QUOTE:  '"';
fragment DOT:  '.';
fragment SPECIAL_CHARACTER_WITHOUT_CLASSIC_BRACKETS: [?!,;'£@$%^&_*=] | '/' | '\\' | '[' | ']';

fragment SPECIAL_CHARACTER: [?!,;'£@$%^&()_*=] | '/' | '\\';


WS  :   [ \t]+ -> skip ; // toss out whitespace
