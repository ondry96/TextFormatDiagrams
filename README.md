# UML Diagrams Conversion between Visual Paradigm and Text-Based Formats

## Master's thesis

Brno University of Technology

Faculty of Information Technology

Author: Bc. Lukáš Ondrák

Supervisor: Rychlý Marek, RNDr., Ph.D.

Brno 2021

## Abstract of thesis

This master's thesis deals with possibilities of modifying and extending functionality of Visual Paradigm with plug-ins
as well as the study of tools for creating UML diagrams. The primary goal of this thesis is to describe graphical tools
and simultaneously, the less known variants of creating UML diagrams. Those are tools that process text UML formats.
Special attention is given to text tool PlantUML and to the graphical tool Visual Paradigm. Furthermore, the thesis
deals with use of the Visual Paradigm open interface for programmers to create plug-ins. The main output is an
implemented plug-in that allows you to convert UML diagrams between Visual Paradigm and text format PlantUML, which was
published as open source software. Grammar for the PlantUML language have also been created for this plug-in.

## Supported diagrams

- Class Diagram
- Usecase Diagram
- Sequence (Interaction) Diagram

## Running this plugin

You have to download published zip file TextFormatDiagrams.zip and install it in Visual Paradigm. Plug-ins in Visual
Paradigm are installed in the Help tab with the Install Plugin button, in which it can be installed directly from a ZIP
file. There is also a second installation option - the content of the ZIP file (TextFormatDiagrams folder) can be
manually copied to the C:\Users\<username>\AppData\Roaming\VisualParadigm\plugins folder. After restart of Visual
Paradigm, the plugin will be ready to use.

The implemented functionality is mainly called using 2 buttons in menu: 'Project/Export' a 'Project/Import' called 'Text
format...' (
import is from PlantUML to VP, export from VP to PlantUML). There are also buttons for exporting active diagram and exporting diagram on right click in more places.

## License

This program is free open source software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation.
