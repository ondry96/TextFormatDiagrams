package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.AttributeElement;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.OperationElement;

import java.io.IOException;

import static org.junit.Assert.*;

public class PlantUMLClassBodyTest {

    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();

    @Test
    public void testEmptyClassBody() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/empty_body.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(4, classDiagramVisitor.getElements().size());

        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(0)).getAttributes().isEmpty());
        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(0)).getOperations().isEmpty());

        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(1)).getAttributes().isEmpty());
        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(1)).getOperations().isEmpty());

        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(2)).getAttributes().isEmpty());
        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(2)).getOperations().isEmpty());

        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(3)).getAttributes().isEmpty());
        assertTrue(((ClassElement) classDiagramVisitor.getElements().get(3)).getOperations().isEmpty());
    }

    @Test
    public void testNotStaticNotAbstract() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/fields_and_methods.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        ClassElement class04 = (ClassElement) classDiagramVisitor.getElements().get(0);
        AttributeElement field = class04.getAttributes().get(0);
        OperationElement method = class04.getOperations().get(0);
        assertFalse(field.isAbstract());
        assertFalse(field.isStatic());
        assertFalse(method.isAbstract());
        assertFalse(method.isStatic());
    }

    @Test
    public void testFieldsAndMethods() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/fields_and_methods.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(8, classDiagramVisitor.getElements().size());

        ClassElement class04 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals(1, class04.getAttributes().size());
        assertEquals(1, class04.getOperations().size());
        assertEquals("field", class04.getAttributes().get(0).getName());
        assertEquals("method()", class04.getOperations().get(0).getName());

        ClassElement class05 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals(2, class05.getAttributes().size());
        assertEquals(0, class05.getOperations().size());
        assertEquals("field", class05.getAttributes().get(0).getName());
        assertEquals("fieldWithBrackets()", class05.getAttributes().get(1).getName());

        ClassElement class06 = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals(2, class06.getAttributes().size());
        assertEquals(3, class06.getOperations().size());
        assertEquals("field", class06.getAttributes().get(0).getName());
        assertEquals("field2", class06.getAttributes().get(1).getName());

        assertEquals("method()", class06.getOperations().get(0).getName());
        assertEquals("method2()", class06.getOperations().get(1).getName());
        assertEquals("field()3", class06.getOperations().get(2).getName());


        ClassElement class07 = (ClassElement) classDiagramVisitor.getElements().get(3);
        assertEquals(1, class07.getAttributes().size());
        assertEquals(1, class07.getOperations().size());
        assertEquals("Field With Space", class07.getAttributes().get(0).getName());
        assertEquals("Method With Space()", class07.getOperations().get(0).getName());

        ClassElement class08 = (ClassElement) classDiagramVisitor.getElements().get(4);
        assertEquals(1, class08.getAttributes().size());
        assertEquals(1, class08.getOperations().size());
        assertEquals("field", class08.getAttributes().get(0).getName());
        assertEquals("field", class08.getOperations().get(0).getName());

        ClassElement class09 = (ClassElement) classDiagramVisitor.getElements().get(5);
        assertEquals(1, class09.getAttributes().size());
        assertEquals(1, class09.getOperations().size());
        assertEquals("String data", class09.getAttributes().get(0).getName());
        assertEquals("void method()", class09.getOperations().get(0).getName());

        ClassElement class10 = (ClassElement) classDiagramVisitor.getElements().get(6);
        assertEquals(2, class10.getAttributes().size());
        assertEquals("data : Integer", class10.getAttributes().get(0).getName());
        assertEquals("time : Date", class10.getAttributes().get(1).getName());

        ClassElement timeUnit = (ClassElement) classDiagramVisitor.getElements().get(7);
        assertTrue(timeUnit.isEnum());
        assertEquals(3, timeUnit.getAttributes().size());
        assertEquals("DAYS", timeUnit.getAttributes().get(0).getName());
        assertEquals("HOURS", timeUnit.getAttributes().get(1).getName());
        assertEquals("MINUTES", timeUnit.getAttributes().get(2).getName());

    }

    @Test
    public void testAccessModifiers() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/signs_static_abstract.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(4, classDiagramVisitor.getElements().size());

        ClassElement class11 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals(5, class11.getAttributes().size());
        assertEquals("data : Integer", class11.getAttributes().get(0).getName());
        assertEquals("public", class11.getAttributes().get(0).getVisibility());

        assertEquals("time : Date", class11.getAttributes().get(1).getName());
        assertEquals("private", class11.getAttributes().get(1).getVisibility());

        assertEquals("name", class11.getAttributes().get(2).getName());
        assertEquals("protected", class11.getAttributes().get(2).getVisibility());

        assertEquals("surname", class11.getAttributes().get(3).getName());
        assertEquals("package private", class11.getAttributes().get(3).getVisibility());

        assertEquals("noModifier", class11.getAttributes().get(4).getName());
        assertNull(class11.getAttributes().get(4).getVisibility());
    }

    @Test
    public void testStaticAndAbstract() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/signs_static_abstract.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement class12 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("data : Integer", class12.getAttributes().get(0).getName());
        assertFalse(class12.getAttributes().get(0).isAbstract());
        assertTrue(class12.getAttributes().get(0).isStatic());

        assertEquals("data2 : Integer", class12.getAttributes().get(1).getName());
        assertTrue(class12.getAttributes().get(1).isAbstract());
        assertTrue(class12.getAttributes().get(1).isStatic());

        assertEquals("time : Date", class12.getAttributes().get(2).getName());
        assertTrue(class12.getAttributes().get(2).isAbstract());
        assertTrue(class12.getAttributes().get(2).isStatic());

        assertEquals("time2 : Date", class12.getAttributes().get(3).getName());
        assertTrue(class12.getAttributes().get(3).isAbstract());
        assertTrue(class12.getAttributes().get(3).isStatic());

        assertEquals("time3 : Date", class12.getAttributes().get(4).getName());
        assertTrue(class12.getAttributes().get(4).isAbstract());
        assertTrue(class12.getAttributes().get(4).isStatic());

        assertEquals("method()", class12.getOperations().get(0).getName());
        assertTrue(class12.getOperations().get(0).isAbstract());
        assertTrue(class12.getOperations().get(0).isStatic());

    }

    @Test
    public void testStaticAndAbstractWithSigns() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/signs_static_abstract.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement class13 = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals("data : Integer", class13.getAttributes().get(0).getName());
        assertTrue(class13.getAttributes().get(0).isAbstract());
        assertTrue(class13.getAttributes().get(0).isStatic());
        assertEquals("package private", class13.getAttributes().get(0).getVisibility());

        assertEquals("time : Date", class13.getAttributes().get(1).getName());
        assertTrue(class13.getAttributes().get(1).isAbstract());
        assertTrue(class13.getAttributes().get(1).isStatic());
        assertEquals("package private", class13.getAttributes().get(1).getVisibility());

        assertEquals("time2 : Date", class13.getAttributes().get(2).getName());
        assertTrue(class13.getAttributes().get(2).isAbstract());
        assertTrue(class13.getAttributes().get(2).isStatic());
        assertEquals("package private", class13.getAttributes().get(2).getVisibility());

        assertEquals("time3 : Date", class13.getAttributes().get(3).getName());
        assertTrue(class13.getAttributes().get(3).isAbstract());
        assertTrue(class13.getAttributes().get(3).isStatic());
        assertEquals("package private", class13.getAttributes().get(3).getVisibility());

        assertEquals("method()", class13.getOperations().get(0).getName());
        assertTrue(class13.getOperations().get(0).isAbstract());
        assertTrue(class13.getOperations().get(0).isStatic());
        assertEquals("package private", class13.getOperations().get(0).getVisibility());

    }

    //classifier is same as static
    @Test
    public void testClassifierKeyword() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/signs_static_abstract.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement class14 = (ClassElement) classDiagramVisitor.getElements().get(3);
        assertEquals("data : Integer", class14.getAttributes().get(0).getName());
        assertFalse(class14.getAttributes().get(0).isAbstract());
        assertTrue(class14.getAttributes().get(0).isStatic());
        assertEquals("package private", class14.getAttributes().get(0).getVisibility());

        assertEquals("data : Integer", class14.getAttributes().get(1).getName());
        assertFalse(class14.getAttributes().get(1).isAbstract());
        assertTrue(class14.getAttributes().get(1).isStatic());
        assertEquals("public", class14.getAttributes().get(1).getVisibility());

        assertEquals("getTime()", class14.getOperations().get(0).getName());
        assertFalse(class14.getOperations().get(0).isAbstract());
        assertTrue(class14.getOperations().get(0).isStatic());
        assertNull(class14.getOperations().get(0).getVisibility());

    }

    @Test
    public void staticWithClassifier() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/static_with_classifier.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement class14 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("method()", class14.getOperations().get(0).getName());
        assertFalse(class14.getOperations().get(0).isAbstract());
        assertTrue(class14.getOperations().get(0).isStatic());
    }

    @Test
    public void multipleStaticAndAbstract() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/multiple_static_abstract.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement class00 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("method()", class00.getOperations().get(0).getName());
        assertFalse(class00.getOperations().get(0).isAbstract());
        assertTrue(class00.getOperations().get(0).isStatic());

        ClassElement class01 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("method()", class01.getOperations().get(0).getName());
        assertTrue(class01.getOperations().get(0).isAbstract());
        assertFalse(class01.getOperations().get(0).isStatic());
    }

    @Test
    public void harderSingsStaticAbstract() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classbody/harder_sings_static_abstract.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement classA = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("int counter", classA.getAttributes().get(0).getName());
        assertFalse(classA.getAttributes().get(0).isAbstract());
        assertTrue(classA.getAttributes().get(0).isStatic());

        assertEquals("void start(int timeout)", classA.getOperations().get(0).getName());
        assertTrue(classA.getOperations().get(0).isAbstract());
        assertFalse(classA.getOperations().get(0).isStatic());
        assertEquals("public", classA.getOperations().get(0).getVisibility());

        ClassElement classB = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("+-field", classB.getOperations().get(0).getName());
        assertFalse(classB.getOperations().get(0).isAbstract());
        assertTrue(classB.getOperations().get(0).isStatic());
        assertEquals("public", classB.getOperations().get(0).getVisibility());

        assertEquals("method name", classB.getOperations().get(1).getName());
        assertTrue(classB.getOperations().get(1).isAbstract());
        assertTrue(classB.getOperations().get(1).isStatic());
        assertEquals("public", classB.getOperations().get(1).getVisibility());

        assertEquals("method name()", classB.getOperations().get(2).getName());
        assertTrue(classB.getOperations().get(2).isAbstract());
        assertTrue(classB.getOperations().get(2).isStatic());
        assertEquals("public", classB.getOperations().get(2).getVisibility());

        assertEquals("method -long name(String a) : String", classB.getOperations().get(3).getName());
        assertTrue(classB.getOperations().get(3).isAbstract());
        assertTrue(classB.getOperations().get(3).isStatic());
        assertNull(classB.getOperations().get(3).getVisibility());
    }

}
