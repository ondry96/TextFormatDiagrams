package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationClass;
import pluginmain.elements.relationships.ImportAssociationElement;

import java.io.IOException;

import static org.junit.Assert.*;

public class PlantUMLRelations {


    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();

    @Test
    public void testBasicRelationsWithMadeUpClasses() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/relations/basic_relations.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassElement abstractCollectionClass = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("AbstractCollection", abstractCollectionClass.getName());

        ClassElement listClass = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("List", listClass.getName());

        ClassElement collectionClass = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals("Collection", collectionClass.getName());

        ImportAssociationElement extension1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);
        assertNull(extension1.getName());
        assertEquals("Collection", extension1.getFromName());
        assertEquals("AbstractCollection", extension1.getToName());
        assertEquals("<|", extension1.getFromAggregationKind());
        assertNull(extension1.getToAggregationKind());
        assertFalse(extension1.isMiddleDotted());

        ImportAssociationElement extension2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);
        assertEquals("extends", extension2.getName());
        assertEquals("Collection", extension2.getFromName());
        assertEquals("List", extension2.getToName());
        assertEquals("<|", extension2.getFromAggregationKind());
        assertNull(extension2.getToAggregationKind());
        assertTrue(extension2.isMiddleDotted());
    }

    @Test
    public void testRelationsCreateClass() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/relations/relations_without_classes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(10, classDiagramVisitor.getElements().size());

        ClassElement class01 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("Class01", class01.getName());

        ClassElement class02 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("Class02", class02.getName());

        ImportAssociationElement firstRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        assertEquals("contains something", firstRelation.getName());
        assertEquals("Class01", firstRelation.getFromName());
        assertEquals("Class02", firstRelation.getToName());
        assertEquals("*", firstRelation.getFromAggregationKind());
        assertNull(firstRelation.getToAggregationKind());
        assertEquals("0..1", firstRelation.getFromMultiplicity());
        assertEquals("1..n", firstRelation.getToMultiplicity());
        assertFalse(firstRelation.isMiddleDotted());

        ImportAssociationElement secondRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);
        assertEquals("contains something", secondRelation.getName());
        assertEquals("Class01", secondRelation.getFromName());
        assertEquals("Class02", secondRelation.getToName());
        assertEquals("*", secondRelation.getFromAggregationKind());
        assertEquals("o", secondRelation.getToAggregationKind());
        assertEquals("one to many", secondRelation.getFromMultiplicity());
        assertNull(secondRelation.getToMultiplicity());
        assertTrue(secondRelation.isMiddleDotted());


        ClassElement class03 = (ClassElement) classDiagramVisitor.getElements().get(4);
        assertEquals("Class03", class03.getName());

        ClassElement class04 = (ClassElement) classDiagramVisitor.getElements().get(5);
        assertEquals("Class04", class04.getName());

        ImportAssociationElement thirdRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(6);
        assertEquals("aggregation", thirdRelation.getName());
        assertEquals("Class03", thirdRelation.getFromName());
        assertEquals("Class04", thirdRelation.getToName());
        assertEquals("o", thirdRelation.getFromAggregationKind());
        assertNull(thirdRelation.getToAggregationKind());
        assertNull(thirdRelation.getFromMultiplicity());
        assertNull(thirdRelation.getToMultiplicity());
        assertFalse(thirdRelation.isMiddleDotted());

        ClassElement class05 = (ClassElement) classDiagramVisitor.getElements().get(7);
        assertEquals("Class05", class05.getName());

        ClassElement class06 = (ClassElement) classDiagramVisitor.getElements().get(8);
        assertEquals("Class06", class06.getName());

        ImportAssociationElement fourthRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(9);
        assertNull(fourthRelation.getName());
        assertEquals("Class05", fourthRelation.getFromName());
        assertEquals("Class06", fourthRelation.getToName());
        assertNull(fourthRelation.getFromAggregationKind());
        assertEquals(">", fourthRelation.getToAggregationKind());
        assertNull(fourthRelation.getFromMultiplicity());
        assertEquals("1", fourthRelation.getToMultiplicity());
        assertTrue(fourthRelation.isMiddleDotted());
    }

    @Test
    public void testRelationPackageWithClass() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/relations/relations_with_packages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("A", packageA.getName());


        ClassElement classB = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("B", classB.getName());

        ImportAssociationElement relation1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        assertNull(relation1.getName());
        assertEquals("A", relation1.getFromName());
        assertEquals("B", relation1.getToName());
        assertNull(relation1.getFromAggregationKind());
        assertNull(relation1.getToAggregationKind());
        assertTrue(relation1.isMiddleDotted());

        ImportAssociationElement relation2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);
        assertNull(relation2.getName());
        assertEquals("A", relation2.getFromName());
        assertEquals("B", relation2.getToName());
        assertEquals("<", relation2.getFromAggregationKind());
        assertNull(relation2.getToAggregationKind());
        assertTrue(relation2.isMiddleDotted());

        ImportAssociationElement relation3 = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);
        assertEquals("name", relation3.getName());
        assertEquals("B", relation3.getFromName());
        assertEquals("B", relation3.getToName());
        assertEquals("x", relation3.getFromAggregationKind());
        assertEquals(">", relation3.getToAggregationKind());
        assertFalse(relation3.isMiddleDotted());

    }


    @Test
    public void testRelationsWithNotes() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/relations/relations_with_notes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(4, classDiagramVisitor.getElements().size());

        NoteElement noteD = (NoteElement) classDiagramVisitor.getElements().get(0);
        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        ImportAssociationElement relation1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        ImportAssociationElement relation2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);

        assertNull(relation1.getName());
        assertEquals("D", relation1.getFromName());
        assertEquals("C", relation1.getToName());
        assertNull(relation1.getFromMultiplicity());
        assertNull(relation1.getToMultiplicity());
        assertNull(relation1.getFromAggregationKind());
        assertNull(relation1.getToAggregationKind());
        assertFalse(relation1.isMiddleDotted());

        assertNull(relation2.getName());
        assertEquals("D", relation2.getFromName());
        assertEquals("C", relation2.getToName());
        assertNull(relation2.getFromMultiplicity());
        assertNull(relation2.getToMultiplicity());
        assertNull(relation2.getFromAggregationKind());
        assertNull(relation2.getToAggregationKind());
        assertTrue(relation2.isMiddleDotted());
    }

    @Test
    public void testAssociationClass() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/relations/association_class.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassElement classEnrollment = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals("Enrollment", classEnrollment.getName());

        ImportAssociationElement relationAssociation = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);

        ImportAssociationClass assocClass1 = (ImportAssociationClass) classDiagramVisitor.getElements().get(4);

        assertNull(relationAssociation.getName());
        assertEquals("Student", relationAssociation.getFromName());
        assertEquals("Course", relationAssociation.getToName());
        assertNull(relationAssociation.getFromMultiplicity());
        assertNull(relationAssociation.getToMultiplicity());
        assertNull(relationAssociation.getFromAggregationKind());
        assertNull(relationAssociation.getToAggregationKind());
        assertFalse(relationAssociation.isMiddleDotted());

        assertNull(assocClass1.getName());
        assertEquals("Student", assocClass1.getAssociation().getFromName());
        assertEquals("Course", assocClass1.getAssociation().getToName());
        assertEquals("Enrollment", assocClass1.getToClassName());
        assertNull(assocClass1.getAssociation().getFromMultiplicity());
        assertNull(assocClass1.getAssociation().getToMultiplicity());
        assertNull(assocClass1.getAssociation().getFromAggregationKind());
        assertNull(assocClass1.getAssociation().getToAggregationKind());
        assertFalse(assocClass1.getAssociation().isMiddleDotted());


    }

}
