package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;

import java.io.IOException;

import static junit.framework.Assert.assertNull;
import static org.junit.Assert.assertEquals;

public class PlantUMLOverridingTest {

    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();

    @Test
    public void testOverrideClasses() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_classes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals(3, firstPackage.getClasses().size());
        assertEquals("Class01", (firstPackage).getClasses().get(0).getName());
        assertEquals("Class02", (firstPackage).getClasses().get(1).getName());
        assertEquals("Class03", (firstPackage).getClasses().get(2).getName());

        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals(0, secondPackage.getClasses().size());

        ClassPackageElement thirdPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(2);
        assertEquals(1, thirdPackage.getClasses().size());
        assertEquals("Class01 ", (thirdPackage).getClasses().get(0).getName());

    }

    @Test
    public void testOverrideHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_classes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals(3, firstPackage.getClasses().size());
        ClassElement class01 = (firstPackage).getClasses().get(0);
        assertEquals(2, class01.getStereotypes().size());
        assertEquals("overriddenStereotype", class01.getStereotypes().get(0));
        assertEquals("interface", class01.getStereotypes().get(1));


        ClassElement class02 = (firstPackage).getClasses().get(1);
        assertEquals(2, class01.getStereotypes().size());
        assertEquals("abstract", class02.getStereotypes().get(0));
        assertEquals("enum", class02.getStereotypes().get(1));

    }

    @Test
    public void testOverrideResources() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_class_resources.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(2, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals(1, firstPackage.getClasses().size());
        ClassElement class01 = (firstPackage).getClasses().get(0);
        assertEquals(3, class01.getAttributes().size());
        assertEquals("attribute1", class01.getAttributes().get(0).getName());
        assertEquals("attribute2", class01.getAttributes().get(1).getName());
        assertEquals("attribute3", class01.getAttributes().get(2).getName());

        assertEquals("operation1()", class01.getOperations().get(0).getName());
        assertEquals("operation1()", class01.getOperations().get(1).getName());
        assertEquals("operation2()", class01.getOperations().get(2).getName());

        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals(0, secondPackage.getClasses().size());

    }


    @Test
    public void testOverrideResourcesInSubpackages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_classes_in_subpackages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, classDiagramVisitor.getElements().size());

        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals(1, packageA.getClasses().size());
        assertEquals(2, packageA.getPackages().size());

        ClassElement class02 = (packageA).getClasses().get(0);
        assertEquals("Class02", class02.getName());
        assertEquals(0, class02.getAttributes().size());
        assertEquals(2, class02.getOperations().size());
        assertEquals("operation1()", class02.getOperations().get(0).getName());
        assertEquals("operation2()", class02.getOperations().get(1).getName());


        ClassPackageElement packageB = (ClassPackageElement) packageA.getPackages().get(0);
        ClassElement class01 = (packageB).getClasses().get(0);
        assertEquals("Class01", class01.getName());
        assertEquals(1, class01.getAttributes().size());
        assertEquals("attribute1", class01.getAttributes().get(0).getName());
        assertEquals(2, class01.getOperations().size());
        assertEquals("operation1()", class01.getOperations().get(0).getName());
        assertEquals("operation2()", class01.getOperations().get(1).getName());

    }


    @Test
    public void testOverridePackages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_package.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals(2, firstPackage.getClasses().size());
        ClassElement class01 = (firstPackage).getClasses().get(0);
        assertEquals(0, class01.getAttributes().size());
        assertEquals(1, class01.getOperations().size());
        assertEquals("operation1()", class01.getOperations().get(0).getName());

        ClassElement class02 = (firstPackage).getClasses().get(1);
        assertEquals(2, class02.getAttributes().size());
        assertEquals("attribute1", class02.getAttributes().get(0).getName());
        assertEquals("attribute2", class02.getAttributes().get(1).getName());
    }


    @Test
    public void testOverridePackageAndClassWithAlias() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_package_and_class_with_alias.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        ClassElement class01OutsidePackage = (ClassElement) classDiagramVisitor.getElements().get(2);

        assertEquals(1, secondPackage.getClasses().size());
        ClassElement class01InPackage = (secondPackage).getClasses().get(0);
        assertEquals(1, class01InPackage.getAttributes().size());
        assertEquals("attribute1", class01InPackage.getAttributes().get(0).getName());

        assertEquals(1, class01OutsidePackage.getAttributes().size());
        assertEquals("attribute2", class01OutsidePackage.getAttributes().get(0).getName());
    }

    @Test
    public void testOverridePackageHeader2() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_package_header2.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("A", firstPackage.getName());
        assertNull(firstPackage.getAlias());
        assertNull(firstPackage.getBackgroundColor());
        assertEquals(1, firstPackage.getStereotypes().size());
        assertEquals("stereotype2", firstPackage.getStereotypes().get(0));
    }

    @Test
    public void testOverridePackageHeader3() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_package_header3.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("A", firstPackage.getName());
        assertNull(firstPackage.getAlias());
        assertEquals("#bbbbbb", firstPackage.getBackgroundColor());
        assertEquals(1, firstPackage.getStereotypes().size());
        assertEquals("stereotype", firstPackage.getStereotypes().get(0));
    }

    @Test
    public void testOverridePackageHeader4() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/overriding/override_package_header4.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("ABC", firstPackage.getName());
        assertEquals("Alias", firstPackage.getAlias());
        assertNull(firstPackage.getBackgroundColor());
        assertEquals(1, firstPackage.getStereotypes().size());
        assertEquals("stereotype2", firstPackage.getStereotypes().get(0));
    }

}
