package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.relationships.ImportAssociationElement;

import java.io.IOException;

import static org.junit.Assert.*;

public class PlantUMLPackageTest {

    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();

    @Test
    public void testEmptyPackages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/empty_packages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(4, classDiagramVisitor.getElements().size());

        assertTrue(((ClassPackageElement) classDiagramVisitor.getElements().get(0)).getClasses().isEmpty());
        assertTrue(((ClassPackageElement) classDiagramVisitor.getElements().get(1)).getClasses().isEmpty());
        assertTrue(((ClassPackageElement) classDiagramVisitor.getElements().get(2)).getClasses().isEmpty());
        assertTrue(((ClassPackageElement) classDiagramVisitor.getElements().get(3)).getClasses().isEmpty());
    }

    @Test
    public void testPackageHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/package_header.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("AAlias", firstPackage.getAlias());
        assertEquals(2, firstPackage.getStereotypes().size());
        assertEquals("Rectangle", firstPackage.getStereotypes().get(0));
        assertEquals("Model", firstPackage.getStereotypes().get(1));
        assertNull(firstPackage.getBackgroundColor());


        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals("BAlias", secondPackage.getAlias());
        assertEquals(0, secondPackage.getStereotypes().size());
        assertEquals("#aaaaaa", secondPackage.getBackgroundColor());


        ClassPackageElement thirdPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(2);
        assertNull(thirdPackage.getAlias());
        assertEquals(3, thirdPackage.getStereotypes().size());
        assertEquals("Stereotype1", thirdPackage.getStereotypes().get(0));
        assertEquals("Stereotype2", thirdPackage.getStereotypes().get(1));
        assertEquals("Stereotype with more words", thirdPackage.getStereotypes().get(2));
        assertEquals("#aaaaaa", thirdPackage.getBackgroundColor());

    }


    @Test
    public void testPackagesWithClasses() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/packages_with_classes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals(3, firstPackage.getClasses().size());
        assertEquals("Class01A", (firstPackage).getClasses().get(0).getName());
        assertEquals("Class02A", (firstPackage).getClasses().get(1).getName());
        assertEquals("Class03A", (firstPackage).getClasses().get(2).getName());

        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals(3, secondPackage.getClasses().size());
        assertEquals("Class01B", secondPackage.getClasses().get(0).getName());
        assertEquals("abstract", secondPackage.getClasses().get(0).getStereotypes().get(0));

        assertEquals("Class02B", secondPackage.getClasses().get(1).getName());
        assertEquals("abstract", secondPackage.getClasses().get(1).getStereotypes().get(0));

        assertEquals("Class03B", secondPackage.getClasses().get(2).getName());
        assertEquals("? extends Class01B", secondPackage.getClasses().get(2).getTemplateParameters().get(0));
        assertEquals("#aaaaaa", secondPackage.getClasses().get(2).getBackgroundColor());

        ClassPackageElement thirdPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(2);
        assertEquals("Class01C", (thirdPackage).getClasses().get(0).getName());
        assertEquals("Class02C", (thirdPackage).getClasses().get(1).getName());

        assertEquals("method()", (thirdPackage).getClasses().get(1).getOperations().get(0).getName());
        assertTrue((thirdPackage).getClasses().get(1).getOperations().get(0).isAbstract());

        assertEquals("field", (thirdPackage).getClasses().get(1).getAttributes().get(0).getName());
        assertTrue((thirdPackage).getClasses().get(1).getAttributes().get(0).isStatic());


    }

    @Test
    public void testPackagesWithSubpackages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/packages_with_subpackages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("1", firstPackage.getName());
        assertEquals(2, firstPackage.getPackages().size());
        assertEquals("subpackage1.1", (firstPackage).getPackages().get(0).getName());
        assertEquals("subpackage1.2", (firstPackage).getPackages().get(1).getName());

        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals("2", secondPackage.getName());
        assertEquals(1, secondPackage.getPackages().size());
        ClassPackageElement subpackageOfSecondPackage = (ClassPackageElement) secondPackage.getPackages().get(0);
        assertEquals("subpackage2.1", subpackageOfSecondPackage.getName());
        assertEquals(1, subpackageOfSecondPackage.getPackages().size());
        assertEquals("subpackage2.1.1", subpackageOfSecondPackage.getPackages().get(0).getName());

        ClassPackageElement thirdPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(2);
        assertEquals("3", thirdPackage.getName());

        ClassPackageElement subpackage12 = (ClassPackageElement) (firstPackage).getPackages().get(1);
        ClassElement classA = subpackage12.getClasses().get(0);
        assertEquals("A", classA.getName());

        ClassPackageElement subsubpackage211 = (ClassPackageElement) (secondPackage).getPackages().get(0).getPackages().get(0);
        ClassElement classB = subsubpackage211.getClasses().get(0);
        assertEquals("B", classB.getName());
        assertNull(classB.getBackgroundColor());
        assertEquals(1, classB.getTemplateParameters().size());
        assertEquals("bbb", classB.getTemplateParameters().get(0));
        assertEquals(1, classB.getStereotypes().size());
        assertEquals("bbb", classB.getStereotypes().get(0));

        ClassElement classC = thirdPackage.getClasses().get(0);
        assertEquals("C", classC.getName());

    }


    @Test
    public void testClassesInSubpackages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/classes_in_subpackages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(2, classDiagramVisitor.getElements().size());

        ClassPackageElement package1 = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        ClassPackageElement subpackage11 = (ClassPackageElement) package1.getPackages().get(0);
        ClassPackageElement subsubpackage111 = (ClassPackageElement) subpackage11.getPackages().get(0);

        assertEquals("1", package1.getName());
        assertEquals(1, package1.getPackages().size());

        assertEquals("1.1", subpackage11.getName());
        assertEquals(1, subpackage11.getPackages().size());

        assertEquals("1.1.1", subsubpackage111.getName());
        assertEquals(0, subsubpackage111.getPackages().size());

        assertEquals(1, package1.getClasses().size());
        assertEquals("A", package1.getClasses().get(0).getName());

        assertEquals(1, subpackage11.getClasses().size());
        assertEquals("B", subpackage11.getClasses().get(0).getName());

        assertEquals(2, subsubpackage111.getClasses().size());
        assertEquals("C", subsubpackage111.getClasses().get(0).getName());
        assertEquals("D", subsubpackage111.getClasses().get(1).getName());

        assertEquals("E", classDiagramVisitor.getElements().get(1).getName());
    }

    @Test
    public void testPackageAlias() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/package_alias.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("A", firstPackage.getName());
        assertEquals("AAlias", firstPackage.getAlias());
        assertEquals("#aaaaaa", firstPackage.getBackgroundColor());

    }

    @Test
    public void testClassAndPackageWithSameName() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/class_and_package_with_same_names.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(6, classDiagramVisitor.getElements().size());

        ClassPackageElement firstPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("A", firstPackage.getName());

        ClassElement firstClass = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("A", firstClass.getName());


        ClassPackageElement secondPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(2);
        assertEquals("aaa", secondPackage.getName());
        assertEquals("aaa", secondPackage.getFullName());
        assertEquals("B", secondPackage.getAlias());

        ClassElement secondClass = (ClassElement) classDiagramVisitor.getElements().get(3);
        assertEquals("aaa", secondClass.getName());
        assertEquals("aaa", secondClass.getFullName());
        assertEquals("B", secondClass.getAlias());


        ClassPackageElement thirdPackage = (ClassPackageElement) classDiagramVisitor.getElements().get(4);
        assertEquals("ddd", thirdPackage.getName());
        assertEquals("ddd", thirdPackage.getFullName());
        assertEquals("D", thirdPackage.getAlias());

        ClassElement thirdClass = (ClassElement) classDiagramVisitor.getElements().get(5);
        assertEquals("classname", thirdClass.getName());
        assertEquals("classname", thirdClass.getFullName());
        assertEquals("D", thirdClass.getAlias());
    }

    @Test
    public void testRemoveClassWhenPackageHasSameName() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/packages/remove_class_when_same_name.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassElement classG = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("G", classG.getName());


        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals("A", packageA.getName());
        assertEquals("AAlias", packageA.getAlias());

        ImportAssociationElement relation = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        assertEquals("test", relation.getName());
        assertEquals("G", relation.getFromName());
        assertEquals("F", relation.getToName());

        ClassPackageElement packageF = (ClassPackageElement) packageA.getPackages().get(0);
        assertEquals("F", packageF.getName());
        assertEquals("F", packageF.getAlias());

        ClassElement classF = packageF.getClasses().get(0);
        assertEquals("F", classF.getName());
        assertEquals("F", classF.getAlias());
    }


}
