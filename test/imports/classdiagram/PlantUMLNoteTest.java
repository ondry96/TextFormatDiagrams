package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.common.NoteElement;
import pluginmain.elements.relationships.ImportAssociationElement;

import java.io.IOException;

import static org.junit.Assert.*;

public class PlantUMLNoteTest {

    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();


    @Test
    public void testBasicNotes() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/basic_notes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        NoteElement noteA = (NoteElement) classDiagramVisitor.getElements().get(0);
        NoteElement noteB = (NoteElement) classDiagramVisitor.getElements().get(1);
        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(2);

        assertEquals("simple note", noteA.getDescription());
        assertEquals("A", noteA.getAlias());
        assertEquals("#aaaaaa", noteA.getBackgroundColor());


        assertEquals("simple note2", noteB.getDescription());
        assertEquals("B", noteB.getAlias());
        assertNull(noteB.getBackgroundColor());

        assertEquals(1, packageA.getNotes().size());
        NoteElement noteInPackage = packageA.getNotes().get(0);
        assertEquals("Note", noteInPackage.getDescription());
        assertEquals("C", noteInPackage.getAlias());
        assertNull(noteInPackage.getBackgroundColor());

    }


    @Test
    public void testComplicatedNotes() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/complicated_notes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(10, classDiagramVisitor.getElements().size());

        ClassElement class01 = (ClassElement) classDiagramVisitor.getElements().get(0);
        NoteElement noteLeft = (NoteElement) classDiagramVisitor.getElements().get(1);
        ImportAssociationElement firstRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);

        NoteElement noteRight = (NoteElement) classDiagramVisitor.getElements().get(3);
        ImportAssociationElement secondRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);

        NoteElement noteTop = (NoteElement) classDiagramVisitor.getElements().get(5);
        ImportAssociationElement thirdRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(6);

        ClassElement class02 = (ClassElement) classDiagramVisitor.getElements().get(7);
        NoteElement noteBottom = (NoteElement) classDiagramVisitor.getElements().get(8);
        ImportAssociationElement fourthRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(9);

        assertEquals("note to class Class01", noteLeft.getDescription());
        assertEquals("N0", noteLeft.getAlias());
        assertEquals("#aaaaaa", noteLeft.getBackgroundColor());


        assertEquals("note to class Class01", noteRight.getDescription());
        assertEquals("N1", noteRight.getAlias());
        assertEquals("#aaaaaa", noteRight.getBackgroundColor());


        assertEquals("\"note to class Class01\"", noteTop.getDescription());
        assertEquals("N2", noteTop.getAlias());
        assertNull(noteTop.getBackgroundColor());


        assertEquals("\"note to class Class02\"", noteBottom.getDescription());
        assertEquals("N3", noteBottom.getAlias());
        assertNull(noteBottom.getBackgroundColor());


        //test of correct class creation
        assertEquals("Class01", class01.getName());
        assertEquals("Class02", class02.getName());


        assertTrue(firstRelation.isMiddleDotted());
        assertEquals("N0", firstRelation.getFromName());
        assertEquals("Class01", firstRelation.getToName());


        assertTrue(secondRelation.isMiddleDotted());
        assertEquals("N1", secondRelation.getFromName());
        assertEquals("Class01", secondRelation.getToName());


        assertTrue(thirdRelation.isMiddleDotted());
        assertEquals("N2", thirdRelation.getFromName());
        assertEquals("Class01", thirdRelation.getToName());


        assertTrue(fourthRelation.isMiddleDotted());
        assertEquals("N3", fourthRelation.getFromName());
        assertEquals("Class02", fourthRelation.getToName());
    }

    @Test
    public void testComplicatedNotes2() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/complicated_notes2.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(8, classDiagramVisitor.getElements().size());

        ClassElement class00 = (ClassElement) classDiagramVisitor.getElements().get(0);
        ClassPackageElement packageC = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(2);

        ImportAssociationElement firstRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);
        ImportAssociationElement secondRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);
        ClassElement class01 = (ClassElement) classDiagramVisitor.getElements().get(5);
        ImportAssociationElement thirdRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(6);
        NoteElement noteOutsideOfPackages = (NoteElement) classDiagramVisitor.getElements().get(7);

        assertEquals(2, packageA.getNotes().size());

        NoteElement firstNoteInPackageA = packageA.getNotes().get(0);
        NoteElement secondNoteInPackageA = packageA.getNotes().get(1);

        assertEquals("note to class \\n Class00", firstNoteInPackageA.getDescription());
        assertEquals("N0", firstNoteInPackageA.getAlias());
        assertNull(firstNoteInPackageA.getBackgroundColor());

        assertEquals("note to note N0", secondNoteInPackageA.getDescription());
        assertEquals("N1", secondNoteInPackageA.getAlias());
        assertEquals("#aaaaaa", secondNoteInPackageA.getBackgroundColor());

        //outside package note
        assertEquals("note to class Class01", noteOutsideOfPackages.getDescription());
        assertEquals("N2", noteOutsideOfPackages.getAlias());
        assertNull(noteOutsideOfPackages.getBackgroundColor());


        assertTrue(firstRelation.isMiddleDotted());
        assertEquals("N0", firstRelation.getFromName());
        assertEquals("Class00", firstRelation.getToName());


        assertTrue(secondRelation.isMiddleDotted());
        assertEquals("N1", secondRelation.getFromName());
        assertEquals("N0", secondRelation.getToName());


        assertTrue(thirdRelation.isMiddleDotted());
        assertEquals("N2", thirdRelation.getFromName());
        assertEquals("Class01", thirdRelation.getToName());

    }

    @Test
    public void testComplicatedNotes3() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/complicated_notes3.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());
        ClassElement classA = (ClassElement) classDiagramVisitor.getElements().get(0);
        ImportAssociationElement firstRelation = (ImportAssociationElement) classDiagramVisitor.getElements().get(1);
        NoteElement noteLeft = (NoteElement) classDiagramVisitor.getElements().get(2);

        assertEquals("On last defined class", noteLeft.getDescription());
        assertEquals("N0", noteLeft.getAlias());
        assertNull(noteLeft.getBackgroundColor());

        assertTrue(firstRelation.isMiddleDotted());
        assertEquals("N0", firstRelation.getFromName());
        assertEquals(classA.getName(), firstRelation.getToName());
    }

    @Test
    public void testCouldNotAttachNote() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/note_cannot_be_attached_exception.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(1, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals("No elements to attach note to, when processing note N0", classDiagramVisitor.getErrorsInGrammar().get(0));
    }

    @Test
    public void testNoteAlreadyInStructure() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/note_already_in_structure.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(1, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals("Note with alias A already exists", classDiagramVisitor.getErrorsInGrammar().get(0));
    }

    @Test
    public void testNoteWithAliasAsPackageName() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/note_with_package_and_class_same_names.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        ClassElement classN0 = (ClassElement) classDiagramVisitor.getElements().get(1);
        ImportAssociationElement relation = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        ClassPackageElement packageC = (ClassPackageElement) classDiagramVisitor.getElements().get(3);

        assertEquals(1, packageA.getNotes().size());

        NoteElement noteInPackage = packageA.getNotes().get(0);
        assertEquals("aaa", noteInPackage.getDescription());
        assertEquals("A", noteInPackage.getAlias());
        assertEquals("#aaaccc", noteInPackage.getBackgroundColor());


        assertTrue(relation.isMiddleDotted());
        assertEquals("A", relation.getFromName());
        assertEquals(classN0.getName(), relation.getToName());

        assertEquals("C", packageC.getClasses().get(0).getName());
        assertEquals("C.C", packageC.getClasses().get(0).getFullName());

    }

    @Test
    public void testAllNoteTypes() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/all_notes_type.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(12, classDiagramVisitor.getElements().size());

        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        NoteElement noteInPackage = packageA.getNotes().get(0);
        assertEquals("aaa \r\n bbb", noteInPackage.getDescription());
        assertEquals("A", noteInPackage.getAlias());
        assertNull(noteInPackage.getBackgroundColor());
        assertEquals(1, packageA.getNotes().size());


        NoteElement outsideNote1 = (NoteElement) classDiagramVisitor.getElements().get(1);
        assertEquals("aaa bbb", outsideNote1.getDescription());
        assertEquals("N0", outsideNote1.getAlias());
        assertEquals("#CCCCCC", outsideNote1.getBackgroundColor());

        NoteElement outsideNote2 = (NoteElement) classDiagramVisitor.getElements().get(2);
        assertEquals("On last defined class end note", outsideNote2.getDescription());
        assertEquals("N1", outsideNote2.getAlias());
        assertEquals("#CCCCCC", outsideNote2.getBackgroundColor());

        NoteElement outsideNote3 = (NoteElement) classDiagramVisitor.getElements().get(3);
        assertEquals("", outsideNote3.getDescription());
        assertEquals("N2", outsideNote3.getAlias());
        assertEquals("#CCCCCC", outsideNote3.getBackgroundColor());

        ImportAssociationElement relation1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);
        assertTrue(relation1.isMiddleDotted());
        assertEquals("N3", relation1.getFromName());
        assertEquals("N2", relation1.getToName());

        NoteElement outsideNote4 = (NoteElement) classDiagramVisitor.getElements().get(5);
        assertEquals("aaa bbb", outsideNote4.getDescription());
        assertEquals("N3", outsideNote4.getAlias());
        assertEquals("#CCCCCC", outsideNote4.getBackgroundColor());

        ImportAssociationElement relation2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(6);
        assertTrue(relation2.isMiddleDotted());
        assertEquals("N4", relation2.getFromName());
        assertEquals("N3", relation2.getToName());

        NoteElement outsideNote5 = (NoteElement) classDiagramVisitor.getElements().get(7);
        assertEquals("aaa \r\n bbb", outsideNote5.getDescription());
        assertEquals("N4", outsideNote5.getAlias());
        assertNull(outsideNote5.getBackgroundColor());

        NoteElement outsideNote6 = (NoteElement) classDiagramVisitor.getElements().get(8);
        assertEquals("aaa \r\n bbb", outsideNote6.getDescription());
        assertEquals("N5", outsideNote6.getAlias());
        assertEquals("#CCCCCC", outsideNote6.getBackgroundColor());

        ImportAssociationElement relation3 = (ImportAssociationElement) classDiagramVisitor.getElements().get(9);
        assertTrue(relation3.isMiddleDotted());
        assertEquals("N5", relation3.getFromName());
        assertEquals("N0", relation3.getToName());

        NoteElement outsideNote7 = (NoteElement) classDiagramVisitor.getElements().get(10);
        assertEquals("aaa bbb ccc as A", outsideNote7.getDescription());
        assertEquals("N6", outsideNote7.getAlias());
        assertNull(outsideNote7.getBackgroundColor());

        ImportAssociationElement relation4 = (ImportAssociationElement) classDiagramVisitor.getElements().get(11);
        assertTrue(relation4.isMiddleDotted());
        assertEquals("N6", relation4.getFromName());
        assertEquals("N0", relation4.getToName());
    }

    @Test
    public void testDirectionalNotes() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/directional_notes.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        NoteElement outsideNote1 = (NoteElement) classDiagramVisitor.getElements().get(0);
        assertEquals("aaa bbb", outsideNote1.getDescription());
        assertEquals("N0", outsideNote1.getAlias());
        assertEquals("#CCCCCC", outsideNote1.getBackgroundColor());

        NoteElement outsideNote2 = (NoteElement) classDiagramVisitor.getElements().get(1);
        assertEquals("aaa \r\n bbb", outsideNote2.getDescription());
        assertEquals("N1", outsideNote2.getAlias());
        assertEquals("#CCCCCC", outsideNote2.getBackgroundColor());

        ImportAssociationElement relation1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        assertTrue(relation1.isMiddleDotted());
        assertEquals("N1", relation1.getFromName());
        assertEquals("N0", relation1.getToName());

        NoteElement outsideNote3 = (NoteElement) classDiagramVisitor.getElements().get(3);
        assertEquals("aaa bbb ccc as A", outsideNote3.getDescription());
        assertEquals("N2", outsideNote3.getAlias());
        assertNull(outsideNote3.getBackgroundColor());

        ImportAssociationElement relation2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);
        assertTrue(relation2.isMiddleDotted());
        assertEquals("N2", relation2.getFromName());
        assertEquals("N0", relation2.getToName());

    }

    @Test
    public void testRemoveNote() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/remove_first_note_with_package.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassPackageElement packageN0 = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        assertEquals("N0", packageN0.getName());
        assertNull(packageN0.getAlias());

        NoteElement outsideNote1 = (NoteElement) classDiagramVisitor.getElements().get(1);
        assertEquals("aaa \r\n bbb", outsideNote1.getDescription());
        assertEquals("N1", outsideNote1.getAlias());
        assertEquals("#CCCCCC", outsideNote1.getBackgroundColor());

        ImportAssociationElement relation1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        assertTrue(relation1.isMiddleDotted());
        assertEquals("N1", relation1.getFromName());
        assertEquals("N0", relation1.getToName());

        NoteElement outsideNote2 = (NoteElement) classDiagramVisitor.getElements().get(3);
        assertEquals("aaa bbb ccc as A", outsideNote2.getDescription());
        assertEquals("N2", outsideNote2.getAlias());
        assertNull(outsideNote2.getBackgroundColor());

        ImportAssociationElement relation2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(4);
        assertTrue(relation2.isMiddleDotted());
        assertEquals("N2", relation2.getFromName());
        assertEquals("N0", relation2.getToName());

    }


    @Test
    public void testAnotherTypesOfNotes() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/notes/note_with_body_of_curly_brackets.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassElement createdClass = (ClassElement) classDiagramVisitor.getElements().get(0);
        NoteElement noteA = (NoteElement) classDiagramVisitor.getElements().get(1);
        ImportAssociationElement relation1 = (ImportAssociationElement) classDiagramVisitor.getElements().get(2);
        ImportAssociationElement relation2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);
        NoteElement noteB = (NoteElement) classDiagramVisitor.getElements().get(4);

        assertEquals("end note test " + System.lineSeparator() +
                " second line", noteA.getDescription());
        assertEquals("N0", noteA.getAlias());

        assertEquals("end note test " + System.lineSeparator() +
                " second line", noteB.getDescription());
        assertEquals("N1", noteB.getAlias());
        assertNull(noteB.getBackgroundColor());

        assertEquals("A", createdClass.getName());

        assertTrue(relation1.isMiddleDotted());
        assertEquals("N0", relation1.getFromName());
        assertEquals("A", relation1.getToName());

        assertTrue(relation2.isMiddleDotted());
        assertEquals("N1", relation2.getFromName());
        assertEquals("N0", relation2.getToName());

    }

}
