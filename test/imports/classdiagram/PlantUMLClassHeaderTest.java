package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.relationships.ImportAssociationElement;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.*;

public class PlantUMLClassHeaderTest {

    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();

    @Test
    public void testDifferentClassNames() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_header_1.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(4, classDiagramVisitor.getElements().size());
        assertEquals("Class01", classDiagramVisitor.getElements().get(0).getName());
        assertEquals("C1", ((ClassElement) classDiagramVisitor.getElements().get(0)).getAlias());

        assertEquals("Class02", classDiagramVisitor.getElements().get(1).getName());
        assertNull(((ClassElement) classDiagramVisitor.getElements().get(1)).getAlias());

        assertEquals("Class With Spaces", classDiagramVisitor.getElements().get(2).getName());
        assertEquals("Class 04", classDiagramVisitor.getElements().get(3).getName());
    }

    @Test
    public void testAnotherTypeOfClass() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_different_types.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(5, classDiagramVisitor.getElements().size());
        ClassElement class1 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("Class", class1.getName());
        assertEquals(0, class1.getStereotypes().size());

        ClassElement interface1 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("Interface", interface1.getName());
        assertEquals("interface", interface1.getStereotypes().get(0));

        ClassElement entity1 = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals("Entity", entity1.getName());
        assertEquals("entity", entity1.getStereotypes().get(0));

        ClassElement enum1 = (ClassElement) classDiagramVisitor.getElements().get(3);
        assertEquals("Enum", enum1.getName());
        assertEquals("enum", enum1.getStereotypes().get(0));

        ClassElement annotation1 = (ClassElement) classDiagramVisitor.getElements().get(4);
        assertEquals("Annotation", annotation1.getName());
        assertEquals("annotation", annotation1.getStereotypes().get(0));
    }

    @Test
    public void testWeirdNames() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_header_2.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(10, classDiagramVisitor.getElements().size());

        ClassElement interface1 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("Interface", interface1.getName());

        ClassElement interface2 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals("interface", interface2.getName());

        ClassElement interface3 = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals("implements", interface3.getName());

        ClassElement class1 = (ClassElement) classDiagramVisitor.getElements().get(3);
        assertEquals("specialchars?!,;'@$%^&()~/\\_*+=#-", class1.getName());

        ClassElement class2 = (ClassElement) classDiagramVisitor.getElements().get(4);
        assertEquals("specialchars?!,;'@$%^&()~/\\_*+=#-.className", class2.getName());
        assertEquals("specialchars?!,;'@$%^&()~/\\_*+=#-.className", class2.getFullName());

        ImportAssociationElement extension = (ImportAssociationElement) classDiagramVisitor.getElements().get(5);
        assertNull(extension.getName());
        assertEquals("<|", extension.getFromAggregationKind());
        assertEquals("extends", extension.getFromName());
        assertEquals("extends", extension.getToName());
        assertNull(extension.getToAggregationKind());
        assertFalse(extension.isMiddleDotted());


        //generated of extends
        ClassElement extendsClass = (ClassElement) classDiagramVisitor.getElements().get(6);
        assertEquals("extends", extendsClass.getName());
        assertEquals("abstract", extendsClass.getStereotypes().get(0));

        ClassElement hahaClass = (ClassElement) classDiagramVisitor.getElements().get(7);
        assertEquals("Haha", hahaClass.getName());

        ImportAssociationElement extension2 = (ImportAssociationElement) classDiagramVisitor.getElements().get(8);
        assertNull(extension2.getName());
        assertEquals("<|", extension2.getFromAggregationKind());
        assertEquals("Haha", extension2.getFromName());
        assertEquals("class", extension2.getToName());
        assertNull(extension2.getToAggregationKind());
        assertFalse(extension2.isMiddleDotted());

        ClassElement class4 = (ClassElement) classDiagramVisitor.getElements().get(9);
        assertEquals("class", class4.getName());

    }

    @Test
    public void testFullHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_header_3.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, classDiagramVisitor.getElements().size());

        ClassElement classA = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("A", classA.getName());

        ClassElement classB = (ClassElement) classDiagramVisitor.getElements().get(2);
        assertEquals("B", classB.getName());

        ClassElement enumClass = (ClassElement) classDiagramVisitor.getElements().get(4);
        assertEquals("Class with all", enumClass.getName());
        assertEquals("#abcA90", enumClass.getBackgroundColor());

        assertEquals("? extends Enum", enumClass.getTemplateParameters().get(0));

        ArrayList<String> stereotypes = enumClass.getStereotypes();
        assertEquals("stereotype1 +11", stereotypes.get(0));
        assertEquals("stereotype2", stereotypes.get(1));
        assertEquals("enum", stereotypes.get(2));


        ImportAssociationElement classExtension = (ImportAssociationElement) classDiagramVisitor.getElements().get(1);
        assertNull(classExtension.getName());
        assertEquals("<|", classExtension.getFromAggregationKind());
        assertEquals("A", classExtension.getFromName());
        assertEquals("Class with all", classExtension.getToName());
        assertNull(classExtension.getToAggregationKind());
        assertFalse(classExtension.isMiddleDotted());

        ImportAssociationElement interfaceImplementation = (ImportAssociationElement) classDiagramVisitor.getElements().get(3);
        assertNull(interfaceImplementation.getName());
        assertEquals("|>", interfaceImplementation.getToAggregationKind());
        assertEquals("Class with all", interfaceImplementation.getFromName());
        assertEquals("B", interfaceImplementation.getToName());
        assertNull(interfaceImplementation.getFromAggregationKind());
        assertTrue(interfaceImplementation.isMiddleDotted());
    }

    @Test
    public void testAliasesInClasses() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_aliases.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        assertEquals("class name 1", classDiagramVisitor.getElements().get(0).getName());
        assertEquals("A", ((ClassElement) classDiagramVisitor.getElements().get(0)).getAlias());
        assertEquals("#aaaaaa", classDiagramVisitor.getElements().get(0).getBackgroundColor());

        assertEquals("class name 3", classDiagramVisitor.getElements().get(1).getName());
        assertEquals("B", ((ClassElement) classDiagramVisitor.getElements().get(1)).getAlias());
        assertEquals(1, ((ClassElement) classDiagramVisitor.getElements().get(1)).getStereotypes().size());
        assertEquals("private2", ((ClassElement) classDiagramVisitor.getElements().get(1)).getStereotypes().get(0));
        assertEquals("aaa", ((ClassElement) classDiagramVisitor.getElements().get(1)).getTemplateParameters().get(0));

        assertEquals("class name 5", classDiagramVisitor.getElements().get(2).getName());
        assertEquals("C", ((ClassElement) classDiagramVisitor.getElements().get(2)).getAlias());
    }


    @Test
    public void testCreatePackageFromClassDefinition() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_create_package.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(6, classDiagramVisitor.getElements().size());

        ClassElement classA = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals("classA", classA.getName());

        ClassPackageElement pApackage = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals("pA", pApackage.getName());
        assertEquals(1, pApackage.getClasses().size());
        assertEquals("classA", pApackage.getClasses().get(0).getName());
        assertEquals("pA.classA", pApackage.getClasses().get(0).getFullName());

        ClassPackageElement pApCpackage = (ClassPackageElement) classDiagramVisitor.getElements().get(2);
        assertEquals("pA.pC", pApCpackage.getName());
        assertEquals(2, pApCpackage.getClasses().size());
        assertEquals("classA", pApCpackage.getClasses().get(0).getName());
        assertEquals("pA.pC.classA", pApCpackage.getClasses().get(0).getFullName());
        assertEquals("classB", pApCpackage.getClasses().get(1).getName());
        assertEquals("pA.pC.classB", pApCpackage.getClasses().get(1).getFullName());

        ClassPackageElement pBpackage = (ClassPackageElement) classDiagramVisitor.getElements().get(3);
        assertEquals("pB", pBpackage.getName());
        assertEquals(1, pBpackage.getClasses().size());
        assertEquals("classA", pBpackage.getClasses().get(0).getName());
        assertEquals("pB.classA", pBpackage.getClasses().get(0).getFullName());

        ClassPackageElement pCpackage = (ClassPackageElement) classDiagramVisitor.getElements().get(4);
        assertEquals("pCpackage", pCpackage.getName());
        assertEquals(1, pCpackage.getClasses().size());
        assertEquals("classA", pCpackage.getClasses().get(0).getName());
        assertEquals("pC.classA", pCpackage.getClasses().get(0).getFullName());

        ClassElement classWithDotOnStart = (ClassElement) classDiagramVisitor.getElements().get(5);
        assertEquals("classWithDotOnStart", classWithDotOnStart.getName());

    }

    @Test
    public void testIncorrectClassAlias() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/classheader/class_alias_incorrectly_created_exception.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(1, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals("Class: Name has name and alias with quotes", classDiagramVisitor.getErrorsInGrammar().get(0));

    }
}
