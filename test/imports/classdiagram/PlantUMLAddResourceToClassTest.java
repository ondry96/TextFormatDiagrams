package imports.classdiagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLClassDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.classdiagram.AttributeElement;
import pluginmain.elements.classdiagram.ClassElement;
import pluginmain.elements.classdiagram.ClassPackageElement;
import pluginmain.elements.classdiagram.OperationElement;

import java.io.IOException;

import static org.junit.Assert.*;

public class PlantUMLAddResourceToClassTest {

    PlantUMLClassDiagramVisitor classDiagramVisitor = new PlantUMLClassDiagramVisitor();

    @Test
    public void testAddResourceToClass() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/addresourcetoclass/add_resource_to_class_1.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        ClassElement class00 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals(2, class00.getAttributes().size());

        AttributeElement firstAttribute = class00.getAttributes().get(0);
        AttributeElement secondAttribute = class00.getAttributes().get(1);
        OperationElement firstMethod = class00.getOperations().get(0);

        assertEquals("field1", firstAttribute.getName());
        assertEquals("field2()", secondAttribute.getName());
        assertEquals("field3()", firstMethod.getName());

        assertFalse(secondAttribute.isAbstract());
        assertTrue(secondAttribute.isStatic());
        assertTrue(firstMethod.isAbstract());
        assertFalse(firstMethod.isStatic());
    }

    @Test
    public void testAddResourceToClassWithSign() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/addresourcetoclass/add_resource_to_class_1.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();

        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassElement class01 = (ClassElement) classDiagramVisitor.getElements().get(1);
        assertEquals(4, class01.getOperations().size());

        OperationElement firstMethod = class01.getOperations().get(0);
        OperationElement secondMethod = class01.getOperations().get(1);
        OperationElement thirdMethod = class01.getOperations().get(2);
        OperationElement fourthMethod = class01.getOperations().get(3);

        assertEquals("operation1()", firstMethod.getName());
        assertEquals("operation2()", secondMethod.getName());
        assertEquals("operation3()", thirdMethod.getName());
        assertEquals("operation4()", fourthMethod.getName());

        assertEquals("public", secondMethod.getVisibility());

        assertEquals("public", thirdMethod.getVisibility());

        assertEquals("private", fourthMethod.getVisibility());
        assertTrue(fourthMethod.isStatic());
    }

    @Test
    public void testAddResourceToNotExistingClass1() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/addresourcetoclass/add_resource_to_not_existing_class.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(2, classDiagramVisitor.getElements().size());

        ClassElement class00 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals(1, class00.getAttributes().size());
        assertEquals(1, class00.getOperations().size());

        ClassPackageElement packageElement = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals(2, packageElement.getClasses().size());
        assertEquals("Class01", packageElement.getClasses().get(0).getName());
        assertEquals("Object", packageElement.getClasses().get(0).getAttributes().get(0).getName());
        assertEquals("Class02", packageElement.getClasses().get(1).getName());
        assertEquals("Object", packageElement.getClasses().get(1).getAttributes().get(0).getName());
        assertTrue(packageElement.getClasses().get(1).getAttributes().get(0).isStatic());
    }


    @Test
    public void testAddResourceToNotExistingClass2() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/addresourcetoclass/add_resource_to_not_existing_class_2.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);

        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, classDiagramVisitor.getElements().size());

        ClassElement class00 = (ClassElement) classDiagramVisitor.getElements().get(0);
        assertEquals(1, class00.getAttributes().size());
        assertEquals("Object", class00.getAttributes().get(0).getName());
        assertEquals(1, class00.getOperations().size());
        assertEquals("method()", class00.getOperations().get(0).getName());

        ClassPackageElement packageA = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        assertEquals(0, packageA.getClasses().size());
        assertEquals(1, packageA.getPackages().size());


        ClassPackageElement subpackageB = (ClassPackageElement) packageA.getPackages().get(0);
        assertEquals(1, subpackageB.getClasses().size());
        ClassElement class01 = subpackageB.getClasses().get(0);

        assertEquals("Class01", class01.getName());
        assertEquals(2, class01.getAttributes().size());
        assertEquals(2, class01.getOperations().size());

        assertEquals("Object", class01.getAttributes().get(0).getName());
        assertEquals("Object2", class01.getAttributes().get(1).getName());
        assertEquals("method()", class01.getOperations().get(0).getName());
        assertEquals("method2()", class01.getOperations().get(1).getName());

    }

    @Test
    public void testAddResourceToClassInSubPackage() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/addresourcetoclass/add_resource_to_class_in_subpackage.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassPackageElement classPackageElement = (ClassPackageElement) classDiagramVisitor.getElements().get(0);
        ClassPackageElement subPackageElement = (ClassPackageElement) classPackageElement.getPackages().get(0);
        ClassElement class00 = subPackageElement.getClasses().get(0);
        assertEquals(2, class00.getAttributes().size());

        AttributeElement firstAttribute = class00.getAttributes().get(0);
        AttributeElement secondAttribute = class00.getAttributes().get(1);
        OperationElement firstMethod = class00.getOperations().get(0);

        assertEquals("field1", firstAttribute.getName());
        assertEquals("field2()", secondAttribute.getName());
        assertEquals("field3()", firstMethod.getName());

        assertFalse(secondAttribute.isAbstract());
        assertTrue(secondAttribute.isStatic());
        assertTrue(firstMethod.isAbstract());
        assertFalse(firstMethod.isStatic());
    }

    @Test
    public void testAddResourceToClassInSubPackage2() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/classdiagram/addresourcetoclass/add_resource_to_class_in_subpackage.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        classDiagramVisitor.visit(parseTree);


        assertEquals(0, classDiagramVisitor.getErrorsInGrammar().size());

        ClassPackageElement classPackageElement = (ClassPackageElement) classDiagramVisitor.getElements().get(1);
        ClassPackageElement subPackageElement = (ClassPackageElement) classPackageElement.getPackages().get(1);
        ClassPackageElement subsubPackageElement = (ClassPackageElement) subPackageElement.getPackages().get(0);


        ClassElement class01 = subsubPackageElement.getClasses().get(0);
        assertEquals(4, class01.getOperations().size());

        OperationElement firstMethod = class01.getOperations().get(0);
        OperationElement secondMethod = class01.getOperations().get(1);
        OperationElement thirdMethod = class01.getOperations().get(2);
        OperationElement fourthMethod = class01.getOperations().get(3);

        assertEquals("operation1()", firstMethod.getName());
        assertEquals("operation2()", secondMethod.getName());
        assertEquals("operation3()", thirdMethod.getName());
        assertEquals("operation4()", fourthMethod.getName());

        assertEquals("public", secondMethod.getVisibility());

        assertEquals("public", thirdMethod.getVisibility());

        assertEquals("private", fourthMethod.getVisibility());
        assertTrue(fourthMethod.isStatic());
    }

}
