package imports.usecasediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLUseCaseDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.usecasediagram.UseCasePackageElement;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlantUMLOverridingTest {

    PlantUMLUseCaseDiagramVisitor useCaseDiagramVisitor = new PlantUMLUseCaseDiagramVisitor();

    @Test
    public void testOverrideElements() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/usecasediagram/overriding/override_usecase_elements.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        useCaseDiagramVisitor.visit(parseTree);

        assertEquals(0, useCaseDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(3, useCaseDiagramVisitor.getElements().size());

        UseCasePackageElement firstPackage = (UseCasePackageElement) useCaseDiagramVisitor.getElements().get(0);
        assertEquals("A", firstPackage.getName());
        assertEquals("Alias", firstPackage.getAlias());
        assertEquals(1, firstPackage.getActors().size());
        assertEquals(1, firstPackage.getUseCases().size());
        assertEquals("Renamed Admin", (firstPackage).getActors().get(0).getName());
        assertEquals("Admin", (firstPackage).getActors().get(0).getAlias());

        assertEquals("Renamed Use Case", (firstPackage).getUseCases().get(0).getName());
        assertEquals("Use", (firstPackage).getUseCases().get(0).getAlias());

        UseCasePackageElement secondPackage = (UseCasePackageElement) useCaseDiagramVisitor.getElements().get(1);
        assertEquals("B", secondPackage.getName());
        assertNull(secondPackage.getAlias());
        assertEquals(1, secondPackage.getActors().size());
        assertEquals(0, secondPackage.getUseCases().size());
        assertEquals("C", (secondPackage).getActors().get(0).getName());
        assertEquals("User", (secondPackage).getActors().get(0).getAlias());
        assertEquals(1, (secondPackage).getActors().get(0).getStereotypes().size());
        assertEquals("aaa", (secondPackage).getActors().get(0).getStereotypes().get(0));
        assertEquals("#aaaaaa", (secondPackage).getActors().get(0).getBackgroundColor());

        UseCasePackageElement thirdPackage = (UseCasePackageElement) useCaseDiagramVisitor.getElements().get(2);
        assertEquals(0, thirdPackage.getActors().size());
        assertEquals(0, thirdPackage.getUseCases().size());
    }
}
