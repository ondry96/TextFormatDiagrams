package imports.usecasediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLUseCaseDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.usecasediagram.ActorElement;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlantUMLActorTest {

    PlantUMLUseCaseDiagramVisitor useCaseDiagramVisitor = new PlantUMLUseCaseDiagramVisitor();

    @Test
    public void testCreateActors() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/usecasediagram/actor/all_actor_declarations.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        useCaseDiagramVisitor.visit(parseTree);


        assertEquals(0, useCaseDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(11, useCaseDiagramVisitor.getElements().size());


        ActorElement actor1 = (ActorElement) useCaseDiagramVisitor.getElements().get(0);
        ActorElement actor2 = (ActorElement) useCaseDiagramVisitor.getElements().get(1);
        ActorElement actor3 = (ActorElement) useCaseDiagramVisitor.getElements().get(2);
        ActorElement actor4 = (ActorElement) useCaseDiagramVisitor.getElements().get(3);
        ActorElement actor5 = (ActorElement) useCaseDiagramVisitor.getElements().get(4);
        ActorElement actor6 = (ActorElement) useCaseDiagramVisitor.getElements().get(5);
        ActorElement actor7 = (ActorElement) useCaseDiagramVisitor.getElements().get(6);
        ActorElement actor8 = (ActorElement) useCaseDiagramVisitor.getElements().get(7);
        ActorElement actor9 = (ActorElement) useCaseDiagramVisitor.getElements().get(8);
        ActorElement actor10 = (ActorElement) useCaseDiagramVisitor.getElements().get(9);
        ActorElement actor11 = (ActorElement) useCaseDiagramVisitor.getElements().get(10);


        assertEquals("name1", actor1.getName());
        assertEquals("alias1", actor1.getAlias());

        assertEquals("name2", actor2.getName());
        assertNull(actor2.getAlias());

        assertEquals("name3", actor3.getName());
        assertEquals("alias3", actor3.getAlias());

        assertEquals("name4", actor4.getName());
        assertNull(actor4.getAlias());

        assertEquals("name with space5", actor5.getName());
        assertEquals("alias5", actor5.getAlias());

        assertEquals("name6", actor6.getName());
        assertEquals("alias6", actor6.getAlias());

        assertEquals("name7", actor7.getName());
        assertEquals("alias7", actor7.getAlias());

        assertEquals("name8", actor8.getName());
        assertEquals("alias8", actor8.getAlias());

        assertEquals("name9", actor9.getName());
        assertNull(actor9.getAlias());

        assertEquals("name10", actor10.getName());
        assertEquals("alias10", actor10.getAlias());

        assertEquals("renamed11", actor11.getName());
        assertEquals("alias11", actor11.getAlias());
    }

    @Test
    public void testActorHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/usecasediagram/actor/actor_header.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        useCaseDiagramVisitor.visit(parseTree);


        assertEquals(0, useCaseDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(4, useCaseDiagramVisitor.getElements().size());

        ActorElement actor1 = (ActorElement) useCaseDiagramVisitor.getElements().get(0);
        ActorElement actor2 = (ActorElement) useCaseDiagramVisitor.getElements().get(1);
        ActorElement actor3 = (ActorElement) useCaseDiagramVisitor.getElements().get(2);
        ActorElement actor4 = (ActorElement) useCaseDiagramVisitor.getElements().get(3);

        assertEquals("name1", actor1.getName());
        assertEquals(2, actor1.getStereotypes().size());
        assertEquals("aa", actor1.getStereotypes().get(0));
        assertEquals("bb", actor1.getStereotypes().get(1));
        assertNull(actor1.getBackgroundColor());
        assertNull(actor1.getAlias());

        assertEquals("name2", actor2.getName());
        assertEquals(0, actor2.getStereotypes().size());
        assertEquals("#aaaaaa", actor2.getBackgroundColor());
        assertNull(actor2.getAlias());

        assertEquals("name3", actor3.getName());
        assertEquals(0, actor3.getStereotypes().size());
        assertEquals("#aaaaaa", actor3.getBackgroundColor());
        assertEquals("alias3", actor3.getAlias());

        assertEquals("name4", actor4.getName());
        assertEquals(2, actor4.getStereotypes().size());
        assertEquals("aa", actor4.getStereotypes().get(0));
        assertEquals("bb", actor4.getStereotypes().get(1));
        assertEquals("#aaaaaa", actor4.getBackgroundColor());
        assertEquals("alias4", actor4.getAlias());

    }

}
