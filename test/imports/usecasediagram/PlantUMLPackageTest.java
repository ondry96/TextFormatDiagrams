package imports.usecasediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLUseCaseDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.usecasediagram.ActorElement;
import pluginmain.elements.usecasediagram.UseCaseElement;
import pluginmain.elements.usecasediagram.UseCasePackageElement;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlantUMLPackageTest {

    PlantUMLUseCaseDiagramVisitor useCaseDiagramVisitor = new PlantUMLUseCaseDiagramVisitor();


    @Test
    public void testPackageHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/usecasediagram/packages/package_with_elements.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        useCaseDiagramVisitor.visit(parseTree);

        assertEquals(0, useCaseDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(1, useCaseDiagramVisitor.getElements().size());

        UseCasePackageElement firstPackage = (UseCasePackageElement) useCaseDiagramVisitor.getElements().get(0);
        assertEquals("A", firstPackage.getName());
        assertEquals("Alias", firstPackage.getAlias());
        assertEquals(2, firstPackage.getStereotypes().size());
        assertEquals("aaa", firstPackage.getStereotypes().get(0));
        assertEquals("bbb", firstPackage.getStereotypes().get(1));
        assertNull(firstPackage.getBackgroundColor());

        ActorElement actorElement = firstPackage.getActors().get(0);
        assertEquals("Main Admin", actorElement.getName());
        assertEquals("Admin", actorElement.getAlias());

        UseCaseElement useCaseElement = firstPackage.getUseCases().get(0);
        assertEquals("Use the application", useCaseElement.getName());
        assertEquals("Use", useCaseElement.getAlias());
    }

}
