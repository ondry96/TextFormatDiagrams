package imports.usecasediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLUseCaseDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.usecasediagram.UseCaseElement;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlantUMLUseCaseTest {

    PlantUMLUseCaseDiagramVisitor useCaseDiagramVisitor = new PlantUMLUseCaseDiagramVisitor();

    @Test
    public void testCreateBasicUseCases() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/usecasediagram/usecase/all_usecase_declarations.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        useCaseDiagramVisitor.visit(parseTree);


        assertEquals(0, useCaseDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(11, useCaseDiagramVisitor.getElements().size());


        UseCaseElement useCase1 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(0);
        UseCaseElement useCase2 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(1);
        UseCaseElement useCase3 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(2);
        UseCaseElement useCase4 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(3);
        UseCaseElement useCase5 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(4);
        UseCaseElement useCase6 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(5);
        UseCaseElement useCase7 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(6);
        UseCaseElement useCase8 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(7);
        UseCaseElement useCase9 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(8);
        UseCaseElement useCase10 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(9);
        UseCaseElement useCase11 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(10);


        assertEquals("name1", useCase1.getName());
        assertNull(useCase1.getAlias());

        assertEquals("name2", useCase2.getName());
        assertEquals("alias2", useCase2.getAlias());

        assertEquals("name3", useCase3.getName());
        assertEquals("alias3", useCase3.getAlias());

        assertEquals("name4", useCase4.getName());
        assertEquals("alias4", useCase4.getAlias());

        assertEquals("name5", useCase5.getName());
        assertEquals("alias5", useCase5.getAlias());

        assertEquals(System.lineSeparator() + "name6" + System.lineSeparator() + "name6" + System.lineSeparator(), useCase6.getName());
        assertEquals("alias6", useCase6.getAlias());

        assertEquals("name7", useCase7.getName());
        assertNull(useCase7.getAlias());

        assertEquals("name8", useCase8.getName());
        assertEquals("alias8", useCase8.getAlias());

        assertEquals("name9", useCase9.getName());
        assertEquals("alias9", useCase9.getAlias());

        assertEquals("name10", useCase10.getName());
        assertEquals("alias10", useCase10.getAlias());

        assertEquals("name11\" as \"namesecondpart", useCase11.getName());
        assertNull(useCase11.getAlias());
    }

    @Test
    public void testUseCaseHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/usecasediagram/usecase/usecase_header.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        useCaseDiagramVisitor.visit(parseTree);


        assertEquals(0, useCaseDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(4, useCaseDiagramVisitor.getElements().size());

        UseCaseElement useCase1 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(0);
        UseCaseElement useCase2 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(1);
        UseCaseElement useCase3 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(2);
        UseCaseElement useCase4 = (UseCaseElement) useCaseDiagramVisitor.getElements().get(3);

        assertEquals("name1", useCase1.getName());
        assertEquals(2, useCase1.getStereotypes().size());
        assertEquals("aa", useCase1.getStereotypes().get(0));
        assertEquals("bb", useCase1.getStereotypes().get(1));
        assertNull(useCase1.getBackgroundColor());
        assertNull(useCase1.getAlias());

        assertEquals("name2", useCase2.getName());
        assertEquals(0, useCase2.getStereotypes().size());
        assertEquals("#aaaaaa", useCase2.getBackgroundColor());
        assertNull(useCase2.getAlias());

        assertEquals("name3", useCase3.getName());
        assertEquals(0, useCase3.getStereotypes().size());
        assertEquals("#aaaaaa", useCase3.getBackgroundColor());
        assertEquals("alias3", useCase3.getAlias());

        assertEquals("name4", useCase4.getName());
        assertEquals(2, useCase4.getStereotypes().size());
        assertEquals("aa", useCase4.getStereotypes().get(0));
        assertEquals("bb", useCase4.getStereotypes().get(1));
        assertEquals("#aaaaaa", useCase4.getBackgroundColor());
        assertEquals("alias4", useCase4.getAlias());

    }

}
