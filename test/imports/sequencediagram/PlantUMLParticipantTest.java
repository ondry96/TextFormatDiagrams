package imports.sequencediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLSequenceDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.sequencediagram.ParticipantElement;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlantUMLParticipantTest {

    PlantUMLSequenceDiagramVisitor sequenceDiagramVisitor = new PlantUMLSequenceDiagramVisitor();

    @Test
    public void testCreateParticipantsAndOrder() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/participant/participants.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(13, sequenceDiagramVisitor.getElements().size());


        ParticipantElement participant12 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(0);
        ParticipantElement participant1 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(1);
        ParticipantElement participant2 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(2);
        ParticipantElement participant6 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(3);
        ParticipantElement participant7 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(4);
        ParticipantElement participant8 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(5);
        ParticipantElement participant9 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(6);
        ParticipantElement participant10 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(7);
        ParticipantElement participant11 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(8);
        ParticipantElement participant3 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(9);
        ParticipantElement participant4 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(10);
        ParticipantElement participant5 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(11);


        assertEquals("name1", participant1.getName());
        assertEquals("alias1", participant1.getAlias());

        assertEquals("name2", participant2.getName());
        assertEquals("alias2", participant2.getAlias());

        assertEquals("name3", participant3.getName());
        assertEquals("alias3", participant3.getAlias());

        assertEquals("name4", participant4.getName());
        assertNull(participant4.getAlias());

        assertEquals("name5", participant5.getName());
        assertNull(participant5.getAlias());

        assertEquals("name6", participant6.getName());
        assertEquals("alias6", participant6.getAlias());

        assertEquals("name7", participant7.getName());
        assertEquals("alias7", participant7.getAlias());

        assertEquals("name8 with space", participant8.getName());
        assertEquals("alias8", participant8.getAlias());

        assertEquals("name9", participant9.getName());
        assertEquals("alias9", participant9.getAlias());

        assertEquals("name10", participant10.getName());
        assertEquals("alias10", participant10.getAlias());

        assertEquals("name11", participant11.getName());
        assertEquals("alias11", participant11.getAlias());

        assertEquals("name12", participant12.getName());
        assertEquals("alias12", participant12.getAlias());
    }

    @Test
    public void testParticipantsHeader() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/participant/participants.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(13, sequenceDiagramVisitor.getElements().size());


        ParticipantElement participant12 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(0);
        ParticipantElement participant1 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(1);
        ParticipantElement participant2 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(2);
        ParticipantElement participant6 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(3);
        ParticipantElement participant7 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(4);
        ParticipantElement participant8 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(5);
        ParticipantElement participant9 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(6);
        ParticipantElement participant10 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(7);
        ParticipantElement participant11 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(8);
        ParticipantElement participant3 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(9);
        ParticipantElement participant4 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(10);
        ParticipantElement participant5 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(11);

        assertEquals(2, participant1.getStereotypes().size());
        assertEquals("aaa", participant1.getStereotypes().get(0));
        assertEquals("bbb", participant1.getStereotypes().get(1));
        assertEquals(0, participant1.getOrder());
        assertNull(participant1.getBackgroundColor());

        assertEquals(2, participant2.getStereotypes().size());
        assertEquals("aaa", participant2.getStereotypes().get(0));
        assertEquals("bbb", participant2.getStereotypes().get(1));
        assertEquals(0, participant2.getOrder());
        assertNull(participant2.getBackgroundColor());

        assertEquals(0, participant3.getStereotypes().size());
        assertEquals(100, participant3.getOrder());
        assertNull(participant3.getBackgroundColor());

        assertEquals(0, participant4.getStereotypes().size());
        assertEquals(100, participant4.getOrder());
        assertEquals("#aaaaaa", participant4.getBackgroundColor());

        assertEquals(0, participant5.getStereotypes().size());
        assertEquals(120, participant5.getOrder());
        assertEquals("#aaaaaa", participant5.getBackgroundColor());

        assertEquals(0, participant6.getStereotypes().size());
        assertEquals(0, participant6.getOrder());
        assertEquals("#aaaaaa", participant6.getBackgroundColor());

        assertEquals(1, participant7.getStereotypes().size());
        assertEquals("actor", participant7.getStereotypes().get(0));
        assertEquals(0, participant7.getOrder());
        assertNull(participant7.getBackgroundColor());

        assertEquals(1, participant8.getStereotypes().size());
        assertEquals("boundary", participant8.getStereotypes().get(0));
        assertEquals(0, participant8.getOrder());
        assertNull(participant8.getBackgroundColor());

        assertEquals(1, participant9.getStereotypes().size());
        assertEquals("control", participant9.getStereotypes().get(0));
        assertEquals(0, participant9.getOrder());
        assertNull(participant9.getBackgroundColor());

        assertEquals(1, participant10.getStereotypes().size());
        assertEquals("database", participant10.getStereotypes().get(0));
        assertEquals(0, participant10.getOrder());
        assertNull(participant10.getBackgroundColor());

        assertEquals(1, participant11.getStereotypes().size());
        assertEquals("collections", participant11.getStereotypes().get(0));
        assertEquals(0, participant11.getOrder());
        assertNull(participant11.getBackgroundColor());

        assertEquals(1, participant12.getStereotypes().size());
        assertEquals("queue", participant12.getStereotypes().get(0));
        assertEquals(-100, participant12.getOrder());
        assertNull(participant12.getBackgroundColor());

    }

}
