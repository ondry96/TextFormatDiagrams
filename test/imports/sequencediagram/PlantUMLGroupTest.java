package imports.sequencediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLSequenceDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.sequencediagram.GroupCaseElement;
import pluginmain.elements.sequencediagram.GroupElement;
import pluginmain.elements.sequencediagram.MessageElement;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class PlantUMLGroupTest {

    PlantUMLSequenceDiagramVisitor sequenceDiagramVisitor = new PlantUMLSequenceDiagramVisitor();

    @Test
    public void testCreateBasicGroups() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/group/groups.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(10, sequenceDiagramVisitor.getElements().size());

        GroupElement groupGroup1 = (GroupElement) sequenceDiagramVisitor.getElements().get(0);
        assertEquals(1, groupGroup1.getGroupCases().size());
        GroupCaseElement case0 = groupGroup1.getGroupCases().get(0);
        assertEquals("case0", case0.getName());

        GroupElement groupAlt = (GroupElement) sequenceDiagramVisitor.getElements().get(2);
        assertEquals(4, groupAlt.getGroupCases().size());
        GroupCaseElement case1 = groupAlt.getGroupCases().get(0);
        assertEquals("case1", case1.getName());
        GroupCaseElement case2 = groupAlt.getGroupCases().get(1);
        assertEquals("case2", case2.getName());
        GroupCaseElement case3 = groupAlt.getGroupCases().get(2);
        assertEquals("case3", case3.getName());
        GroupCaseElement case4 = groupAlt.getGroupCases().get(3);
        assertEquals("case4", case4.getName());

        GroupElement groupGroup2 = (GroupElement) sequenceDiagramVisitor.getElements().get(7);
        assertEquals(1, groupGroup2.getGroupCases().size());
        GroupCaseElement case5 = groupGroup2.getGroupCases().get(0);
        assertEquals("case5", case5.getName());
    }

    @Test
    public void testCreateSubGroups() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/group/group_with_subgroup_and_participants.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(12, sequenceDiagramVisitor.getElements().size()); //participants are not in groups, but in base list called elements

        GroupElement groupOpt = (GroupElement) sequenceDiagramVisitor.getElements().get(0);
        assertEquals(2, groupOpt.getGroupCases().size());
        GroupCaseElement case1 = groupOpt.getGroupCases().get(0);
        GroupCaseElement case2 = groupOpt.getGroupCases().get(1);
        assertEquals("case1 group name", case1.getName());
        assertEquals("case2", case2.getName());


        GroupElement groupAlt = case2.getGroupElements().get(0);
        assertEquals(1, groupAlt.getGroupCases().size());
        GroupCaseElement case3 = groupAlt.getGroupCases().get(0);
        assertEquals("case3", case3.getName());


        GroupElement groupBreak = case2.getGroupElements().get(1);
        assertEquals(3, groupBreak.getGroupCases().size());
        GroupCaseElement case4 = groupBreak.getGroupCases().get(0);
        GroupCaseElement case5 = groupBreak.getGroupCases().get(1);
        GroupCaseElement case6 = groupBreak.getGroupCases().get(2);
        assertEquals("case4", case4.getName());
        assertEquals("case5", case5.getName());
        assertEquals("case6", case6.getName());


        GroupElement groupGroup = (GroupElement) sequenceDiagramVisitor.getElements().get(10);
        assertEquals(1, groupGroup.getGroupCases().size());
        GroupCaseElement unnamedCaseOfGroup = groupGroup.getGroupCases().get(0);
        assertNull(unnamedCaseOfGroup.getName());
    }


    @Test
    public void testSubGroupsWithMessages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/group/groups_with_messages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, sequenceDiagramVisitor.getElements().size());

        GroupElement groupOpt = (GroupElement) sequenceDiagramVisitor.getElements().get(2);
        assertEquals(2, groupOpt.getGroupCases().size());
        GroupCaseElement case1 = groupOpt.getGroupCases().get(0);
        GroupCaseElement case2 = groupOpt.getGroupCases().get(1);

        assertEquals(1, case1.getMessageElements().size());
        MessageElement message1 = case1.getMessageElements().get(0);
        assertEquals(2, case2.getMessageElements().size());
        MessageElement message2 = case2.getMessageElements().get(0);
        MessageElement message3 = case2.getMessageElements().get(1);


        GroupElement groupAlt = case2.getGroupElements().get(0);
        assertEquals(1, groupAlt.getGroupCases().size());
        GroupCaseElement case3 = groupAlt.getGroupCases().get(0);
        assertEquals(1, case3.getMessageElements().size());
        MessageElement message4 = case3.getMessageElements().get(0);


        GroupElement groupBreak = case2.getGroupElements().get(1);
        assertEquals(3, groupBreak.getGroupCases().size());
        GroupCaseElement case4 = groupBreak.getGroupCases().get(0);
        GroupCaseElement case5 = groupBreak.getGroupCases().get(1);
        GroupCaseElement case6 = groupBreak.getGroupCases().get(2);

        assertEquals(1, case4.getMessageElements().size());
        MessageElement message5 = case4.getMessageElements().get(0);
        assertEquals(1, case5.getMessageElements().size());
        MessageElement message6 = case5.getMessageElements().get(0);
        assertEquals(1, case6.getMessageElements().size());
        MessageElement message7 = case6.getMessageElements().get(0);

        MessageElement message8 = (MessageElement) sequenceDiagramVisitor.getElements().get(3);


        GroupElement groupGroup = (GroupElement) sequenceDiagramVisitor.getElements().get(4);
        assertEquals(1, groupGroup.getGroupCases().size());
        GroupCaseElement unnamedCaseOfGroup = groupGroup.getGroupCases().get(0);

        assertEquals(1, unnamedCaseOfGroup.getMessageElements().size());
        MessageElement message9 = unnamedCaseOfGroup.getMessageElements().get(0);

        assertEquals("name1", message1.getName());
        assertEquals("name2", message2.getName());
        assertEquals("name7", message3.getName());
        assertEquals("name3", message4.getName());
        assertEquals("name4", message5.getName());
        assertEquals("name5", message6.getName());
        assertEquals("name6", message7.getName());
        assertEquals("name8", message8.getName());
        assertEquals("name9", message9.getName());

    }

}
