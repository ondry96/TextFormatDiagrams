package imports.sequencediagram;

import antlr.plantumlgrammar.PlantUMLLexer;
import antlr.plantumlgrammar.PlantUMLParser;
import antlr.visitors.PlantUMLSequenceDiagramVisitor;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.junit.Test;
import pluginmain.elements.sequencediagram.MessageElement;
import pluginmain.elements.sequencediagram.ParticipantElement;

import java.io.IOException;

import static org.junit.Assert.*;

public class PlantUMLMessageTest {


    PlantUMLSequenceDiagramVisitor sequenceDiagramVisitor = new PlantUMLSequenceDiagramVisitor();

    @Test
    public void testCreateDifferentTypeOfMessages() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/messages/different_messages.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(17, sequenceDiagramVisitor.getElements().size());

        ParticipantElement participant1 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(0);
        ParticipantElement participant2 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(1);
        MessageElement message1 = (MessageElement) sequenceDiagramVisitor.getElements().get(2);
        ParticipantElement participant3 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(3);
        ParticipantElement participant4 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(4);
        MessageElement message2 = (MessageElement) sequenceDiagramVisitor.getElements().get(5);
        MessageElement message3 = (MessageElement) sequenceDiagramVisitor.getElements().get(6);
        MessageElement message4 = (MessageElement) sequenceDiagramVisitor.getElements().get(7);
        MessageElement message5 = (MessageElement) sequenceDiagramVisitor.getElements().get(8);
        ParticipantElement participant5 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(9);
        MessageElement message6 = (MessageElement) sequenceDiagramVisitor.getElements().get(10);
        ParticipantElement participant6 = (ParticipantElement) sequenceDiagramVisitor.getElements().get(11);
        MessageElement message7 = (MessageElement) sequenceDiagramVisitor.getElements().get(12);
        MessageElement message8 = (MessageElement) sequenceDiagramVisitor.getElements().get(13);
        MessageElement message9 = (MessageElement) sequenceDiagramVisitor.getElements().get(14);
        MessageElement message10 = (MessageElement) sequenceDiagramVisitor.getElements().get(15);
        MessageElement message11 = (MessageElement) sequenceDiagramVisitor.getElements().get(16);

        //messages
        assertEquals("name name1", message1.getName());
        assertEquals("Lola", message1.getFromName());
        assertEquals("Bob", message1.getToName());
        assertEquals("x<", message1.getLeftArrowPart());
        assertEquals("", message1.getRightArrowPart());
        assertFalse(message1.isDotted());

        assertEquals("name name2", message2.getName());
        assertEquals("John", message2.getFromName());
        assertEquals("Alice", message2.getToName());
        assertEquals("", message2.getLeftArrowPart());
        assertEquals(">x", message2.getRightArrowPart());
        assertTrue(message2.isDotted());

        assertNull(message3.getName());
        assertEquals("Bob", message3.getFromName());
        assertEquals("Alice", message3.getToName());
        assertEquals("", message3.getLeftArrowPart());
        assertEquals(">", message3.getRightArrowPart());
        assertFalse(message3.isDotted());

        assertNull(message4.getName());
        assertEquals("Bob", message4.getFromName());
        assertEquals("Alice", message4.getToName());
        assertEquals("", message4.getLeftArrowPart());
        assertEquals(">", message4.getRightArrowPart());
        assertFalse(message4.isDotted());

        assertNull(message5.getName());
        assertEquals("Alice", message5.getFromName());
        assertEquals("Bob", message5.getToName());
        assertEquals("<", message5.getLeftArrowPart());
        assertEquals("", message5.getRightArrowPart());
        assertFalse(message5.isDotted());

        assertEquals("name name3", message6.getName());
        assertEquals("Adolf", message6.getFromName());
        assertEquals("Alice", message6.getToName());
        assertEquals("", message6.getLeftArrowPart());
        assertEquals(">", message6.getRightArrowPart());
        assertFalse(message6.isDotted());

        assertEquals("name name4", message7.getName());
        assertEquals("Damian", message7.getFromName());
        assertEquals("Bob", message7.getToName());
        assertEquals("<", message7.getLeftArrowPart());
        assertEquals("", message7.getRightArrowPart());
        assertFalse(message7.isDotted());

        assertNull(message8.getName());
        assertNull(message8.getFromName());
        assertEquals("Damian", message8.getToName());
        assertEquals("[", message8.getLeftArrowPart());
        assertEquals(">", message8.getRightArrowPart());
        assertTrue(message8.isDotted());

        assertEquals("name name5", message9.getName());
        assertNull(message9.getFromName());
        assertEquals("Damian", message9.getToName());
        assertEquals("?", message9.getLeftArrowPart());
        assertEquals(">>", message9.getRightArrowPart());
        assertFalse(message9.isDotted());

        assertNull(message10.getName());
        assertEquals("Damian", message10.getFromName());
        assertNull(message10.getToName());
        assertEquals("", message10.getLeftArrowPart());
        assertEquals(">]", message10.getRightArrowPart());
        assertTrue(message10.isDotted());

        assertEquals("name name7", message11.getName());
        assertEquals("Damian", message11.getFromName());
        assertNull(message11.getToName());
        assertEquals("", message11.getLeftArrowPart());
        assertEquals(">>?", message11.getRightArrowPart());
        assertFalse(message11.isDotted());

        //created participants
        assertEquals("Bob", participant1.getName());
        assertNull(participant1.getAlias());

        assertEquals("Lola", participant2.getName());
        assertNull(participant2.getAlias());

        assertEquals("John", participant3.getName());
        assertNull(participant3.getAlias());

        assertEquals("Alice", participant4.getName());
        assertNull(participant4.getAlias());

        assertEquals("AdolfName", participant5.getName());
        assertEquals("Adolf", participant5.getAlias());

        assertEquals("DamianName", participant6.getName());
        assertEquals("Damian", participant6.getAlias());

    }

    @Test
    public void testMessageWithReturnType() throws IOException {
        CharStream charStream = CharStreams.fromFileName("test/imports/sequencediagram/messages/message_with_return_type.puml");
        PlantUMLLexer lexer = new PlantUMLLexer(charStream);
        CommonTokenStream cts = new CommonTokenStream(lexer);
        PlantUMLParser parser = new PlantUMLParser(cts);
        ParseTree parseTree = parser.uml();


        sequenceDiagramVisitor.visit(parseTree);


        assertEquals(0, sequenceDiagramVisitor.getErrorsInGrammar().size());
        assertEquals(5, sequenceDiagramVisitor.getElements().size());

        MessageElement message1 = (MessageElement) sequenceDiagramVisitor.getElements().get(2);
        MessageElement message2 = (MessageElement) sequenceDiagramVisitor.getElements().get(3);
        MessageElement message3 = (MessageElement) sequenceDiagramVisitor.getElements().get(4);

        assertEquals("hello(String a): void", message1.getName());
        assertEquals("Lola", message1.getFromName());
        assertEquals("Bob", message1.getToName());
        assertEquals("<", message1.getLeftArrowPart());
        assertEquals("", message1.getRightArrowPart());
        assertFalse(message1.isDotted());

        assertEquals("hello(): String", message2.getName());
        assertEquals("Bob", message2.getFromName());
        assertEquals("Lola", message2.getToName());
        assertEquals("", message2.getLeftArrowPart());
        assertEquals(">>", message2.getRightArrowPart());
        assertTrue(message2.isDotted());

        assertEquals("hello(): String", message3.getName());
        assertEquals("Bob", message3.getFromName());
        assertEquals("Lola", message3.getToName());
        assertEquals("", message3.getLeftArrowPart());
        assertEquals(">>", message3.getRightArrowPart());
        assertTrue(message3.isDotted());
    }
}
